package day_12_17;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @author soberw
 * @Classname Cracle
 * @Description 随机圆形，随机颜色，随机透明度
 * @Date 2021-12-17 10:54
 */
public class Circle {
    public static void main(String[] args) {
        //生成一个图片缓冲流，并设置图片格式
        BufferedImage img = new BufferedImage(1000, 1000, BufferedImage.TYPE_INT_ARGB);

        //设置画笔，为平面，不推荐
        ///Graphics g1 = img.getGraphics();

        //2D形式画笔
        Graphics2D g2 = img.createGraphics();

        //这三行是设置抗锯齿
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

        //设置画笔粗细
        //g2.setStroke(new BasicStroke(5));

        //末两位为设置宽高，相等则为正圆，否则为椭圆
        //空心圆
        //g2.drawOval(100,100,100,100);
        //实心圆
        //g2.fillOval(100, 100, 100, 100);

        //画的落点以及到达点
        //g2.drawLine(0,0,100,100);

        Random random = new Random();
        //随机200次画圆，在图片随机位置，随机透明度，随机大小,随机颜色
        for (int i = 0; i < 100; i++) {
            int r = random.nextInt(256);
            int g = random.nextInt(256);
            int b = random.nextInt(256);
            int a = random.nextInt(150) + 100;
            //设置画笔颜色，以及透明度（不写默认为全色）
            g2.setColor(new Color(r, g, b, a));
            //x轴坐标
            int x = random.nextInt(800);
            //y轴坐标
            int y = random.nextInt(800);
            //宽
            int w = random.nextInt(100) + 50;
            //高（宽高相等为正圆，否则为椭圆）
//            int h = random.nextInt();

            g2.fillOval(x, y, w, w);
        }
        //最后一步，切记一定要关闭画笔
        g2.dispose();


        try {
            //写入文件
            ImageIO.write(img, "png", new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\main\\java\\circle.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //打开控制台cmd窗口
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("cmd /k start E:\\HelloJava\\Java_Study_Beiyou\\src\\main\\java\\circle.png");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
