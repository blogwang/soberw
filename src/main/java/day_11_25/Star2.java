package day_11_25;

/**
 * 打印菱形星号
 *
 * @author soberw
 */
public class Star2 {
    public static void main(String[] args) {
        int max = 7;
        int h = max / 2 + 1;
        for (int i = 1; i <= h; i++) {
            for (int j = 1; j <= (h - i); j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= (2 * i - 1); k++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (int i = (h - 1); i >= 1; i--) {
            for (int j = 1; j <= (h - i); j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= (2 * i - 1); k++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
