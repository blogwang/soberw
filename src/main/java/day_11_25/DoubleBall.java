package day_11_25;

import java.util.Random;

/**
 * 双色球
 * 红球1~33  篮球1~16
 *
 * @author soberw
 */
public class DoubleBall {
    public static void main(String[] args) {
        Random ran = new Random();
        //基数组，从这里抽数放入red
        int[] base = new int[33];
        //存放红球
        int[] red = new int[6];
        int ranIndex;
        //给base元素依次赋值1~33
        for (int i = 1; i <= base.length; i++) {
            base[i - 1] = i;
        }

        for (int i = 0; i < red.length; i++) {
            while (true) {
                //随机base的下标
                ranIndex = ran.nextInt(33);
                //判断是不是被抽走了,被抽走了就继续随机，没有的话就放入red，并将base中对应的赋值为0
                if (0 != base[ranIndex]) {
                    red[i] = base[ranIndex];
                    base[ranIndex] = 0;
                    break;
                }
            }
        }
        System.out.print("{ ");
        for (int i : red) {
            System.out.printf("[%02d] ", i);
        }
        System.out.print("}");
        System.out.printf("{ [%02d] }", ran.nextInt(16) + 1);

    }
}
