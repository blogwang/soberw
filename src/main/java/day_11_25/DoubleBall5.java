package day_11_25;

import java.util.Random;
import java.util.Vector;

/**
 * 双色球
 * 红球1~33  篮球1~16
 *
 * @author soberw
 */
public class DoubleBall5 {
    public static void main(String[] args) {
        Random random = new Random();
        //声明一个空数组用于存放红球
        Vector<Integer> red = new Vector<Integer>();
        //超出六个退出
        while (red.size() < 6) {
            int ranRed = random.nextInt(33) + 1;
            //如果不存在则添加
            if (!red.contains(ranRed)) {
                red.add(ranRed);
            }
        }
        System.out.print("{ ");
        for (int i : red) {
            System.out.printf("[%02d] ", i);
        }
        System.out.print("}");
        System.out.printf("{ [%02d] }", random.nextInt(16) + 1);
    }
}
