package day_11_25;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * @author soberw
 */
public class SwitchExpression2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入年份：");
        int year = sc.nextInt();
        System.out.print("请输入月份：");
        int month = sc.nextInt();
        int day = switch (month) {
            case 4, 6, 9, 11 -> 30;
            case 2 -> {
                //这里是代码块
                System.out.println("现在是二月！");
                yield (LocalDate.of(year, month, 1).isLeapYear()) ? 29 : 28;
            }
            default -> 31;
        };
        System.out.printf("%d年%d月有%d天%n", year, month, day);
    }
}
