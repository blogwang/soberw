package day_11_25;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * 双色球
 * 红球1~33  篮球1~16
 *
 * @author soberw
 */
public class DoubleBall2 {
    public static void main(String[] args) {
        Random random = new Random();
        Set<Integer> red = new HashSet<Integer>();
        do {
            red.add(random.nextInt(33) + 1);
        } while (red.size() != 6);
        System.out.print("{ ");
        red.forEach((value) -> {
            System.out.printf("[%02d] ", value);
        });
        System.out.print("}");
        System.out.printf("{ [%02d] }", random.nextInt(16) + 1);
    }
}
