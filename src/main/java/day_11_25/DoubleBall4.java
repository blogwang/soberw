package day_11_25;

import java.util.Arrays;
import java.util.Random;

/**
 * 双色球
 * 红球1~33  篮球1~16
 *
 * @author soberw
 */
public class DoubleBall4 {
    public static void main(String[] args) {
        Random random = new Random();
        //放红球
        int[] red = new int[6];
        Arrays.fill(red,33);
        //存放号码球
        int ranRed;
        for (int i = 0; i < red.length; i++) {
            boolean flag = true;
            //确保数组有序
            Arrays.sort(red);
            while (flag) {
                ranRed = random.nextInt(33) + 1;
                //判断数组中是否存在此元素
                if (Arrays.binarySearch(red, ranRed) < 0) {
                    flag = false;
                    red[i] = ranRed;
                }
            }
        }
        System.out.print("{ ");
        for (int i : red) {
            System.out.printf("[%02d] ", i);
        }
        System.out.print("}");
        System.out.printf("{ [%02d] }", random.nextInt(16) + 1);
    }
}
