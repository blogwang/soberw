package cn.soberw.banksystem.view;

import cn.soberw.banksystem.service.ToDoAdmin;
import cn.soberw.banksystem.utils.MyUtils;

/**
 * @author soberw
 * @Classname AdminMenu
 * @Description
 * @Date 2021-12-20 16:09
 */
public class AdminLogin {

    private static ToDoAdmin tda = new ToDoAdmin();

    private static AdminMenu am = new AdminMenu();

    /**
     * @description: 管理员登录
     * @author: soberw
     * @time: 2021/12/21 14:12
     */
    public static void loginA() {
        for (int i = 3; i >= 1; i--) {
            System.out.print("请输入你的账号：");
            String id = MyUtils.readId();
            System.out.print("请输入你的密码：");
            String password = MyUtils.readPwd();
            String name = tda.loginAdmin(id, password);
            if (name != null) {
                System.out.println(name + "，欢迎你登录！");
                //登陆成功后跳入功能页面
                adminMenu(id);
                break;
            }
            System.out.println("账号或者密码错误！请重新输入！！！");
            System.out.println("你当前剩余" + (i - 1) + "次机会！");
        }
    }

    /**
     * @param id   账号
     * @description: 管理员登录后跳转到本页面，并传入参数id方便操作
     * @author: soberw
     * @time: 2021/12/21 14:12
     */
    public static void adminMenu(String id) {
        soberw:
        for (; ; ) {
            System.out.println("-------------------------------------------------------------------");
            System.out.println("功能选择：1、查看全部用户信息");
            System.out.println("        2、开户");
            System.out.println("        3、帮指定用户存钱");
            System.out.println("        4、帮指定用户取钱");
            System.out.println("        5、修改用户资料");
            System.out.println("        6、修改自己资料");
            System.out.println("        7、删除用户");
            System.out.println("        8、退出登录");
            System.out.println();
            System.out.print("请选择：_");
            char c = MyUtils.menuSelection(8);
            switch (c) {
                case '1':
                    am.showAllUsers();
                    break;
                case '2':
                    am.addUser();
                    break;
                case '3':
                    am.deposit();
                    break;
                case '4':
                    am.withdraw();
                    break;
                case '5':
                    am.updateUser();
                    break;
                case '6':
                    am.updateSelf(id);
                    break soberw;
                case '7':
                    am.deleteUser();
                    break;
                default:
                    System.out.print("你确定退出吗？(Y/N)：");
                    char confirm = MyUtils.confirmKey();
                    if (confirm == 'Y') {
                        System.out.println();
                        System.out.println("谢谢你使用本系统！欢迎你再次使用！");
                        break soberw;
                    }
                    break;
            }
        }
    }
}
