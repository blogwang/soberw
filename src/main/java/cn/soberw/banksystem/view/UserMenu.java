package cn.soberw.banksystem.view;

import cn.soberw.banksystem.service.ToDoUser;
import cn.soberw.banksystem.service.UserInfo;
import cn.soberw.banksystem.utils.InfoException;
import cn.soberw.banksystem.utils.MyUtils;

import java.util.List;

/**
 * @author soberw
 * @Classname UserMenu
 * @Description 用户功能的最终实现，加入了与用户交互的信息
 * @Date 2021-12-20 21:10
 */
public class UserMenu {
    private static ToDoUser tdu = new ToDoUser();

    /**
     * @param id 账号
     * @description: 显示对应账号用户的余额
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 21:20
     */
    public void showBalance(String id) {
        double v = tdu.showBalance(id);
        System.out.printf("你的余额为：%.2f\n", v);
    }

    /**
     * @param id 对应账号
     * @description: 对应账号取款，若余额不足则报错，取款成功返回当前余额
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 8:32
     */
    public void withdraw(String id) {
        System.out.print("请输入取款金额：");
        double balance = MyUtils.readBalance();
        double v = tdu.withDraw(id, balance);
        if (v == -1) {
            try {
                throw new InfoException("余额不足！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.print("取款成功！");
            MyUtils.readReturn();
        }
    }

    /**
     * @param id 账号
     * @description: 对应账号存入金额
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 8:40
     */
    public void deposit(String id) {
        System.out.print("你要存入多少钱：");
        double balance = MyUtils.readBalance();
        tdu.deposit(id, balance);
        System.out.println("存款成功！");
        MyUtils.readReturn();
    }


    /**
     * @param id 账号
     * @description: 对应账号的用户查看交易明细
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 8:46
     */
    public void messUser(String id) {
        List<String> mess = tdu.getUserMess(id);
        System.out.printf("当前时间为：%tF %<tT，截至目前你的交易明细为：\n", System.currentTimeMillis());
        for (String s : mess) {
            System.out.println(s);
        }
        MyUtils.readReturn();
    }

    /**
     * @param id 账号
     * @description: 用户修改资料
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 10:18
     */
    public void updateUser(String id) {
        UserInfo ui = tdu.getUI(id);
        System.out.print("请输入新的用户名(不修改直接按回车跳过)：");
        String name = MyUtils.readName(ui.getName());
        ui.setName(name);
        for (; ; ) {
            System.out.print("请输入新的密码(不修改直接按回车跳过)：");
            String pass1 = MyUtils.readPwd(ui.getPassword());
            if (pass1.equals(ui.getPassword())) {
                System.out.println("新旧密码相同，已为你取消更改！");
                MyUtils.readReturn();
                break;
            } else {
                System.out.print("请再次输入新的密码：");
                String pass2 = MyUtils.readPwd();
                if (pass1.equals(pass2)) {
                    System.out.println("修改成功！");
                    ui.setPassword(pass1);
                    MyUtils.readReturn();
                    break;
                } else {
                    System.out.println("两次输入不一致！");
                }
            }
        }
        tdu.updateUser(id, ui);
    }

    /**
     * @param id 账号
     * @description: 对应账号用户注销（用让用户输密码验证身份）
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 10:31
     */

    public void logout(String id) {
        System.out.print("你确定要注销吗？(Y/N)");
        char c = MyUtils.confirmKey();
        switch (c) {
            case 'Y':
                UserInfo ui = tdu.getUI(id);
                System.out.print("请输入你的密码验证身份：");
                String pass = MyUtils.readPwd();
                if (ui.getPassword().equals(pass)) {
                    System.out.println("感谢你使用本系统！有缘再见！");
                    tdu.deleteUser(id);
                } else {
                    System.out.println("身份验证失败！将为你打开主页面。");
                }
                MyUtils.readReturn();
                break;
            default:
                break;
        }
    }
}
