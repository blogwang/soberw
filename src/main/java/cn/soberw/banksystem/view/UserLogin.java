package cn.soberw.banksystem.view;

import cn.soberw.banksystem.service.ToDoUser;
import cn.soberw.banksystem.utils.MyUtils;

/**
 * @author soberw
 * @Classname UserMenu
 * @Description  普通用户的登录和注册功能以及主菜单
 * @Date 2021-12-20 16:09
 */
public class UserLogin {
    private static ToDoUser tdu = new ToDoUser();

    private static UserMenu um = new UserMenu();

    /**
     * @description: 用户登录页面
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 17:27
     */
    public static void loginU() {
        for (int i = 3; i >= 1; i--) {
            String s = tdu.loginUser("", "");
            if ("".equals(s)) {
                break;
            }
            System.out.print("请输入你的账号：");
            String id = MyUtils.readId();
            System.out.print("请输入你的密码：");
            String password = MyUtils.readPwd();
            String name = tdu.loginUser(id, password);
            if (name != null) {
                System.out.println(name + "，欢迎你登录！");
                //登陆成功后跳入功能页面
                userMenu(id);
                break;
            }
            System.out.println("账号或者密码错误！请重新输入！！！");
            System.out.println("你当前剩余" + (i - 1) + "次机会！");
        }
    }

    /**
     * @description: 用户注册界面
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 17:28
     */
    public static void registerU() {
        System.out.print("请输入你的用户名（必须全英或者全中且不能有空格）：");
        String name = MyUtils.readName();
        String pwd = "";
        for (; ; ) {
            System.out.print("请输入你的密码（必须由数字或者字母组成，长度6-12位）：");
            pwd = MyUtils.readPwd();
            System.out.print("请确认你的密码：");
            String pwd1 = MyUtils.readPwd();
            if (pwd.equals(pwd1)) {
                System.out.println("恭喜你注册成功！");
                break;
            } else {
                System.out.println("请确保两次输入一致！");
            }
        }
        System.out.print("请存入你的开户金额：");
        double balance = MyUtils.readBalance();
        String id = tdu.addUser(name, pwd, balance);
        System.out.printf("请牢记你的账号！%s\n", id);
        System.out.println("正在为你跳转功能页面...");
        MyUtils.readReturn();
        userMenu(id);
    }

    /**
     * @param id 指定账号对应用户的操作
     * @description: 用户选择功能界面
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 21:12
     */

    public static void userMenu(String id) {
        soberw:
        for (; ; ) {
            System.out.println("-------------------------------------------------------------------");
            System.out.println("功能选择：1、查看余额");
            System.out.println("        2、存钱");
            System.out.println("        3、取钱");
            System.out.println("        4、交易明细");
            System.out.println("        5、修改资料");
            System.out.println("        6、注销账号");
            System.out.println("        7、退出登录");
            System.out.println();
            System.out.print("请选择：_");
            char c = MyUtils.menuSelection(7);
            switch (c) {
                case '1':
                    um.showBalance(id);
                    break;
                case '2':
                    um.deposit(id);
                    break;
                case '3':
                    um.withdraw(id);
                    break;
                case '4':
                    um.messUser(id);
                    break;
                case '5':
                    um.updateUser(id);
                    break;
                case '6':
                    um.logout(id);
                    break soberw;
                case '7':
                    System.out.print("你确定退出吗？(Y/N)：");
                    char confirm = MyUtils.confirmKey();
                    if (confirm == 'Y') {
                        System.out.println();
                        System.out.println("谢谢你使用本系统！欢迎你再次使用！");
                        break soberw;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
