package cn.soberw.banksystem.view;

import cn.soberw.banksystem.service.AdminInfo;
import cn.soberw.banksystem.service.ToDoAdmin;
import cn.soberw.banksystem.service.ToDoUser;
import cn.soberw.banksystem.service.UserInfo;
import cn.soberw.banksystem.utils.InfoException;
import cn.soberw.banksystem.utils.MyUtils;

import java.util.Map;
import java.util.Set;

/**
 * @author soberw
 * @Classname AdminMenu
 * @Description  管理员功能的最终实现，加入了与用户交互的信息
 * @Date 2021-12-20 21:10
 */
public class AdminMenu {
    private static ToDoAdmin tda = new ToDoAdmin();
    private static ToDoUser tdu = new ToDoUser();
    private static UserMenu um = new UserMenu();

    /**
     * @description: 显示所有用户的信息（包括账号，用户名，余额）
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:24
     */
    public void showAllUsers() {
        Map<Integer, UserInfo> map = tda.getUserMap();
        Set<Integer> set = map.keySet();
        System.out.println("编号\t\t\t账号\t\t\t用户名\t\t余额");
        if(set.size() == 0){
            System.out.println("当前暂无用户！");
        }
        for (Integer index : set) {
            UserInfo info = map.get(index);
            System.out.printf("%2d\t\t%s\t\t%s\t\t%.2f\n", index, info.getId(), info.getName(), info.getBalance());
        }
    }


    /**
     * @description: 管理员开户
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:24
     */

    public void addUser() {
        System.out.print("请输入用户名（必须全英或者全中且不能有空格）：");
        String name = MyUtils.readName();
        String pwd = "";
        for (; ; ) {
            System.out.print("请输入密码（必须由数字或者字母组成，长度6-12位）：");
            pwd = MyUtils.readPwd();
            System.out.print("请确认密码：");
            String pwd1 = MyUtils.readPwd();
            if (pwd.equals(pwd1)) {
                System.out.println("开户成功！");
                break;
            } else {
                System.out.println("请确保两次输入一致！");
            }
        }
        System.out.print("请存入此用户的开户金额：");
        double balance = MyUtils.readBalance();
        String id = tdu.addUser(name, pwd, balance);
        System.out.printf("此用户的账号为：%s 请妥善保管\n", id);
        System.out.println("正在为你跳转功能页面...");
        MyUtils.readReturn();
    }

    /**
     * @description: 管理员帮用户存款
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:26
     */
    public void deposit() {
        System.out.print("请输入用户的账号：");
        String id = MyUtils.readId();
        Map<Integer, UserInfo> map = tda.getUserMap();
        boolean flag = false;
        for (Integer index : map.keySet()) {
            UserInfo ui = (UserInfo) map.get(index);
            if (ui.getId().equals(id)) {
                um.deposit(id);
                flag = true;
                break;
            }
        }
        if (!flag) {
            try {
                throw new InfoException("查无此人！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * @description: 管理员帮用户取款
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:26
     */
    public void withdraw() {
        System.out.print("请输入用户的账号：");
        String id = MyUtils.readId();
        Map<Integer, UserInfo> map = tda.getUserMap();
        boolean flag = false;
        for (Integer index : map.keySet()) {
            UserInfo ui = (UserInfo) map.get(index);
            if (ui.getId().equals(id)) {
                um.withdraw(id);
                flag = true;
                break;
            }
        }
        if (!flag) {
            try {
                throw new InfoException("查无此人！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * @param id 管理员自己的账号
     * @description: 管理员修改自己的资料
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:27
     */
    public void updateSelf(String id) {
        AdminInfo ai = tda.getAI(id);
        System.out.print("请输入新的名字(不修改直接按回车跳过)：");
        String name = MyUtils.readName(ai.getName());
        ai.setName(name);
        for (; ; ) {
            System.out.print("请输入新的密码(不修改直接按回车跳过)：");
            String pass1 = MyUtils.readPwd(ai.getPassword());
            if (pass1.equals(ai.getPassword())) {
                System.out.println("新旧密码相同，已为你取消更改！");
                MyUtils.readReturn();
                break;
            } else {
                System.out.print("请再次输入新的密码：");
                String pass2 = MyUtils.readPwd();
                if (pass1.equals(pass2)) {
                    System.out.println("修改成功！");
                    ai.setPassword(pass1);
                    MyUtils.readReturn();
                    break;
                } else {
                    System.out.println("两次输入不一致！");
                }
            }
        }
        tda.updateSelfInfo(id, ai);
    }

    /**
     * @description: 管理员修改用户的资料
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:27
     */
    public void updateUser() {
        System.out.print("请输入用户的账号：");
        String id = MyUtils.readId();
        Map<Integer, UserInfo> map = tda.getUserMap();
        boolean flag = false;
        for (Integer index : map.keySet()) {
            UserInfo ui = (UserInfo) map.get(index);
            if (ui.getId().equals(id)) {
                um.updateUser(id);
                flag = true;
                break;
            }
        }
        if (!flag) {
            try {
                throw new InfoException("查无此人！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * @description: 管理员删除用户
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:28
     */
    public void deleteUser() {
        System.out.print("请输入用户的账号：");
        String id = MyUtils.readId();
        Map<Integer, UserInfo> map = tda.getUserMap();
        boolean flag = false;
        for (Integer index : map.keySet()) {
            UserInfo ui = (UserInfo) map.get(index);
            if (ui.getId().equals(id)) {
                System.out.print("你确定要删除吗？(Y/N)");
                char c = MyUtils.confirmKey();
                if (c == 'Y') {
                    tdu.deleteUser(id);
                }
                MyUtils.readReturn();
                flag = true;
                break;
            }
        }
        if (!flag) {
            try {
                throw new InfoException("查无此人！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
