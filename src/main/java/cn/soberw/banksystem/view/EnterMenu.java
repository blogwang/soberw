package cn.soberw.banksystem.view;

import cn.soberw.banksystem.utils.MyUtils;

import static cn.soberw.banksystem.view.AdminLogin.loginA;
import static cn.soberw.banksystem.view.UserLogin.loginU;
import static cn.soberw.banksystem.view.UserLogin.registerU;

/**
 * @author soberw
 * @Classname EnterMenu
 * @Description
 * @Date 2021-12-20 16:11
 */
public class EnterMenu {
    public static void main(String[] args) {
        //程序入口
        mainMenu();
    }

    /**
     * @description:  程序主界面
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 8:40
     */

    public static void mainMenu() {
        soberw:
        for (; ; ) {
            System.out.println("-----------------------soberw银行交易系统----------------------------");
            System.out.println();
            System.out.println("                     欢迎你登录本银行管理系统！");
            System.out.println();
            System.out.println("-------------------------------------------------------------------");
            System.out.println("功能选择：1、用户登录  2、用户开户  3、管理员登录  4、退出");
            System.out.print("请选择：_");
            char c = MyUtils.menuSelection(4);
            switch (c) {
                case '1':
                    loginU();
                    break;
                case '2':
                    registerU();
                    break;
                case '3':
                    loginA();
                    break;
                default:
                    System.out.print("你确定退出吗？(Y/N)：");
                    char confirm = MyUtils.confirmKey();
                    if (confirm == 'Y') {
                        System.out.println();
                        System.out.println("谢谢你使用本系统！欢迎你再次使用！");
                        break soberw;
                    }
                    break;
            }
        }
    }
}
