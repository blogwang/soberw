package cn.soberw.banksystem.test;

import cn.soberw.banksystem.data.DataPath;
import cn.soberw.banksystem.data.FileInOut;
import cn.soberw.banksystem.service.AdminInfo;
import cn.soberw.banksystem.service.InfoList;
import cn.soberw.banksystem.service.Information;

/**
 * @author soberw
 * @Classname AllListTest
 * @Description
 * @Date 2021-12-18 14:49
 */
public class AllListTest {
    public static void main(String[] args) {
//        InfoList a = new InfoList();
//        a.addInfo(new UserInfo("王", "123456", 123.0));
//        a.addInfo(new UserInfo("王hgj", "123456", 123.0));
//        a.addInfo(new UserInfo("王ghaj", "123456", 123.0));
//        List<Information> list = a.getList();
//        for (Information i : list) {
//            System.out.println(i.getId());
//            System.out.println(i.getName());
//            System.out.println(i.getPassword());
//        }
//        UserInfo info = (UserInfo) a.getInfo("1002");
//        System.out.println(info.getName());
//        info.setBalance(111.0);
//        info.setName("账单");
//        a.updateInfo(info.getId(), info);
//        System.out.println(a.getInfo("1002").getName());

//        InfoList il = new InfoList();
//        il.addInfo(new AdminInfo("soberw", "admin123456"));
//        FileInOut fio = new FileInOut();
//        fio.writeOut(il, DataPath.ADMIN);

        FileInOut fio = new FileInOut();
        InfoList list = fio.readIn(DataPath.ADMIN);
        for (Information i : list.getList()) {
            AdminInfo ai = (AdminInfo) i;
            System.out.println(ai.getId());
        }
    }
}
