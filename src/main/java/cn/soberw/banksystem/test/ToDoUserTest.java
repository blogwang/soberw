package cn.soberw.banksystem.test;

import cn.soberw.banksystem.data.DataPath;
import cn.soberw.banksystem.data.FileInOut;
import cn.soberw.banksystem.service.InfoList;
import cn.soberw.banksystem.service.Information;
import cn.soberw.banksystem.service.ToDoUser;
import cn.soberw.banksystem.service.UserInfo;

/**
 * @author soberw
 * @Classname ToDoUserTest
 * @Description
 * @Date 2021-12-20 18:35
 */
public class ToDoUserTest {
    public static void main(String[] args) {
        FileInOut i = new FileInOut();
        InfoList infoList = i.readIn(DataPath.USER);
        for (Information ii : infoList.getList()) {
            UserInfo ui = (UserInfo) ii;
            System.out.println(ui.getMessage());
            System.out.println(ui.getId());
        }
//        infoList.getList().clear();
//        i.writeOut(infoList,DataPath.USER);
        System.out.println(infoList.getSize());
        ToDoUser tdu = new ToDoUser();
//        System.out.println(tdu.addUser("王海鹏", "123456", 999));
        System.out.println(tdu.getUserMess("0004786057"));
        tdu.deposit("0004786057",100);
        System.out.println(tdu.getUserMess("0004786057"));

    }
}
