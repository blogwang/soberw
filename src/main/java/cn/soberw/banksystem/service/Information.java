package cn.soberw.banksystem.service;

import java.io.Serializable;

/**
 * @author soberw
 * @Classname Information
 * @Description   管理员和普通用户的父类，是个抽象类，定义了两者的公共属性
 * @Date 2021-12-18 14:14
 */
public abstract class Information implements Serializable {

    @java.io.Serial
    private static final long serialVersionUID = 5767710L;
    /**
     * 账号
     */
    private String id;
    /**
     * 用户名
     */
    private String name;
    /**
     * 密码
     */
    private String password;


    public Information(String id, String name, String password) {
        this.name = name;
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
