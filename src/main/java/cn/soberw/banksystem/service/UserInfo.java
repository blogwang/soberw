package cn.soberw.banksystem.service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author soberw
 * @Classname UserInfo
 * @Description  用户的信息，相比与父类新加了余额和交易明细两个属性
 * @Date 2021-12-18 11:00
 */
public class UserInfo extends Information {

    /**
     * 由当前时间生成的id
     */
    private static String id = String.valueOf(System.currentTimeMillis());
    /**
     * 余额
     */
    private double balance;

    /**
     * 用户交易明细
     */
    private List<String> message = new ArrayList<>();

    public UserInfo(String userName, String password, double balance) {
        //截取后十位为账号
        super(id.substring(id.length() - 10), userName, password);
        this.balance = balance;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
