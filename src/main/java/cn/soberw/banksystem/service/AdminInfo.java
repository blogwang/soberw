package cn.soberw.banksystem.service;

/**
 * @author soberw
 * @Classname AdminInfo
 * @Description  管理员的信息
 * @Date 2021-12-18 11:01
 */
public class AdminInfo extends Information {
    @java.io.Serial
    private static final long serialVersionUID = 576776875670L;
    /**
     * 由当前时间生成的id
     */
    private static String id = String.valueOf(System.currentTimeMillis());


    public AdminInfo(String adminName, String password) {
        //截取后十位为账号
        super(id.substring(id.length() - 10), adminName, password);
    }
}
