package cn.soberw.banksystem.service;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author soberw
 * @Classname AllList
 * @Description   对父类进行控制，以数组形式，并提供一系列操作，实现系列化，此类直接存储在文件中
 * @Date 2021-12-18 11:51
 */
public class InfoList implements Serializable {
    @java.io.Serial
    private static final long serialVersionUID = 576584667710L;


    /**
     * 用于保存信息的数组
     */
    List<Information> list = new ArrayList<>();

    /**
     * 用于保存用户交易明细
     */
//    Map<String, List<String>> map = new HashMap<>();

    /**
     * @description: 获取当前list长度
     * @return: int
     * @author: soberw
     * @time: 2021/12/18 14:26
     */
    public int getSize() {
        return this.list.size();
    }

    /**
     * @param info 将要添加的信息
     * @description: 将个人信息添加在数组中
     * @return: void
     * @author: soberw
     * @time: 2021/12/18 14:27
     */
    public void addInfo(Information info) {
        this.list.add(info);
    }

    /**
     * @param id 对应的账号
     * @description: 获取指定账号的个人信息，如果指定的id不存在则返回null
     * @return: cn.soberw.banksystem.service.Information
     * @author: soberw
     * @time: 2021/12/18 14:29
     */
    public Information getInfo(String id) {
        for (Information info : this.list) {
            if (id.equals(info.getId())) {
                return info;
            }
        }
        return null;
    }

    /**
     * @param info 新的信息
     * @description: 修改信息
     * @return: void
     * @author: soberw
     * @time: 2021/12/18 14:42
     */
    public void updateInfo(String id, Information info) {
        for (int i = 0; i < list.size(); i++) {
            if (id.equals(list.get(i).getId())) {
                list.set(i, info);
            }
        }
    }


    /**
     * @param id 将要删除的信息id
     * @description: 删除指定id的信息
     * @return: boolean  成功与否
     * @author: soberw
     * @time: 2021/12/18 14:46
     */
    public boolean deleteInfo(String id) {
        for (int i = 0; i < list.size(); i++) {
            if (id.equals(list.get(i).getId())) {
                list.remove(i);
                return true;
            }
        }
        return false;
    }

    /**
     * @description: 返回全部的信息
     * @return: java.util.List<cn.soberw.banksystem.service.Information>
     * @author: soberw
     * @time: 2021/12/18 14:30
     */
    public List<Information> getList() {
        return list;
    }

    /**
     * @param list 一个保存当前所有信息的数组
     * @description: 重新设置当前信息数组
     * @author: soberw
     * @time: 2021/12/18 14:30
     */
    public void setList(List<Information> list) {
        this.list = list;
    }

}
