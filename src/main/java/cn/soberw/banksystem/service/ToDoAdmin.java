package cn.soberw.banksystem.service;

import cn.soberw.banksystem.data.FileInOut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.soberw.banksystem.data.DataPath.ADMIN;
import static cn.soberw.banksystem.data.DataPath.USER;

/**
 * @author soberw
 * @Classname ToDoAdmin
 * @Description   对管理员功能的进一步实现，包括了所有管理员的功能
 * @Date 2021-12-18 17:21
 */
public class ToDoAdmin {
    /**
     * 用于读入读出数据
     */
    private FileInOut uuu = new FileInOut();



    /**
     * @param id       账号
     * @param password 密码
     * @description: 管理员登录操作
     * @return: 成功返回用户名，失败返回null
     * @author: soberw
     * @time: 2021/12/21 12:59
     */
    public String loginAdmin(String id, String password) {
        InfoList als = uuu.readIn(ADMIN);
        for (Information i : als.list) {
            AdminInfo ai = (AdminInfo) i;
            if (id.equals(ai.getId())) {
                if (password.equals(ai.getPassword())) {
                    return ai.getName();
                }
            }
        }
        return null;
    }


    /**
     * @param i    用户的账号
     * @param info 新的信息
     * @description: 修改用户的个人信息
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 16:33
     */

    public void updateUserInfo(int i, UserInfo info) {
        Map<Integer, UserInfo> map = getUserMap();
        UserInfo userInfo = map.get(i);

    }

    /**
     * @param id 账号
     * @description: 获取指定账号的管理员，若账号不存在返回null
     * @return: cn.soberw.banksystem.service.AdminInfo
     * @author: soberw
     * @time: 2021/12/21 18:46
     */
    public AdminInfo getAI(String id) {
        InfoList list = uuu.readIn(ADMIN);
        for (Information i : list.getList()) {
            AdminInfo ai = (AdminInfo) i;
            if (ai.getId().equals(id)) {
                return ai;
            }
        }
        return null;
    }

    /**
     * @param id   账号
     * @param info 新的信息
     * @description: 管理员修改自己的资料
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 13:23
     */
    public void updateSelfInfo(String id, AdminInfo info) {
        InfoList ail = uuu.readIn(ADMIN);
        ail.updateInfo(id, info);
        uuu.writeOut(ail, ADMIN);
    }

    /**
     * @description: 返回一个由当前全部用户组成的map，键是编号，值是对应的用户信息
     * @return: 一个由当前全部用户组成的map，键是编号，值是对应的用户信息
     * @author: soberw
     * @time: 2021/12/21 12:53
     */
    public Map<Integer, UserInfo> getUserMap() {
        InfoList infoList = uuu.readIn(USER);
        Map<Integer, UserInfo> map = new HashMap<>();
        int index = 1;
        for (Information i : infoList.getList()) {
            UserInfo ui = (UserInfo) i;
            map.put(index++, ui);
        }
        return map;
    }

    /**
     * @param map 新的用户信息
     * @description: 重新将map中的值（用户信息）保存在文件中（即更新文件中的用户信息）
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 13:19
     */
    public void saveUserMap(Map<Integer, UserInfo> map) {
        InfoList infoList = uuu.readIn(USER);
        List<Information> list = new ArrayList<>();
        for (UserInfo value : map.values()) {
            list.add(value);
        }
        infoList.setList(list);
        uuu.writeOut(infoList, USER);
    }
}
