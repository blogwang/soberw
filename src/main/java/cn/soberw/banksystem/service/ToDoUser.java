package cn.soberw.banksystem.service;

import cn.soberw.banksystem.data.FileInOut;
import cn.soberw.banksystem.utils.InfoException;

import java.util.List;

import static cn.soberw.banksystem.data.DataPath.USER;

/**
 * @author soberw
 * @Classname ToDoUser
 * @Description  对用户功能的进一步实现
 * @Date 2021-12-18 17:21
 */
public class ToDoUser {
    /**
     * 用于读入读出数据
     */
    private FileInOut uuu = new FileInOut();



    /**
     * @param name     用户名
     * @param password 密码
     * @param balance  金额
     * @description: 添加用户, 并且返回账号给用户（账号是系统自动生成的）
     * @return: 返回账号
     * @author: soberw
     * @time: 2021/12/20 12:45
     */
    public String addUser(String name, String password, double balance) {
        //读取文件
        InfoList il = uuu.readIn(USER);
        //创建一个新用户
        UserInfo ui = new UserInfo(name, password, balance);
        List<String> list = ui.getMessage();
        //存入开户信息
        list.add(String.format("%tF %<tT 新开户。", System.currentTimeMillis()));
        ui.setMessage(list);
        //加入中间层
        il.addInfo(ui);
        //写入文件
        uuu.writeOut(il, USER);
        return ui.getId();
    }

    /**
     * @param id       账号
     * @param password 密码
     * @description: 用户登录操作
     * @return: 成功返回用户名，失败返回null
     * @author: soberw
     * @time: 2021/12/20 12:59
     */
    public String loginUser(String id, String password) {
        InfoList ils = uuu.readIn(USER);
        if (ils == null || ils.getSize() == 0) {
            try {
                throw new InfoException("当前系统暂无用户，请先注册！");
            } catch (InfoException e) {
                System.out.println(e.getMessage());
                return "";
            } catch (Exception ee) {

            }
        }
        for (Information ui : ils.list) {
            UserInfo i = (UserInfo) ui;
            if (id.equals(i.getId())) {
                if (password.equals(i.getPassword())) {
                    return i.getName();
                }
            }
        }
        return null;
    }

    /**
     * @param id   账号
     * @param info 新的信息
     * @description: 修改用户信息
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 13:17
     */
    public void updateUser(String id, UserInfo info) {
        InfoList infoList = uuu.readIn(USER);
        infoList.updateInfo(id, info);
        uuu.writeOut(infoList, USER);
    }

    /**
     * @param id 账号
     * @description: 注销账号，对应的交易明细也要销毁
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 13:19
     */
    public void deleteUser(String id) {
        InfoList infoList = uuu.readIn(USER);
        //删除用户
        infoList.deleteInfo(id);
        uuu.writeOut(infoList, USER);
    }

    /**
     * @param id 账号
     * @description: 用户查看余额
     * @return: double
     * @author: soberw
     * @time: 2021/12/20 13:09
     */
    public double showBalance(String id) {
        InfoList infoList = uuu.readIn(USER);
        for (Information i : infoList.list) {
            UserInfo ui = (UserInfo) i;
            if (id.equals(ui.getId())) {
                return ui.getBalance();
            }
        }
        return -1;
    }

    /**
     * @param balance 取出金额
     * @description: 取钱
     * @return: 如果取款金额大于余额返回-1，否则返回当前余额
     * @author: soberw
     * @time: 2021/12/20 13:22
     */
    public double withDraw(String id, double balance) {
        InfoList il = uuu.readIn(USER);
        for (Information i : il.list) {
            UserInfo ui = (UserInfo) i;
            if (id.equals(i.getId())) {
                if (balance <= ui.getBalance()) {
                    List<String> mess = ui.getMessage();
                    ui.setBalance(ui.getBalance() - balance);
                    mess.add(String.format("%tF %<tT 取出 %.2f元。", System.currentTimeMillis(), balance));
                    ui.setMessage(mess);
                    uuu.writeOut(il, USER);
                    return ui.getBalance() - balance;
                }
            }
        }
        return -1;
    }

    /**
     * @param balance 存入金额
     * @description: 存钱
     * @return: void
     * @author: soberw
     * @time: 2021/12/20 13:23
     */
    public void deposit(String id, double balance) {
        InfoList il = uuu.readIn(USER);
        for (Information i : il.list) {
            UserInfo ui = (UserInfo) i;
            if (id.equals(ui.getId())) {
                List<String> mess = ui.getMessage();
                ui.setBalance(ui.getBalance() + balance);
                mess.add(String.format("%tF %<tT 存入 %.2f元。", System.currentTimeMillis(), balance));
                ui.setMessage(mess);
                uuu.writeOut(il, USER);
            }
        }
    }

    /**
     * @param id 账号
     * @description: 返回用户账户对应的交易明细
     * @return: 一个存放交易明细的list
     * @author: soberw
     * @time: 2021/12/20 16:06
     */
    public List<String> getUserMess(String id) {
        InfoList il = uuu.readIn(USER);
        for (Information i : il.list) {
            UserInfo ui = (UserInfo) i;
            if (id.equals(ui.getId())) {
                return ui.getMessage();
            }
        }
        return null;
    }

    /**
     * @param id 账号
     * @description: 返回指定id的用户
     * @return: cn.soberw.banksystem.service.UserInfo
     * @author: soberw
     * @time: 2021/12/20 19:42
     */
    public UserInfo getUI(String id) {
        InfoList il = uuu.readIn(USER);
        for (Information i : il.list) {
            UserInfo ui = (UserInfo) i;
            if (id.equals(ui.getId())) {
                return ui;
            }
        }
        return null;
    }
}
