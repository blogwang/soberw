package cn.soberw.banksystem.data;

/**
 * @author soberw
 * @Classname DataPath
 * @Description  存放文件的路径
 * @Date 2021-12-20 8:53
 */
public class DataPath {
    /**
     * user.dat  用户数据
     */
    public static final String USER = "E:\\HelloJava\\soberw\\src\\main\\java\\cn\\soberw\\banksystem\\data\\user.dat";
    /**
     * admin.dat  管理员数据
     */
    public static final String ADMIN = "E:\\HelloJava\\soberw\\src\\main\\java\\cn\\soberw\\banksystem\\data\\admin.dat";
}
