package cn.soberw.banksystem.data;

import cn.soberw.banksystem.service.InfoList;

import java.io.*;

/**
 * @author soberw
 * @Classname FileInOut
 * @Description 控制用户信息的读入和写出
 * @Date 2021-12-18 11:25
 */
public class FileInOut {
    /**
     * @description: 从文件中读取信息
     * @param path: 文件路径
     * @return: 信息
     * @author: soberw
     * @time: 2021/12/18 11:36
     */
    public InfoList readIn(String path) {
        InfoList il = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            il = (InfoList) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return il;
    }


    /**
     * @param il 要写入的信息
     * @param path: 文件路径
     * @description: 写入信息到文件
     * @author: soberw
     * @time: 2021/12/18 11:37
     */
    public void writeOut(InfoList il, String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(il);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
