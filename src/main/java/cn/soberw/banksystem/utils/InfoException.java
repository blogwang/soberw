package cn.soberw.banksystem.utils;

/**
 * @author soberw
 * @Classname InfoException
 * @Description  自定义异常类
 * @Date 2021-12-18 13:36
 */
public class InfoException extends Exception {
    @java.io.Serial
    static final long serialVersionUID = 4765528798424229948L;


    public InfoException() {

    }

    public InfoException(String msg) {
        super(msg);
    }
}
