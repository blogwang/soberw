package cn.soberw.banksystem.utils;

import java.util.Locale;
import java.util.Scanner;

/**
 * @author soberw
 * @Classname MyUtils
 * @Description 本模块为功能模块，主要控制用户输入
 * @Date 2021-12-18 8:57
 */
public class MyUtils {
    public static Scanner sc = new Scanner(System.in);

    /**
     * @param limit 控制在1-limit之间
     * @description: 读取键盘，如果用户键入1 - limit之间的任意字符，则返回，否则一直输入
     * @return: 输入的字符
     * @author: soberw
     * @time: 2021/12/18 9:39
     */
    public static char menuSelection(int limit) {
        char c;
        char[] cs = new char[limit];
        for (int i = 1; i <= limit; i++) {
            cs[i - 1] = (char) (48 + i);
        }
        soberw:
        for (; ; ) {
            String str = keyBoard(1, false);
            c = str.charAt(0);
            for (int i = 1; i <= limit; i++) {
                if (cs[i - 1] == c) {
                    break soberw;
                }
            }
            System.out.print("选择错误，请重新输入：");
        }
        return c;
    }


    /**
     * @description: 读取用户输入账号，账号必须全数字
     * @return: 输入的账号
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static String readId() {
        String id;
        for (; ; ) {
            id = keyBoard(0, false).trim();
            if (id.matches("[0-9]+")) {
                break;
            } else {
                System.out.print("账号必须全部位数字！请重新输入：");
            }
        }
        return id;
    }

    /**
     * @description: 读取用户输入用户名，用户名必须全英或者全中文且不能有空格
     * @return: 输入的名字
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static String readName() {
        String name;
        for (; ; ) {
            name = keyBoard(0, false).trim();
            if (name.matches("[a-zA-Z]+") || name.matches("[\\u4e00-\\u9fa5]+")) {
                break;
            } else {
                System.out.print("用户名必须全英或者全中文且不能有空格！请重新输入：");
            }
        }
        return name;
    }

    /**
     * @param defaultName 默认值
     * @description: 读取用户输入用户名，用户名必须全英或者全中文且不能有空格,若输入为空，则按默认值返回
     * @return: 输入的名字
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static String readName(String defaultName) {
        String name;
        for (; ; ) {
            name = keyBoard(0, true).trim();
            if ("".equals(name)) {
                return defaultName;
            }
            if (name.matches("[a-zA-Z]+") || name.matches("[\\u4e00-\\u9fa5]+")) {
                break;
            } else {
                System.out.print("用户名必须全英或者全中文且不能有空格！请重新输入：");
            }
        }
        return name;
    }

    /**
     * @description: 读取用户输入密码，密码必须由数字或者字母组成，长度不小于6位，不大于12位
     * @return: 密码
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static String readPwd() {
        String pwd;
        for (; ; ) {
            pwd = keyBoard(0, false).trim();
            if (pwd.matches("[a-zA-Z0-9]{6,12}")) {
                break;
            } else {
                System.out.print("密码必须由数字或者字母组成，长度6-12位！请重新输入：");
            }
        }
        return pwd;
    }

    /**
     * @param defaultPwd 默认值
     * @description: 读取用户输入密码，密码必须由数字或者字母组成，长度不小于6位，不大于12位,如果输入为空，则按默认值返回
     * @return: 密码
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static String readPwd(String defaultPwd) {
        String pwd;
        for (; ; ) {
            pwd = keyBoard(0, true).trim();
            if ("".equals(pwd)) {
                return defaultPwd;
            }
            if (pwd.matches("[a-zA-Z0-9]{6,12}")) {
                break;
            } else {
                System.out.print("密码必须由数字或者字母组成，长度6-12位！请重新输入：");
            }
        }
        return pwd;
    }

    /**
     * @description: 读取用户输入金额，若转换错误则报错提示，直到正确输入为止
     * @return: 金额
     * @author: soberw
     * @time: 2021/12/18 10:25
     */
    public static double readBalance() {
        String str;
        double balance;
        for (; ; ) {
            str = keyBoard(0, false).trim();
            try {
                balance = Double.parseDouble(str);
                break;
            } catch (Exception e) {
                System.out.println("请输入正确的格式！");
            }
        }
        return balance;
    }

    /**
     * @description: 用户按下回车键后方可执行下一次操作
     * @return: void
     * @author: soberw
     * @time: 2021/12/21 8:38
     */

    public static void readReturn() {
        System.out.print("按回车键继续...");
        keyBoard(1000, true);
    }


    /**
     * @description: 从键盘读取‘Y’或’N’，并返回
     * @return: ‘Y’或’N’
     * @author: soberw
     * @time: 2021/12/18 10:13
     */
    public static char confirmKey() {
        char c;
        for (; ; ) {
            String s = keyBoard(1, false).toUpperCase(Locale.ROOT);
            c = s.charAt(0);
            if (c != 'Y' && c != 'N') {
                System.out.print("选择错误，请重新输入：");
            } else {
                break;
            }
        }
        return c;
    }


    /**
     * @param limit  要输入字符的长度
     * @param isNull 是否可以为空
     * @description: 从键盘录入指定长度信息，并控制是否可以为空，此方法供内部使用，若长度为0，则用户可输入任意长度
     * @return: 输入的信息
     * @author: soberw
     * @time: 2021/12/18 9:18
     */
    private static String keyBoard(int limit, boolean isNull) {
        String line = "";

        while (sc.hasNextLine()) {
            line = sc.nextLine().trim();
            if (line.length() == 0) {
                if (isNull) {
                    return line;
                } else {
                    continue;
                }
            }
            if (limit == 0) {
                return line;
            }
            if (line.length() < 1 || line.length() > limit) {
                System.out.print("输入长度（不大于" + limit + "）错误，请重新输入：");
                continue;
            }
            break;

        }
        return line;
    }

}
