package day_12_22;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author soberw
 * @Classname StreamTest
 * @Description
 * @Date 2021-12-22 14:00
 */
public class StreamTest {
    public static void main(String[] args) {
        Random random = new Random();
        Stream<Integer> s = Stream.generate(() -> random.nextInt(100) + 1).limit(10);
        //System.out.println(s.max(Integer::compareTo).get());

        //创建一个空的流
        IntStream is = IntStream.empty();

        //每一次操作都是对上一个操作的覆盖
        is = IntStream.of(10, 20, 30, 40, 50);
        is = IntStream.range(1, 9);
        is = IntStream.rangeClosed(1, 10);
        //limit表示产生的最大长度，skip表示丢弃前面的n个元素，将之后的加入流中
        is = IntStream.generate(() -> random.nextInt(100) + 1).limit(8).skip(8);

        //只输出最后一次操作产生的结果
//        is.sorted().forEach(System.out::println);

        //Stream<Integer> si = is.boxed();
        //si.sorted(Integer::compareTo).forEach(System.out::println);

//        //双色球
        Stream.generate(() -> random.nextInt(33) + 1).distinct().limit(6).forEach(System.out::println);



    }
    @Test
    public void findAny(){
        Stream<String> stream = Stream.of("you", "give", "me", "stop");
        String value2 = stream.findAny().get();
        System.out.println(value2);
    }
}
