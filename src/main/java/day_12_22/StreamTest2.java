package day_12_22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * @author soberw
 * @Classname StreamTest2
 * @Description
 * @Date 2021-12-22 17:39
 */
public class StreamTest2 {
    public static void main(String[] args) {
        File f = new File("E:\\HelloJava\\soberw\\src\\main\\java\\day_12_22\\StreamTest.java");
        try (var read = new BufferedReader(new FileReader(f))) {
            int i = 0;
            Iterator<String> iterator = read.lines().iterator();
            while (iterator.hasNext()) {
                System.out.printf("%02d、%s\n",++i,iterator.next());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
