package day_01_04.kaoshi;

import java.util.Arrays;

/**
 * @author soberw
 * @Classname Test01
 * @Description  声明并初始化一个String类型数组，赋值张三、李四、王五，并遍历打印到控制台
 * @Date 2022-01-04 9:29
 */
public class Test01 {
    public static void main(String[] args) {
        String[] str = {"张三", "李四", "王五"};
        Arrays.stream(str).forEach(System.out::println);
        System.out.println("------------------");
        for (String s : str) {
            System.out.println(s);
        }
        System.out.println("------------------");
        for (int i = 0; i < str.length; i++) {
            System.out.println(str[i]);
        }
    }
}
