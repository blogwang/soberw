package day_01_04.kaoshi.test05;


/**
 * @author soberw
 * @Classname InterfaceDemo
 * @Description
 * @Date 2022-01-04 10:17
 */
public class InterfaceDemo {
    public static void main(String[] args) {
        Vehicle v1 = new Bike();
        v1.start(35);
        v1.start(10);
        v1.stop(15);
        v1.stop(5);
        System.out.println("---------------");
        Vehicle v2 = new Bus();
        v2.start(40);
        v2.start(15);
        v2.stop(25);
        v2.stop(10);
    }
}
