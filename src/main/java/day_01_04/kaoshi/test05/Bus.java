package day_01_04.kaoshi.test05;

/**
 * @author soberw
 * @Classname Bus
 * @Description
 * @Date 2022-01-04 10:08
 */
public class Bus implements Vehicle {
    @Override
    public void start(int speed) {
        if (speed < 30) {
            System.out.println("你起步车速为：" + speed + "km\\h,路途注意安全！");
        } else {
            System.out.println("驾驶公交车，请注意车速，你当前车速为：" + speed + ",一车人安全最重要！");
        }
    }

    @Override
    public void stop(int speed) {
        if (speed < 20) {
            System.out.println("停靠减速，你当前车速为：" + speed + ",注意行人！");
        } else {
            System.out.println("急刹车过于危险！");
        }
    }
}
