package day_01_04.kaoshi.test05;

/**
 * @author soberw
 * @Classname Bike
 * @Description
 * @Date 2022-01-04 10:07
 */
public class Bike implements Vehicle{
    @Override
    public void start(int speed) {
        if (speed < 30) {
            System.out.println("你起步车速为：" + speed + "km\\h,路途注意安全！");
        } else {
            System.out.println("你当前车速为：" + speed + ",你要把腿蹬冒烟吗？");
        }
    }

    @Override
    public void stop(int speed) {
        if (speed < 10) {
            System.out.println("停靠减速，你当前车速为：" + speed + ",注意过往车辆，切勿在路中间停车！");
        } else {
            System.out.println("急刹车容易摔倒！");
        }
    }
}
