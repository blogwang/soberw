package day_01_04.kaoshi.test05;

/**
 * @author soberw
 * @Classname Vehicle
 * @Description
 * @Date 2022-01-04 10:04
 */
public interface Vehicle {
    /**
     *  控制车辆启动
     * @param speed  速度
     */
    void start(int speed);

    /**
     * 控制车辆停止
     * @param speed  速度
     */
    void stop(int speed);
}
