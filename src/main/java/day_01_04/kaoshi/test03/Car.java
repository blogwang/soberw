package day_01_04.kaoshi.test03;

/**
 * @author soberw
 * @Classname Car
 * @Description 建立一个汽车类，包括轮胎个数、汽车颜色、车身重量等属性。
 * 并通过不同的构造方法创建事例。
 * 至少要求：汽车能够加速、减速、停车。
 * 要求：命名规范、代码体现层次、有友好的操作提示。
 * @Date 2022-01-04 9:39
 */
public class Car {
    /**
     * 轮胎个数
     */
    private int tyres;
    /**
     * 汽车颜色
     */
    private String color;
    /**
     * 车身重量，吨为计量单位
     */
    private double weight;
    /**
     * 汽车名字
     */
    private String name;

    public Car() {
    }

    public Car(String name, int tyres, String color, double weight) {
        this.tyres = tyres;
        this.color = color;
        this.weight = weight;
        this.name = name;
    }

    /**
     * 加速操作
     * @param speed  速度
     */
    public void speedUp(int speed) {
        System.out.println("你当前车速为：" + speed + "km\\h,请谨慎驾驶！");
    }

    /**
     * 减速操作
     * @param speed  速度
     */
    public void speedDown(int speed) {
        System.out.println("你当前车速为：" + speed + "km\\h,减速请注意后方开车！");
    }

    /**
     * 停车操作
     */
    public void stop() {
        System.out.println("路边停车，注意避让行人！");
    }

    public int getTyres() {
        return tyres;
    }

    public void setTyres(int tyres) {
        this.tyres = tyres;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("你当前驾驶的是：%s，颜色为：%s，有%d个轮胎，总重量%.2f吨", name, color, tyres, weight);
    }
}
