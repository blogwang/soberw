package day_01_04.kaoshi.test03;

/**
 * @author soberw
 * @Classname CarTest
 * @Description
 * @Date 2022-01-04 9:52
 */
public class CarTest {
    public static void main(String[] args) {
        Car c1 = new Car();
        c1.setName("奔驰");
        c1.setColor("白色");
        c1.setTyres(4);
        c1.setWeight(2.0);
        System.out.println(c1);
        c1.speedUp(120);
        c1.speedDown(40);
        c1.stop();
        System.out.println("-----------------------");
        Car c2 = new Car("宝马", 4, "红色", 1.5);
        System.out.println(c2);
        c2.speedUp(150);
        c2.speedDown(60);
        c2.stop();
    }
}
