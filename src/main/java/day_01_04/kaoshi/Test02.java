package day_01_04.kaoshi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author soberw
 * @Classname Test02
 * @Description  声明String类型集合，添加五条数据，使用for或forEach遍历将结果打印到控制台
 * @Date 2022-01-04 9:33
 */
public class Test02 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("张三");
        list.add("李四");
        list.add("王五");
        list.add("马六");
        list.add("赵七");
        for (String s : list) {
            System.out.println(s);
        }
        System.out.println("---------------");
        list.forEach(System.out::println);
        System.out.println("---------------");
        Iterator<String> it = list.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
