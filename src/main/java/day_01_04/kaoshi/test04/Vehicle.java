package day_01_04.kaoshi.test04;

/**
 * @author soberw
 * @Classname Vehicle
 * @Description
 * @Date 2022-01-04 9:57
 */
public abstract class Vehicle {
    /**
     *  显示车轮信息
     * @return  车轮的具体信息
     */
    public abstract String NoOfWheels();
}
