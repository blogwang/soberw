package day_01_04.kaoshi.test04;

/**
 * @author soberw
 * @Classname Test
 * @Description
 * @Date 2022-01-04 10:03
 */
public class Test {
    public static void main(String[] args) {
        Vehicle v1 = new Car();
        Vehicle v2 = new Motorbike();

        System.out.println(v1.NoOfWheels());
        System.out.println(v2.NoOfWheels());
    }
}
