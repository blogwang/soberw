package day_01_04.kaoshi.test04;

/**
 * @author soberw
 * @Classname Motorbike
 * @Description
 * @Date 2022-01-04 10:02
 */
public class Motorbike extends Vehicle{
    /**
     * 对父类方法的重写
     * @return  “双轮车”信息
     */
    @Override
    public String NoOfWheels() {
        return "这是一个双轮车";
    }
}
