package day_01_04.kaoshi.test04;

/**
 * @author soberw
 * @Classname Car
 * @Description
 * @Date 2022-01-04 10:01
 */
public class Car extends Vehicle{
    /**
     * 对父类方法的重写
     * @return  “四轮车”信息
     */
    @Override
    public String NoOfWheels() {
        return "这是一个四轮车";
    }
}
