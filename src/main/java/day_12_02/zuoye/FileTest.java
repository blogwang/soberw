package day_12_02.zuoye;

/**
 * @author soberw
 */
public class FileTest {
    public static void main(String[] args) {
        Files file = new Files("c:/user/abc/upload/xxsfasf.afasf-asfas.jpg");
        System.out.println(file.getFile());
        System.out.println(file.dir());
        System.out.println(file.suffix());
        System.out.println(file.fileName());
        System.out.println(file.replaceFileWith("20211202154333"));
        System.out.println("------------------------------------");
        String fileAddress = "d:\\Admin\\QQ\\images\\20200303051156.psd";
        System.out.println(file.dir(fileAddress));
        System.out.println(file.fileName(fileAddress));
        System.out.println(file.suffix(fileAddress));
        System.out.println(file.replaceFileWith(fileAddress,"asdfghjhkl"));

    }
}
