package day_12_02.zuoye;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * 对StringArrays的测试
 *
 * @author soberw
 */
public class StringArr {

    public static void main(String[] args) {
        StringArrays sa = new StringArrays();
        //初始数组
        Vector<String> v = new Vector<>(List.of("eat", "tea", "tan", "ate", "nat", "bat"));
        //保存数据
        Vector<Vector<String>> saveV = new Vector<>();
        while (true) {
            //暂存数组
            Vector<String> ver = new Vector<>();
            for (int j = 0; j < v.size(); j++) {
                if (v.get(j) != null) {
                    ver.add(v.get(j));
                    v.set(j, null);
                    break;
                }
            }
            if (ver.size() == 0) {
                break;
            }
            for (int k = 0; k < v.size(); k++) {
                if (v.get(k) != null) {
                    if (sa.compareNow(ver.get(0), v.get(k))) {
                        ver.add(v.get(k));
                        v.set(k, null);
                    }
                }
            }
            System.out.println(ver);
            saveV.add(ver);
        }
        System.out.println(Arrays.toString(saveV.toArray()));
    }
}
