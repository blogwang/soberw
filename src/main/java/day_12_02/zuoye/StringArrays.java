package day_12_02.zuoye;

import java.util.Arrays;

/**
 * 作业：将一个字符串数组进行分组输出，每组中的字符串都由相同的字符组成。
 * 举个例子：输入["eat","tea","tan","ate","nat","bat"]
 * 输出[["ate","eat","tea"],["nat","tan"],["bat"]]
 *
 * @author soberw
 */

public class StringArrays {

    /**
     * 给定两个字符串，判定第二个串中的字符是否在第一个字符串中全部存在
     * contains()当且仅当此字符串包含指定的char值序列时才返回true。
     *
     * @param a 第一个字符串
     * @param b 第二个字符串
     * @return boolean
     */
    private boolean compareReverse(String a, String b) {
        //记录次数
        int count = 0;
        char[] newB = b.toCharArray();
        for (char bb : newB) {
            if (a.contains(String.valueOf(bb))) {
                count++;
            }
        }
        //相等说明全部包含在内
        if (count == a.length()) {
            return true;
        }

        return false;
    }

    /**
     * 对字符串排序
     *
     * @param str 字符串
     * @return 排序后的
     */
    private String sortString(String str) {
        char[] s = str.toCharArray();
        Arrays.sort(s);
        return String.valueOf(s);
    }

    /**
     * 判断两个字符串是否由相同字符组成，切长度相同
     *
     * @param m 第一个字符串
     * @param n 第二个字符串
     * @return boolean
     */
    public boolean compare(String m, String n) {
        if (m.length() != n.length()) {
            return false;
        }
        //避免出现子包含情况
        return compareReverse(m, n) && compareReverse(n, m);
    }

    /**
     * 对compare方法的改进
     *
     * @param m 第一个字符串
     * @param n 第二个字符串
     * @return boolean
     */
    public boolean compareNow(String m, String n) {
        if (m.length() != n.length()) {
            return false;
        }
        return sortString(m).equalsIgnoreCase(sortString(n));
    }

}
