package day_12_06.day;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 在字符串"123456789"中间随机插入"+"或者"-"，使得表达式计算结果为100,列出一个这样的式子
 *
 * @author soberw
 * @Classname SumIs100
 * @Description
 * @Date 2021-12-06 17:28
 */
public class SumIs100 {
    public static void main(String[] args) {
        //控制运算符
        String[] op = {"", "-", "+"};
        Random random = new Random();
        while (true) {
            //建一个字符串对象用于存放表达式
            StringBuilder sbu = new StringBuilder("1");
            //存放和
            int sum = 0;
            //从第二位开始随机插入运算符
            for (int i = 2; i <= 9; i++) {
                sbu.append(op[random.nextInt(op.length)]);
                sbu.append(i);
            }
            //通过正则筛选出所有的数字，带符号，如 +3、如 -56
            Pattern p = Pattern.compile("[+-]*[0-9]+");
            Matcher matcher = p.matcher(sbu.toString());
            while (matcher.find()) {
                //转换为Integer便于计算，累加在sum中
                sum += Integer.parseInt(matcher.group());
            }
            //等于100表示找到了，退出循环
            if (sum == 100) {
                System.out.printf("%s = 100", sbu);
                break;
            }
        }
    }
}
