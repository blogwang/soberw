package day_12_31.localdate;

import java.time.LocalTime;

/**
 * @author soberw
 * @Classname LocalTimeTest
 * @Description
 * @Date 2021-12-31 14:32
 */
public class LocalTimeTest {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.toString());
        System.out.println(System.currentTimeMillis());//毫秒数
        System.out.println(System.nanoTime());//纳秒数
        System.out.println(localTime.getHour());
        System.out.println(localTime.getMinute());
        System.out.println(localTime.getSecond());
        System.out.println(localTime.getNano());
    }
}
