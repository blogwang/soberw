package day_12_31.localdate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author soberw
 * @Classname LocalDateTimeTest
 * @Description
 * @Date 2021-12-31 14:38
 */
public class LocalDateTimeTest {
    public static void main(String[] args) {
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt.toString());

        LocalDate localDate = LocalDate.parse("2021-11-30");
        System.out.println(localDate);
        //格式化日期
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        System.out.println(localDate.format(formatter));

    }
}
