package day_12_31.localdate;

import java.time.LocalDate;

/**
 * @author soberw
 * @Classname LocalDateTest
 * @Description
 * @Date 2021-12-31 11:34
 */
public class LocalDateTest {
    public static void main(String[] args) {
        //获取当前系统时间
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.toString());
//        System.out.println(localDate.getYear());
//        System.out.println(localDate.getMonthValue());
//        System.out.println(localDate.getDayOfMonth());
//        System.out.println(localDate.getDayOfYear());

        //获取两天前的时间
        LocalDate d = localDate.plusDays(-2);
        System.out.println(d.toString());
        //购买会员到期日，一个月
        d = localDate.plusMonths(1);
        System.out.println(d.toString());
        //同比日期
//        d = localDate.plusYears(-1);
        d = localDate.minusYears(1);
        System.out.println("同比日期:" + d.toString());
        //环比
        d = localDate.plusMonths(-1);
        System.out.println("环比日期:" + d.toString());
    }
}
