package day_12_31.localdate;

import java.time.Clock;
import java.time.Instant;

/**
 * @author soberw
 * @Classname ClockTest
 * @Description
 * @Date 2021-12-31 15:35
 */
public class ClockTest {
    public static void main(String[] args) {
        Clock clock = Clock.systemDefaultZone();
        System.out.println(clock.millis());
        System.out.println(System.currentTimeMillis());
        Instant instant = clock.instant();
        System.out.println(instant.toEpochMilli());
    }
}
