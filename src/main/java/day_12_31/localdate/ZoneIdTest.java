package day_12_31.localdate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.Set;

/**
 * @author soberw
 * @Classname ZoneIdTest
 * @Description
 * @Date 2021-12-31 15:16
 */
public class ZoneIdTest {
    public static void main(String[] args) {
        Set<String> set = ZoneId.getAvailableZoneIds();
        Iterator<String> it = set.iterator();
//        while(it.hasNext()){
//            System.out.println(it.next());
//        }

        System.out.println(ZoneId.systemDefault().getId());
        LocalDateTime l = LocalDateTime.now(ZoneId.systemDefault());
        System.out.println(l);
        l = LocalDateTime.now(ZoneId.of("Europe/London"));
        System.out.println(l);

    }
}
