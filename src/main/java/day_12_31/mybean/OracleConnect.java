package day_12_31.mybean;

/**
 * @author soberw
 * @Classname OracleConnect
 * @Description
 * @Date 2021-12-31 8:58
 */
@MyAnnotation
public class OracleConnect implements Connect{
    @Override
    public void safe() {
        System.out.println("数据保存在oracle中...");
    }
}
