package day_12_31.mybean;

/**
 * @author soberw
 * @Classname Connect
 * @Description
 * @Date 2021-12-31 8:55
 */
public interface Connect {
    /**
     * 保存数据库文件
     */
    void safe();
}
