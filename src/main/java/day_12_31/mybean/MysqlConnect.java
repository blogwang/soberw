package day_12_31.mybean;

/**
 * @author soberw
 * @Classname MysqlConnect
 * @Description
 * @Date 2021-12-31 8:57
 */

public class MysqlConnect implements Connect{
    @Override
    public void safe() {
        System.out.println("数据保存在mysql中...");
    }
}
