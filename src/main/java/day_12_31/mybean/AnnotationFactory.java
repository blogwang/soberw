package day_12_31.mybean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author soberw
 * @Classname AnnotationFactory
 * @Description
 * @Date 2021-12-31 10:00
 */
public class AnnotationFactory {
    private static String packageName = "day_12_31.mybean";
    private static List<String> list = new ArrayList<>();

    static {
        StringBuilder path = new StringBuilder(AnnotationFactory.class.getClassLoader().getResource("").getPath());
        String[] split = packageName.split("[.]");
        for (String s : split) {
            path.append(s).append("/");
        }
        File file = new File(path.toString());
        File[] files = file.listFiles();
        for (File f : files) {
            String s = f.getName().substring(0, f.getName().lastIndexOf("."));
            String className = packageName + "." + s;
            list.add(className);
        }
    }

    public static Object getBean() {
        for (String s : list) {
            try {
                if (Class.forName(s).isAnnotationPresent(MyAnnotation.class)) {
                    return Class.forName(s).getDeclaredConstructor().newInstance();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
