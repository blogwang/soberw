package day_12_31.mybean;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/**
 * @author soberw
 * @Classname MyBeanFactory
 * @Description
 * @Date 2021-12-31 9:00
 */
public class MyBeanFactory {
    private static Properties props;
    private static String className;
    static{
        String path = MyBeanFactory.class.getClassLoader().getResource("data/bean.properties").getPath();
        props = new Properties();
        try {
            props.load(new FileInputStream(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        className = props.getProperty("user");
    }

    public static Object getBean(){
        Class clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            return clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}
