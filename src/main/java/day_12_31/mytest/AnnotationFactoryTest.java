package day_12_31.mytest;

import day_12_31.mybean.AnnotationFactory;
import day_12_31.mybean.Connect;

/**
 * @author soberw
 * @Classname AnnnotationFactoryTest
 * @Description
 * @Date 2021-12-31 9:59
 */
public class AnnotationFactoryTest {
    public static void main(String[] args) {
        Object bean = AnnotationFactory.getBean();
        if (bean != null) {
            Connect c = (Connect) bean;
            c.safe();
        }else {
            System.out.println("数据保存失败...");
        }
    }
}
