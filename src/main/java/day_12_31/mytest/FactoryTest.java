package day_12_31.mytest;

import day_12_31.mybean.Connect;
import day_12_31.mybean.MyBeanFactory;

import java.sql.Connection;

/**
 * @author soberw
 * @Classname FactoryTest
 * @Description
 * @Date 2021-12-31 9:03
 */
public class FactoryTest {
    public static void main(String[] args) {
        Object bean = MyBeanFactory.getBean();
        Connect c = (Connect) bean;
        c.safe();
    }
}
