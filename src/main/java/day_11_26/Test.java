package day_11_26;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author soberw
 */
public class Test {
    public static void main(String[] args) {
        Person p1 = new Man();
        p1.lookLike();
        Person p2 = new Woman();
        p2.lookLike();
        var p3 = new Woman();
        p3.lookLike();
    }
}
@AllArgsConstructor
@NoArgsConstructor
@Data
abstract class Person {
    private String name;
    private byte age;

    public abstract void lookLike();

}

class Man extends Person {
    @Override
    public void lookLike() {
        System.out.println("男人帅气！");
    }
}

class Woman extends Person {
    @Override
    public void lookLike() {
        System.out.println("女人漂亮！");
    }
}
