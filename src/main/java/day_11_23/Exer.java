package day_11_23;

/**
 * @author soberw
 */
public class Exer {
    public static void main(String[] args) {
        String name = "张三";
        char sex = '男';
        int age = 18;
        System.out.println("姓名：" + name + "，年龄：" + age + "，性别：" + sex + "。");
//        姓名：张三，年龄：18，性别：男。

        System.out.printf("姓名：%s，年龄：%d，性别：%c。", name, age, sex);
//        姓名：张三，年龄：18，性别：男。


    }
}
