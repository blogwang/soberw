package day_11_23;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author soberw
 */
public class RandomDate2 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //指定开始日期
        long start = sdf.parse("2021-1-1 00:00:00").getTime();
        //指定结束日期
        long end = sdf.parse("2021-11-11 00:00:00").getTime();

        //调用方法产生随机数
        long randomDate = nextLong(start, end);

        //格式化输出日期
        System.out.printf("随机日期为:%1$tF %1$tT", randomDate);

    }

    public static long nextLong(long start, long end) {
        Random random = new Random();
        return start + (long) (random.nextDouble() * (end - start + 1));
    }
}
