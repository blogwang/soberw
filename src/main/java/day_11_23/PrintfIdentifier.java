package day_11_23;

/**
 * @author soberw
 */
public class PrintfIdentifier {
    public static void main(String[] args) {
        //+  为正数或者负数添加符号
        System.out.printf("%+d,%+d\n",15,-3);
        //+15,-3

        //-   左对齐，后跟数字控制长度
        System.out.printf("|%-5d|\n",123);
        //|123  |

        //0   数字前面补零，后跟数字控制长度
        System.out.printf("%02d\n",3);
        //03
        System.out.printf("%06.2f\n",1.2555);
        //001.26

        //空格  在整数之前添加指定数量的空格，后跟数字控制长度(空格也可省略)
        System.out.printf("|% 5d|\n",99);
        //|   99|
        System.out.printf("|%5d|\n",99);
        //|   99|

        //,  对数字分组，一般千位分组
        System.out.printf("%,d\n",1000000);
        //1,000,000
        System.out.printf("%,.2f\n",1000000.00000);
        //1,000,000.00

        //(   用括号括住负数
        System.out.printf("%(.2f\n",-33.333);
        //(33.33)

        //#   如果是浮点数则包括小数点，如果是16进制或者8进制则添加0x或者0
        System.out.printf("%#x\n",99);
        System.out.printf("%#o\n",99);
        System.out.printf("%#.2f\n",.233);

        //<   格式化前一个转换符所描述的参数，可以连写，可实现共用参数
        System.out.printf("%f和%<3.2f和%<3.3f\n",99.45);
        //99.450000和99.45和99.450

        //$   通过索引号获取参数，索引号从1开始，可实现共用参数(第一个可省略不写)
        System.out.printf("%1$d,%1$d,%1$d\n",11);
        //11,11,11
        System.out.printf("%1$d,%2$d,%1$d\n",11,222);
        //11,222,11
    }
}
