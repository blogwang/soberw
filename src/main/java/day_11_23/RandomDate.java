package day_11_23;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * 生成指定时间段内的随机日期：如1999-11-30 00:00:00到现在
 *
 * @author soberw
 */
public class RandomDate {
    public static void main(String[] args) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();


        //1.
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR, 1999);
//        c.set(Calendar.MONTH, 11 - 1);
//        c.set(Calendar.DATE, 30);
//        c.set(Calendar.HOUR_OF_DAY, 0);
//        c.set(Calendar.MINUTE, 0);
//        c.set(Calendar.SECOND, 0);
//        c.set(1999,11-1,30,0,0,0);
//        long before = c.getTimeInMillis();

        //2.
//        Date date = new Date(1999-1900,11-1,30,00,00,00);

        //3
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse("1999-11-30 00:00:00");
//        System.out.println(sdf.format(date));

        long before = date.getTime();

        Random random = new Random();

        //产生long类型指定范围随机数
        long randomDate = before + (long) (random.nextFloat() * (now - before + 1));

        System.out.printf("从1999-11-30 00:00:00到现在的一个随机日期为:%1$tF %1$tT", randomDate);

    }
}
