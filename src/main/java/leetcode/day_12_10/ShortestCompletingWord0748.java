package leetcode.day_12_10;

import org.junit.Test;

/**
 * 给你一个字符串 licensePlate 和一个字符串数组 words ，请你找出并返回 words 中的 最短补全词 。
 * <p>
 * 补全词 是一个包含 licensePlate 中所有的字母的单词。在所有补全词中，最短的那个就是 最短补全词 。
 * <p>
 * 在匹配 licensePlate 中的字母时：
 * <p>
 * 忽略licensePlate 中的 数字和空格 。
 * 不区分大小写。
 * 如果某个字母在 licensePlate 中出现不止一次，那么该字母在补全词中的出现次数应当一致或者更多。
 * 例如：licensePlate = "aBc 12c"，那么它的补全词应当包含字母 'a'、'b' （忽略大写）和两个 'c' 。可能的 补全词 有 "abccdef"、"caaacab" 以及 "cbca" 。
 * <p>
 * 请你找出并返回 words 中的 最短补全词 。题目数据保证一定存在一个最短补全词。当有多个单词都符合最短补全词的匹配条件时取 words 中 最靠前的 那个。
 * <p>
 * 示例 1：
 * <p>
 * 输入：licensePlate = "1s3 PSt", words = ["step", "steps", "stripe", "stepple"]
 * 输出："steps"
 * 解释：最短补全词应该包括 "s"、"p"、"s"（忽略大小写） 以及 "t"。
 * "step" 包含 "t"、"p"，但只包含一个 "s"，所以它不符合条件。
 * "steps" 包含 "t"、"p" 和两个 "s"。
 * "stripe" 缺一个 "s"。
 * "stepple" 缺一个 "s"。
 * 因此，"steps" 是唯一一个包含所有字母的单词，也是本例的答案。
 * 示例 2：
 * <p>
 * 输入：licensePlate = "1s3 456", words = ["looks", "pest", "stew", "show"]
 * 输出："pest"
 * 解释：licensePlate 只包含字母 "s" 。所有的单词都包含字母 "s" ，其中 "pest"、"stew"、和 "show" 三者最短。答案是 "pest" ，因为它是三个单词中在 words 里最靠前的那个。
 * 示例 3：
 * <p>
 * 输入：licensePlate = "Ah71752", words = ["suggest","letter","of","husband","easy","education","drug","prevent","writer","old"]
 * 输出："husband"
 * 示例 4：
 * <p>
 * 输入：licensePlate = "OgEu755", words = ["enough","these","play","wide","wonder","box","arrive","money","tax","thus"]
 * 输出："enough"
 * 示例 5：
 * <p>
 * 输入：licensePlate = "iMSlpe4", words = ["claim","consumer","student","camera","public","never","wonder","simple","thought","use"]
 * 输出："simple"
 *
 * @author soberw
 * @Classname ShortestCompletingWord748
 * @Description
 * @Date 2021-12-10 11:47
 */
public class ShortestCompletingWord0748 {
    //思路，通过正则匹配筛出所有的字母，遍历字母表（从前向后，保证最后结果在最前），找到符合条件的且最短的，输出(不成立)
    //创建一个26单位长度的数组，记录出现次数

    /**
     * @param licensePlate 待匹配单词
     * @param words        单词表
     * @description: 匹配最短补全词
     * @return: 最短补全词
     * @author: soberw
     * @time: 2021/12/10 11:51
     */
    public String shortestCompletingWord(String licensePlate, String[] words) {
        int[] count = new int[26];
        for (int i = 0; i < licensePlate.length(); i++) {
            if (Character.isLetter(licensePlate.charAt(i))) {
                count[Character.toLowerCase(licensePlate.charAt(i)) - 'a']++;
            }
        }

        String str = null;
        for (String word : words) {
            if (infor(word, count) && (str == null || str.length() > word.length())) {
                str = word;
            }
        }
        return str;
    }

    private boolean infor(String word, int[] lcnt) {
        int[] count = new int[26];
        for (int i = 0; i < word.length(); i++) {
            count[word.charAt(i) - 'a']++;
        }

        for (int i = 0; i < 26; i++) {
            if (lcnt[i] > count[i]) {
                return false;
            }
        }

        return true;
    }

    @Test
    public void test() {
        ShortestCompletingWord0748 s = new ShortestCompletingWord0748();
        //husband
        System.out.println(s.shortestCompletingWord("1s3 456", new String[]{"looks", "pest", "stew", "show"}));
        //"1s3 456"
        //["looks","pest","stew","show"]
    }
}
