package leetcode.day_12_10;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个整数数组nums 和一个正整数 k，找出是否有可能把这个数组分成 k 个非空子集，其总和都相等。
 * <p>
 * 示例 1：
 * <p>
 * 输入： nums = [4, 3, 2, 3, 5, 2, 1], k = 4
 * 输出： True
 * 说明： 有可能将其分成 4 个子集（5），（1,4），（2,3），（2,3）等于总和。
 *
 * @author soberw
 * @Classname CanPartitionKSubsets0698
 * @Description
 * @Date 2021-12-10 18:20
 */
public class CanPartitionKSubsets0698 {

    /**
     * @param nums 待划分的数组
     * @param k    几等分
     * @description: 给定一个整数数组nums 和一个正整数 k，找出是否有可能把这个数组分成 k 个非空子集，其总和都相等。
     * @return: boolean
     * @author: soberw
     * @time: 2021/12/10 18:21
     */

    public boolean canPartitionKSubsets(int[] nums, int k) {
        if (k == 1) {
            return true;
        }
        int sum = 0;
        int min = Integer.MIN_VALUE;
        List<Integer> list = new ArrayList<Integer>();
        List<List<Integer>> last = new ArrayList<>();
        for (int num : nums) {
            list.add(num);
            sum += num;
        }
        if (sum % k != 0) {
            return false;
        }
        sum = sum / k;
        //降序
        list.sort((a, b) -> b - a);
        if (list.get(0) > sum) {
            return false;
        }
        System.out.println(list);
        for (int i = 0; i < k; i++) {
            List<Integer> l = new ArrayList<>();
            if (list.get(i) == sum) {
                l.add(list.get(i));
                list.set(i, min);
                last.add(l);
                continue;
            }
            if (list.get(i) > min) {
                int is = list.get(i);
                for (int j = list.size() - 1; j >= i + 1; j--) {
                    if (list.get(j) > min) {
                        is = is + list.get(j);
                        System.out.println(is);
                    } else {
                        continue;
                    }
                    if (is < sum) {
                        continue;
                    }
                    if (is == sum) {
                        l.add(list.get(i));
                        l.add(list.get(j));
                        list.set(i, min);
                        list.set(j, min);
                        last.add(l);
                    }
//                    if (is > sum) {
//                        return false;
//                    }

                }
            }
            System.out.println(last + "-------------");
        }
        System.out.println(last);
        System.out.println(last.size());
        return last.size() == k;
    }

    @Test
    public void test() {
        //[1,1,1,1,2,2,2,2]
        //2
        int[] nums = {1, 1, 1, 1, 2, 2, 2, 2};
        int k = 2;
        CanPartitionKSubsets0698 c = new CanPartitionKSubsets0698();
        System.out.println(c.canPartitionKSubsets(nums, k));
        List<List<Integer>> last = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>(List.of(1, 2, 3, 4));
        List<Integer> list2 = new ArrayList<>(List.of(8, 9));
        last.add(list1);
        last.add(list2);
        System.out.println(last);
        System.out.println(last.size());
    }

}
