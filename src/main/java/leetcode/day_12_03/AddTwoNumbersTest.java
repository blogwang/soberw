package leetcode.day_12_03;

public class AddTwoNumbersTest {
    public static void main(String[] args) {
        AddTwoNumbers0002 a = new AddTwoNumbers0002();
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next = new ListNode(3);
        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next = new ListNode(4);

        ListNode l3 = a.addTwoNumbers(l1, l2);
        while (l3 != null) {
            System.out.println(l3.val);
            if (l3 != null) {
                l3 = l3.next;
            }
        }
    }
}
