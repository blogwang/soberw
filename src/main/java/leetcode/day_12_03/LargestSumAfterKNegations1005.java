package leetcode.day_12_03;

/**
 * 给你一个整数数组 nums 和一个整数 k ，按以下方法修改该数组：
 * <p>
 * 选择某个下标 i 并将 nums[i] 替换为 -nums[i] 。
 * 重复这个过程恰好 k 次。可以多次选择同一个下标 i 。
 * <p>
 * 以这种方式修改数组后，返回数组 可能的最大和 。
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [4,2,3], k = 1
 * 输出：5
 * 解释：选择下标 1 ，nums 变为 [4,-2,3] 。
 * 示例 2：
 * <p>
 * 输入：nums = [3,-1,0,2], k = 3
 * 输出：6
 * 解释：选择下标 (1, 2, 2) ，nums 变为 [3,1,0,2] 。
 * 示例 3：
 * <p>
 * 输入：nums = [2,-3,-1,5,-4], k = 2
 * 输出：13
 * 解释：选择下标 (1, 4) ，nums 变为 [2,3,-1,5,4] 。
 *
 * @author soberw
 */
public class LargestSumAfterKNegations1005 {
    /**
     * 求数组的最小值下标
     *
     * @param nums 数组
     * @return 最小值的下标
     */
    public static int minIndexOf(int[] nums) {
        int min = nums[0];
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (min > nums[i]) {
                min = nums[i];
                index = i;
            }
        }
        return index;
    }

    public static void main(String[] args) {
        int[] nums = {2, -3, -1, 5, -4};
        int k = 2;
        int sum = 0;
        for (int i = 0; i < k; i++) {
            nums[minIndexOf(nums)] = -nums[minIndexOf(nums)];
        }
        for (int num : nums) {
            sum += num;
        }
        System.out.println(sum);
    }

}
