package leetcode.day_12_07;

/**
 * 给你一个大小为 m x n 的整数矩阵 grid ，表示一个网格。另给你三个整数row、col 和 color 。网格中的每个值表示该位置处的网格块的颜色。
 * <p>
 * 当两个网格块的颜色相同，而且在四个方向中任意一个方向上相邻时，它们属于同一 连通分量 。
 * <p>
 * 连通分量的边界 是指连通分量中的所有与不在分量中的网格块相邻（四个方向上）的所有网格块，或者在网格的边界上（第一行/列或最后一行/列）的所有网格块。
 * <p>
 * 请你使用指定颜色color 为所有包含网格块grid[row][col] 的 连通分量的边界 进行着色，并返回最终的网格grid 。
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：grid = [[1,1],[1,2]], row = 0, col = 0, color = 3
 * 输出：[[3,3],[3,2]]
 * 示例 2：
 * <p>
 * 输入：grid = [[1,2,2],[2,3,2]], row = 0, col = 1, color = 3
 * 输出：[[1,3,3],[2,3,3]]
 * 示例 3：
 * <p>
 * 输入：grid = [[1,1,1],[1,1,1],[1,1,1]], row = 1, col = 1, color = 2
 * 输出：[[2,2,2],[2,1,2],[2,2,2]]
 *
 * @author soberw
 * @Classname ColorBorder1034
 * @Description
 * @Date 2021-12-07 10:59
 */
public class ColorBorder1034 {
    /**
     * @param grid  需要染色的网格
     * @param row   行号
     * @param col   列号
     * @param color 着色
     * @description: 给网格染色
     * @return: 染色后的网格
     * @author: soberw
     * @time: 2021/12/7 11:02
     */
    public int[][] colorBorder(int[][] grid, int row, int col, int color) {
        //思路，先将内部着色，再把边界着色
        //着色原理，给定坐标的四个方向上如果有与坐标颜色不同的，就把不同的那个的四个方向着色
        //所有边界都要着色

        //定义最大边界
        int width = grid[0].length;
        int high = grid.length;
//        while(true){
//
//        }
        return grid;
    }
}
