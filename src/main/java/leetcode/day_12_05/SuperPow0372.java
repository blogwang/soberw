package leetcode.day_12_05;

/**
 * 你的任务是计算a的b次方对1337 取模，a 是一个正整数，b 是一个非常大的正整数且会以数组形式给出。
 * <p>
 * 示例 1：
 * <p>
 * 输入：a = 2, b = [3]
 * 输出：8
 * 示例 2：
 * <p>
 * 输入：a = 2, b = [1,0]
 * 输出：1024
 * 示例 3：
 * <p>
 * 输入：a = 1, b = [4,3,3,8,5,2]
 * 输出：1
 * 示例 4：
 * <p>
 * 输入：a = 2147483647, b = [2,0,0]
 * 输出：1198
 *
 * @Classname SuperPow0372
 * @Description
 * @Date 2021-12-05 14:48
 */
public class SuperPow0372 {
    /**
     * @param a 基数
     * @param b 幂，以数组形式存放
     * @description: 计算 ab 对 1337 取模，a 是一个正整数，b 是一个非常大的正整数且会以数组形式给出。
     * @return: [int] 对 1337 取模后的数
     * @author: soberw
     * @time: 2021/12/5 14:50
     */
    public int superPow(int a, int[] b) {

        return 0;
    }

    public static void main(String[] args) {
        System.out.println(Long.MAX_VALUE);
//        System.out.println(Math.);
        System.out.println(3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 / 5);
        System.out.println(1024 / 3);
        System.out.println((10 * 10 * 10 * 10 * 10) / 7);
        System.out.println((7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 * 7 * 7) / 9);


    }

}
