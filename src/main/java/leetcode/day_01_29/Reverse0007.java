package leetcode.day_01_29;

/**
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 * 如果反转后整数超过 32 位的有符号整数的范围[−231, 231− 1] ，就返回 0。
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 * <p>
 * 示例 1：
 * 输入：x = 123
 * 输出：321
 * <p>
 * 示例 2：
 * 输入：x = -123
 * 输出：-321
 * <p>
 * 示例 3：
 * 输入：x = 120
 * 输出：21
 * <p>
 * 示例 4：
 * 输入：x = 0
 * 输出：0
 *
 * @author soberw
 * @Classname Reverse0007
 * @Description
 * @Date 2022-01-29 22:14
 */
public class Reverse0007 {
    public int reverse(int x) {
        if (x == 0) {
            return 0;
        }
        //决定符号位
        boolean flag = false;
        //转换为字符串方便反转
        StringBuilder s = new StringBuilder(String.valueOf(x));
        if (x < 0) {
            flag = true;
            s.delete(0, 1);
        }
        s.reverse();
        int finalX = 0;
        //超出范围捕获异常
        try {
            finalX = Integer.parseInt(s.toString());
        } catch (Exception e) {
            return 0;
        }
        if (flag) {
            finalX = -finalX;
        }
        return finalX;
    }

    public static void main(String[] args) {
        Reverse0007 r = new Reverse0007();
        System.out.println(r.reverse(-123));
    }
}
