package leetcode.day_12_04;


/**
 * 为了不在赎金信中暴露字迹，从杂志上搜索各个需要的字母，组成单词来表达意思。
 * <p>
 * 给你一个赎金信 (ransomNote) 字符串和一个杂志(magazine)字符串，判断 ransomNote 能不能由 magazines 里面的字符构成。
 * <p>
 * 如果可以构成，返回 true ；否则返回 false 。
 * <p>
 * magazine 中的每个字符只能在 ransomNote 中使用一次。
 * <p>
 * 示例 1：
 * <p>
 * 输入：ransomNote = "a", magazine = "b"
 * 输出：false
 * 示例 2：
 * <p>
 * 输入：ransomNote = "aa", magazine = "ab"
 * 输出：false
 * 示例 3：
 * <p>
 * 输入：ransomNote = "aa", magazine = "aab"
 * 输出：true
 *
 * @author soberw
 */
public class CanConstruct0383 {

    /**
     * @param b 字符串
     * @param a 字符串
     * @description: TODO
     * @return: [java.lang.String, java.lang.String]
     * @author: soberw
     * @time: 2021/12/4 16:12
     */

    public boolean canConstruct(String b, String a) {
        //记录次数
        int count = 0;
        //转化为StringBuilder，方便操作
        StringBuilder aa = new StringBuilder(a);
        //拆分为char数组，方便筛选
        char[] newB = b.toCharArray();
        //记下标
        int index = 0;
        for (char bb : newB) {
            //寻找下标
            index = aa.indexOf(String.valueOf(bb));
            //说明找到了，删除对应的字符，防止出现子包含情况
            if (index >= 0) {
                count++;
                aa.deleteCharAt(index);
            }
        }
        //相等说明全部包含在内
        if (count == b.length()) {
            return true;
        }

        return false;
    }


    public static void main(String[] args) {
        System.out.println(new CanConstruct0383().canConstruct("aa", "ab"));
    }
}
