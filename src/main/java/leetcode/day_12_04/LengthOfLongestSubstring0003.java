package leetcode.day_12_04;

/**
 * 给定一个字符串 s ，请你找出其中不含有重复字符的最长子串的长度。
 * <p>
 * 示例1:
 * <p>
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 * <p>
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 * <p>
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是wke，所以其长度为 3。
 * 请注意，你的答案必须是 子串 的长度，"pwke"是一个子序列，不是子串。
 * 示例 4:
 * <p>
 * 输入: s = ""
 * 输出: 0
 *
 * @author soberw
 * @Classname LengthOfLongestSubstring0003
 * @Description
 * @Date 2021-12-04 15:51
 */


public class LengthOfLongestSubstring0003 {
    /**
     * @param s 将要查找的字符串
     * @description:
     * @return: 最长子串的长度
     * @author: soberw
     * @time: 2021/12/4 16:30
     */

    public int lengthOfLongestSubstring(String s) {
        //转换为StringBuilder方便操作
        StringBuilder ss = new StringBuilder(s);
        //记录每次开始的位置
        int index = 0;
        //保存最大值
        int max = 0;
        while (index < ss.length()) {
            StringBuilder str = new StringBuilder();
            //存入一个元素作为基值
            str.append(ss.charAt(index));
            index++;
            for (int i = index; i < ss.length(); i++) {
                //不存在加入，存在就终止
                if (str.indexOf(String.valueOf(ss.charAt(i))) < 0) {
                    str.append(ss.charAt(i));
                } else {
                    break;
                }
            }
            //保证max始终是最大的
            max = Math.max(max, str.length());
        }
        return max;
    }

    public static void main(String[] args) {
        LengthOfLongestSubstring0003 l = new LengthOfLongestSubstring0003();
        System.out.println(l.lengthOfLongestSubstring("anviaj")); //5
        System.out.println(l.lengthOfLongestSubstring("bbbbb"));  //1
        System.out.println(l.lengthOfLongestSubstring(""));  //0
        System.out.println(l.lengthOfLongestSubstring("abcabcbb")); //3
        System.out.println(l.lengthOfLongestSubstring("pwwkew"));  //3
        System.out.println(l.lengthOfLongestSubstring("abcbcbcbcbcbc"));  //3
        System.out.println(l.lengthOfLongestSubstring("jhawegjhfgahjasdgvfjhagsdfkahjwgerUJWHGFJHEAGSJFHASDGJSAHFDGJFMHSERG"));  //3
    }
}
