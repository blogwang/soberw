package leetcode.day_12_12;

/**
 * 709. 转换成小写字母
 * 给你一个字符串 s ，将该字符串中的大写字母转换成相同的小写字母，返回新的字符串。
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "Hello"
 * 输出："hello"
 * 示例 2：
 * <p>
 * 输入：s = "here"
 * 输出："here"
 * 示例 3：
 * <p>
 * 输入：s = "LOVELY"
 * 输出："lovely"
 *
 * @author soberw
 * @Classname haha
 * @Description
 * @Date 2021-12-12 14:42
 */
public class ToLowerCase0709 {
    /**
     * @param s  将要转换的字符串
     * @description:  将字符串 s 中的大写字母转换成相同的小写字母，返回新的字符串。
     * @return: 新的字符串
     * @author: soberw
     * @time: 2021/12/12 14:44
     */

    public String toLowerCase(String s) {
        return s.toLowerCase();
    }
}
