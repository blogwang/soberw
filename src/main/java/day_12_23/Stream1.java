package day_12_23;

import java.util.stream.IntStream;

/**
 * @author soberw
 * @Classname Stream1
 * @Description
 * @Date 2021-12-23 9:03
 */
public class Stream1 {
    public static void main(String[] args) {
        var s = IntStream.rangeClosed(1, 6);
        s.boxed().forEach(System.out::println);
    }
}
