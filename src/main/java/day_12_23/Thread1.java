package day_12_23;

import org.junit.Test;

/**
 * @author soberw
 * @Classname Thrend
 * @Description
 * @Date 2021-12-23 19:23
 */
public class Thread1 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
        }
    }

}
class ThreadTest{
    public static void main(String[] args) {
        Thread1 t1 = new Thread1();
        //启用的是新的线程
        t1.start();
        System.out.println(t1.getId());
        System.out.println(t1.getName());
        System.out.println(t1.getPriority());
        //用的还是当前线程，不是新线程
        t1.run();

        var t2  = new Thread(()->{

        },"t");
        t2.start();
        System.out.println(t2.getName());

    }
}
