package day_12_13;

import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author soberw
 * @Classname FileTest
 * @Description
 * @Date 2021-12-13 10:30
 */
public class FileTest {
    @Test
    public void mkdirTest() {
        //创建一个抽象路径，用于之后操作,可以是一级目录可以是多级目录
        //此举并不会在内存中产生一个文件，想要产生文件必须调用方法（mkdir）
        //File类的实例是不可变的; 也就是说，一旦创建， File对象表示的抽象路径File永远不会改变。

        //几种方式如下：

        //默认在当前项目根目录
        //File file1 = new File("xxx");
        //File file11 = new File("a/b/c/d/e");

        //在当前项目所在盘符下
        //File file2 = new File("/xxx");

        //在指定位置
        //File file3 = new File("c:/xxx");
        //File file4 = new File("c:\\xxxx");


        //2.创建方法 mkdir()和 mkdirs()  返回一个Boolean值，
        //     创建成功返回true，失败返回false，如果指定目录下已经存在则不会创建成功（false）
        //  两者区别是：mkdir只能创建单级目录，mkdirs可以创建多级目录
        //    如果声明的是多级目录，则调用mkdirs，声明的是单级则调用mkdir

        //System.out.println(file1.mkdir());
        //System.out.println(file11.mkdir());  //false  因为声明的是多级的，应该调用mkdirs
        //System.out.println(file11.mkdirs());
        //System.out.println(file2.mkdir());
        //System.out.println(file3.mkdir());
        //System.out.println(file4.mkdir());
    }

    @Test
    public void createTest() {
        //创建目录
        File file = new File("d:/hello/abc");
        if (!file.exists()) {
            file.mkdirs();
            System.out.println("创建成功！");
        }
        //在刚创建的目录中在创建一个文件createNewFile()
        File file1 = new File("d:/hello/abc/hello.txt");
        try {
            file1.createNewFile();
        } catch (IOException e) {
            System.out.println("创建失败！");
        }

        if (file1.exists()) {
            file1.delete();
            System.out.println("删除成功！");
        }

        //在刚创建的目录中在创建一个指定文件格式的文件(例如   (...).jpg)

        //createTempFile(String prefix, String suffix,【 File directory】)
        //参数
        //prefix     -  用于生成文件名的前缀字符串; 必须至少三个字符长
        //suffix     -  用于生成文件名的后缀字符串; 可以是null ，在这种情况下将使用后缀".tmp"
        //directory  -  要在其中创建文件的目录，如果要使用默认临时文件目录， null    （选填，不填默认为null）
        //结果           表示新创建的空文件的抽象路径名
        try {
            File.createTempFile("123", ".jpg", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void existsTest() {
        //1.exists()测试此抽象路径名表示的文件或目录是否存在。

        File file11 = new File("a/b/c/d/e");
        System.out.println(file11.exists());   //false
        file11.mkdirs();
        System.out.println(file11.exists());    //true

    }

    @Test
    public void isTest() {
        File file11 = new File("a/b/c/d/e");
        System.out.println(file11.exists());   //false
        file11.mkdirs();
        System.out.println(file11.exists());    //true

        //测试此抽象路径名表示的文件是否为普通文件。
        System.out.println(file11.isFile());   //当前为目录，所以为false
        System.out.println(file11.listFiles()[0].isFile());   //1.txt 所以为true


        //测试此抽象路径名表示的文件是否为目录。
        System.out.println(file11.isDirectory());

        //测试此抽象路径名是否为绝对路径。
        System.out.println(file11.isAbsolute());

        //测试此抽象路径名指定的文件是否为隐藏文件。
        System.out.println(file11.isHidden());
    }


    @Test
    public void compareToTest() {
        //如果参数等于此抽象路径名，则为零;如果此抽象路径名按字典顺序小于参数，则为小于零;如果此抽象路径名在字典上大于参数，则值大于零
        File f1 = new File("d:\\hello");
        File f2 = new File("d:\\hello\\a");
        File f3 = new File("d:\\hell");
        File f4 = new File("d:\\hello");
        //System.out.println(f1.compareTo(f2));   //-2
        //System.out.println(f1.compareTo(f3));   //1
        //System.out.println(f1.compareTo(f4));   //0
    }

    @Test
    public void deleteTest() {
        //3.删除文件方法delete()

        //默认在当前项目根目录
        //File file1 = new File("xxx");
        //File file11 = new File("a/b/c/d/e");

        //如果此文件或者目录不存在，则删除失败返回false
        //如果此文件不是空文件（空目录），删除失败false
        //如果此路径是多级目录，则只删除最内层的文件或者目录
        //System.out.println(file1.delete());  //true
        //System.out.println(file11.delete());   //true
    }

    @Test
    public void renameTest() throws IOException {
        File file = new File("ahello/a/abc.txt");
//        System.out.println(file.mkdirs());
        System.out.println(file.createNewFile());
//        file.mkdirs();
        System.out.println(file);

        //如果重命名的文件已存在，则不会重命名
        System.out.println(file.renameTo(new File("a/b/c/abcd.txt")));
        System.out.println(file.getName());

        //注意事项：
        //第一种场景：同一路径下文件重命名【文件重命名】
        //第二种场景：将文件从一个路径移动另一个路径下，并且移动的文件进行重命名【文件移动重命名】
        //第三种场景：修改文件夹的名称，文件夹里面没有文件的时候修改成功！
    }

    @Test
    public void lengthTest() {
        File file = new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_13\\zuoye\\AddFile.java");
        if (file.exists()) {
            //获取文件字节数，如果是文件夹则为 0
            System.out.println(file.length());
        }
    }

    @Test
    public void toStringTest() {
        //4.toString() 将所指路径转换为字符串
        //File file11 = new File("a/b/c/d/e");
        //System.out.println(file11.toString());   //"a\b\c\d\e"
    }

    @Test
    public void getTest() throws IOException {
        /*
        返回值类型    方法名                        描述

        File        getAbsoluteFile()       返回此抽象路径名的绝对形式。
        String      getAbsolutePath()       返回此抽象路径名的绝对路径名字符串。
        File        getCanonicalFile()      返回此抽象路径名的规范形式。
        String      getCanonicalPath()      返回此抽象路径名的规范路径名字符串。
        String      getName()               返回此抽象路径名表示的文件或目录的名称。
        String      getPath()               将此抽象路径名转换为路径名字符串。
        String      getParent()             返回此抽象路径名父项的路径名字符串，如果此路径名未指定父目录，则返回 null 。
        File        getParentFile()         返回此抽象路径名父项的抽象路径名，如果此路径名未指定父目录，则返回 null 。
        long        getFreeSpace()          通过此抽象路径名返回分区 named中未分配的字节数。
        long        getUsableSpace()        通过此抽象路径名返回分区 named上此虚拟机可用的字节数。
        long        getTotalSpace()         通过此抽象路径名返回分区 named的大小。
         */

        File file = new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_13");
        System.out.println(file.getAbsoluteFile());     //E:\HelloJava\Java_Study_Beiyou\src\day_12_13
        System.out.println(file.getAbsolutePath());     //E:\HelloJava\Java_Study_Beiyou\src\day_12_13
        System.out.println(file.getCanonicalFile());    //E:\HelloJava\Java_Study_Beiyou\src\day_12_13
        System.out.println(file.getCanonicalPath());    //E:\HelloJava\Java_Study_Beiyou\src\day_12_13
        System.out.println(file.getName());             //day_12_13
        System.out.println(file.getPath());             //E:\HelloJava\Java_Study_Beiyou\src\day_12_13
        System.out.println(file.getParent());           //E:\HelloJava\Java_Study_Beiyou\src
        System.out.println(file.getParentFile());       //E:\HelloJava\Java_Study_Beiyou\src
        System.out.println(file.getFreeSpace());        //339921727488
        System.out.println(file.getTotalSpace());       //786633322496
        System.out.println(file.getUsableSpace());      //339921727488
    }

    @Test
    public void lastModifiedAndSetTest() {
        //记录文件的最后修改时间,是一个long时间戳
        File file = new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_13");
        long time = file.lastModified();
        System.out.printf("%tF %<tT", time);   //2021-12-14 08:44:38

        System.out.println();
        //boolean  setLastModified(long time)  设置此抽象路径名指定的文件或目录的上次修改时间。
        long now = System.currentTimeMillis();
        file.setLastModified(now);
        System.out.printf("%tF %<tT", now);   //2021-12-14 18:15:32
    }

    @Test
    public void listTest() {

        /*
        String[]        list()                              返回一个字符串数组，用于命名此抽象路径名表示的目录中的文件和目录。
        String[]        list(FilenameFilter filter)         返回一个字符串数组，用于命名由此抽象路径名表示的目录中的文件和目录，以满足指定的过滤器。
        File[]          listFiles()                         返回一个抽象路径名数组，表示此抽象路径名表示的目录中的文件。
        File[]          listFiles(FileFilter filter)        返回一个抽象路径名数组，表示此抽象路径名表示的目录中满足指定过滤器的文件和目录。
        File[]          listFiles(FilenameFilter filter)    返回一个抽象路径名数组，表示此抽象路径名表示的目录中满足指定过滤器的文件和目录。
        static File[]   listRoots()                         列出可用的文件系统根目录。
         */
        File file = new File("E:\\谷歌");

        //  list()  返回的是字符串数组
        String[] listStr = file.list();
        System.out.println(Arrays.toString(listStr));

        // listFiles() 返回的是 File[]  可以对文件直接操作
        File[] listF = file.listFiles();
        for (File file1 : listF) {
            System.out.print(file1.getName() + ", ");
        }

        // listRoots() 列出可用的文件系统根目录。
        System.out.println();
        File[] listRoots = File.listRoots();
        for (File root : listRoots) {
            System.out.println(root.getPath());
        }

        //对文件筛选操作
        //两种方式
        // FilenameFilter filter  --- boolean accept(File dir, String name); dir为文件根目录（不包含此文件）  name为文件名
        // FileFilter filter -------- boolean accept(File pathname);   pathname为文件路径（包含此文件）

        //list(FilenameFilter filter)    FilenameFilter支持lambda，两个参数（）
        String[] listStrs = file.list((path, name) -> name.endsWith("zip"));  //列出所有以zip结尾的文件
        System.out.println(Arrays.toString(listStrs));

        //listFiles(FilenameFilter filter)
        FilenameFilter filter;
        File[] listFiles = file.listFiles((path, name) -> name.matches(".*[\\d+].*"));  //获取所有文件名中带有数字的
        for (File file1 : listFiles) {
            System.out.print(file1.getName() + ", ");
        }

        //listFiles(FileFilter filter)
        File[] lfs = file.listFiles(e -> e.getName().matches(".*[\\u4e00-\\u9fa5]+.*"));   //获得包含中文的文件
        for (File f : lfs) {
            System.out.print(f.getName() + ", ");
        }

    }

    @Test
    public void canTest() {
        /*
        boolean     canExecute()    测试应用程序是否可以执行此抽象路径名表示的文件。
        boolean     canRead()       测试应用程序是否可以读取此抽象路径名表示的文件。
        boolean     canWrite()      测试应用程序是否可以修改此抽象路径名表示的文件。
         */
        File file = new File("aaa\\bbb\\ccc\\test.txt");
        System.out.println(file.canExecute());  //true
        System.out.println(file.canRead());     //true
        System.out.println(file.canWrite());    //true
    }

    @Test
    public void setTest() {
        /*
        boolean     setExecutable(boolean executable)                    一种方便的方法，用于设置此抽象路径名的所有者执行权限。
        boolean     setExecutable(boolean executable, boolean ownerOnly) 设置此抽象路径名的所有者或每个人的执行权限。
        boolean     setReadable(boolean readable)                       一种方便的方法，用于设置此抽象路径名的所有者读取权限。
        boolean     setReadable(boolean readable, boolean ownerOnly)    设置此抽象路径名的所有者或每个人的读取权限。
        boolean     setReadOnly()                                       标记此抽象路径名指定的文件或目录，以便仅允许读取操作。
        boolean     setWritable(boolean writable)                       一种方便的方法，用于设置此抽象路径名的所有者写入权限。
        boolean     setWritable(boolean writable, boolean ownerOnly)    设置此抽象路径名的所有者或每个人的写入权限。
         */
        File file = new File("aaa\\bbb\\ccc\\test.txt");

        //System.out.println(file.setExecutable(false, false));  //false
        //System.out.println(file.canExecute());  //true
        //System.out.println(file.canRead());     //true
        //System.out.println(file.canWrite());    //true

        //System.out.println(file.setReadable(false,false));
        //System.out.println(file.canExecute());  //true
        //System.out.println(file.canRead());     //true
        //System.out.println(file.canWrite());    //true

        //System.out.println(file.setWritable(false,false));
        //System.out.println(file.canExecute());  //true
        //System.out.println(file.canRead());     //true
        //System.out.println(file.canWrite());    //false

        //System.out.println(file.setReadOnly());
        //System.out.println(file.canExecute());  //true
        //System.out.println(file.canRead());     //true
        //System.out.println(file.canWrite());    //false
    }
}
