package work.two;

/**
 * @author soberw
 */

public class PubTest1 {
    public static void main(String[] args) {
        Pub p = new Pub(1.1f,2);
        System.out.println(p.sum_f_I());
    }
}

class Pub {
    public float fvar1;
    public float fvar2;
    public int ivar1;

    public Pub(float fvar1, int ivar1) {
        this.fvar1 = fvar1;
        this.ivar1 = ivar1;
    }

    public float sum_f_I() {
        fvar2 = fvar1 + ivar1;
        return fvar2;
    }

}

