package day_12_27;

import java.util.concurrent.TimeUnit;

/**
 * @author soberw
 * @Classname WaitAndNotify
 * @Description  创建五个线程并进入等待状态，等两秒后主线程开始并释放全部线程，最后主线程结束
 * @Date 2021-12-27 15:47
 */
public class WaitAndNotify {
    public static void main(String[] args) {
        Object co = new Object();

        for (int i = 0; i < 5; i++) {
            MyThread t = new MyThread("Thread" + i, co);
            t.start();
        }

        try {
            TimeUnit.SECONDS.sleep(2);
            System.out.println("-----Main Thread notify-----");
            synchronized (co) {
                //co.notify();
                co.notifyAll();
            }

            TimeUnit.SECONDS.sleep(2);
            System.out.println("Main Thread is end.");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class MyThread extends Thread {
        private String name;
        private Object co;

        public MyThread(String name, Object o) {
            this.name = name;
            this.co = o;
        }

        @Override
        public void run() {
            System.out.println(name + " is waiting.");
            try {
                synchronized (co) {
                    co.wait();
                }
                System.out.println(name + " has been notified.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
