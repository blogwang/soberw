package day_12_27.enum_test;

/**
 * @author soberw
 * @Classname Enum1
 * @Description
 * @Date 2021-12-27 18:33
 */
enum Enum1 {
    RED, GREEN, BLUE
}

class A{
    public static final String RED = "RED";
}

class B{
    public static void main(String[] args) {
        Enum1 e = Enum1.RED;
    }
}
