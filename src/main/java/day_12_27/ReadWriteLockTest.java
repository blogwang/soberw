package day_12_27;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author soberw
 * @Classname ReadWriteLockTest
 * @Description ReadWriteLock的测试
 * @Date 2021-12-27 10:10
 */
public class ReadWriteLockTest {
    public static void main(String[] args) {
        var rwl = new RWL();
        //情况一：
        new Thread(() -> rwl.write(100), "A").start();
        //情况二：
        new Thread(() -> rwl.write(99), "AA").start();
        new Thread(() -> rwl.write(88), "AAA").start();

        for (int i = 0; i < 20; i++) {
            new Thread(rwl::read, "B" + i).start();
        }
    }
}

class RWL {
    //可以传参，若为true则为公平锁，默认为false，不写也为false
    private ReadWriteLock rwl = new ReentrantReadWriteLock();
    public int number;


    void write(int number) {
        rwl.writeLock().lock();
        try {
            this.number = number;
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + ":start--" + number);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            rwl.writeLock().unlock();
        }
    }

    void read() {
        rwl.readLock().lock();
        try {
            Thread.sleep(100);
            System.out.println(Thread.currentThread().getName() + ":" + number);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            rwl.readLock().unlock();
        }
    }
}
