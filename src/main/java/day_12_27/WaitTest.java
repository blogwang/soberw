package day_12_27;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * @author soberw
 * @Classname WaitTest
 * @Description
 * @Date 2021-12-27 11:34
 */
public class WaitTest {
    public static void main(String[] args) {
        Wait w = new Wait();
        Thread add1 = new Thread(w::add, "Add1");
        Thread add2 = new Thread(w::add, "Add2");
        Thread add3 = new Thread(w::add, "Add3");
        Thread add4 = new Thread(w::add, "Add4");
        Thread add5 = new Thread(w::add, "Add5");
        Thread count = new Thread(w::count, "count");
        //将此线程的优先级调大，释放的时候就会优先执行此线程
        count.setPriority(Thread.MAX_PRIORITY);
//        add1.setPriority(1);
//        add2.setPriority(1);
//        add3.setPriority(1);
//        add4.setPriority(1);
//        add5.setPriority(1);
        add1.start();
        add2.start();
        add3.start();
        add4.start();
        add5.start();
        count.start();
    }
}

class Wait {
    private List<String> list = new ArrayList<>();
    private final Object obj = new Object();

    void add() {
        String t = Thread.currentThread().getName();
        System.out.println(t + ":线程启动...");
        for (int i = 0; i < 10; i++) {
            synchronized (obj) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String item = "item" + (list.size());
                list.add(item);
                System.out.println(t + ":增加" + item);
                if (list.size() == 6) {
                    try {
                        obj.notify();
                        obj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    void count() {
        String t = Thread.currentThread().getName();
        System.out.println(t + "统计线程已经启动...");
        synchronized (obj) {
            try {
                obj.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t + ":已经6个元素了...");
            System.out.println(t + ":监控线程结束...");
            obj.notify();
        }
    }
}
