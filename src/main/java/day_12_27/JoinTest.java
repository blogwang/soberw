package day_12_27;

/**
 * @author soberw
 * @Classname JoinTest
 * @Description join() 的测试
 * @Date 2021-12-27 15:50
 */

/**
 * 若当前线程调用了其他线程的join方法，则其他线程会加塞到当前线程中，直到加塞的线程运行结束，当前线程才开始执行
 * 相当于串行执行，这改变了线程的并行执行性质
 */
public class JoinTest {
    void work() {
        String tn = Thread.currentThread().getName();
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s = %d\n", tn, i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        var jt = new JoinTest();
        var t1 = new Thread(jt::work, "T1");
        var t2 = new Thread(jt::work, "T2");
        var t3 = new Thread(jt::work, "T3");
        var t4 = new Thread(jt::work, "T4");

        t1.start();
        t2.start();
        t2.join();
        t3.start();
        t4.start();

        String tn = Thread.currentThread().getName();
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s = %d\n", tn, i);
        }
    }
}
