package day_12_27;

/**
 * @author soberw
 * @Classname NotifyTest
 * @Description   对notifyAll()以及notify()的测试
 * @Date 2021-12-27 11:59
 */
public class NotifyTest {
    public static void main(String[] args) {
        N n = new N();
        new Thread(n::work, "t1").start();
        new Thread(n::work, "t2").start();
        new Thread(n::work, "t3").start();
        new Thread(n::work, "t4").start();
        new Thread(n::work, "ok").start();
    }
}

class N {
    private final Object obj = new Object();

    void work() {
        String t = Thread.currentThread().getName();
        System.out.println(t + "线程已启动...");
        if (t.equals("ok")) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (obj) {
                //全部唤醒
                obj.notifyAll();
                //唤醒一个
                //obj.notify();
            }
        } else {
            for (int i = 0; i < 3; i++) {
                System.out.printf("%s ：%d\n", t, i);
                synchronized (obj) {
                    if (i < 1) {
                        try {
                            System.out.println(t + "wait()...");
                            obj.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        System.out.println(t + "线程已结束...");
    }
}
