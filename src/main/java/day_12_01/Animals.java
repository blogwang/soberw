package day_12_01;

/**
 * @author soberw
 */
@FunctionalInterface
public interface Animals {
    void eat();

    static void drunk(){
        System.out.println("所有动物都喝水。。。");
    }

    public final static String kind = null;

    default void breath(){
        System.out.println("所有动物都会呼吸");
    }

}
