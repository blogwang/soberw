package day_12_01.abs;

/**
 * @author soberw
 */
public abstract class A {
    //报错，不能定义抽象属性
//    abstract int a;

    //可以声明属性
   private int a;
    //可以声明一个普通方法
    public void a(){

    }
    //报错，因为抽象方法不能有方法体
//    public abstract void aa(){
//
//    }
    //抽象方法正确格式
    public abstract void aa();

    //抽象方法不能是私有的private
//    private abstract  void bb();
}
//没有重写父类的抽象方法aa()，报错
//class B extends A{
//
//}

class TestA{
    public static void main(String[] args) {
        //报错
//        A a = new A();
    }
}

//一个空的抽象类
abstract class AA{

}

//解决方式一：重写父类抽象方法
class B extends A{
    @Override
    public void aa() {

    }
}
//解决方式二：将B声明为abstract
abstract class Bb extends A{

}