package day_12_01.zuoye;

import org.testng.annotations.Test;

/**
 * @author soberw
 */
public class Max {
    public static void main(String[] args) {
        System.out.println(Double.MIN_VALUE);
        System.out.println(max());
        System.out.println(max(new String[]{"111", "123", "abc"}));
        System.out.println(max(new String[]{"11.1", "12.3", "abc"}, 100, 99.99f));
        System.out.println(max(11, 22, 3, 45));
        System.out.println(max(1.2, 1.3, 1.4));
        System.out.println(max(new String[]{"1", "a", "120.111"}, new int[]{23, 45, 64, 120}, new double[]{1.3, 5.6, 78.9}));
        System.out.println(max("1.3", 2, 1.5, "2.1", "a"));
        System.out.println(max(new String[][]{{"a"}, {"1", "2", "3.3"}, {"1.1", "1.3", "abc"}}));
        System.out.println(max(new int[][]{{1}, {1, 0}, {-1, -3, 2}, {5, 3, 4}}));
        System.out.println(max(new int[][]{{1}, {1, 0}, {-1, -3, 2}}, new String[][]{{"a"}, {"1", "2", "3.3"}, {"1.1", "1.4", "1.3", "abc"}}));
        System.out.println(max(new int[][]{{1}, {1, 0}, {-1, -3, 2}}, new double[][]{{1.1}, {1.0, 2.0, 3.3}, {1.1, 1.3}}));
    }

    @Test
    public void test1() {
//        String[] str = new String[]{"abc", "123"};
//        for (int i = 0; i < str.length; i++) {
//            try {
//                Integer.parseInt(str[i]);
//            } catch (Exception e) {
//                str[i] = "0";
//            }
//
//        }
//        System.out.println(Arrays.toString(str));
//        System.out.println((double) 1);
//        String[] str = {"hello", "abc", "111"};
//        for (String s : str) {
//            s = "aaa";
//        }
//        System.out.println(Arrays.toString(str));
    }

    //string必须是数字字符串，不是数字的判断为“0”
    private static String caseOf(String str) {
        try {
            Double.parseDouble(str);
        } catch (Exception e) {
            str = "0.0";
        }
        return str;
    }

    public static double max(Object... o) {
        double m = Double.MIN_VALUE;
        //空参数
        if (o.length == 0) {
            m = 0;
            //一个参，可以为数组
        } else if (o.length == 1) {
            //可以是int型，double型，string型数组（string数组中必须是数字字符串，不是数字的判断为“0”）
            if (o[0] instanceof Integer || o[0] instanceof String || o[0] instanceof Double) {
                //string型（string必须是数字字符串，不是数字的判断为“0”）
                if (o[0] instanceof String) {
                    for (int i = 0; i < o.length; i++) {
                        o[0] = caseOf(o[0].toString());
                    }
                    //int double 可直接转换
                } else {
                    m = Double.parseDouble(o[0].toString());
                }
                //如果为一维double数组
            } else if (o[0] instanceof double[]) {
                for (double oo : (double[]) o[0]) {
                    if (m < oo) {
                        m = oo;
                    }
                }
                //如果为一维String数组
            } else if (o[0] instanceof String[]) {
                for (String oo : (String[]) o[0]) {
                    oo = caseOf(oo);

                    if (m < Double.parseDouble(oo)) {
                        m = Double.parseDouble(oo);
                    }
                }
                //如果为一维int数组
            } else if (o[0] instanceof int[]) {
                for (int oo : (int[]) o[0]) {
                    if (m < oo) {
                        m = oo;
                    }
                }
                //如果为二维double数组
            } else if (o[0] instanceof double[][]) {
                for (double[] item : (double[][]) o[0]) {
                    m = max(item);
                }
                //如果为二维String数组
            } else if (o[0] instanceof String[][]) {
                for (String[] item : (String[][]) o[0]) {
                    m = max(item);
                }
                //如果为二维int数组
            } else if (o[0] instanceof int[][]) {
                for (int[] item : (int[][]) o[0]) {
                    m = max(item);
                }
            }
        } else {
            for (Object a : o) {
                if (a instanceof double[] || a instanceof String[] || a instanceof int[]) {
                    if (a instanceof double[]) {
                        for (double oo : (double[]) a) {
                            if (m < oo) {
                                m = oo;
                            }
                        }
                        //如果为一维String数组
                    } else if (a instanceof String[]) {
                        for (String oo : (String[]) a) {
                            oo = caseOf(oo);

                            if (m < Double.parseDouble(oo)) {
                                m = Double.parseDouble(oo);
                            }
                        }
                        //如果为一维int数组
                    } else {
                        for (int oo : (int[]) a) {
                            if (m < oo) {
                                m = oo;
                            }
                        }
                    }
                } else if (a instanceof int[][] || a instanceof double[][] || a instanceof String[][]) {
                    if (a instanceof double[][]) {
                        for (double[] i : (double[][]) a) {
                            m = max(i);
                        }
                        //如果为二维String数组
                    } else if (a instanceof String[][]) {
                        for (String[] i : (String[][]) a) {
                            m = max(i);
                        }
                        //如果为二维int数组
                    } else {
                        for (int[] i : (int[][]) a) {
                            m = max(i);
                        }
                    }
                } else {
                    if (a instanceof Double && m < Double.parseDouble(a.toString())) {
                        m = Double.parseDouble(a.toString());
                    } else if (a instanceof Integer && m < Double.parseDouble(a.toString())) {
                        m = Double.parseDouble(a.toString());
                    } else if (a instanceof String) {
                        a = caseOf((String) a);
                        if (m < Double.parseDouble(a.toString())) {
                            m = Double.parseDouble(a.toString());
                        }
                    }
                }
            }
        }
        return m;
    }
}
