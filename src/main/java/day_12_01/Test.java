package day_12_01;

public class Test {
    public static void main(String[] args) {
        Animals cat = new Cat();
        cat.breath();
        cat.eat();
        System.out.println("`````````````````````");
        Animals dog = new Dog();
        dog.breath();
        dog.eat();
        System.out.println("`````````````````````");
        Animals animal = new Animals(){
            @Override
            public void eat() {
                System.out.println("动物会吃东西");
            }
        };
        animal.eat();
        animal.breath();
        System.out.println("`````````````````````");
        Animals animals = () -> System.out.println("动物会吃东西");
        animals.eat();
        animals.breath();
    }
}
