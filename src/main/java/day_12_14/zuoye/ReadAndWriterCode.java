package day_12_14.zuoye;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author soberw
 * @Classname ReadAndWriterCode
 * @Description 将所有java源文件读取内容，一并写入一个code.txt文件
 * @Date 2021-12-14 10:59
 */
public class ReadAndWriterCode {
    /**
     * 创建一个Map用于存放所有的文件名称，以及字节（为避免文件名字重复，需要将文件名字拼接上其绝对路径）
     */
    private static Map<String, String> map = new HashMap<>();

    /**
     * @param file 指定目录路径（绝对路径）
     * @description: 将指定目录中所有java源文件读取内容，一并写入一个code.txt文件，保存在要读取的目录下，并返回文件路径字符串
     * @return: void
     * @author: soberw
     * @time: 2021/12/14 15:24
     */
    public static String show(File file) {
        if (!file.isAbsolute()) {
            System.out.println("请传入一个绝对路径!!!");
        } else {
            //获取所有文件字节并存入map
            countJavaFile(file);
            //放入新的文件
            Set<String> set = map.keySet();
            String splitter = ("-".repeat(100) + "\r\n");
            try (FileOutputStream fos = new FileOutputStream(new File(file, "code.txt"), false)) {
                for (String s : set) {
//                System.out.println(s);
                    //读取出来但是会报错，不稳定，不推荐
                    //int line = Files.readAllLines(Path.of(file.getPath())).size();
                    //稳定读取
                    long line = 0;
                    try {
                        line = new BufferedReader(new FileReader(s)).lines().count();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    fos.write(String.format("%s文件，共%d行\r\n", s, line).getBytes(StandardCharsets.UTF_8));
                    fos.write(splitter.getBytes(StandardCharsets.UTF_8));
                    fos.write(map.get(s).getBytes(StandardCharsets.UTF_8));
                    fos.write(splitter.getBytes(StandardCharsets.UTF_8));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file.getAbsolutePath() + "\\code.txt";
    }

    /**
     * @param file 将要获取的文件
     * @description: 递归获取指定文件的所有字节，将文件名以及此文件的字节放入map
     * @return: void
     * @author: soberw
     * @time: 2021/12/14 15:10
     */

    private static void countJavaFile(File file) {
        if (file.exists()) {
            if (file.isFile() && file.getName().endsWith(".java")) {
                try (FileInputStream fis = new FileInputStream(file.getAbsolutePath())) {
                    map.put(file.getAbsolutePath(), new String(fis.readAllBytes(), StandardCharsets.UTF_8));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (file.isDirectory() && file.list() != null) {
                for (File listFile : Objects.requireNonNull(file.listFiles())) {
                    countJavaFile(listFile);
                }
            }
        }
    }
}
