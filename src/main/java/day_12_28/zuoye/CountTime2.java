package day_12_28.zuoye;

import java.util.concurrent.CountDownLatch;

/**
 * @author soberw
 * @Classname CountTime
 * @Description 开十个线程打印输出1~10000中偶数的值，计算总耗时    用闭锁（门栓）
 * @Date 2021-12-28 15:22
 */
public class CountTime2 {
    public static void main(String[] args) {
        LatchDemo ld = new LatchDemo(new CountDownLatch(10));
        long start = System.currentTimeMillis();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 10; i++) {
            new Thread(ld::run).start();
        }
        try {
            ld.cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println((end - start - 100) + "--------------");
    }
}

class LatchDemo implements Runnable {
     CountDownLatch cdl;

    public LatchDemo(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }
        cdl.countDown();
    }
}
