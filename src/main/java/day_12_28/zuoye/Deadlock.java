package day_12_28.zuoye;

/**
 * @author soberw
 * @Classname Deadlock
 * @Description 编写程序模拟死锁
 * @Date 2021-12-28 10:59
 */
public class Deadlock {
    private final Object o1 = new Object();
    private final Object o2 = new Object();

    public static void main(String[] args) {
        Deadlock d = new Deadlock();
        new Thread(d::m1).start();
        new Thread(d::m2).start();
    }

    void m1(){
        System.out.println(Thread.currentThread().getName() + "启动等待...");
        synchronized(o1){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized(o2){
                System.out.println("哈哈..");
            }
        }
    }

    void m2(){
        System.out.println(Thread.currentThread().getName() + "启动等待...");
        synchronized(o2){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized(o1){
                System.out.println("哈哈..");
            }
        }
    }

}
