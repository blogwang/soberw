package day_12_28.zuoye;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author soberw
 * @Classname AddAndCount
 * @Description 实现一个容器，提供两个方法，add，count 写两个线程，
 * 线程1添加10个元素到容器中，线程2实现监控元素的个数，
 * 当个数到5个时，线程2给出提示并结束。
 * @Date 2021-12-28 10:45
 */
public class AddAndCount {
    CountDownLatch cdl = new CountDownLatch(1);
    List<String> list = new ArrayList<>();

    public static void main(String[] args) {
        var aac = new AddAndCount();
        new Thread(aac::add, "A").start();
        new Thread(aac::count, "B").start();
    }

    void add() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String item = String.format("%s - %d", "item", i);
            list.add(item);
            System.out.println(Thread.currentThread().getName() + ":" + item);
            if (i == 4) {
                cdl.countDown();
            }
        }

    }

    void count() {
        try {
            cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("程序结束...");
        System.exit(0);
    }
}
