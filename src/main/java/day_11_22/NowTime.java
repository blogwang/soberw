package day_11_22;

import java.util.Calendar;

/**
 * @author soberw
 */
public class NowTime {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println(c.getTime());
        System.out.printf("%1$tF  %1$tA  %1$tr",c.getTime());
    }
}
