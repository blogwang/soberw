package day_11_22;

import java.util.Arrays;

/**
 * @author soberw
 */
public class ArrayTest {
    public static void main(String[] args) {
        int[] elements = {6, 5, 4, 3, 2, 1};
        int[] hold = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

        System.arraycopy(elements, 0, hold, 0, elements.length);
        System.out.println(Arrays.toString(hold));
    }

}
