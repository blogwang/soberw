package day_12_20;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author soberw
 * @Classname Logo
 * @Description
 * @Date 2021-12-20 10:48
 */
public class Logo {
    @Test
    public void test(){
        System.out.printf("%tT %<tF",System.currentTimeMillis());
    }
    public static void main(String[] args) throws IOException {
        BufferedImage b = ImageIO.read(new File("C:\\Users\\soberw\\Desktop\\1.jpg"));
        BufferedImage logo = ImageIO.read(new File("E:\\HelloJava\\soberw\\src\\main\\resources\\images\\logo.png"));
        int w = b.getWidth();
        int h = b.getHeight();
        Graphics2D g = b.createGraphics();

        int x = w - logo.getWidth() - 10;
        int y = h - logo.getHeight() - 10;

        //设置透明度
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, .5f));

        g.drawImage(logo, x, y, null);


        ImageIO.write(b,"png",new File("C:\\Users\\soberw\\Desktop\\2.jpg"));
    }
}
