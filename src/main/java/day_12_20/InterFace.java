package day_12_20;

/**
 * @author soberw
 * @Classname Info
 * @Description
 * @Date 2021-12-20 20:26
 */
public class InterFace {
    public static void main(String[] args) {
        A.aa();
        A a = new B();
        a.aaa();
    }
}

interface A{

//    void b();

    static void aa(){

    }
    default void aaa(){

    }
}

class B implements A{

}
