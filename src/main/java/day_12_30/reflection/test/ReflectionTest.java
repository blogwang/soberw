package day_12_30.reflection.test;

import day_12_30.reflection.java.Person;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author soberw
 * @Classname ReflectionTest
 * @Description 调用运行时类中指定的结构：属性、方法、构造器
 * @Date 2021-12-30 15:52
 */
public class ReflectionTest {
    Class<Person> clazz = Person.class;

    //构造器
    @Test
    public void test1() throws Exception {
        Constructor<Person> constructor = clazz.getDeclaredConstructor();
        Person person = constructor.newInstance();
        System.out.println(person);
        //Person{name='null', age=0, id=0}

        //获取私有构造器
        Constructor<Person> declaredConstructor = clazz.getDeclaredConstructor(String.class);
        //将访问权限打开
        declaredConstructor.setAccessible(true);
        Person soberw = declaredConstructor.newInstance("soberw");
        System.out.println(soberw);
        //Person{name='soberw', age=0, id=0}
    }

    //属性
    @Test
    public void test2() throws Exception {
        //第一步： 一定要先创建对象
        Person person = clazz.getDeclaredConstructor().newInstance();
        Field pname = clazz.getDeclaredField("name");
        pname.setAccessible(true);
        pname.set(person, "soberw");
        System.out.println(pname.get(person));
        //soberw
    }

    //方法
    @Test
    public void test3() throws Exception {
        Person person = clazz.getDeclaredConstructor().newInstance();
        Method show = clazz.getDeclaredMethod("show", String.class);
        show.setAccessible(true);
        Object invoke = show.invoke(person, "中国");
        //我的国籍是中国
        System.out.println(invoke);
        //中国
        //调用静态方法
        Method showDesc = clazz.getDeclaredMethod("showDesc");
        showDesc.setAccessible(true);
        Object val1 = showDesc.invoke(null);
        //我是一个可爱的人
        Object val2 = showDesc.invoke(clazz);
        //我是一个可爱的人
        Object val3 = showDesc.invoke(Person.class);
        //我是一个可爱的人
        System.out.println(val1);
        //null
        System.out.println(val2);
        //null
        System.out.println(val3);
        //null
    }
}
