package day_12_30.reflection.test;

import day_12_30.reflection.java.Person;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author soberw
 * @Classname OtherTest
 * @Description  获取运行时类的构造器、接口等
 * @Date 2021-12-30 15:20
 */
public class OtherTest {
    Class<Person> clazz = Person.class;

    /**
     * 获取构造器结构
     */
    @Test
    public void test1(){
        Constructor<?>[] constructors = clazz.getConstructors();
        for (Constructor<?> c : constructors) {
            System.out.println(c);
            /*
            public day_12_30.reflection.java.Person(java.lang.String,int)
            public day_12_30.reflection.java.Person()
             */
        }
        System.out.println();
        Constructor<?>[] declaredConstructors = clazz.getDeclaredConstructors();
        for (Constructor<?> d : declaredConstructors) {
            System.out.println(d);
            /*
            public day_12_30.reflection.java.Person(java.lang.String,int)
            private day_12_30.reflection.java.Person(java.lang.String)
            public day_12_30.reflection.java.Person()
             */
        }
    }
    /**
     * 获取父类(包括带泛型的，以及其泛型）
     */
    @Test
    public void test3(){
        Class<? super Person> superclass = clazz.getSuperclass();
        System.out.println(superclass);
        //class day_12_30.reflection.java.Creature

        //获取运行时类的带泛型的父类
        Type genericSuperclass = clazz.getGenericSuperclass();
        System.out.println(genericSuperclass);
        //day_12_30.reflection.java.Creature<java.lang.String>

        //获取运行时类的带泛型的父类的泛型
        //转换为参数化类型，即为泛型
        ParameterizedType paramType = (ParameterizedType) genericSuperclass;
        //获取泛型的参数
        Type[] actualTypeArguments = paramType.getActualTypeArguments();
        for (Type a : actualTypeArguments) {
            //两种都可
            //System.out.println(a.getTypeName());
            System.out.println(((Class<?>)a).getName());
            //java.lang.String
        }
    }

    @Test
    public void test4(){
        //获取运行时类实现的接口
        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> c : interfaces) {
            System.out.println(c);
            /*
            interface day_12_30.reflection.java.MyInterface
            interface java.lang.Comparable
             */
        }
        System.out.println();
        //获取运行时类的父类实现的接口
        Class<?>[] interfaces1 = clazz.getSuperclass().getInterfaces();
        for (Class<?> c : interfaces1) {
            System.out.println(c);
            //interface java.io.Serializable
        }
        System.out.println();
        //返回运行时类直接实现的接口的类型。
        Type[] genericInterfaces = clazz.getGenericInterfaces();
        for (Type g : genericInterfaces) {
            System.out.println(g.getTypeName());
            /*
            day_12_30.reflection.java.MyInterface
            java.lang.Comparable<java.lang.String>
             */
        }
    }
    @Test
    public void test5(){
        Package aPackage = clazz.getPackage();
        System.out.println(aPackage);
        //package day_12_30.reflection.java
    }
    @Test
    public void test6(){
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation a : annotations) {
            System.out.println(a);
            //@day_12_30.reflection.java.MyAnnotation("soberw")
        }
    }
}
