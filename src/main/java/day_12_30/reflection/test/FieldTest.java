package day_12_30.reflection.test;

import day_12_30.reflection.java.Person;
import org.junit.Test;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @author soberw
 * @Classname FieldTest
 * @Description  获取当前运行时类的属性结构
 * @Date 2021-12-30 14:54
 */
public class FieldTest {
    //获取当前运行时类（Class实例）
    Class<Person> clazz = Person.class;

    @Test
    public void test1(){
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            System.out.println(field);
            /*
            public int day_12_30.reflection.java.Person.id
            public double day_12_30.reflection.java.Creature.weight
             */
        }
        System.out.println();

        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            System.out.println(field);
            /*
            private java.lang.String day_12_30.reflection.java.Person.name
            int day_12_30.reflection.java.Person.age
            public int day_12_30.reflection.java.Person.id
             */
        }

    }
    //权限修饰符  数据类型 变量名
    @Test
    public void test2(){
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field f : declaredFields) {
            //1.权限修饰符
            int modifier = f.getModifiers();
            System.out.print(Modifier.toString(modifier) + "\t");

            //2.数剧类型
            Class<?> type = f.getType();
            System.out.print(type.getName() + "\t");

            //3,变量名
            String name = f.getName();
            System.out.print(name);

            System.out.println();
        }
        /*
        private	java.lang.String	name
        	int	age
        public	int	id
         */
    }
}
