package day_12_30.reflection.java;

/**
 * @author soberw
 * @Classname MyInterface
 * @Description
 * @Date 2021-12-30 14:39
 */
public interface MyInterface {
    void info();
}
