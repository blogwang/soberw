package day_12_30.reflection.java;

import java.io.Serializable;

/**
 * @author soberw
 * @Classname Creature
 * @Description
 * @Date 2021-12-30 14:37
 */
public class Creature<T> implements Serializable {
    private char gender;
    public double weight;

    private void breath(){
        System.out.println("生物呼吸");
    }

    public void eat(){
        System.out.println("生物吃东西");
    }
}
