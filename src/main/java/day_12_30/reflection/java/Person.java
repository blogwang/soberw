package day_12_30.reflection.java;

/**
 * @author soberw
 * @Classname Person
 * @Description
 * @Date 2021-12-30 14:31
 */
@MyAnnotation
public class Person extends Creature<String> implements MyInterface, Comparable<String> {
    private String name;
    int age;
    public int id;

    //无参构造器
    public Person() {
    }

    //私有构造器
    @MyAnnotation(value = "hello")
    private Person(String name) {
        this.name = name;
    }

    //带参共有构造器
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @MyAnnotation
    private String show(String nation) {
        System.out.println("我的国籍是" + nation);
        return nation;
    }

    public String display(String interests, int age) throws NullPointerException, ClassCastException {
        return interests + age;
    }

    @Override
    public void info() {

    }

    @Override
    public int compareTo(String o) {
        return 0;
    }

    private static void showDesc() {
        System.out.println("我是一个可爱的人");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
                "} ";
    }
}
