package day_12_30.reflection_test;

import day_12_30.reflection.java.Person;

import java.util.Date;
import java.util.Random;

/**
 * @author soberw
 * @Classname RefTest
 * @Description 体会反射的动态性
 * @Date 2021-12-30 20:56
 */
public class RefTest {

    public static void main(String[] args) throws Exception {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int j = random.nextInt(3);
            Class clazz = switch (j) {
                case 1 -> User.class;
                case 2 -> Date.class;
                default -> Person.class;
            };
            Object o = clazz.getDeclaredConstructor().newInstance();
            System.out.println(o);
            /*
            Thu Dec 30 21:06:17 CST 2021
            Person{name='null', age=0, id=0}
            Person{name='null', age=0, id=0}
            User{name='wang', age=18}
            Thu Dec 30 21:06:17 CST 2021
            Person{name='null', age=0, id=0}
            Thu Dec 30 21:06:17 CST 2021
            User{name='wang', age=18}
            Thu Dec 30 21:06:17 CST 2021
            User{name='wang', age=18}
             */
        }
    }
}

class User {
    private String name;
    private int age;
    public User() {}
    {
        this.age = 18;
        this.name = "wang";
    }
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
