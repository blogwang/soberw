package day_12_29;

import org.junit.Test;

/**
 * @author soberw
 * @Classname Test1
 * @Description
 * @Date 2021-12-29 9:23
 */
public class Test1 {
    private int i;

    public Test1(int val) {
        this.i = val;
    }

    public static void main(String[] args) {
        Test1 t = new Test1(3);
        System.out.println(t.i);

    }
}

class Testt {
    static boolean flag;

    public static void main(String[] args) {
        System.out.println(flag);
    }

    @Test
    public void test() {
        int count = 0;
        int sum = 0;
        for (int i = 0; i <= 5; i++) {
            sum += i;
            count = count++;
        }
        System.out.println(sum * count);
    }
}


