package day_12_29;

import org.junit.Test;

/**
 * @author soberw
 * @Classname ReflectionTest
 * @Description 反射之获取Class的实例的四种方式
 * @Date 2021-12-29 20:44
 */
public class ReflectionTest {
    //获取Person类
    @Test
    public void test() {
        //1.调用运行时类的属性
        Class<Person> clazz1 = Person.class;
        System.out.println(clazz1);

        //2. 通过运行时类的对象，调用getClass()
        Person p1 = new Person();
        Class<? extends Person> clazz2 = p1.getClass();
        System.out.println(clazz2);

        //3. 调用Class静态方法：forName(String classPath)  开发中最常用
        Class<?> clazz3 = null;
        try {
            clazz3 = Class.forName("day_12_29.Person");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(clazz3);

        //4. 使用类的加载器：ClassLoader  (了解就行)
        ClassLoader classLoader = ReflectionTest.class.getClassLoader();
        Class<?> clazz4 = null;
        try {
            clazz4 = classLoader.loadClass("day_12_29.Person");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(clazz4);

    }

}

class Person {

}
