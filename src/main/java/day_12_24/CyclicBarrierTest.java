package day_12_24;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @author soberw
 * @Classname CyclicBarrierTest
 * @Description
 * @Date 2021-12-24 17:21
 */
public class CyclicBarrierTest {

    public static void main(String[] args) {
        TT tt = new TT();
        new Thread(tt::show, "张三").start();
        new Thread(tt::show, "李四").start();
        new Thread(tt::show, "王五").start();
        new Thread(tt::show, "马六").start();
    }

}

class TT {
    //控制在执行几个线程后，开始执行里面的代码，其中参数3是代表三个线程，当三个线程结束后开始执行此线程
    CyclicBarrier cy = new CyclicBarrier(3, () -> {
        System.out.println("人到齐了，开饭！");
        System.exit(0);
    });

    public void show() {

        Random random = new Random();
        String name = Thread.currentThread().getName();
        int m = random.nextInt(10) + 2;
        System.out.println(name + "出发，预计需要(" + m + ")分钟。。。");
        try {
            TimeUnit.SECONDS.sleep(m);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(name + "到了...");
        try {
            cy.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

    }
}
