package day_12_24;

import java.util.concurrent.TimeUnit;

/**
 * @author soberw
 * @Classname Ticket
 * @Description
 * @Date 2021-12-24 9:29
 */
public class Ticket implements Runnable {
    private int num;

    public Ticket() {
    }

    public Ticket(int num) {
        this.num = num;
    }

    @Override
    public void run() {
        String n = Thread.currentThread().getName();
        while (true) {
            //给线程加锁，保证当前线程执行完之后其他线程才执行（线程安全）
            //同步锁，锁的是当前实例对象
            //也可写成Ticket.class
            synchronized (this) {
                try {
                    //sleep()处理异常只能try-catch不能抛，因为父类没有抛，
                    // 有规定：子类重写父类的方法时，之类抛的异常不能大于父类抛的
                    //当前父类的run()方法没有抛异常，所以子类重写的不能抛，只能处理
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (num > 0) {
                    System.out.printf("%s:[售出%d号票，还剩%d张]\n", n, num--, num);
                } else {
                    break;
                }
            }
        }

    }
}

class TicketTest {
    public static void main(String[] args) {
        Ticket t = new Ticket(20);
        Runnable target;
        Thread t1 = new Thread(t, "东风路");
        Thread t2 = new Thread(t, "南阳路");
        Thread t3 = new Thread(t, "二马路");
        Thread t4 = new Thread(t, "建设路");
        Thread t5 = new Thread(t, "凤阳路");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}
