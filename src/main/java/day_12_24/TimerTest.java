package day_12_24;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * @author soberw
 * @Classname TimerTest
 * @Description
 * @Date 2021-12-24 14:05
 */
public class TimerTest {
    //定时器功能
    public static void main(String[] args) throws ParseException {
        //写法一：
//        Timer timer = new Timer();
//        TimerTask t = new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("hello world!!");
//            }
//         };
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        timer.schedule(t,sdf.parse("2021-12-24 14:13:00"),2000);

        //写法二：
        Timer tt = new Timer();
        tt.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello -world");
            }
        },2000,1000);
    }
}
