package day_12_24;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author soberw
 * @Classname Ticket
 * @Description
 * @Date 2021-12-24 9:29
 */
public class Ticket2 implements Runnable {
    //原子类，保证可见性，但是还是没有线程锁安全，可以结合起来使用
    private AtomicInteger num;

    public Ticket2() {
    }

    public Ticket2(int num) {
        this.num = new AtomicInteger(num);
    }

    @Override
    public void run() {
        String n = Thread.currentThread().getName();
        while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (num.get() > 0) {
                    System.out.printf("%s:[售出%d号票，还剩%d张]\n", n, num.decrementAndGet(), num.get());
                } else {
                    break;
                }
        }

    }
}

class Ticket2Test {
    public static void main(String[] args) {
        Ticket2 t = new Ticket2(20);
        Thread t1 = new Thread(t, "东风路");
        Thread t2 = new Thread(t, "南阳路");
        Thread t3 = new Thread(t, "二马路");
        Thread t4 = new Thread(t, "建设路");
        Thread t5 = new Thread(t, "凤阳路");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}
