package day_12_24;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author soberw
 * @Classname ReentrantLockTest
 * @Description 对同步锁的测试
 * @Date 2021-12-24 21:00
 */
public class ReentrantLockTest {
    //ReentrantLock 两种作用：
    // 1.相当于synchronize同步锁机制
    // 2.可以开启公平锁
    Lock lock = new ReentrantLock();

    void m1() {
        //锁【lock.lock】必须紧跟try代码块，且unlock要放到finally第一行。
        lock.lock();
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
        }
        lock.unlock();
    }

    void m2() {
        lock.lock();
        System.out.println("me()...");
        lock.unlock();
    }


    public static void main(String[] args) {
        ReentrantLockTest t = new ReentrantLockTest();
        new Thread(t::m1, "one").start();
        new Thread(t::m2, "two").start();
    }
}

/**
 * @author soberw
 * @Classname ReentrantLockTest
 * @Description 对公平锁的测试
 * @Date 2021-12-24 21:00
 */
class ReentrantLockTest1 {
    ReentrantLock lock = new ReentrantLock(true);

     void show() {
        String name = Thread.currentThread().getName();
        for (int i = 0; i < 10; i++) {
            //此处加锁会一个一个线程执行，不加会三个三个执行
            lock.lock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + "线程正在运行：" + i);
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        ReentrantLockTest1 t = new ReentrantLockTest1();
        new Thread(t::show,"one").start();
        new Thread(t::show,"two").start();
        new Thread(t::show,"three").start();
    }
}
