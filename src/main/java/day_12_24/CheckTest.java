package day_12_24;

import java.util.concurrent.CountDownLatch;

/**
 * @author soberw
 * @Classname CheckTest
 * @Description
 * @Date 2021-12-24 14:50
 */
public class CheckTest {
    public static void main(String[] args) {
        Latch l = new Latch();
        new Thread(() -> l.show(), "t1").start();
        new Thread(() -> l.show(), "t2").start();
        new Thread(() -> l.show(), "t3").start();
        new Thread(() -> l.show(), "t4").start();
        new Thread(() -> l.show(), "t5").start();
        new Thread(() -> {
            try {
                Latch.cdl.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("收尾工作开始执行。。。");
        }).start();
    }

}

class Latch {
    static CountDownLatch cdl = new CountDownLatch(5);

    public void show() {
        String name = Thread.currentThread().getName();
        System.out.println(name + "线程已经启动...");
        for (int i = 0; i < 10; i++) {
            System.out.println(name + "item" + i + "正在执行");
        }
        cdl.countDown();
    }
}
