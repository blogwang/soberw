package day_01_11;

import java.io.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author soberw
 * @Classname wordList
 * @Description 劈分有道云单词
 * @Date 2022-01-11 8:38
 */
public class wordList {
    public static void main(String[] args) {
        String path = "C:\\Users\\soberw\\Desktop\\java-note\\有道云单词.txt";
        StringBuilder sbu = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final Pattern P = Pattern.compile("[a-z]+", Pattern.CASE_INSENSITIVE);
        List<String> list = br.lines().toList();
        for (String s : list) {
            if (s.matches("([0-9]+)(, )([a-z|A-Z]+)(.*)")) {
                Matcher m = P.matcher(s);
                if (m.find()) {
                    sbu.append(m.group());
                    sbu.append("\n");
                }
            }
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("C:\\Users\\soberw\\Desktop\\java-note\\有道云单词111.txt"));
            bw.write(sbu.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
