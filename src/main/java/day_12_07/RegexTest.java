package day_12_07;

import org.junit.Test;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author soberw
 * @Classname RegexTest
 * @Description
 * @Date 2021-12-07 20:35
 */
public class RegexTest {
    @Test
    public void test1() {
        String s = "fsjkdhfgjh666hgdfshjhahaksgefj";
        System.out.println(s.matches("^.*[\\d+].*$"));
    }

    @Test
    public void test2() {
        // .  一个 . 代表一个任意符号，所以四个点匹配四个
        System.out.println("a4^-".matches("....")); //true  此句表示匹配四个任意字符

        // -  表示范围  如 a-z 0-9
        // []  字符集 匹配里面包含的任意字符 如 [0-9]匹配从0-9之间任意数字一个
        System.out.println("h".matches("a-z"));  //false  此句直接写表示并列 a 或 - 或 z
        System.out.println("h".matches("[a-z]"));  //true  表示[] 内的字符任意一个

        // ^ $   表示开头和结尾
        System.out.println("a4^-".matches("^....$"));  //true

        // *  取值长度范围 表示0个到多个 {0,} 属于量词
        System.out.println("a4^-".matches(".*"));  //true  此句表示匹配任意长度任意符号

        // +  取值长度范围 表示0个到多个 {1,} 属于量词
        System.out.println("123".matches("[0-9]+"));  //true 此句表示最少匹配一个数字
        System.out.println("abc".matches("[0-9]+"));  //false

        // ?  取值长度范围 表示0个到多个 {0,1} 属于量词
        System.out.println("8".matches("[0-9]?")); //true  此句表示 匹配 0 个或者 1 个数字
        System.out.println("88".matches("[0-9]?"));  //false 两个，超出范围

        // {m,n} 取值长度范围 m个到n个
        System.out.println("12345".matches("[0-9]{4,8}"));  // true表示数字长度在4-8之间
        System.out.println("123".matches("[0-9]{4,8}"));  // False 不在范围内

        // \d  数字 与[0-9]相同
        //  \D  非数字
        System.out.println("123".matches("\\d+"));  //true 注意这里用两个\\是因为在Java中单个\是转义符
        System.out.println("abc".matches("\\D+"));  //true
        //举例： 判断字符串中有没有数字
        System.out.println("zdjghj9jd".matches(".*\\d+.*"));//true 前后.*是因为我们不确定前后字符数量


        // |  或 前后跟要匹配的字符
        //举例说明：判断字符串是不是以
        System.out.println("123".matches("[0-9]+|[a-z]+"));  //true
        System.out.println("abc".matches("[0-9]+|[a-z]+"));  //true
        System.out.println("abc123".matches("[0-9]+|[a-z]+"));  //false

        // ^  注意，放在字符集里面表示取反
        System.out.println("abc".matches("[0-9]+"));  //false
        System.out.println("abc".matches("[^0-9]+"));  //true  与\D相同

        // ()  分组操作
        System.out.println("food".matches("(f|z).*"));  //true
        System.out.println("zood".matches("(f|z).*"));  //true
        System.out.println("hood".matches("(f|z).*"));  //false

        //另外，需要记住汉字的格式  \u4e00-\u9fa5
        //举例:判断字符串中有没有汉字
        System.out.println("abhhj会更好hu".matches(".*[\\u4e00-\\u9fa5]+.*"));  //true
    }

    @Test
    public void test3() {
        //split()
        //举例：将下面字符串按数字劈分成数组，不保留数字
        String s = "abc123hello6world101java";
        String[] ss = s.split("\\d+");
        System.out.println(Arrays.toString(ss));  //[abc, hello, world, java]

        //replaceAll()  替换所有的
        //举例：替换字符串中所有的连续数字为"java"
        String s1 = "hello123c++456world789python";
        System.out.println(s1.replaceAll("[\\d]+", "java"));  //hellojavac++javaworldjavapython

        //replaceFirst()  替换第一个
        String s2 = "javajavajava";
        System.out.println(s2.replaceFirst("java", "hello"));  //hellojavajava

    }

    @Test
    public void test4() {
        String s = "123abc";
        Pattern p = Pattern.compile("[0-9a-z]+");  //编译一个正则
        Pattern p2 = Pattern.compile("[0-9a-z]+", Pattern.CASE_INSENSITIVE);   //编译一个正则 第二个参数表示不区分大小写
        Matcher m = p.matcher(s);  //传入一个字符串与正则进行匹配，结果存放在Matcher对象中，我们可以通过其方法去操作这个结果
    }

    @Test
    public void test5() {
        //matches()  尝试将整个输入序列与模式进行匹配
        //例子：判断字符串是否存在数字，用法与String的matches()类似
        String s = "abd56jgh";
        Pattern p = Pattern.compile(".*\\d.*");
        Matcher m = p.matcher(s);
        System.out.println(m.matches());  //true

        //find() 与 group()
        //举例：在字符串中找到所有的连续字母并输出
        String s1 = "123java%&^hello687*&^email   _python";
        Pattern p1 = Pattern.compile("[a-z]+", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(s1);
        while (m1.find()) {
            System.out.println(m1.group());
            //  java
            //  hello
            //  email
            //  python
        }

    }

    @Test
    public void test6() {
        //举例：筛选出<div></div>标签内包含的内容
        String s = "<div>hello</div><html>hello</html><div>hello 15</div>";
        Pattern p = Pattern.compile("<div>(.*)</div>");  //贪婪模式
        Matcher m = p.matcher(s);
        while(m.find()){
            System.out.printf("%s\t",m.group(1));
            //  hello</div><html>hello</html><div>hello 15
        }

        System.out.println();
        Pattern p1 = Pattern.compile("<div>(.*?)</div>");   //禁用贪婪模式
        Matcher m1 = p1.matcher(s);
        while(m1.find()){
            System.out.println(m1.group(1));
            //hello
            //hello 15
        }
    }
}
