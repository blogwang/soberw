package day_12_10;

/**
 * @author soberw
 * @Classname MapTest
 * @Description
 * @Date 2021-12-10 10:38
 */

public class MapTest {
    //封装性
    private int age = 1;
    private String name;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MapTest{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

class Test1 {
    public static void main(String[] args) {
        MapTest m = new MapTest();
        m.setName("李四");
        System.out.println(m.getName());
        m.setAge(5);
        System.out.println(m.getAge());
        System.out.println(m.toString());
        int i = '1';
        System.out.println(i);
        char[] c = {1};
        System.out.println(c[0]);
//        int s = c[0];
//        System.out.println(s);
    }

}



