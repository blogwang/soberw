package day_12_10;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * @author soberw
 * @Classname ListTest
 * @Description
 * @Date 2021-12-10 8:40
 */
public class ListTest2 {
    public static void main(String[] args) {
        System.out.printf("ArrayList插入300000个元素的时间是:%d\n",arrayAdd());
        System.out.printf("LinkedList插入300000个元素的时间是:%d\n",linkAdd());
    }

    public static long arrayAdd() {
        Random random = new Random();
        List<Integer> arr = new ArrayList<>();
        //添加一个元素
        arr.add(1);
        for (int i = 0; i < 300000; i++) {
            //在 0 位置添加300000个元素
            arr.add(0, random.nextInt(10));
        }
        long start = System.currentTimeMillis();
        arr.indexOf(3);
        long end = System.currentTimeMillis();
        return end - start;
    }

    public static long linkAdd() {
        Random random = new Random();
        List<Integer> link = new LinkedList<>();
        //添加一个元素
        link.add(1);
        for (int i = 0; i < 300000; i++) {
            //在 0 位置添加300000个元素
            link.add(0, random.nextInt(10));
        }
        long start = System.currentTimeMillis();
        link.indexOf(3);
        long end = System.currentTimeMillis();
        return end - start;
    }
}
