package day_12_25;

import java.util.concurrent.TimeUnit;

/**
 * @author soberw
 * @Classname WaitTest
 * @Description 两个线程交替打印1~100，方式一：
 * @Date 2021-12-25 15:48
 */
public class WaitTest {
    public static void main(String[] args) {
        Wait w = new Wait();
        new Thread(w, "甲").start();
        new Thread(w, "乙").start();
    }
}

class Wait implements Runnable {

    // 原理是加上唤醒和阻塞机制，一个进程进去，执行完之后此线程阻塞，
    // 等另一个线程进去后将另一个唤醒，然后此线程执行完再阻塞，此时另一个被唤醒了
    // 如此交替，形成交替的效果，但是只限于两个线程，
    // 如果线程多的情况下，因为notify()只能一次唤醒一个，因此会优先唤醒级别高的那一个
    // 另有notifyAll()方法，可以一次唤醒全部线程

    //说明：wait()notify()以及notifyAll()必须使用在同步代码块或者同步方法中
    //方法调用者必须是同步代码块或同步方法中的同步监视器，否则会出异常
    //它们都定义在Object类中
    @Override
    public void run() {
        for (int i = 1; i <= 20; i++) {
            //加上同步锁
            synchronized (this) {
                //唤醒锁，只能唤醒一把
                notify();
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "线程正在执行：" + i);
                try {
                    //将自己阻塞，等着别人去唤醒
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
