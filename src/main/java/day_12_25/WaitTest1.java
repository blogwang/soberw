package day_12_25;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author soberw
 * @Classname WaitTest
 * @Description 两个线程交替打印1~100，方式二：公平锁
 * @Date 2021-12-25 15:48
 */
public class WaitTest1 {
    public static void main(String[] args) {
        Wait1 w = new Wait1();
        new Thread(w, "甲").start();
        new Thread(w, "乙").start();
    }
}

class Wait1 implements Runnable {
    //公平锁机制

    private ReentrantLock t = new ReentrantLock(true);

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            //手动加上同步锁
            t.lock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "线程正在执行：" + i);
            //手动去除
            t.unlock();
        }
    }
}
