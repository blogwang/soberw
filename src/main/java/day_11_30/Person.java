package day_11_30;

/**
 * @author soberw
 */
public class Person {
    public String name = "人类";

    public Person(){

    }

    public void show() {
        System.out.println("我是一个人");
    }

    //重载
    public void show(String name) {
        this.name = name;
        System.out.println("我是一个人，我的名字叫做" + name);
    }
    private void p1(int i){

    }
    public final void p2(){

    }
}
