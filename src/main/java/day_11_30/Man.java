package day_11_30;

/**
 * @author soberw
 */
public class Man extends Person{
    String name = "张三";

    //报错，构造方法不能被重写
//    public Person(){
//
//    }
    //重写
    @Override
    public void show() {
        System.out.println("我是男人");
    }

    @Override
    public void show(String name) {
        this.name = name;
        System.out.println("我是一个男人，我的名字叫做" + name);
    }
    //重写权限不能小于父类
//    protected void show(String name) {
//        this.name = name;
//        System.out.println("我是一个男人，我的名字叫做" + name);
//    }

    //此处不算构成重写，只算定义了同名同参的方法，此方法与父类的无关系
    private void p1(int i){

    }

    //报错  最终的（final）不能被重写
//    public final void p2(){
//
//    }
}
