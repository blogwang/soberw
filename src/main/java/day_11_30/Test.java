package day_11_30;

/**
 * @author soberw
 */
public class Test {
    public static void main(String[] args) {
        Person p = new Person();
        p.show(p.name);
        p.show();
        Man m = new Man();
        m.show();
        m.show(m.name);
        Woman w = new Woman();
        w.show();
        w.show(w.name);


        Person pp = new Man();
        Man mm = (Man)pp;
        mm.show();

//        Man mm1 = (Man)new Person();

    }
}
