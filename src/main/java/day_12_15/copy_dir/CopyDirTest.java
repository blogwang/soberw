package day_12_15.copy_dir;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipInputStream;

/**
 * @author soberw
 * @Classname CopyDir
 * @Description 拷贝目录（包含目录里的文件）
 * @Date 2021-12-15 19:48
 */
public class CopyDirTest {
    public static void main(String[] args) {
        //CopyDir.copyDir(new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_15"),new File("E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_14"));
        File f1 = new File("C:\\Users\\soberw\\Desktop\\src");
//        File f2 = new File("C:\\Users\\soberw\\Desktop\\copySrc");
        File f3 = new File("d:\\src");
        long start = System.currentTimeMillis();
        System.out.println(CopyDir.copyDir(f1, f3));
        long end = System.currentTimeMillis();
        System.out.println((end - start) / 1000);
        long start1 = System.currentTimeMillis();
        try {
            FileUtils.copyDirectory(f1, f3);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end1 = System.currentTimeMillis();
        System.out.println((end1 - start1) / 1000);
    }
}









