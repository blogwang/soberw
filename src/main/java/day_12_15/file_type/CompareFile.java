package day_12_15.file_type;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author soberw
 * @Classname CompareFile
 * @Description 判断两个文件是不是同一个文件（内容一样）
 * @Date 2021-12-15 13:16
 */
public class CompareFile {
    private static final int MB = 1024;
    /*
    思路是：先判断文件是否都存在，再判断字节数，再比较真实文件类型，最后进一步进行判断
     */

    /**
     * @param file1 第一个文件
     * @param file2 第二个文件
     * @description: 判断两个文件是不是同一个文件（内容一样）
     * @return: boolean
     * @author: soberw
     * @time: 2021/12/15 13:18
     */
    public static boolean compareFile(File file1, File file2) {
        if (!file1.exists() || !file2.exists()) {
            return false;
        }
        if (file1.length() != file2.length()) {
            return false;
        }
        String t1 = RealFileType.getFileType(file1);
        String t2 = RealFileType.getFileType(file2);
        if (t1 == null || t2 == null) {
            return false;
        }
        if (!t1.equals(t2)) {
            return false;
        }
        String d1 = "";
        String d2 = "";
        try {
            //调用comments-codec组件比较
            d1 = DigestUtils.md5Hex(new FileInputStream(file1));
            d2 = DigestUtils.md5Hex(new FileInputStream(file2));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return d1.equals(d2);
    }

}
