package day_12_15.file_type;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author soberw
 * @Classname Test1
 * @Description
 * @Date 2021-12-15 10:53
 */
public class Test1 {
    public static void main(String[] args) {
        String path = "C:\\Users\\soberw\\Desktop\\ha.pptx";
        path = "E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_15\\file_type\\RealFileType.java";    //7061636b616765206461
//        path = "E:\\HelloJava\\Java_Study_Beiyou\\src\\day_12_15\\file_type\\SomeFileType.java";  //7061636b616765206461
        path = "E:\\HelloJava\\Java_Study_Beiyou\\out\\production\\Java_Study_Beiyou\\day_12_10\\Test1.class";  //7061636b616765206461
        path = "E:\\HelloJava\\Java_Study_Beiyou\\out\\production\\Java_Study_Beiyou\\day_12_10\\charTest.class";  //7061636b616765206461
        File file = new File(path);
        if (file.isFile()) {
            byte[] b = new byte[10];
            try (FileInputStream fis = new FileInputStream(file)) {
                if (file.length() >= 10) {
                    fis.read(b, 0, b.length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (byte b1 : b) {
                System.out.printf("%02x",b1);
            }
        }
    }
}
