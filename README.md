《Java入门与精通》

*2021 第三版*

Java是一门面向对象编程语言，不仅吸收了C++语言的各种优点，还摒弃了C++里难以理解的多继承、指针等概念，因此Java语言具有功能强大和简单易用两个特征。Java语言作为静态面向对象编程语言的代表，极好地实现了面向对象理论，允许程序员以优雅的思维方式进行复杂的编程  。

![image-20211204104014290](https://gitee.com/webrx/wx_note/raw/master/images/image-20211204104014290.png)		

Java具有简单性、面向对象、分布式、健壮性、安全性、平台独立与可移植性、多线程、动态性等特点   。Java可以编写桌面应用程序、Web应用程序、分布式系统和嵌入式系统应用程序等  。

编程语言参考：https://www.tiobe.com/tiobe-index/

![image-20210724113646284](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724113646284.png)

## 第一章  简介及开发环境

### 1.1 java简介

詹姆斯·高斯林 （James Gosling）是一名软件专家，1955年5月19日出生于加拿大，Java编程语言的共同创始人之一，一般公认他为“Java之父”。

![img](https://gitee.com/webrx/wx_note/raw/master/images/1c950a7b02087bf40fe8767ff8d3572c11dfcf1b)

Sun Microsystems 

Sun Microsystems是IT及互联网技术服务公司（已被甲骨文2009年收购）Sun Microsystems 创建于1982年。主要产品是工作站及服务器。1986年在美国成功上市。1992年sun推出了市场上第一台多处理器台式机SPARCstation 10 system，并于1993年进入财富500强。

![img](https://gitee.com/webrx/wx_note/raw/master/images/14ce36d3d539b6003af33a98ab1f222ac65c103819c7)

### 1.2 java发展历史

> 1990年初，最初被命名为Oak；
> 		1995年5月23日，Java语言诞生；
>
> 1996年1月，第一个JDK-JDK1.0诞生；
>
> 1996年4月，10个最主要的操作系统供应商申明将在其产品中嵌入Java技术；
>
> 1996年9月，约8.3万个网页应用了Java技术来制作；
>
> 1997年2月18日，JDK1.1发布；
>
> 1997年4月2日，JavaOne会议召开，参与者逾一万人，创当时全球同类会议纪录；
>
> 1997年9月，JavaDeveloperConnection社区成员超过十万；
>
> 1998年2月，JDK1.1被下载超过2,000,000次；
>
> 1998年12月8日，Java 2企业平台J2EE发布；
>
> 1999年6月，SUN公司发布Java三个版本：标准版（J2SE）、企业版（J2EE）和微型版（J2ME）；
>
> 2000年5月8日，JDK1.3发布；
>
> 2000年5月29日，JDK1.4发布；
>
> 2001年6月5日，Nokia宣布到2003年将出售1亿部支持Java的手机；
>
> 2001年9月24日，J2EE1.3发布；
>
> 2002年2月26日，J2SE1.4发布，此后Java的计算能力有了大幅提升；
>
> 2004年9月30日，J2SE1.5发布，成为Java语言发展史上的又一里程碑。为了表示该版本的重要性，J2SE1.5更名为Java SE 5.0；
>
> 2005年6月，JavaOne大会召开，SUN公司公开Java SE 6。此时，Java的各种版本已经更名，以取消其中的数字“2”：J2EE更名为Java EE，J2SE更名为Java SE，J2ME更名为Java ME；
>
> 2006年12月，SUN公司发布JRE6.0
>
> 2008年sun公司收购mysql 10亿
>
> 2009年4月20日，甲骨文以74亿美元的价格收购SUN公司，取得java的版权，业界传闻说这对Java程序员是个坏消息（其实恰恰相反）；
>
> 2010年11月 由于甲骨文对Java社区的不友善，因此Apache扬言将退出JCP
>
> 2011年7月28日甲骨文发布Java SE 7
>
> 2014年3月18日 甲骨文发表Java SE 8 LTS 公司通用
>
> 2017年9月22日 甲骨文发表Java SE 9
>
> 2018年3月21日 甲骨文发表Java SE 10
>
> 2018年9月26日 甲骨文发表Java SE 11 LTS 长期
>
> 2019年3月19日 甲骨文发表Java SE 12
>
> 2019年9月17日 Java SE 13 Text Blocks (Preview)
>
> 2020年3月17日 Java SE 14 
>
> 2020年9月15-17日 Java se 15
>
> 2021年3月19日 Java SE 16
>
> 2021年9月14日 java se 17 LTS 长期支持版本
>
> …

java 1.0  j2se 

java 1.2 - 1.5 第二代 j2se  j2ee j2me   

java 1.5  就是java 5.0   javase javaee  javame  javatv javafx

2020 年是值得纪念的一年，这一年中我们庆祝了 Java 的 25 岁生日。经过二十多年的持续创新，Java 一直在：

1、通过适应不断变化的技术格局来保持灵活性，同时维持平台独立性。

2、通过保持向后兼容性来保证可靠性。

3、在不牺牲安全性的前提下加速创新来保持优势。

Java 凭借自身不断提高平台性能、稳定性和安全性的能力，一直是开发人员中最流行的编程语言。IDC 的最新报告“Java Turns 25”显示，超过 900 万名开发人员（全球专职开发人员中的 69%）在使用 Java——比其他任何语言都多。

甲骨文还在继续探索 Java 的持续创新之路，并自豪地宣布 Java 16 正式发布，这也是我们转向六个月发布周期后的第七个特性版本。这种可预测水平使开发人员可以更轻松地管理他们对创新的采用计划。

![image-20210724121323523](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724121323523.png)

### 1.3 java相关的概念

* jvm Java Virtual Machine（Java虚拟机）

* jdk Java Development Kit

* jre JavaRuntimeEnvironment缩写，指Java运行环境

* jcp （Java Community Process) 是一个开放的国际组织，主要由Java开发者以及被授权者组成，职能是发展和更新

* jsr Java Specification Requests，Java规范请求，由JCP成员向委员会提交的Java发展议案，经过一系列流程后，如果通过最终会体现在未来的Java中

  2018年5月18日， 阿里巴巴获邀加入JCP最高执行委员会(Java Community Process)，以替代恩智浦被选举为该委员会委员，成为第一家加入JCP的中国企业。

  此次阿里巴巴以126票赞成，19票反对，12票弃权高票当选，成为新的JCP委员。成员任期为两年。

*    java程序相关的扩展名

  * .java 源代码 ascii 文件
  * .class 程序编译后的字节码 javac Test.java 产生Test.class
  * .jar     程序包
  * .war web程序包
  * .ear 程序包

### 1.4 下载JDK安装配置

![image-20210724151623059](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724151623059.png)

![image-20210724151651654](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724151651654.png)

#### 1.4.1 安装jdk8

下载jdk8

![image-20211118092209818](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118092209818.png)

CLASSPATH = %JAVA_HOME%\lib;%JAVA_HOME%\dt.jar;%JAVA_HOME%\tools.jar;.

![image-20210724152035668](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724152035668.png)

![image-20210724152146284](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724152146284.png)

![image-20210724152348896](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724152348896.png)

![image-20210724153117396](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724153117396.png)

![image-20210724152829261](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724152829261.png)

下载java8 demos examples 

![image-20211118092954488](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118092954488.png)

![image-20211118093017552](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118093017552.png)

![image-20211118094609606](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118094609606.png)

```cmd
#utf-8 cmd命令：chcp 65001 
#gbk cmd命令：chcp 936
#cmd 默认是GBK

D:\>java --version
java 17.0.1 2021-10-19 LTS
Java(TM) SE Runtime Environment (build 17.0.1+12-LTS-39)
Java HotSpot(TM) 64-Bit Server VM (build 17.0.1+12-LTS-39, mixed mode, sharing)

#Abc.java 源代码文件编码是gbk默认没有问题，如果是utf-8在win10系统下如下编译运行就可以解决乱码问题
D:\>javac -encoding utf-8 Abc.java
D:\>java Abc
hello world 中文效果

D:\>java -Dfile.encoding=utf-8 Abc.java
hello world 中文效果
```



#### 1.4.2 安装配置jdk16

* java16 环境变量配置
  * JAVA_HOME  = D:\jdk\jdk-16.0.2
  * CLASSPATH =  %JAVA_HOME%\lib;.
  *   编辑path 添加 %JAVA_HOME%\bin
  * ![image-20210724154440270](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724154440270.png)

### 1.5 HelloWorld程序入门

编写HelloWorld程序，在桌面上建立一个Demo.java文件，用记事本编写内容

```java
public class Demo{
    public static void main(String[] args){
        System.out.println("hello world");
        System.out.println(System.getProperty("java.home"));//javahome
        System.out.println(System.getProperty("java.version"));//java version
        System.out.println(System.getProperty("os.name"));// 获取系统名称 Windows 10
    }
}
```

编译程序

```cmd
# 编译程序
javac Demo.java

# 运行程序类 java Demo.class 错误的
java Demo 

```

java10 版本后可以直接运行源程序

```cmd
C:\Users\Administrator\Desktop>java --version
java 16.0.2 2021-07-20
Java(TM) SE Runtime Environment (build 16.0.2+7-67)
Java HotSpot(TM) 64-Bit Server VM (build 16.0.2+7-67, mixed mode, sharing)

C:\Users\Administrator\Desktop>java -version
java version "16.0.2" 2021-07-20
Java(TM) SE Runtime Environment (build 16.0.2+7-67)
Java HotSpot(TM) 64-Bit Server VM (build 16.0.2+7-67, mixed mode, sharing)

# java 10以后可以直接运行java程序
C:\Users\Administrator\Desktop>java Demo.java
hello world
D:\jdk\jdk-16.0.2
16.0.2
Windows 10
```

```java
public class Demo{
    //String...s  相当于 String[] s  String[] args
    public static void main(String...s){
        System.out.println("hello world 中文字符串");
    }
}
```

```cmd
C:\Users\Administrator\Desktop>javac Demo.java

C:\Users\Administrator\Desktop>java Demo
hello world

C:\Users\Administrator\Desktop>java Demo.java
hello world

C:\Users\Administrator\Desktop>java Demo.java
hello world 中文字符串

C:\Users\Administrator\Desktop>java Demo
hello world

C:\Users\Administrator\Desktop>javac Demo.java

C:\Users\Administrator\Desktop>java Demo
hello world 中文字符串

C:\Users\Administrator\Desktop>
```

![image-20211118101103616](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118101103616.png)

> java 17.0.1 win10系统环境变量配置方式
>
> JAVA_HOME = D:\jdk\jdk-17.0.1
>
> CLASSPATH = .;%JAVA_HOME%\lib
>
> PATH变量添加新变量  .;%JAVA_HOME%\bin;%JAVA_HOME%\lib;

```cmd
D:\>javac -encoding utf-8 A.java

D:\>java A
hello world 中文效果 3*3 = 9
17.0.1
D:\jdk\jdk-17.0.1
```

### 1.6 java开发工具

#### 1.6.1  VSCode

1. 下载vscode,解压 

   ![image-20211118141448609](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118141448609.png)

2. 在解压目录下建立data目录

   ![image-20211118141829268](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118141829268.png)

3. 安装插件

   * Debugger for Java
   * Language Support for Java(TM) by Red Hat

4. 按下ctrl + , 配置一下

   ```json
   //settings.json
   {
       "workbench.startupEditor": "none",
       "editor.mouseWheelZoom": true,
       "files.autoSave": "afterDelay",
       "editor.fontSize": 28,
       "java.home": "d:/jdk/jdk-16.0.1",
       "files.exclude": {
           "**/.classpath": true,
           "**/.project": true,
           "**/.settings": true,
           "**/.factorypath": true
       },
       "workbench.iconTheme": "vscode-icons"
   }
   ```

5. 修改一下 

   C:\vsc\data\extensions\vscjava.vscode-java-debug-0.34.0\scripts\launcher.bat

   ```bat
   @echo off
   cls
   REM Change code page to UTF-8 for better compatibility.
   REM GBK 936 UTF-8 65001
   @chcp.com 936 > NUL 
   
   REM Execute real command passed by args
   %*
   
   ```

   ![image-20210725170027676](https://gitee.com/webrx/wx_note/raw/master/images/20210725170032.png)
   
   建立右键快捷方式：建立增加鼠标右键工具，addright.inf在vsc根目录下，内容如下，然后在windows中右键选择安装
   
   ```ini
   [Version]
   Signature="$Windows NT$"
   
   [DefaultInstall]
   AddReg=VSCode
   
   [VSCode]
   hkcr,"*\\shell\\VSCode",,,"Open with Code"
   hkcr,"*\\shell\\VSCode\\command",,,"""%1%\Code.exe"" ""%%1"" %%*"
   hkcr,"Directory\shell\VSCode",,,"Open with Code"
   hkcr,"*\\shell\\VSCode","Icon",0x20000,"%1%\Code.exe, 0"
   hkcr,"Directory\shell\VSCode\command",,,"""%1%\Code.exe"" ""%%1"""
   ```
   

删除右键 delright.reg 

```init
Windows Registry Editor Version 5.00
[-HKEY_CLASSES_ROOT\*\shell\VSCode]
[-HKEY_CLASSES_ROOT\Directory\shell\VSCode]
```



常用快捷键

```text
  ctrl+shift+w    关闭所有打开的文档窗口（默认不是自己设定的）
  alt+/    代码提示快捷键（默认是ctrl+space)
  ctrl+x    剪切当前行
  ctrl+shift+k    删除当前行
  alt+shift+up(down)    向上复制代码行
  ctrl + d 向下复制当前一行
  alt+up(down)    向上移动代码行
  ctrl++    窗口增大
  ctrl+-    窗口缩小
  ctrl+0(数字键盘)    窗口恢复默认
  ctrl + mouseWheelZoom 设置编辑器字号大小写（settings.json 配置"editor.mouseWheelZoom": true）
  ctrl+/    注释取消注释当前行
  alt+shift+a    块注释
  alt  设置快捷键为ctrl+shift+alt+f1  临时显示菜单
  alt+shift+f    格式化文档
  ctrl+b    打开左活动面板
  ctrl+j    打开关闭面板
  ctrl+`    打开终端面板,可以输入live-server
  ctrl+g 快捷进行指定行代码位置
  ctrl+shift+t 快速翻译当前选中的词汇（需要安装Yao Translate翻译插件）
  F2 修改内容或修改文件名
  ctrl+shift+alt+F12 打开浏览器使用live-serve服务器，直接浏览当前网页
  ctrl+shift+alt+f1 菜单 开关
  ctrl+shift+alt+f2 左侧，活动条 开关
  ctrl+shift+alt+f3 show breadcrumbs 开关
  ctrl+shift+alt+f4 show minimap 开关
  ctrl+shift+alt+F6 状态条开关
  ctrl+shift+alt+u 转换为大写字母
  ctrl+shift+alt+l 转换为小写字母
  ctrl+shift+t 翻译选中的文字
  ctrl+shift+r 翻译选中的文字进行直接替换
```

#### 1.6.2 Eclipse

是开源免费的，有一个优秀的插件myeclipse，功能非常强大，但不免费。

下载:

![image-20210726114727588](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726114727588.png)



![image-20210726120159578](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726120159578.png)

#### 1.6.3 IntelliJ IDEA

第一步：下载idea工具

![image-20210724174221402](https://gitee.com/webrx/wx_note/raw/master/images/image-20210724174221402.png)

![image-20210726151636485](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726151636485.png)

### 作业

1. 自己下载jdk,并配置环境变量，编写一个HelloWorld程序，并编译运行出结果。
2. vscode 编写Helloworld并运行出结果
3. eclipse 建立项目编写Helloworld运行出结果
4. idea 建立项目编写Helloworld运行出结果

### 词典软件项目

> 01 下载组件jar  http://www.jsoup.org

![image-20211118155214622](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118155214622.png)

> 02 建立java项目，给项目添加依赖jar

![image-20211118160052956](https://gitee.com/webrx/wx_note/raw/master/images/image-20211118160052956.png)

> 03 编写程序类，并测试运行

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import org.jsoup.Jsoup;

import javax.swing.*;
import java.io.IOException;

/**
 * <p>Project: untitled2 - Demo
 * <p>Powered by webrx On 2021-11-18 15:47:32
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Demo {
   public static void main(String[] args) throws IOException {
       String w = JOptionPane.showInputDialog("请输入词汇：");
       String u = "http://youdao.com/w/eng/"+w+"/#keyfrom=dict2.index";
       String o = Jsoup.connect(u).get().select("div[class=trans-container]").get(0).text();
       JOptionPane.showMessageDialog(null,o);
   }
}

```

作业：把翻译软件使用有道，改为微软词典

## 第二章 java基本语法

### 2.1 java程序基本结构

> java程序的源码文件是Xxx.java，Xxx代码类名和文件名一致（一般要求首字母大写，从第二个单词首字母大写。`UserINfo StudentBook `
>
> UserInfo.java  userinfo.java Demo.java Book.java public class Book{}

基本结构：

```java
//例一
class User{
	
}

//例二
public class UserInfo{
    
}

//例三
public class Book {
    int age = 50;
    public static void main(String[] args) {
        int i = 50;
        i = 300;
        String name = "李四";
        System.out.println(i);
        System.out.println(name);
        System.out.println("name");
    }
}

//例四 HelloWorld 保存为 HelloWorld.java文件 程序编译后 HelloWorld.class 二进制字节码
public class HelloWorld{
    public static void main(String[] args){
        System.out.println("hello world 中文效果")
    }
}

//例五
package org;
public class HelloWorld{
    //main() 入口方法，一个类中只能一个main方法，代表该类可以直接执行
    public static void main(String[] args) {
        System.out.println("hello world");
    }
}
```

![image-20211119102157188](assets/image-20211119102157188.png)

### 2.2 程序注释

* 单行注释

  ```java
  // 单行注释
  ```

  

* 多行注释

  ```java
  /*
      多行注释
  */
  ```

  

* 文档注释，主要依赖于javadoc.exe，提取java源代码文件中的注解结构，生成html-api 参考文件

  ```java
  /**
   * 文件注释
   * 类注释
   * 属性注释
   * 方法注释
   */
  ```

  ```java
  package com;
  /**
   * @author webrx
   * javac -d . Hello.java
   * java com.Hello
   */
  public class Hello {
  	/**
  	 * 程序入口 
  	 */
      public static void main(String[] args) {
  		// 单行注释
          System.out.println("Hello com");
      }
  }
  
  ```

  idea 生成项目api html

  ![image-20210726173250292](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726173250292.png)

  ![image-20211119143046574](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119143046574.png)
  
  

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - A
 * <p>Powered by Administrator On 2021-07-26 17:49:15
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [w@126.com]
 * @version 1.0
 * @since 16
 */
public class A {
    public static void main(String[] args) {
        System.out.println("Hello world");
    }
}
```

![image-20211119143239146](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119143239146.png)

### 2.3 标识符、关键字、保留字

标识符（identifier）是指用来标识某个实体的一个符号，在不同的应用环境下有不同的含义。在计算机编程语言中，标识符是用户编程时使用的名字，用于给变量、常量、函数、语句块等命名，以建立起名称与使用之间的关系。标识符通常由字母和数字以及其它字符构成。

> 标识符是由字母 数字 下滑线 $组成的，不能数字开头，不能使用空格和特殊的符号，如下红框内的是非法

![image-20201225135009580](https://gitee.com/webrx/wx_note/raw/master/images/image-20201225135009580.png)



保留字(reserved word)，指在高级语言中已经定义过的字，使用者不能再将这些字作为变量名或过程名使用。
保留字包括关键字和未使用的保留字。

关键字则指在语言中有特定含义，成为语法中一部分的那些字。在一些语言中，一些保留字可能并没有应用于当前的语法中，这就成了保留字与关键字的区别。一般出现这种情况可能是由于考虑扩展性。例如，Javascript有一些未来保留字，如abstract、double、goto等等。

> java编程中有没有goto语句？`goto` `const`



![image-20201225140642278](https://gitee.com/webrx/wx_note/raw/master/images/image-20201225140642278.png)



![image-20201225141344492](https://gitee.com/webrx/wx_note/raw/master/images/image-20201225141344492.png)

| **关键字**   | **含义**                                                     |
| ------------ | ------------------------------------------------------------ |
| abstract     | 表明类或者成员方法具有抽象属性                               |
| assert       | 断言，用来进行程序调试                                       |
| boolean      | 基本数据类型之一，声明布尔类型的关键字                       |
| break        | 提前跳出一个块                                               |
| byte         | 基本数据类型之一，字节类型                                   |
| case         | 用在switch语句之中，表示其中的一个分支                       |
| catch        | 用在异常处理中，用来捕捉异常                                 |
| char         | 基本数据类型之一，字符类型                                   |
| class        | 声明一个类                                                   |
| const        | 保留关键字，没有具体含义                                     |
| continue     | 回到一个块的开始处                                           |
| default      | 默认，例如，用在switch语句中，表明一个默认的分支。Java8 中也作用于声明接口函数的默认实现 |
| do           | 用在do-while循环结构中                                       |
| double       | 基本数据类型之一，双精度浮点数类型                           |
| else         | 用在条件语句中，表明当条件不成立时的分支                     |
| enum         | 枚举                                                         |
| extends      | 表明一个类型是另一个类型的子类型。对于类，可以是另一个类或者抽象类；对于接口，可以是另一个接口 |
| final        | 用来说明最终属性，表明一个类不能派生出子类，或者成员方法不能被覆盖，或者成员域的值不能被改变，用来定义常量 |
| finally      | 用于处理异常情况，用来声明一个基本肯定会被执行到的语句块     |
| float        | 基本数据类型之一，单精度浮点数类型                           |
| for          | 一种循环结构的引导词                                         |
| goto         | 保留关键字，没有具体含义                                     |
| if           | 条件语句的引导词                                             |
| implements   | 表明一个类实现了给定的接口                                   |
| import       | 表明要访问指定的类或包                                       |
| instanceof   | 用来测试一个对象是否是指定类型的实例对象                     |
| int          | 基本数据类型之一，整数类型                                   |
| interface    | 接口                                                         |
| long         | 基本数据类型之一，长整数类型                                 |
| native       | 用来声明一个方法是由与计算机相关的语言（如C/C++/FORTRAN语言）实现的 |
| new          | 用来创建新实例对象                                           |
| package      | 包                                                           |
| private      | 一种访问控制方式：私用模式                                   |
| protected    | 一种访问控制方式：保护模式                                   |
| public       | 一种访问控制方式：共用模式                                   |
| return       | 从成员方法中返回数据                                         |
| short        | 基本数据类型之一,短整数类型                                  |
| static       | 表明具有静态属性                                             |
| strictfp     | 用来声明FP_strict（单精度或双精度浮点数）表达式遵循[IEEE 754](https://baike.baidu.com/item/IEEE 754)算术规范 |
| super        | 表明当前对象的父类型的引用或者父类型的构造方法               |
| switch       | 分支语句结构的引导词                                         |
| synchronized | 表明一段代码需要同步执行                                     |
| this         | 指向当前实例对象的引用                                       |
| throw        | 抛出一个异常                                                 |
| throws       | 声明在当前定义的成员方法中所有需要抛出的异常                 |
| transient    | 声明不用序列化的成员域                                       |
| try          | 尝试一个可能抛出异常的程序块                                 |
| void         | 声明当前成员方法没有返回值                                   |
| volatile     | 表明两个或者多个变量必须同步地发生变化                       |
| var          | java10新增的关键字，用来推断类型 var i = 20;                 |
| while        | 用在循环结构中                                               |

java语言有没有,goto const语句？

![image-20210727094639295](https://gitee.com/webrx/wx_note/raw/master/images/image-20210727094639295.png)

### 2.4 变量、常量

* 变量

> 变量：是指值在程序运行期间可以被改变的量。变量用于储存信息。它指向内存的某个单元，而且指明了这块内存有多大。java是强类型，变量必须先声明初始化才可以使用。java程序中，变量名是区分大小写。

* 变量作用域

![image-20201225152800294](https://gitee.com/webrx/wx_note/raw/master/images/image-20201225152800294.png)

* java是严格区分大小写的

(1)类，接口和枚举或对象是首字母大小，如果有多个单词第二个单词首字母大写:User.java UserBook.java

System Scanner Date String 。

(2)变量名，常量名严格区分大小写 java中常量名要求全部大写 AGE。

(3)方法名一般是

setXxx  getXxx isXxxx  getUserName() 动词开头，从第二个单词开始使用大写。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - Var2
 * <p>Powered by webrx On 2021-11-19 15:33:52
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Var2 {
    //声明变量，并初始化
    int a = 3;
    //声明变量
    int b;
    public static void main(String[] args) {
        //System.out.println(a); 不能直接使用

        Var2 v = new Var2();
        System.out.println(v.a);

        //声明整型变量
        int i;

        //给变量初始化赋值
        i = 6;

        //i = "abc"; 错误的，因为java强类型语言
        i = 'a';//正确的，单个号为char字符型，只有一个符号，赋的值为a ascii 97

        //使用这个变量
        System.out.println(i);

        //也叫声明语句，声明并赋值
        String str = "hello";
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - Var3
 * <p>Powered by webrx On 2021-11-19 15:40:12
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Var3 {
    int y = 100;
    public static void main(String[] args) {
        int i = 5;
        System.out.println(i);
        {
            {
                int c = 6;
                System.out.println(i);
                System.out.println(c);
            }
        }
        int c = 7;
        System.out.println(i);
        System.out.println(c);
    }
}

```

> `java10 以后的新版本中，可以使用var推荐类型`

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * <p>Project: javase - Var4
 * <p>Powered by webrx On 2021-11-19 15:43:24
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Var4 {
    public static void main(String[] args) {
        //java 10以后新增了 var关键字，主要用来进行推断类型声明
        var i = 6;
        var str = "hello world";
        //var y; 错误的,不赋值，不能进行推断类型

        //i = new Date();
        var sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar c2 = Calendar.getInstance();
        var c1 = Calendar.getInstance();
    }
}

```

* 常量

  `在开发领域常量一般都要求全大写`

 指在程序执行期间其值不能发生变化的数据。例如数学中的π= 3.1415……又如：整数123，小数1.23，字符’A’，布尔常量  true、false等，程序开发中，常量名一般规则全部大写，声明常量时必须赋值，以后只能使用，不能再赋值，java语言声明常量使用final。

```java
final int AGE = 50;  
System.out.println(Math.PI);
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.io.File;

/**
 * <p>Project: javase - Final1
 * <p>Powered by webrx On 2021-11-19 15:49:50
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Final1 {
    final String NAME = "jack";
    public static void main(String[] args) {
        final int AGE = 18;
        System.out.println(Math.PI);
        //windows \\ 输出一个\
        //linux / 输出一个 /
        System.out.println(File.separator);
    }
}

```



### 2.5 数据类型、类型转换

（1）基本数据类型  byte short int long float double char boolean (8大类型)

​		1、整型 byte short int long

​        2、浮点型 float double

​        3、字符型  char a = ‘y’;

​        4、布尔  boolean  

（2）引用数据类型（对象 类  接口 数组）

![image-20201225155716412](https://gitee.com/webrx/wx_note/raw/master/images/image-20201225155716412.png)

```java
package cn.webrx;
public class Var4 {
	public static void main(String[] args) {
		int a = 18;
		int b = 30;
		//print() println() printf()格式化输出
		System.out.print(a);
		System.out.println(b);
		System.out.printf("%d + %d = %d \n", a, b, a + b);
		System.out.println(a + " + " + b + " = " + (a+b));
	}
}

/*
    //f d l
    //.5f
    //.68d
    //21424L
    
byte short int long float double boolean char

boolean f = true;

char y = ‘c’;

float f1 = .5f;    

double f2 = 10.5d;

long mm = 10;
*/

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - TypeDemo
 * <p>Powered by webrx On 2021-11-22 09:06:59
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class TypeDemo {
    public static void main(String[] args) {
        //java 8 基本类型类型byte short int long float double boolean char
        //byte short int long 整型
        byte b = 127; //-128 - 127 整数范围 1字节
        short s = 32767; //2个字节
        int i = 3;//4byte
        long n = 6;//8byte

        int age = 18;
        byte ba = 18;

        //float double 小数类型
        float f = 1;
        System.out.println(f); //1.0
        System.out.printf("%.5f%n",f);//1.00000
        double d = .567;
        System.out.printf("%.2f%n",d);//0.57

        //boolean 布尔 = 赋值  == 判断是不是一个对象
        boolean bool = true ? 1==3 : !true;
        System.out.println(bool);

        String s1 = new String("yes");
        String s2 = new String("yes");
        System.out.println(s1 == s2);//false

        //char 字符型
        char c1 = '中';
        char c2 = 97;
        int c3 = 'A';
        System.out.println(c1);//中
        System.out.println(c2);//a
        System.out.println(c3);//65
        int c4 = '中';
        System.out.println(c4);//20013
        System.out.println((char)20013);//中

        float f5 = .6f;
        double d6 = 18d;
        long num = 1l; //1L long
    }
}
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - TypeOut
 * <p>Powered by webrx On 2021-11-22 09:25:39
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class TypeOut {
    public static void main(String[] args) {
        System.out.println();//空行
        System.out.print("\n");//空行
        //printf() 格式化输出%03d 代表　3位数字宽度，如果长度不够，则左边补0
        System.out.printf("%03d - %03d",1,1254);//001 - 1254
    }
}

```

* 数据类型转换

* 强制转换

```java
int i = 30;
short n = (short)i;

package cn;

public class Var2 {

	public static void main(String[] args) {
		int a = 5;
		a += 1;
		a = a + 1;
		System.out.println(a);//7
		short b = (short) a;//7
		b += 1;//b 8 (+=可以实现自动的类型转换)
		a = b + 1;//a = 9  b=8  (b+1 结果类型为int 此)
		System.out.println(a);//9
		System.out.println(b);//8
	}
}

```

* 自动隐式转换

```text
自动类型转换
 （也叫隐式类型转换）由小到大 
    (byte，short，char)--int--long--float—double。 
注：这里我们所说的“大”与“小”,并不是指占用字节的多少,而是指表示值的范围的大小。
	例：short s=10;
	     	int i=s;      //将short类型的变量向int类型的变量		//赋值时，（从小到大）实现了自动转换

		注：char c=’A’; 65
		int i=c;(这时会将’A’ 对应的ASCII码值赋值给i)

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - TypeConvert
 * <p>Powered by webrx On 2021-11-22 09:32:02
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class TypeConvert {
    public static void main(String[] args) {
        //数据类型转换
        //1. 自动转换
        int a = 3;
        long b = a; //自动隐式转换
        //2. 强制类型转换
        byte c = (byte)a;//强制转换
        byte c2 = Byte.parseByte(String.valueOf(a));
        System.out.println(c);//3
        System.out.println(c2);//3



        short n = 0;
        n +=1;
        System.out.println(n);//1


        n = (short)(n +1) ;
        System.out.println(n);//2

    }
}

```



### 2.6 运算符表达式

运算符：是一种特殊符号，用以表示数据的运算、赋值和比较。

表达式：使用运算符将运算数据连接起来的符合Java语法规则的式子。

int i = 20*30;

（1）、算术运算符

`+ - * / % ++ --`

```java
int i = 5;
++i; //自身加1
i++;
System.out.println(--i); //自身减1 如果++ -- 在变量前，先执行再使用，如果在后，先使用再自执行。

public class Var4 {
    public static void main(String[] args) {
        int i = 5;
        ++i;
        int b = i--;//b=6 i=5
        System.out.println(i);//5
        System.out.println(b);//6

        System.out.println(6%2);//0 取余数
        System.out.println(3/2);//1 取整商
        System.out.printf("%.3f%n",3/2f);//1.500
        System.out.printf("%.3f",3/2.0);//1.500
    }
}
```

（2）、赋值运算符

`= += -= *= /= %=`

```java
int i = 3;
i++;
i+=5; //相当于 i = i + 5;
int c = i-=2;
System.out.println(--c);
```

![image-20201228144107635](https://gitee.com/webrx/wx_note/raw/master/images/image-20201228144107635.png)

```java
package cn;
public class Var5 {

	public static void main(String[] args) {
		int i = 3;
		i++;
		i += 5; // 相当于 i = i + 5;
		int c = i -= 2;
		System.out.println(--c); //6
	}
}

package cn;
public class Hello {
	public static void main(String[] args) {
		System.out.println("Hello World");

		System.out.println("*".repeat(8));
		int i = 3;
		i += 2;
		i = i + 2;
		System.out.println(i);

		short a = 3;
		a += 2;
		a = (short) (a + 2);
		System.out.println(a);
	}
}

```

（3）、比较运算符

> ​	java  == != > >= < <=  instanceof 

**== 和 = 结果不一样，==是判断等不等，=是赋值**

注1：比较运算符的结果都是boolean型，也就是要么是true，要么是false。
        注2：比较运算符“==”不能误写成“=” ，切记！切记！

![image-20201228164020739](https://gitee.com/webrx/wx_note/raw/master/images/image-20201228164020739.png)

```java
package cn;

import java.util.Calendar;
import java.util.Locale;

public class Var5 {

	public static void main(String[] args) {
		//分时问候
		//var d = new Date();
		//System.out.println(d.getHours());//16
		
		Calendar c =  Calendar.getInstance(Locale.CHINA);
		int hour = c.get(Calendar.HOUR_OF_DAY);//推荐写法
		//int hour = c.get(11);
		//int hour = c.get(0xb); //0x 16进制  a 10 b 11 c 12 f 15
		//if语句是一分支语句，
		if(hour>=12) { //true
			System.out.println("下午好");
		}else {
			System.out.println("上午好");
		}
	}
}

```

（4）、逻辑运算符

`&&逻辑与    ||逻辑域   !逻辑非`

![image-20201228164056822](https://gitee.com/webrx/wx_note/raw/master/images/image-20201228164056822.png)

```
&
|
^
!
&&
||
```

```java
package cn;
import java.util.Calendar;
import java.util.Locale;
public class Var5 {

	public static void main(String[] args) {
		//分时问候
		//var d = new Date();
		//System.out.println(d.getHours());//16
		
		Calendar c =  Calendar.getInstance(Locale.CHINA);
		int hour = c.get(Calendar.HOUR_OF_DAY);//推荐写法
		//int hour = c.get(11);
		//int hour = c.get(0xb); //0x 16进制  a 10 b 11 c 12 f 15
		//if语句是一分支语句，
		//if(hour>=12) { //true
		//	System.out.println("下午好");
		//}else {
		//	System.out.println("上午好");
		//}
		
		//6 - 8 早  8-12 am 12-14 中午  14 - 18 下载 18-19 晚上19-23
		if(hour>=6 && hour<8) {
			System.out.println("早上好");
		}else if(hour>=8 && hour<12) {
			System.out.println("上午好");
		}else if(hour>=12 && hour<14) {
			System.out.println("中午好");
		}else if(hour>=14 && hour<18) {
			System.out.println("下午好");
		}
	}
}

package cn;
public class Var6 {

	public static void main(String[] args) {
		System.out.println(1 == 2);// false
		System.out.println(1 != 2);// true
		System.out.println(!(1 == 2));// true

		// &&短路 &不支持短路
		int a = 1;
		int b = 2;
		if (a==b & ++a == b) {
			System.out.println("yes");
		}
		System.out.println(a);
	}

}

```

java 日期工具类 Date  Calendar类使用

```java
package cn;
import java.util.Calendar;
import java.util.Date;
public class DateTest {
	public static void main(String[] args) {
		var d = new Date();//实例化日期对象
		System.out.println(d.toLocaleString());
		
		System.out.println(d.getTime());//时间戳 是一个long的整数
		System.out.println(System.currentTimeMillis());
		
		var c = Calendar.getInstance();
		System.out.println(c.getTime().getTime());
		
		//1609145876771 是数字，代表一个日期对象
		var d2 = new Date(1609145876771L);
		System.out.printf("%1$tF %1$tT \n",d2);
	}
}

```

（5）、位运算符 >> <<

![image-20201229135956872](https://gitee.com/webrx/wx_note/raw/master/images/image-20201229135956872.png)



![image-20201229140014907](https://gitee.com/webrx/wx_note/raw/master/images/image-20201229140014907.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - Num2816
 * <p>Powered by webrx On 2021-11-22 14:19:16
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Num2816 {
    public static void main(String[] args) {
        int num = 12;
        System.out.println(num);

        //十进制转换为二进制字符串 //1100
        System.out.println(Integer.toBinaryString(num));

        //十进制转换为十六进制 //c
        System.out.println(Integer.toHexString(num));

        //十进制转换为八进制 14
        System.out.println(Integer.toOctalString(num));

        //十六进制转换为 10 FF 255
        System.out.println(Integer.valueOf("ff",16));
        //12 16进制转10
        System.out.println(Integer.valueOf("c",16));
        //12 16进制转10
        System.out.println(Integer.valueOf("0c",16));
        //12 2进制转10
        System.out.println(Integer.valueOf("1100",2));
        //12 8进制转10
        System.out.println(Integer.valueOf("14",8));
    }
}

```



```java
package cn;

public class Bin1 {

	public static void main(String[] args) {
		System.out.println(true & false); //逻辑与
		System.out.println(true && false); //逻辑与 && 支持短路
		
		System.out.println(12&5);//4
		System.out.println(Integer.toBinaryString(12)); //1100
		System.out.println(Integer.toBinaryString(5));//  0101  0100
		
		//参数是2进制输出是10进制 0b
		System.out.println(0b100);//4
		System.out.println(0xff);//255
		//2进制
        System.out.println(0b1100);
        //16进制 0x
        System.out.println(0x0c);
        System.out.println(0xc);
        //8进制 0开头
        System.out.println(070); //56
        System.out.println(Integer.valueOf("70",8)); //56

        //12  014是8进制代表10进制的12 
        System.out.println(014);	
		//二进制 1  1  2  10
	}

}

```

```java
package cn;

public class Bin2 {

	public static void main(String[] args) {
		//~ & | >> << 
		int a = 12;
		System.out.println(Integer.toBinaryString(a)); //1100
		int b = 5;
		System.out.println(Integer.toBinaryString(b));//0101
		
		System.out.println(Integer.valueOf("1100",2));//12
		System.out.println(Integer.valueOf("0101",2));//5
		
		System.out.println(a&b);//4
		System.out.println(a|b);//13
	}
}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javase - Num11
 * <p>Powered by webrx On 2021-11-22 14:40:51
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Num11 {
    public static void main(String[] args) {
        System.out.println(2 & 3); //2 位与运算
        System.out.println(Integer.toBinaryString(2));
        //10
        // 0000 0010 2
        // 0000 0010 & 0000 0011  = 0000 0010 (2)
        // 0000 0011 3
        // 0000 0010 2

        System.out.println(2==2 & 3==1); //false 逻辑与 不短路

        System.out.println(3>>1<<3);
        //3 0000 0011 >> 1
        //3 0000 0001 >> 1 << 3
        //  0000 1000
        System.out.println(0b1000);

    }
}

```



![image-20201229140934665](https://gitee.com/webrx/wx_note/raw/master/images/image-20201229140934665.png)

位移运算，实现IP地址和数字的转换

```java
package cn;
import java.net.InetAddress;
import java.net.UnknownHostException;
public class IpUtils {
	public static void main(String[] args) throws UnknownHostException {
		// 将ipv4 格式转换为long类型的数字
		String ip = "58.83.160.156";
		String[] ips = ip.split("\\."); // 字符串[58,83,160,156]
		long num = 0;
		for (int i = 0; i < ips.length; i++) {
			num += Integer.parseInt(ips[i]) * Math.pow(256, ips.length - i - 1);
		}
		System.out.println(num);
		// 978559132 转换为ipv4字符串
		long nn = 978559132L;
		byte[] bys = new byte[] { (byte) (nn >> 24), (byte) (nn >> 16), (byte) (nn >> 8), (byte) nn };
		System.out.println(InetAddress.getByAddress(bys).getHostAddress());
	}

}

```

### 2.7 格式化

>System.out.printf();
>
>String.format();
>
>DecimalFormat
>
>SimpleDateFormat
>
>NumberFormat

String类的format()方法用于创建格式化的字符串以及连接多个字符串对象。熟悉C语言的同学应该记得C语言的sprintf()方法，两者有类似之处。format()方法有两种重载形式。
format(String format, Object... args) 新字符串使用本地语言环境，制定字符串格式和参数生成格式化的新字符串。

format(Locale locale, String format, Object... args) 使用指定的语言环境，制定字符串格式和参数生成格式化的字符串。

显示不同转换符实现不同数据类型到字符串的转换，如图所示。

![image-20211122151828692](https://gitee.com/webrx/wx_note/raw/master/images/image-20211122151828692.png)

![image-20211122151905164](https://gitee.com/webrx/wx_note/raw/master/images/image-20211122151905164.png)

日期和事件字符串格式化
在程序界面中经常需要显示时间和日期，但是其显示的 格式经常不尽人意，需要编写大量的代码经过各种算法才得到理想的日期与时间格式。字符串格式中还有%tx转换符没有详细介绍，它是专门用来格式化日期和时 间的。%tx转换符中的x代表另外的处理日期和时间格式的转换符，它们的组合能够将日期和时间格式化成多种格式。

常见日期和时间组合的格式，如图所示。

![image-20211122151941946](https://gitee.com/webrx/wx_note/raw/master/images/image-20211122151941946.png)

和日期格式转换符相比，时间格式的转换符要更多、更精确。它可以将时间格式化成时、分、秒甚至时毫秒等单位。格式化时间字符串的转换符如图所示。![image-20211122152027844](https://gitee.com/webrx/wx_note/raw/master/images/image-20211122152027844.png)

```java
public static void main(String[] args) {
        Date d = new Date();
        System.out.printf("%tF %tT %tc%n",d,d,d);
        System.out.printf("%tF %1$tT %1$tc %1$tp %1$tA%n",d);
        System.out.printf("%tF %1$tT %1$tA%n",d);
        String str = String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS_%1$ts.jpg%n",d);
        System.out.println(str);
        System.out.println();
        System.out.printf("%tF %1$tT %1$tA%n",System.currentTimeMillis());
        System.out.printf("%1$tY年%1$tm月%1$td日 %1$tH时%1$tM分%1$tS秒 %1$tA",System.currentTimeMillis());


    }
```

![image-20211123094008047](assets/image-20211123094008047.png)



```java
public class Ex1 {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.err.println("hello");
        System.out.printf("%d + %d = %d \r\n", a, b, a + b);
        System.out.println(a + " + " + b + " = " + (a + b));
        String name = "李四";
        int age = 5;
        String addr = "河南省郑州市科学大道88号";
        double money = 1000.98;

        System.out.println("姓名：" + name + "，年龄：0" + age + "岁，金额：" + money + "元，家庭地址：" + addr + "。");
        System.out.printf("姓名：%s，年龄：%02d岁，金额：%.1f元，家庭地址：%s。%n", name, age, money, addr);

        System.out.printf("#%02x%02x%02x",255,0,0); //#ff0000

        long time = System.currentTimeMillis();
        System.out.printf("%n %1$tF %1$tT",time);
        //Locale.setDefault(Locale.US);
        System.out.printf("%1$tY年%1$tm月%1$td日 %1$tT %1$tA\n",time);
        System.out.printf("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS.jpg",time);
    }
}
```

```java
public class Demo {
    public static void main(String[] args) {
        //系统时间戳
        long t1 = System.currentTimeMillis();
        //java.util.Date
        Date t2 = new Date();
        //java.util.Calendar
        var t3 = Calendar.getInstance();
        //java8 新增的日期时间API printf() String.format() %tc不支持 LocalDateTime
        LocalDateTime t4 = LocalDateTime.now();
        System.out.printf("%tc%n", t1);
        System.out.printf("%tc%n", t2);
        System.out.printf("%tc%n", t3);
        System.out.printf("%tF%n", t3);
        System.out.printf("%tT%n", t3);
        System.out.printf("%tr%n", t3);
        System.out.printf("%1$tF %1$tT%n", t3);
        System.out.printf("%1$tY年%1$tm月%1$td日 %1$tT %1$tA\n",t2);

        //设置新的日期内容
        t3.set(1, 1985);
        t3.set(Calendar.MONTH,6-1);
        t3.set(5,9);
        t3.set(Calendar.HOUR_OF_DAY,11);
        t3.set(Calendar.MINUTE,20);
        t3.set(Calendar.SECOND,55);
        System.out.printf("%1$tY年%1$tm月%1$td日 %1$tT %1$tA\n",t3);
        
    }
}
```

SimpleDateFormat 格式化日期 `yyyy-MM-dd HH:mm:ss`

![img](https://gitee.com/webrx/wx_note/raw/master/images/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0NjI2MDk3,size_16,color_FFFFFF,t_70.png)

String.format() 字符串对象格式化方法，参数和System.out.printf() 一样

```java
public class Ex2 {
    public static void main(String[] args) {
        //java.lang.String 有一个format() 方法，格式并返回字符串信息
        String s1 = String.format("%1$tF %1$tT", System.currentTimeMillis());
        System.out.println(s1);

        String s2 = String.format("#%02x%02x%02x", 255, 0, 0);
        System.out.println(s2);

        String s3 = String.format("%02d", 8);
        System.out.println(s3);
        String s4 = String.format("%.2f", 30.12348);
        System.out.println(s4);

        //格式化日期是 %tF %tT %tA %tr
        SimpleDateFormat abc = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = new Date();
        System.out.println(abc.format(d));
    }
}

在Java中，数字输出时，默认采用十进制输出。如果想要输出二进制，八进制和十六进制，需要一些特殊的方法。本文总结了一部分可以实现的方法如下：

1. 使用Integer.toXXXString()

说明：

二进制 Integer.toBinaryString()
八进制 Integer.toOctalString()
十六进制 Integer.toHexString()

代码示例：

int num1 = 25;

// 十进制输出(默认)

System.out.println("在十进制中，num1 = " + num1);

// 二进制输出

System.out.println("在二进制中，num1 = " + Integer.toBinaryString(num1));

// 八进制输出

System.out.println("在八进制中，num1 = " + Integer.toOctalString(num1));

// 十六进制输出

System.out.println("在十六进制中，num1 = " + Integer.toHexString(num1));

输出结果：

在十进制中，num1 = 25
在二进制中，num1 = 11001
在八进制中，num1 = 31

在十六进制中，num1 = 19

2. 使用DecimalFormat，输出固定长度的二进制，八进制和十六进制

说明：

你可能已经发现，使用Integer.toXXXString()输出的二进制，八进制和十六进制结果中，前面的0默认是不显示的，但是有的时候，我们可能需要输出固定长度的结果，例如00011001而非11001，怎样在结果的前方自动补齐呢？

可以使用DecimalFormat这个类来实现。

把上面的代码示例修改一下：

代码示例：

int num1 = 25;

// 输出固定长度为8的结果

DecimalFormat df = new DecimalFormat("00000000");

// 十进制输出(默认)

System.out.println("在十进制中，num1 = " + df.format(num1));

// 二进制输出
String num1InBinaryStr = Integer.toBinaryString(num1);
Integer num1InBinaryInt = Integer.valueOf(num1InBinaryStr);

System.out.println("在二进制中，num1 = " + df.format(num1InBinaryInt));

// 八进制输出

String num1InOctalStr = Integer.toOctalString(num1);

Integer num1InOctalInt = Integer.valueOf(num1InOctalStr);

System.out.println("在八进制中，num1 = " + df.format(num1InOctalInt));

// 十六进制输出

String num1InHexStr = Integer.toHexString(num1);

Integer num1InHexInt = Integer.valueOf(num1InHexStr);

System.out.println("在十六进制中，num1 = " + df.format(num1InHexInt));

输出结果：

在十进制中，num1 = 00000025
在二进制中，num1 = 00011001
在八进制中，num1 = 00000031
在十六进制中，num1 = 00000019

3. 使用printf()输出固定长度的八进制和十六进制

说明：

还有一种方法，可以输出固定长度的八进制和十六进制结果，就是printf()，但是这个方法不能输出二进制。

代码示例：

int num1 = 25;

// 十进制输出(默认)

System.out.printf("在十进制中，num1 = %08d\n", num1);

// 八进制输出

System.out.printf("在八进制中，num1 = %08o\n", num1);

// 十六进制输出  %02x

System.out.printf("在十六进制中，num1 = %08x\n", num1);

输出结果：

在十进制中，num1 = 00000025

在八进制中，num1 = 00000031

在十六进制中，num1 = 00000019
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * <p>Project: javase - Format4
 * <p>Powered by webrx On 2021-11-23 10:26:51
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Format4 {
    public static void main(String[] args) {

        //10.00 020 ff 09
        System.out.printf("%.2f %03d %02x %02x%n",10f,20,255,9);

        //124,234,212.57
        System.out.printf("%,.2f%n",124234212.567);

        NumberFormat nf = NumberFormat.getPercentInstance();
        //20%
        System.out.println(nf.format(.2));

        nf = NumberFormat.getCurrencyInstance(Locale.CHINA);
        System.out.println(nf.format(100.56));

        nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        System.out.println(nf.format(1242342.2341243));

        DecimalFormat df = new DecimalFormat("#,##0.##");
        System.out.println(df.format(1234246.55555));

    }
}

```





作业：

格式输出 ： 2021年11月22日 15:57:30 星期一

```java
//2021-11月-日 15:57:30 星期一
System.out.printf("%tF %1$tT %1$tA%n",System.currentTimeMillis());

//2021年11月22日 15时57分30秒 星期一
System.out.printf("%1$tY年%1$tm月%1$td日 %1$tH时%1$tM分%1$tS秒 %1$tA",System.currentTimeMillis());

//2021年11月22日 15:57:30 星期一
System.out.printf("%1$tY年%1$tm月%1$td日 %1$tT %1$tA",System.currentTimeMillis());

//2021年11月23日 09:51:59 星期二 上午
SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss E a");
System.out.println(sdf.format(new Date()));
```





### 2.8 日期工具类

java.util.Date

```java
package cn.webrx.example;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class Date01 {
    public static void main(String[] args) {
        Date d = new Date();

        //时间戳 long 数字
        long t1 = d.getTime();
        long t2 = System.currentTimeMillis();

        //将long转成Date
        Date d3 = new Date(t2);

        Calendar c = Calendar.getInstance();

        c.set(2021, 5 - 1, 18);

        //将Calendar 日历类实例，转成Date
        Date d2 = c.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy年MM月dd日");
        System.out.println(s.format(d));
        System.out.println(s.format(d2));
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Project: javase - Date1
 * <p>Powered by webrx On 2021-11-23 14:07:19
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Date1 {
    public static void main(String[] args) {
        //java.util.Date
        //1637648060786
        Date d = new Date();
        System.out.println(d.getTime());

        //java.util.Calendar
        Calendar c = Calendar.getInstance();
        System.out.println(c.getTimeInMillis());

        //系统时间戳，是一个long整数
        long time = System.currentTimeMillis();
        System.out.println(time);

        //格式化，字符串与日期对象的转换的
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Project: javase - Date2
 * <p>Powered by webrx On 2021-11-23 14:14:39
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Date2 {
    public static void main(String[] args) {
        //java10 可以使用var关键推荐类型直接声明
        System.out.println(Integer.MAX_VALUE);
        //java 中最大的int 2147483647

        //var d = new Date(); 当前系统的时间
        var d = new Date(1637648060786l);//指定的时间
        // System.out.println(d.getTime());
        System.out.println(d.getMonth()+1);


        var s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(s.format(d));

        //实例化（将一个声明好类，new实例进行使用）
        var c = Calendar.getInstance();
        System.out.println(c.get(Calendar.MONTH)+1);
        System.out.println(c.get(2)+1);
        //Calendar实例转换为Date实例
        var date = c.getTime();

        int yy = c.get(Calendar.YEAR);
        int mm = c.get(Calendar.MONTH)+1;
        int dd = c.get(Calendar.DATE);
        int hh = c.get(Calendar.HOUR_OF_DAY);
        int mi = c.get(Calendar.MINUTE);
        int ss = c.get(Calendar.SECOND);
        //当前日历增加10天，产生10天后日期
        c.add(Calendar.DAY_OF_MONTH,10);

        //1-7 日 一 二 三 四 五 六 七
        System.out.println(c.get(Calendar.DAY_OF_WEEK));
        //2021年11月23日 14:34:51
        System.out.printf("%d年%d月%d日 %d:%d:%d%n",yy,mm,dd,hh,mi,ss);

        //2021-11-23 14:34:51 星期二
        System.out.printf("%1$tF %1$tT %1$tA",c);


    }
}

```

```java
public class Date02 {
    public static void main(String[] args) throws ParseException {
        //使用Date 导入并实例化，java.util.Date
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(d));
        System.out.println(d.getTime()); //返回一个时间戳

        long now = d.getTime() + (1000 * 60 * 60 * 24 * 10);
        d.setTime(now);
        System.out.println(sdf.format(d));

        //计算两个日期之间相关的天数
        Date birth = sdf.parse("2001-7-20 0:0:0");
        //将字符串解析转换为Date
        Date ddd = new Date();

        long days = (ddd.getTime() - birth.getTime())/1000/60/60/24;
        System.out.println(days);
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Project: javase - Date3
 * <p>Powered by webrx On 2021-11-23 14:52:18
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Date3 {
    public static void main(String[] args) {
        //java 10
        var d = Calendar.getInstance();
        //System.out.printf("%1$tF %1$tT %1$tA%n",d);
        //时间一 1985-10-20 18:06:23
        //时间二 now

        long t2 = d.getTimeInMillis();

        d.set(Calendar.YEAR,2021);
        d.set(Calendar.MONTH,11-1);
        d.set(Calendar.DAY_OF_MONTH,22);
        d.set(Calendar.HOUR_OF_DAY,0);
        d.set(Calendar.MINUTE,0);
        d.set(Calendar.SECOND,0);
        //System.out.printf("%1$tF %1$tT %1$tA%n",d);
        long t1 = d.getTimeInMillis();

        //3万天 （13182)
        System.out.println((t2-t1)/(1000*60*60*24));

        var c = Calendar.getInstance();
        //7天前日期
        c.add(Calendar.DAY_OF_MONTH,-7);
        System.out.printf("%1$tF %1$tT %1$tA%n",c);

        //7天后日期
        c.add(Calendar.DAY_OF_MONTH,14);
        System.out.printf("%1$tF %1$tT %1$tA%n",c);
        //当前月第一天
        c.set(Calendar.DAY_OF_MONTH,1);
        System.out.printf("%1$tF %1$tT %1$tA%n",c);

        //当前月最后一天
        c.set(Calendar.MONTH,c.get(Calendar.MONTH)+1);
        c.set(Calendar.DAY_OF_MONTH,0);
        System.out.printf("%1$tF %1$tT %1$tA%n",c);


        //1790-01-01 08:00:00
        var dd = new Date(0);
        var sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(dd));

        //9999-12-31 23:59:59
        var md = new Date(9999-1900,12-1,31,23,59,59);
        System.out.println(sdf.format(md));
    }
}

```

```java
public class Date03 {
    public static void main(String[] args) throws ParseException {
        Locale.setDefault(Locale.US);
        Calendar c = Calendar.getInstance();

        c.set(2021, 7 - 1, 27);
        //year
        int year = c.get(1);
        System.out.println(year);

        int month = c.get(Calendar.MONTH) + 1;
        System.out.println(month);

        int day = c.get(Calendar.DAY_OF_MONTH);
        System.out.println(day);

        int hh = c.get(Calendar.HOUR);//12小时
        hh = c.get(Calendar.HOUR_OF_DAY);//24小时
        System.out.println(hh);

        int mm = c.get(Calendar.MINUTE);
        System.out.println(mm);
        int ss = c.get(Calendar.SECOND);
        System.out.println(ss);
        int wk = c.get(Calendar.DAY_OF_WEEK);
        //1 星期日 2 星期一  3星期二 4 星期三 5 星期四 6星期五 7星期六
        //System.out.println(wk);
        if (wk == 1) {
            System.out.println("今日是星期日");
        } else if (wk == 2) {
            System.out.println("今日是星期一");
        } else if (wk == 3) {
            System.out.println("今日是星期二");
        } else if (wk == 4) {
            System.out.println("今日是星期三");
        } else if (wk == 5) {
            System.out.println("今日是星期四");
        } else if (wk == 6) {
            System.out.println("今日是星期五");
        } else {
            System.out.println("今日是星期六");
        }


        Locale.setDefault(Locale.CHINA);
        System.out.printf("%tA%n",c);

        Date d = new Date();
        //0 星期日 1 星期一 6 星期六
        int ww = d.getDay();
        System.out.println(ww);
    }
}

```

### 2.9 随机工具类

`java.util.Random类`

```java
public class Random1 {
    public static void main(String[] args) {
        //随机id(UUID)
        UUID id = UUID.randomUUID();
        System.out.println(id);
        System.out.println(id.toString().length());//36
        //19485389-9d53-4466-8886-82863d1c2085
        //093c3b05-67fb-4a70-9763-ac6d739d46cb.jpg

        //随机类Random
        Random rand = new Random();
        //true false 真假随机
        System.out.println(rand.nextBoolean());//false true

        //0 - 5随机整数
        System.out.println(rand.nextInt(6));

        //Math.random() 返回double

        //随机颜色
        int rr = rand.nextInt(256);
        int gg = rand.nextInt(256);
        int bb = rand.nextInt(256);
        System.out.printf("#%02x%02x%02x%n",rr,gg,bb);

        System.out.println(rand.nextDouble());
        System.out.println(rand.nextFloat());
        System.out.println(rand.nextLong());
        System.out.println(rand.nextInt());

        System.out.println(rand.nextGaussian());

    }
}

```

```java
public class Random2 {
    public static void main(String[] args) {
        String[] stus = new String[]{"李四", "赵六", "张三", "王五", "李强", "jack", "james gosling"};
        int size = stus.length;
        System.out.println(size);
        var rand = new Random();
        int index = rand.nextInt(stus.length);

        //index 5
        String info = String.format("有%d人学生，本次选中的学生是：%s", size, stus[index]);
        System.out.println(info);

    }
}
```

作业：利用随机生成一个1985-10-20 08:00:00 - now 之间的随机日期

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * <p>Project: javase - Ex1
 * <p>Powered by webrx On 2021-11-24 09:06:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Ex1 {
    public static void main(String[] args) throws ParseException {
        //a-b 之间随机日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        var a =  "2021-01-01 00:00:00";
        var d1 = sdf.parse(a).getTime();
        //System.out.printf("%1$tF %1$tT%n",946656000000l);

        var b =  "2021-11-24 09:23:30";
        var d2 = sdf.parse(b).getTime();
        //System.out.printf("%1$tF %1$tT%n",1451577599000L);
        var r = new Random();

        // 0 - num 
        var num = d2-d1+1;
        var d3 = d1 + (long)(r.nextDouble()*num);
        System.out.printf("%1$tF %1$tT%n",d3);
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * <p>Project: javase - Ex2
 * <p>Powered by webrx On 2021-11-24 09:29:00
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Ex2 {
    public static void main(String[] args) throws ParseException {
        System.out.printf("%1$tF %1$tT %n",0l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(int i=0;i<=15;i++)
        System.out.printf("%1$tF %1$tT %n",nextLong(sdf.parse("2021-01-01 00:00:00").getTime(),new Date().getTime()));
    }

    /**
     * 静态方法，用于生成指定范围的long
     * @param a
     * @param b
     * @return
     */
    public static long nextLong(long a,long b){
        var rand = new Random();
        return (long)(rand.nextDouble() * (b-a+1) + a);
    }
}

```



### 2.10 Math工具类

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.example;

/**
 * <p>Project: javaseapp - Math01
 * <p>Powered by webrx On 2021-07-28 16:45:37
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Math01 {
    public static void main(String[] args) {
        //java.lang.Math 构造方法是私有，不让new
        double pi = Math.PI;
        System.out.println(pi);
        System.out.println(Math.random());
        System.out.println(Math.round(1.4));//1
        System.out.println(Math.round(1.5));//2
        System.out.println(Math.floor(1.4));//1.0
        System.out.println(Math.floor(1.5));//1.0
        System.out.println(Math.ceil(1.4));//2.0
        System.out.println(Math.ceil(1.5));//2.0
        System.out.println(Math.pow(2,3));
        System.out.println(Math.max(10,20));//20
        System.out.println(Math.min(10,20));//10
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>Project: javaseapp - Math01
 * <p>Powered by webrx On 2021-07-28 16:45:37
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Math02 {
    public static void main(String[] args) throws ParseException {
        //System.out.println(Math.random());
        //System.out.println(Math.random());
        //System.out.println(Math.random());
        //System.out.println(Math.random());
        //产生0-9 随机数
        for (int i = 1; i <= 20; i++) {
            //0 - 9 随机整数
            int num = (int) Math.round(Math.random() * (9 - 0)) + 0;
            // 0.0000001 * 9   0.000009  0
            // 0.999999924 *9  8.1   8
            System.out.printf("%d ", num);
        }

        //编写程序，输出2021-07-01 0:0:0 到 now 之间的随机Date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = sdf.parse("2021-7-1 0:0:0");
        long start = d.getTime();
        long now = System.currentTimeMillis();

        long time = Math.round(Math.random() * (now - start)+ start) ;
        System.out.println(sdf.format(time));

        //随机16进制颜色
        int red = (int)Math.round(Math.random()*255);
        int green = (int)Math.round(Math.random()*255);
        int blue = (int)Math.round(Math.random()*255);
        String color = String.format("#%02x%02x%02x",red,green,blue);
        System.out.println(color);
    }
}
```



### 作业：

1. 计算除数余数表达式

   ```text
   #4 ÷＋－×÷
   4 ÷ 2 = 2
   4 ÷ 3 = 1 ......1
   ```

   ```java
   package cn.webrx;
   public class Ex3 {
       public static void main(String[] args) {
           int a = Integer.valueOf(args[0]);
           int b = Integer.valueOf(args[1]);
           if (a % b == 0) {
               System.out.printf("%2d ÷ %2d = %d", a, b, a / b);
           }else{
               System.out.printf("%2d ÷ %2d = %d ... %d", a, b, a / b,a%b);
           }
       }
   }
   
   ```

   

2. 分时显示早上好 上午好 中午好 下午好 晚上好效果

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   import java.util.Calendar;
   
   /**
    * <p>Project: javase - Ex4
    * <p>Powered by webrx On 2021-11-24 10:24:31
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 17
    */
   public class Ex4 {
       public static void main(String[] args) {
           var c = Calendar.getInstance();
           var h = c.get(Calendar.HOUR_OF_DAY);
           //早上 5-8  上午 8-12 中午　12-14 下午　14-18 晚上 18-23 0-2 午夜 3-5 凌晨
           h = 21;
           if(h>=5 && h<8){
               System.out.println("早上好");
           }else if(h>=8 && h<12){
               System.out.println("上午好");
           }else if(h>=12 && h<14){
               System.out.println("中午好");
           }else if(h>=14 && h<18){
               System.out.println("下午好");
           }else if(h>=18 && h<=23){
               System.out.println("晚上好");
           }else if(h>=0 && h<3){
               System.out.println("午夜，多注意身体");
           }else if(h>=3 && h<5){
               System.out.println("凌晨好");
           }
       }
   }
   
   ```
   
   

### 掌握

* ```
  java.util.Calendar
  java.lang.String
  java.util.Date 
  java.lang.Math
  java.util.Random
  java.text.SimpleDateFormat
  基本类型 byte short int long float double char boolean 
  var 是java10 新的关键字
  算术 + - * / % ++ -- 
  赋值 = += -=
  比较 > >= < <= !=
  逻辑 && || !
  移位 >> <<
  ```



## 第三章 流程控制语句

### 3.1 idea 项目操作

![image-20210728175218413](https://gitee.com/webrx/wx_note/raw/master/images/image-20210728175218413.png)

![image-20211124103539102](assets/image-20211124103539102.png)

![image-20210728175337908](https://gitee.com/webrx/wx_note/raw/master/images/image-20210728175337908.png)

### 3.2 java 输入输出

#### 3.2.1 输入

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import javax.swing.*;
import java.io.Console;
import java.util.Scanner;

/**
 * <p>Project: javaseapp - Input1
 * <p>Powered by webrx On 2021-07-29 09:29:05
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Input1 {
    public static void main(String[] args) {
        //0. args 参数输入  java Demo 10 20  args[0] 10   args[1] 20
        
        //1. java.util.Scanner
        //var sc = new Scanner(System.in);
        //System.out.print("请输入姓名：");
        //String name = sc.nextLine();
        //System.out.printf("%n欢迎你：%s。",name);

        //2. JOptionPane 输入内容确定就字符串值，只要不是确定都是null
        //String w = JOptionPane.showInputDialog("请输入词汇：");
        //String w = JOptionPane.showInputDialog("请输入词汇", "word");
        //String w = JOptionPane.showInputDialog(null, "请输入词汇", "超级词典",JOptionPane.QUESTION_MESSAGE);
        //System.out.println(w);

        //3. 安全输入
        //在控制台下,windows 控制台默认是GBK 936
        // chcp.com 65001 > NUL 设置控制台为utf8-8   也可以 chcp.com 936 > NUL 设置为GBK
        //设置为utf-8 java Input1.java 就没有乱码。
        Console con = System.console();
        String name = con.readLine("请输入姓名：");
        String password = new String(con.readPassword("请输入密码："));
        System.out.println(name);
        System.out.println(password);
    }
}
```

![image-20211124142432003](https://gitee.com/webrx/wx_note/raw/master/images/image-20211124142432003.png)



![image-20210729095727143](https://gitee.com/webrx/wx_note/raw/master/images/image-20210729095727143.png)

![image-20210729095802759](https://gitee.com/webrx/wx_note/raw/master/images/image-20210729095802759.png)

#### 3.2.2 输出

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import org.w3c.dom.ls.LSOutput;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * <p>Project: javaseapp - Out1
 * <p>Powered by webrx On 2021-07-29 11:15:27
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Out1 {
    public static void main(String[] args) throws FileNotFoundException {
        //控制台输出
        System.out.println();
        System.out.print("\n");
        System.out.print("a");
        System.out.print("b");
        System.out.println("java");
        System.out.printf("%s%d%f%tF%n","java",20,1.5,System.currentTimeMillis());
        String info = String.format("%1$tY年%1$tm月%1$td日",System.currentTimeMillis());
        System.out.println(info);
        System.err.println("hello error message");

        //对话框输出
        JOptionPane.showMessageDialog(null, "hello world");
        JOptionPane.showMessageDialog(null, "132", "showuser", JOptionPane.ERROR_MESSAGE);

        //文件输出，将内容输出到文件中
        //PrintWriter out = new PrintWriter("c:/user.log");  每次覆盖文件

        //true 代表追加文件写文件
        PrintWriter out = new PrintWriter(new FileOutputStream("c:/user.log", true));
        out.printf("今日日期：%1$tF %1$tT %1$tA %n", System.currentTimeMillis());
        //out.append(String.format("今日日期：%1$tF %1$tT %1$tA %n", System.currentTimeMillis()));
        out.close();
    }
}
```

### 3.3 分支语句

#### 3.3.1 if else if else if else

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - If01
 * <p>Powered by webrx On 2021-07-29 14:47:19
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class If01 {
    public static void main(String[] args) {
        if (true) System.out.println("true");
        //规范的代码编写，一行语句也要加{}
        if (!false) {
            System.out.println("!false");
        }
        if (true && false)
            System.out.println("ok1");

        System.out.println("ok2");
        System.out.println("ok3");
        System.out.println("-".repeat(50));
        //1==3 false
        if(1==3){
            //条件判断为true 要执行的
            System.out.println("==");
            System.out.println("==");
        }else{
            //否则要执行的
            System.out.println("!=");
            System.out.println("!=");
        }

    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Random;

/**
 * <p>Project: javaseapp - If02
 * <p>Powered by webrx On 2021-07-29 14:54:33
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class If02 {
    public static void main(String[] args) {
        Random rand = new Random();
        //(if())   (else if())  (else)
        if (rand.nextBoolean()) {
            System.out.println("one");
        } else if (rand.nextBoolean()) {
            System.out.println("two");
        } else if (rand.nextBoolean()) {
            System.out.println("three");
        } else if (rand.nextBoolean()) {
            System.out.println("four");
        } else {
            System.out.println("other");
        }
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Calendar;
import java.util.Date;

/**
 * <p>Project: javaseapp - If03
 * <p>Powered by webrx On 2021-07-29 14:59:25
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class If03 {
    public static void main(String[] args) {
        var d = new Date();
        int day = d.getDay();
        if (day == 0) System.out.println("星期日");
        if (day == 1) System.out.println("星期一");
        if (day == 2) System.out.println("星期二");
        if (day == 3) System.out.println("星期三");
        if (day == 4) System.out.println("星期四");
        if (day == 5) System.out.println("星期五");
        if (day == 6) System.out.println("星期六");

        //改进程序
        if (day == 1) {
            System.out.println("星期一");
        } else if (day == 2) {
            System.out.println("星期二");
        } else if (day == 3) {
            System.out.println("星期三");
        } else if (day == 4) {
            System.out.println("星期四");
        } else if (day == 5) {
            System.out.println("星期五");
        } else if (day == 6) {
            System.out.println("星期六");
        } else {
            System.out.println("星期日");
        }
        System.out.println("今日是：星期" + "日一二三四五六".charAt(day));

        var c = Calendar.getInstance();
        int wk = c.get(Calendar.DAY_OF_WEEK) - 1;
        System.out.println("今日是：星期" + "日一二三四五六".charAt(wk));

        System.out.printf("今日是：%tA%n", System.currentTimeMillis());


    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Calendar;

/**
 * <p>Project: javaseapp - If03
 * <p>Powered by webrx On 2021-07-29 14:59:25
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class If04 {
    public static void main(String[] args) {
        var c = Calendar.getInstance();
        c.set(2016,8-1,1);
        int m = c.get(Calendar.MONTH) + 1;
        int d = c.get(Calendar.DATE);
        System.out.println(m);
        System.out.println(d);
        if (m == 5 && d == 1) {
            System.out.println("五一劳动节...");
        }else if(m == 7 && d == 1) {
            System.out.println("建党节...");
        }else if(m == 8 && d == 1) {
            System.out.println("建军节...");
        }

        int year = c.get(Calendar.YEAR);
        System.out.println("猴鸡狗猪鼠牛虎兔龙蛇马羊".charAt(year%12));

        //if(1) System.out.println("error1");
        //if(c=1) System.out.println("error2");
        //if(c==1) System.out.println("error3");
        //if() xxx
        //if(){xxx}


    }
}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Calendar;

/**
 * <p>Project: javase - If3
 * <p>Powered by webrx On 2021-11-25 09:09:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class If3 {
    public static void main(String[] args) {
        var c = Calendar.getInstance();
        var h = c.get(Calendar.HOUR_OF_DAY);
        //01 if语句
        if (h < 12) {
            System.out.println("上午");
        } else {
            System.out.println("下午");
        }

        //02 格式化输出实现
        System.out.printf("%tp%n", c);

        //03 三元运算表达式
        System.out.println(h < 12 ? "上午" : "下午");
    }
}

```

作业：

1. 利用if语句，日期工具类，编写分时问候（上午好）
2. 利用if输出星期几
3. 利用if语句输出指定年月的天数（需要判断闰年）

#### 3.3.2 switch case default

switch 多分支开关语句，基本写法

```java
var c = Calendar.getInstance();
//1 - 7
int w = c.get(Calendar.DAY_OF_WEEK);
String wk = "";
switch (w) {
    case 2:
        wk = "星期一";
        break;
    case 3:
        wk = "星期二";
        break;
    case 4:
        wk = "星期三";
        break;
    case 5:
        wk = "星期四";
        break;
    case 6:
        wk = "星期五";
        break;
    case 7:
        wk = "星期六";
        break;
    default:
        wk = "星期日";
        break;
}
//...
System.out.println(wk);
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Scanner;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Switch2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int year = 2021;
        int month = 2;
        System.out.print("请输入年 ：");
        year = sc.nextInt();
        System.out.println();
        System.out.print("请输入月 ：");
        month = sc.nextInt();
        var c = Calendar.getInstance();
        int days = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                days = 30;
                break;
            case 2:
                days = year % 400 == 0 || year % 4 == 0 && year % 100 != 0 ? 29 : 28;

                LocalDate now = LocalDate.of(year, month, 1);
                days = now.isLeapYear() ? 29 : 28;
                break;

            default:
                days = 0;
                break;
        }
        System.out.printf("%d年%d月有%d天", year, month, days);

    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Scanner;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Switch3 {
    public static void main(String[] args) {
        System.out.println((int)'a');//97
        System.out.println((char)65);//A
        var c = MyC.RED;
        //byte short int char String enum(枚举)
        switch(c){
            case RED:
                break;
            case BLUE:
                break;
        }
    }
}

```

java12,switch支持表达式 -> 

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Switch4 {
    public static void main(String[] args) {
        int y = 2021;
        int m = 7;

        //java12 switch表达式
        int days = switch (m) {
            case 2 -> y % 400 == 0 || y % 4 == 0 && y % 100 != 0 ? 29 : 28;
            case 4, 6, 9, 11 -> 30;
            default -> 31;
        };
        System.out.printf("%d年%d月有%d天", y, m, days);
    }
}

```

最新的switch语句，表达式，->   yield

```java
public class Switch5 {
    public static void main(String[] args) {
        int y = 2021;
        int m = 11;

        //java12 switch表达式 -> 3;
        int days = switch (m) {
            case 2 -> {
                System.out.println("22222");
                yield (y % 400 == 0 || y % 4 == 0 && y % 100 != 0 ? 29 : 28);
            }
            case 4, 6, 9, 11 -> 30;
            default -> 31;
        };
        System.out.printf("%d年%d月有%d天", y, m, days);
        System.out.println("-------------------------------------");
        switch (m) {
            case 2 -> {
                System.out.println(y % 400 == 0 || y % 4 == 0 && y % 100 != 0 ? 29 : 28);
                System.out.println(y % 400 == 0 || y % 4 == 0 && y % 100 != 0 ? 29 : 28);
                System.out.println(y % 400 == 0 || y % 4 == 0 && y % 100 != 0 ? 29 : 28);
            }
            case 4, 6, 9, 11 -> {
                System.out.println(30);
            }
            default -> System.out.println(31);
        }
    }
}
```

![image-20211125092428292](https://gitee.com/webrx/wx_note/raw/master/images/image-20211125092428292.png)

### 3.4 循环语句

#### 3.4.1 for

![image-20210730095336151](https://gitee.com/webrx/wx_note/raw/master/images/image-20210730095336151.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class For1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 15; i++)
            System.out.printf("%03d\t", i);
        System.out.println();
        for (int i = 15; i >= 1; i -= 2) {
            System.out.printf("%03d\t", i);
        }
        //计算1+2+3+...+100 = ?
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        System.out.println("\n1+2+3+...+100="+sum);
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class For2 {
    public static void main(String[] args) {
        /*
        for (;;) {
            System.out.println("aaa");
        }
         */
        for (int i = 3; i <= 13; i++) {
            System.out.printf("%02d ", i);
        }
        System.out.println();
        for (int i = 3; i <= 13; i++) {
            if(i%2 != 0){
                System.out.printf("%02d ", i);
            }
        }
        System.out.println();
        for(int i=1;i<20;i++){
            if(i%2!=0) {
                System.out.printf("%d ", i);
            }
        }
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class For3 {
    public static void main(String[] args) {
        for (int i = 1; i < 7; i++) {
            System.out.printf("%d ", i);
        }
        System.out.println();
        for (int i = 6; i >= 1; i--) {
            System.out.printf("%d ", i);
        }
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class For4 {
    public static void main(String[] args) {
        int num = 15;
        for (int i = 1; i < 15; i += 2) {
            //String str = " ".repeat(num--) + "*".repeat(i);
            //System.out.println(str);
            for (int n = --num; n >= 0; n--) {
                System.out.print(" ");
            }

            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class For5 {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < i+1; j++) {
                System.out.printf("%d × %d = %2d \t", j, i, i * j);
            }
            System.out.println();
        }
    }
}

```

作业：

1. 计算1-100之间的素数和

2. 输出九九乘法表

3. 输出菱形图案`*`

   

#### 3.4.2 for in

for in

foreach

for of

>for(int i : nums){
>
>}

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * <p>Project: javaseapp - Switch1
 * <p>Powered by webrx On 2021-07-29 17:20:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class ForIn {
    public static void main(String[] args) {
        //for in  foreach for of 循环集合 数组
        int[] nums = {10, 20, 30};

        for(int i : Stream.of(1,2,3,4,5,6).toList()){
            System.out.println(i);
        }

        for (int am : nums) {
            System.out.println(am);
            break;
        }

        for(String s : new String[]{"java","php","python"}){
            System.out.println(s);
        }

        for(char c : "javascript".toCharArray()){
            System.out.println(c);
        }

        System.out.println("--------------------------------");
        for (String value : System.getenv().values()) {
            System.out.println(value);
        }

    }
}

```

```java
public class ForIn2 {
    public static void main(String[] args) {
       String ns = "李四,张三,王五,张三丰,李强";
        for (String s : ns.split(",")) {
            System.out.println(s);
        }
        System.out.println("------------------------------");
        String[] nnn = ns.split(",");
        Random rand = new Random();
        System.out.println(nnn[rand.nextInt(nnn.length)]);
    }
}
```



#### 3.4.3 while do while

条件循环语句

* while(条件){}
* do{}while();

```java
public class While1 {
    public static void main(String[] args) {
        int i = 100;
        while (i <= 4) {
            System.out.println(i++);
        }

        for (int j = 1; j <= 4; j++) {
            System.out.println(j);
        }
    }
}

```

```java
public class While2 {
    public static void main(String[] args) throws InterruptedException {
        int i = 1;
        do {
            System.out.println(i++);
            Thread.currentThread().sleep(2000);
        } while (true);

    }
}
```

猜数游戏：

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Random;
import java.util.Scanner;

/**
 * <p>Project: javaseapp - While1
 * <p>Powered by webrx On 2021-07-30 14:57:39
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class While3 {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        int[] lev = {10, 50, 100, 200, 500, 1000, 2000, 10000};
        int max = lev[rand.nextInt(lev.length)];
        //出题1-100 随机号数
        int num = rand.nextInt(max) + 1;
        String msg = "";
        int index = 0;
        while (true) {
            ++index;
            System.out.print("请输入数字(1-" + max + "):");
            int t = sc.nextInt();
            if (t <= 0) {
                msg = String.format("放弃游戏，程序退出。");
                break;
            }
            if (t > num) {
                //太大了
                msg = String.format("%d、太大了。", index);
            } else if (t < num) {
                //太小了
                msg = String.format("%d、太小了。", index);
            } else {
                //恭喜你猜对了.
                msg = String.format("%d、恭喜猜对了，你的游戏（%d分）。", index, 110 - index * 10);
                break;
            }
            System.out.println(msg);
        }
        System.out.println(msg);
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Random;
import java.util.Scanner;

/**
 * <p>Project: javase - Guess
 * <p>Powered by webrx On 2021-11-25 14:59:11
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Guess {
    public static void main(String[] args) {
        var rand = new Random();
        var num = rand.nextInt(100) + 1;
        var index = 0;
        var sc = new Scanner(System.in);
        while (true) {
            System.out.print("请输入数字：");

            if (!sc.hasNextInt()) {
                System.out.println("输入有误，请输入整数！！！");
                sc.next();
                continue;
            }
            var t = sc.nextInt();
            if (t > num) {
                System.out.printf("%d、太大了%n", ++index);
            } else if (t < num) {
                System.out.printf("%d、太小了%n", ++index);
            } else {
                System.out.printf("恭喜：你猜对了，你游戏成绩：%d分。%n", (100 - index * 10));
                break;
            }
            if (index >= 10) {
                System.out.printf("游戏失败：你超过了10次，智商太低，你游戏成绩：0分。%n");
                break;
            }
        }
    }
}


```



#### 3.4.4 break continue

> break; 终止当前循环语句
>
> continue; 结束这一次循环，立即准备开启下一次循环

```java
int i = 0;
while (++i < 20) {
    if (i % 2 == 0) continue;
    System.out.println(i);
    if (i > 10) break;
}
```



### 作业:

1. if 编写分时问候程序

2. switch语句及表达式使用

3. 打印九九乘法表

4. 打印菱形图案

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx.exam;
   
   /**
    * <p>Project: javase - Ex2
    * <p>Powered by webrx On 2021-11-26 09:15:17
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 17
    */
   public class Ex2 {
       public static void main(String[] args) {
           int star = 7;
           int space = star;
           for (int i = 1; i <= star; i += 2) {
               --space;
               for (int m = 0; m <= space; m++) {
                   System.out.print(" ");
               }
               for (int n = 0; n < i; n++) {
                   System.out.print("*");
               }
               System.out.println();
           }
           for (int i = star - 2; i >= 1; i -= 2) {
               ++space;
               for (int m = 0; m <= space; m++) {
                   System.out.print(" ");
               }
               for (int n = 0; n < i; n++) {
                   System.out.print("*");
               }
               System.out.println();
           }
       }
   }
   
   ```

   

5. 编写双色球彩票选号案例

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx.ex;
   
   import java.util.HashSet;
   import java.util.Random;
   import java.util.Set;
   
   /**
    * <p>Project: javaseapp - Ex3
    * <p>Powered by webrx On 2021-08-04 09:01:10
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class Ex3 {
       public static void main(String[] args) {
           for (int i = 0; i < 5; i++) {
               game();
           }
       }
   
       public static void game() {
           Random rand = new Random();
           //blue ball 1-16 (1)
           int blue = rand.nextInt(16) + 1;
           //red ball 1-33  (6)
           //System.out.println(blue);
   
           //Set集合工具类，内容没有顺序，但不能重复
           Set<Integer> red = new HashSet<Integer>();
           while (red.size() != 6) {
               red.add(rand.nextInt(33) + 1);
           }
           //System.out.println(red);
           System.out.print("红：");
           for (int i : red) {
               System.out.printf("%02d ", i);
           }
           System.out.printf("\t蓝：%02d%n", blue);
       }
   }
   
   
   
   package cn.webrx.exam;
   
   import java.util.Arrays;
   import java.util.Random;
   public class Ex3 {
       public static void main(String[] args) {
           var rand = new Random();
           //red 1-33 6
           int[] redballs = new int[6]; //建立数组int[] 一维数组
           for (int i = 0; i < redballs.length; i++) {
               while (true) {
                   int temp = rand.nextInt(33) + 1;
                   boolean flag = true;
                   for (int j = 0; j < redballs.length; j++) {
                       if (redballs[j] == temp) {
                           flag = false;
                       }
                   }
                   if (flag) {
                       redballs[i] = temp;
                       break;
                   }
               }
           }
           Arrays.sort(redballs);
           System.out.print("红：");
           for (int b : redballs) {
               System.out.printf("%d ", b);
           }
   
           //blue 1-16 1
           int blueball = rand.nextInt(16) + 1;
           System.out.println(" 蓝：" + blueball);
   
       }
   }
   
   ```

6. 编写猜数游戏`重点`

7. 百钱百鸡



## 第四章 数组(Array)

**数组**（Array）是有序的元素序列。 若将有限个类型相同的变量的[集合](https://baike.baidu.com/item/集合/2908117)命名，那么这个名称为数组名。组成数组的各个变量称为数组的分量，也称为数组的元素，有时也称为[下标变量](https://baike.baidu.com/item/下标变量/12713827)。用于区分数组的各个元素的数字编号称为下标。数组是在[程序设计](https://baike.baidu.com/item/程序设计/223952)中，为了处理方便， 把具有相同类型的若干元素按有序的形式组织起来的一种形式。 [1] 这些有序排列的同类数据元素的集合称为数组。

数组是用于储存多个相同类型数据的集合。



![img](https://gitee.com/webrx/wx_note/raw/master/images/060828381f30e924b0fbb9894f086e061d95f72e)

### 4.1 声明数组

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Arr1
 * <p>Powered by webrx On 2021-08-04 09:22:31
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Arr1 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        System.out.println(nums[2]);
    }
}

```

![image-20210804092511970](https://gitee.com/webrx/wx_note/raw/master/images/image-20210804092511970.png)

>java中数组是固定长度，数组变量是个对象，.length是属性不能写成.length()

```java
 public static void main(String[] args) {
     int a[];
     int[] c;
     String[] ns = {"a", "b", "c"};
     String[] s2 = new String[]{"java", "php"};
     double[] ds = new double[6];//下标是0 - 5
     System.out.println(ds[0]);//0.0
     System.out.println(ds[5]);//0.0
     System.out.println(ds.length);//6
     //System.out.println(ds[6]);//有异常，运行时报错，下标越界

     int[] nums = {1, 2, 3};
     System.out.println(nums.length); //属性是返回数组元素的个数
     System.out.println(nums[1]);
 }
```

### 4.2 数组操作

#### 4.2.1 遍历排序

```java
public class Arr2 {
    public static void main(String[] args) {
        //数组是固定长度，声明后不能调整大小
        int[] a = new int[]{1, 20, 300};
        System.out.println(a.length);//3
        System.out.println(a[2]);//300

        //a[3] = 6;  数组是固定长度，不能调整大小
        System.out.println(a.length);

        //遍历一
        for (var i : a) {
            System.out.println(i);
        }

        //遍历二
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }

        //遍历三 倒序遍历
        for (int i = a.length - 1; i >= 0; i--) {
            System.out.println(a[i]);
        }

        //遍历四 java8 lambda 表达式遍历方式
        Arrays.stream(a).forEach(System.out::println);
    }
}
```

```java
 public static void main(String[] args) {
        Random rand = new Random();
        //声明一个数组，初始化10个1-100随机数
        Integer[] n = new Integer[10];
        for (int i = 0; i < n.length; i++) {
            n[i] = rand.nextInt(100) + 1;
        }

        //排序前遍历输出
        for (var e : n) {
            System.out.printf("%d ", e);
        }
        System.out.println();

        //使用Arrays工具类，sort方法，排序（升序）
        //Arrays.sort(n);
        //升序 此排序要求使用对象，int 需要修改为Integer对象
        //Arrays.sort(n, (a, b) -> a - b);
        //降序
        Arrays.sort(n, (a, b) -> b - a);

        //排序后遍历输出
        for (var e : n) {
            System.out.printf("%d ", e);
        }
        System.out.println();

    }
```

```java
public class Arr5 {
    public static void main(String[] args) {
        //冒泡排序
        Random rand = new Random();

        int[] nums = new int[10];

        for (int i = 0; i < nums.length; i++) {
            nums[i] = rand.nextInt(100) + 1;
        }

        //排序前遍历输出
        print(nums);

        //冒泡
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = 0; j < nums.length - 1 - i; j++) {
                //> 升序   < 降序
                if (nums[j] < nums[j + 1]) {
                    int t = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = t;
                }
            }
        }

        //排序后
        print(nums);

    }

    public static void print(int[] a) {
        for (int i : a) {
            System.out.printf("%d ", i);
        }
        System.out.println();
    }
}


//乱序
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Arrays;
import java.util.Random;

/**
 * <p>Project: javase - Arr3
 * <p>Powered by webrx On 2021-11-26 14:24:05
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Arr3 {
    public static void main(String[] args) {
        var n = new Integer[]{1, 2, 50, 6, 99, 3};
        System.out.println(Arrays.toString(n));
        //Arrays.sort(n);
        //System.out.println(Arrays.binarySearch(n, 150));
        var rand = new Random();
        //js =>  java ->
        //Arrays.sort(n, (a, b) -> rand.nextBoolean() ? 1 : -1);
        //System.out.println(Arrays.toString(n));

        int index = 0;
        while (true) {
            int a = rand.nextInt(n.length);
            int b = rand.nextInt(n.length);
            if (a == b) continue;
            int t = n[a];
            n[a] = n[b];
            n[b] = t;
            if (++index >= 100000) break;
        }

        System.out.println(Arrays.toString(n));
    }
}

```

#### 4.2.2 二维数组

```java
public class Arr6 {
    public static void main(String[] args) {
        //一维数组
        int[] i;
        Date[] ds;

        //要求声明一个数组，数组中什么都可以放入，这个数组必须为Object数组
        Object[] obj = new Object[5];
        obj[0] = 3;
        obj[0] = "ok";
        obj[1] = 1d;
        obj[2] = 3.5f;
        obj[3] = new Date();
        obj[4] = Calendar.getInstance();

        //二维
        int[][] ns = new int[3][5];
        ns[2][4] = 100;
        System.out.println(ns[1][2]);
        System.out.println(ns[2][4]);

        int[][] ns2 = new int[][]{{1, 2, 3}, {40, 50, 60}, {10, 200, 3000, 10000}};
        System.out.println(ns2[2][2]);
        System.out.println(ns2[2][3]);
        System.out.println("---------------------------------------");
        //遍历二维数组
        for (int[] a : ns2) {
            for (int a2 : a) {
                System.out.printf("%d ", a2);
            }
            System.out.println();
        }
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Date;

/**
 * <p>Project: javase - Arry5
 * <p>Powered by webrx On 2021-11-26 15:33:30
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Arry5 {
    public static void main(String[] args) {
        int[] a = new int[3];
        //声明一个数组，什么元素都可以

        //java.lang.Object 是基类，是所有默认的父类
        Object[] obs = new Object[10];
        obs[0] = 1;
        obs[1] = .5;
        obs[2] = new Date();

        //三维数组
        int[][][] nss = new int[][][]{
                {
                        {1, 2, 3}, {4, 5, 6}
                },
                {
                        {10, 20, 30}
                },
                {
                        {11, 22, 33}, {44, 55, 66, 77, 88, 99, 10}
                }
        };
        System.out.println(nss[2][0][1]);
        System.out.println(nss[2][1][0]);
        System.out.println(nss[0][1][2]);

        for (int[][] ints : nss) {
            for (int[] anInt : ints) {
                for (int i : anInt) {
                    System.out.println(i);
                }
                System.out.println("======");
            }
            System.out.println("----------");
        }
    }
}

```

#### 4.2.3 Arrays工具类使用

```java
public class Arrays1 {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 60, 700};
        System.out.println(a.length);//7
        System.out.println(a);//[I@119d7047
        //数组转字符串
        System.out.println(Arrays.toString(a)); //[1, 2, 3, 4, 5, 60, 700]

        int[][][] b = {{{1, 2}, {1, 2, 3}}, {{10, 20, 30}, {55, 66}}, {{1}, {2, 3}, {4, 5050, 6}, {7, 8, 9, 10, 11}}};
        System.out.printf("%d%n", b[2][2][1]);
        System.out.println(Arrays.deepToString(b));

        //填充数组
        int[] c = new int[6];
        Arrays.fill(c, 3);
        System.out.println(Arrays.toString(c));
        Arrays.fill(c, 1, 2, 10);
        Random rand = new Random();
        Arrays.fill(c, rand.nextInt(100) + 1);
        System.out.println(Arrays.toString(c));

        Integer[] nn = new Integer[]{3, 1, 0, 10, 50, 20, 35, 99};
        System.out.println(Arrays.toString(nn));
        //Arrays.sort(nn);
        Arrays.sort(nn, (x, y) -> x - y);
        Arrays.sort(nn, (x, y) -> y - x);
        System.out.println(Arrays.toString(nn));

        String[] ss = new String[]{"an", "javascript", "egg", "book", "user", "zoo", "c", "c++"};
        System.out.println(Arrays.toString(ss));
        //Arrays.sort(ss);
        //升序，按字母顺序
        Arrays.sort(ss, (s1, s2) -> s1.compareTo(s2));
        //降序
        Arrays.sort(ss, (s1, s2) -> s2.compareTo(s1));

        Arrays.sort(ss, (s1, s2) -> s1.length() - s2.length());
        Arrays.sort(ss, (s1, s2) -> s2.length() - s1.length());
        System.out.println(Arrays.toString(ss));
        
       var n = new Integer[]{1, 2, 50, 6, 99, 3};
        System.out.println(Arrays.toString(n));
        //Arrays.sort(n);
        //二分查找，数组需要先有序
        //System.out.println(Arrays.binarySearch(n, 150));
    }
```

### 作业：

1. 利用数组实现冒泡排序效果
2. 掌握一维数组，二维数组声明，遍历，使用
3. 掌握Arrays工具类的学用方法
3. 实现数组升序，降序，乱序



## 第五章 面向对象(OOP)

> 面向对象技术利用对现实世界中对象的抽象和对象之间相互关联及相互作用的描述来对现实世界进行模拟，并且使其映射到目标系统中。其以基本对象模型为单位，将对象内部处理细节封装在模型内部，重视对象模块间的接口联系和对象与外部环境间的联系，能层次清晰地表示对象模型。面向对象的特点主要概括为抽象性、继承性、封装性和多态性。
>
> java 是单继承，多实现的面向对象的高级语言。OOP  OOD  AOP
>
> 面向对象程序设计(Object Oriented Programming)
>
> 面向对象设计（Object-Oriented Design，OOD
>
> 面向切面编程 AOP  Aspect Oriented Programming
>
> 面向对象的三大特点：封装 继承 多态

* 抽象
* 继承
* 封装
* 多态

### 5.1 对象特征(属性+方法)

> 用来描述客观事物的一个实体，由一组属性和方法构成
>
> 

1. 属性

2. 方法

   ```java
   //cn 是包名，要求全部小写;
   package cn;
   
   //User是类名要求首字母大写，
   public class User {
   	//属性 成员属性 成员变量
   	private int id;
   	private String name;
   	private byte age;
   
   	/**
   	 * 方法 成员方法
   	 * @return
   	 */
   	public String showInfo() {
   		return String.format("ID:%d,姓名：%s，年龄：%d岁。", id, name, age);
   	}
   
   	public int getId() {
   		return id;
   	}
   
   	public void setId(int id) {
   		this.id = id;
   	}
   
   	public String getName() {
   		return name;
   	}
   
   	public void setName(String name) {
   		this.name = name;
   	}
   
   	public byte getAge() {
   		return age;
   	}
   
   	public void setAge(byte age) {
   		this.age = age;
   	}
   	
   }
   
   
   ```

 

### 5.2 类 class

> **类就是一块模板，确定对象将会拥有的特征（属性）和行为（方法）**
>
> 具有相同属性和方法的一组对象的集合，就是类，类是抽象的。

张三 和  人类的关系  张三是对象  人类是类，

人类 zs = new 人类（）;

new 实例化一个对象

Person zs = new Person(“张三”); 实例化类，产生对象，编程中使用的是对象

对象与类的关系

![image-20210106103541585](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106103541585.png)



### 5.3 定义类

> 类是一种抽象数据类型，在Java中类也被当作一个数据类型来定义。类成员包括数据成员(成员变量)和方法成员(成员方法)。java.lang.Object 此类是java程序类的基类，声明定义的类，会自动继续Object类，`java 类是单继承，多实现`。

1. 声明类

   ```text
   [public][abstract|final] class className [extends superclassName] 
   [implements interfaceNameList] {
   	变量成员声明和初始化；
   	方法声明及方法体；
   }
   
```java
package cn;
import java.util.Date;
public class A {

}

class C{

}

class B extends C{

}
```

   ```java
   package cn;
   import java.awt.event.ActionEvent;
   import java.awt.event.ActionListener;
   import java.io.IOException;
   import javax.swing.JButton;
   import javax.swing.JFrame;
   import javax.swing.JOptionPane;
   
   public class MyGui extends JFrame implements ActionListener {
   	JButton btn = new JButton();
   	JButton exit = new JButton("退出软件");
   
   	public MyGui(String title) {
   		super(title);
   		setSize(500, 380);
   
   		setLayout(null);
   
   		btn.setText("打开百度");
   		btn.setBounds(100, 250, 100, 25);
   		btn.addActionListener(this);
   		add(btn);
   
   		exit.setBounds(300, 250, 100, 25);
   		exit.addActionListener(this);
   		add(exit);
   
   		setLocationRelativeTo(null);
   		setDefaultCloseOperation(2);
   		setVisible(true);
   	}
   
   	public void actionPerformed(ActionEvent e) {
   		if (e.getSource() == btn) {
   			int i = JOptionPane.showConfirmDialog(this, "要打开百度码？");
   			if (i == 0) {
   				Runtime run = Runtime.getRuntime();
   				try {
   					run.exec("cmd /k start http://www.baidu.com");
   				} catch (IOException e1) {
   					e1.printStackTrace();
   				}
   			}
   		}else if(e.getSource() == exit) {
   			this.dispose();
   			System.exit(0);
   		}
   
   	}
   
   	public static void main(String[] args) {
   		new MyGui("词典程序");
   	}
   
   }
   
   ```

   

#### 5.3.1 定义属性

> 成员变量又可以叫做属性或字段。它是用来存储数据以记录对象的状态的

```text
[public | protected | private ] 
[static] [final] [transient] [volatile]
 type　variableName;   int i;
其中，    static: 静态变量（类变量）注：相对于实例变量
 final: 常量                                             
transient: 暂时性变量                          
     volatile: 共享变量，用于并发线程的共享


```

1. 实例变量
2. 类变量

![image-20210106111916414](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106111916414.png)

```java
package cn;

public class My1 {

	int age = 18;
	static double money;
	public static void main(String[] args) {
		//方法一 在main方法如何要使用成员变量，java静态的方法可以直接使用静态成员
		// 5行改为 static int age = 18;
		//System.out.println(age); 
		System.out.println(money);//0.0

		
		//方法二 在main方法如果要使用成员变量,可以实例化使用对象来调用
		var m = new My1();
		m.age = 25;
		System.out.println(m.age);
		m.show();

	}
	
	public void show() {
		System.out.println(age);
	}
	
}


```

3. 成员变量和局部变量区别

   ![image-20210106112619113](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106112619113.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - User
 * <p>一个.java文件，声明一个类
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class User {
    /* 常量属性 */
    final int AGE = 18;
    /* 变量属性 */
    int id;
    String name = "张三";

    /**
     * 方法声明
     *
     * @param i int
     * @return int
     */
    public int getPf(int i) {
        return i * i;
    }

    /* 声明类时的静态程序段，可以没有，在第一次实例化时执行一次，一般用于程序类的初始化工作*/
    static {
        System.out.println("user类static程序段");
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }
    }
}


```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Demo
 * <p>Powered by webrx On 2021-08-04 14:29:24
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Demo {
    public static void main(String[] args) {
        //使用类，先import 导入，如果在同一个包下，不用导入，可以直接使用
        User u = new User(); //实例化， 创建实例，实例就是对象，User 就是类， u对象的变量名

        //调用执行对象的方法，3实参，并返回结果。
        System.out.println(u.getPf(3));

        System.out.println(u.name);
    }
}

```



#### 5.3.2 定义方法

```java
[public | protected | private ] 
[static] [final　| abstract]
 [native] [synchronized] 
    returnType methodName([paramList])
[throws exceptionList] {
     方法体(statements)
} 　

```

类方法定义案例`返回值、方法名、形参、实参，参数不确定、return 3; return; 结束方法执行`

```java
package cn.oop;

public class C1 {

	public static void main(String[] args) {
		System.out.println(getPf(17));// 289

		var c = new C1();
		System.out.println(c.getLf(3));// 27
		c.show(9);
	}

	/**
	 * <p>desc: </p>
	 * 
	 * @param i
	 * @return
	 */
	public static int getPf(int i) {
		return i * i;
	}

	public void show(int i) {
		System.out.printf("%d 的立方是：%d",i,getLf(i));
	}

	public int getLf(int i) {
		return i * i * i;
	}

}

```

```java
package cn.oop;

public class C2 {
	String i = "ok";
	void m1() {
		int i = 30;
		System.out.println(i);
	}
	
	void m2() {
		System.out.println(i);
		int i = 60;
		if(true) return;
		System.out.println(i);
		System.out.println(this.i);
	}
	
	void m3() {
		System.out.println(i);
		System.out.println(this.i);
		{
			int i = 100;
			System.out.println(i);
			System.out.println(this.i);
			
			int a = 50;
		}
		int a = 1000;
		System.out.println(a);
	}

}

```

![image-20210106141637037](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106141637037.png)

方法调用

![image-20210106143217692](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106143217692.png)

方法常见概念 void 没有返回值 方法名 形参 实参 return; return 3; 方法返回值

![image-20210106144155861](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106144155861.png)



* 方法 多个形参，形参的个数不确定

  ```java
  void show(){}
  int show(int i){return i*i;}
  int sum(int[] ns){return 0;}
  int sum(int...n){return 0;}
  ```

  ![image-20210106144918920](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106144918920.png)
  
  作业：
  
  1.编写一个类，编写一个方法实现求和（参数个数不限）int[] a  int…a
  
  2.一定要理解类、对象、实例化new 、属性（变量，成员变量）、类变量(static)、方法（成员方法）

#### 5.3.3 递归方法

> 方法中再次调用自己，

```java
package cn.oop;

public class M2 {
	static int i = 0;
	
	/**
	 * 此方法就是递归方法，死循环 
	 */
	public void m() {
		m();
	}
	
	public void mm(int n) {
		++i;
		System.out.print(i + " ");
		if(n>i) mm(n);
	}
	
	public static void main(String[] args) {
		M2 m = new M2();
		//m.mm(13);
		m.m();
		
		"*".repeat(3);
	}
}
```



![image-20210107094845456](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107094845456.png)

![image-20210107095002655](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107095002655.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */

/**
 * <p>Project: javase - T5
 * <p>Powered by webrx On 2021-11-29 14:06:28
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class T5 {
    static int index = 3;
    static int a = 0, b = 1;

    public static void main(String[] args) {
        for (int i = 0; i <= 6; i++)
            System.out.print(fib(i) + " ");
    }

    public static long fib(long number) {
        if ((number == 0) || (number == 1))
            return number;
        else
            return fib(number - 1) + fib(number - 2);
    }

    public static void m(int i) {
        System.out.println(index - i-- + 1);
        if (i < 1) {
            return;
        } else {
            m(i);
        }
    }
}

```



#### 5.3.4 定义静态代码程序段

```java
package cn.oop;

public class M3 {
	
	static {
		int age = 3;
		System.out.println(age);
	}
	
	int age = 18;
	
	static {
		int age = 33;
		System.out.println(age);
		
	}
	
	public void show() {
		System.out.println("信息显示");
	}
	
	public static void main(String[] args) {
		System.out.println("程序开始");
		
		M3 m1 = new M3();
		M3 m2 = new M3();
		M3 m3 = new M3();
		M3 m4 = new M3();
	}
	
	static {
		int age = 333;
		System.out.println(age);
		
	}
}

```

![image-20210107104522767](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107104522767.png)

面向对象时，类的名称要求首字母大写，属性首字线小写，方法名首字母小写，常量属性全部大写，类名和文件名保持一致。



### 5.4 实例化对象

![image-20210106111355314](https://gitee.com/webrx/wx_note/raw/master/images/image-20210106111355314.png)

```java
import java.util.Date;
import cn.webrx.Book;
```

```java
public static void main(String[] args) {
    //Book类 , 类实例化后，建立对象
    Book book = new Book();
    book.show();

    //java 10以后的新版本中，有一个var推断类型的关键字
    var c = Calendar.getInstance();
    var d = new Date();//Date 对象
    var bks = new Book[3];//数组对象
    var b = new Book();
    b.show();
}
```



### 5.5 抽象(实现)

![image-20210107104700379](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107104700379.png)

Book.java

Admin.java



### 5.6 构造方法

> 类的构造方法是特殊的方法，此方法名称必须和类名一致，构造不能有返回值 不使用void，不能直接调用，在类对象实例化时自动调用，new 的时候可以调用。一般构造方法用于类对象实例化时的初始化。如果一个类没有编写构造方法，系统自动给此类编译时添加一个无参构造方法。如果声明类编写了构造方法，系统不再添加无参构造方法，**建议编写完构造方法时，最好编写一个无参构造方法。**

```java
package cn.oop;

public class M5 {
	public M5(String title) {
	}
	public M5() {
	}
	public static void main(String[] args) {
		var m1 = new M5();
		var m2 = new M5("");
	}
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Book
 * <p>Powered by webrx On 2021-08-04 14:41:09
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Book {
    protected String name;

    static {
        System.out.println("static - 1");
    }

    public void show() {
        if (this.name == null) {
            System.out.println("hello world");
        } else {
            System.out.printf("hello %s%n", this.name);
        }
    }

    /**
     * 空构造方法，java中任何一个类，如果没有构造方法，java系统在编译时会自动添加一个无参的空构造方法
     * Book book = new Book();
     * 如果有构造方法，java系统在编译时不在添加无参构造方法
     * Book book = new Book(); 此行代码就是错误的
     * 建议如果声明类时，声明了构造方法，建议一定要添加一个无参构造方法
     * 如果构造方法有多个，肯定名称是一样的，这样叫构造方法重载。
     */
    public Book(String name) {
        this.name = name;
        System.out.println("Book(String name) 有参构造方法");
    }

    /**
     * 无参构造方法
     */
    public Book() {
        System.out.println("Book() 无参构造方法");
    }

    static {
        System.out.println("static - 2");
    }
}


```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.app;

import cn.webrx.Book;

/**
 * <p>Project: javaseapp - Test
 * <p>Powered by webrx On 2021-08-04 15:28:41
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Test {
    public static void main(String[] args) {

        //第一次实例化时执行静态程序段static-1  static-2
        var b0 = new Book("php");
        b0.show();
        System.out.println("---------------------------------- ---");

        var b1 = new Book();
        b1.show();

        System.out.println("-------------------------------------");
        var b2 = new Book("《java程序设计入门》");
        b2.show();

        System.out.println("-------------------------------------");
        Book b3 = new Book("《python入门》");
        b3.show();
    }
}

```

### 5.7 lombok组件

1. 官方下载

![image-20210107110810351](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107110810351.png)

也可以直接在eclipse中安装 `https://projectlombok.org/p2`

![image-20210107111100498](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107111100498.png)

2. 使用lombok快速生成getter setter 相关的代码

   ![image-20210107112721815](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107112721815.png)

```java
package cn.oop;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Data @AllArgsConstructor @NoArgsConstructor 
public class Book {
	private int bookId;
	@Getter @Setter
	private String name;
	@Setter
	private double price;
	private String author;	
}

```

idea 使用lombok组件，快速编写实体 

> （1）安装一个插件 lombok，最新idea自动集成

![image-20211129153320319](https://gitee.com/webrx/wx_note/raw/master/images/image-20211129153320319.png)

> （2）下载lombok.jar文件

![image-20211129153429465](https://gitee.com/webrx/wx_note/raw/master/images/image-20211129153429465.png)

![image-20211129153447225](https://gitee.com/webrx/wx_note/raw/master/images/image-20211129153447225.png)

> （3） 引入lombok.jar文件到项目中

![image-20211129153822112](https://gitee.com/webrx/wx_note/raw/master/images/image-20211129153822112.png)

> （4）编写程序类，就可以使用lombok快速写代码，alt + 7可以查看类代码结构

![image-20211129154731828](https://gitee.com/webrx/wx_note/raw/master/images/image-20211129154731828.png)

### 5.8 封装

![image-20210107140325757](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107140325757.png)

### 5.9 package包

```text
包的作用：
     1、组织相关的源代码文件
     2、不同包中的类名可以相同，用来避免名字冲突
     3、提供包一级的封装和存取权限
     
定义包名的语法：package 包名;  
    例如：package cn.webrx;
    
    注意  1、定义包的语句必须放在所有程序的最前面
         2、包定义语句不是必须的，如果没有定义包，则当前编译单元属于无名包，生成的class文件放在一般与.java文件同目录。
         3、Java编译器会自动引入包java.lang，对于其他的包，如果程序中使用到包中的类，则必须使用import引入。
         
java 是单继承，多实现，每个java类，都会默认继承Object类，java.lang包下的所有类，在程序远行时，不用导入直接使用，java.lang.String  java.lang.System类都是不用导入，直接使用的，同一个包的类之间相互引用，也不需要导入。
abc公司
abc.com

oa项目
com.abc.oa.ui
com.abc.oa.services
com.abc.oa.entity
com.abc.oa.App 主程序对象


包名命名方法：
	全部小写,公司网址倒写。
	package com.baidu.wenku.ui;
	
	package org.apache.commons.io;
	
	abc.com
	http://www.abc.com
	
	cn.webrx;
	
	package com.abc.oa.db;
```

![image-20210804172730519](https://gitee.com/webrx/wx_note/raw/master/images/image-20210804172730519.png)

### 5.10 类的继承extends

> 为什么使用继承：减少代码冗余，提高程序的复用度
>
> java语言中，类class 是单继承，多实现接口。接口interface 是多继承的。

![image-20210107143559131](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107143559131.png)

![image-20210805085227055](https://gitee.com/webrx/wx_note/raw/master/images/image-20210805085227055.png)

UML 1.类图  2.用例图   3.时序图

统一建模语言(Unified Modeling Language，UML)是一种为[面向对象](https://baike.baidu.com/item/面向对象/2262089)系统的产品进行说明、可视化和[编制](https://baike.baidu.com/item/编制/9907954)文档的一种标准语言，是非专利的第三代建模和规约语言。UML是面向对象设计的建模工具，独立于任何具体程序设计语言。

![image-20210107141917556](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107141917556.png)

![img](https://gitee.com/webrx/wx_note/raw/master/images/src=http%253A%252F%252Fyzhtml01.book118.com%252F2016%252F11%252F28%252F06%252F45867673%252F11.files%252Ffile0001.png&refer=http%253A%252F%252Fyzhtml01.book118.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg)

![img](https://gitee.com/webrx/wx_note/raw/master/images/10920833-e406f7eaaa1f8273.jpg)

![img](https://gitee.com/webrx/wx_note/raw/master/images/cffd3eb7500a497cb42cbf63b90cd528.jpg)

![image-20210804175425368](https://gitee.com/webrx/wx_note/raw/master/images/image-20210804175425368.png)

访问修饰符 public private protected 保护  默认 friendly

| 访问修饰符     | 本类 | 同包 | **子类** | 其他实例对象 |
| -------------- | ---- | ---- | -------- | ------------ |
| private        | √    |      |          |              |
| 默认(friendly) | √    | √    |          |              |
| protected      | √    | √    | √        |              |
| public         | √    | √    | √        | √            |

### 5.11 方法重写（覆盖）、重载

重写 覆盖：方法名，返回类型，形参都一样。有此种情况的，必须为继承关系。

重载：方法名一样，返回类型，形参个数，类型不样的方法，不一定必须是继承关系，同一个类中都可以，比如 构造方法重载



![image-20210107160308167](https://gitee.com/webrx/wx_note/raw/master/images/image-20210107160308167.png)

覆盖时必须是继承关系，子类对父类的方法不满意，重写。

![image-20210805090945286](https://gitee.com/webrx/wx_note/raw/master/images/image-20210805090945286.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.oop;

/**
 * <p>Project: javaseapp - User
 * <p>Powered by webrx On 2021-08-05 08:53:47
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */

public class User {
    public int id;
    public String name;

    static {
        System.out.println("User static{}");
    }

    public User() {
        System.out.println("User()");
    }

    public User(String name) {
        this.name = name;
        System.out.println(name);
    }

    public int pf(int i) {
        System.out.println("user");
        return i * i;
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.oop;

/**
 * <p>Project: javaseapp - Student
 * <p>Powered by webrx On 2021-08-05 08:54:06
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Student extends User {
    static {
        System.out.println("Student static1");
    }

    public Student() {
        System.out.println("Student()");
    }

    public Student(String name) {
        super(name);
        this.name = name;
    }

    static {
        System.out.println("Student static2");
    }

    /**
     * 覆盖
     *
     * @param i int
     * @return int
     */
    public int pf(int i) {
        System.out.println("student");
        return i * i;
    }

    public void exec(int i) {
        //super() 在构造方法中，用来调用你类的构造方法; 也可以super("jack")
        //this super
        //pf(i);
        //this.pf(i);

        //调用父类的 pf
        super.pf(i);
    }

    public int pf(String i) {
        int n = Integer.parseInt(i);
        return n * n;
    }
}

```



### 5.12 抽象类 abstract class

> java语言，声明类时　`abstract` class Db{}  说明Db类为抽象类。
>
> java语言中，抽象方法是说没有方法的实现（方法体）此方法为抽象方法，只有抽象类和接口中才可以有抽象方法。
>
> public abstract int pf(int i); //抽象方法

```java
package cn.webrx.abs;

/**
 * 抽象类
 * 
 * @author webrx
 *
 */
public abstract class Abs1 {

	/**
	 * 抽象方法
	 * 
	 * @param i
	 * @return
	 */
	public abstract int getPf(int i);

	/**
	 * 实现方法
	 * @param a
	 * @param b
	 * @return
	 */
	public int getSum(int a, int b) {
		return a + b;
	}
}

```

```text
使用规则
   1. 抽象类必须被继承，抽象方法必须被重写。
   2. 抽象类中的抽象方法只需声明，无需实现，没有方法体；
   3. 抽象类不能被实例化，抽象类不一定要包含抽象方法，而定义抽象方法的类必须是抽象类。
   4. 抽象类包含的抽象方法必须在其子类中被实现，否则该子类也必须是抽象类。
   5. 抽象类可以有构造方法供子类实例化对象
   6. 抽象类不能直接实例化使用，一般用的是抽象类的子类，
   		A a = new B();
   		A和B之间的关系，继承关系，接口实例关系。
   
   类：方法+属性+静态程序段+程序段
   抽象类：方法+抽象方法+属性+程序段
   
抽象类本质
   1.从类的组成上看其是抽象方法和非抽象方法的集合，还有属性（常量）。
   2.从设计的角度来看，实际上抽象类定义了一套规范(抽象方法)。（规范）
   
   规范：抽象类和接口

```

### 5.13 接口 interface

![image-20210108110821091](https://gitee.com/webrx/wx_note/raw/master/images/image-20210108110821091.png)



```java
package cn.webrx.in;

/**
 * 接口是静态常量，和抽象方法的集合
 * java 8.0 接口增加了静态实现方法和默认实现方法
 * @author webrx
 *
 */
public interface A {
	public static final int AGE = 18;
	
	// 接口中抽象方法，没有使用abstract 没有public 也是public
	int getPf(int i);
	
	public abstract int getLf(int i);
	
	public default void hello(String name) {
		System.out.println("hello :"+name);
	}
	
	public static void welcome(String name) {
		System.out.println("welcome :"+name);
	}
	

}

```

实现接口

```java
package cn.webrx.in;

public  class B implements A{

	public int getPf(int i) {
		
		return i*i;
	}

	public int getLf(int i) {
		return i*i*i;
	}

}

```

使用接口

```java
package cn.webrx.in;

public class Test {

	public static void main(String[] args) {
		System.out.println(A.AGE);
		
		A.welcome("李四");
		
		A a = new B();
	    a.hello("张三丰");
	    System.out.println(a.getLf(3));
	    System.out.println(a.getPf(3));

	}

}

```



![image-20210108112832209](https://gitee.com/webrx/wx_note/raw/master/images/image-20210108112832209.png)

```java
package cn.ex;

public class Demo {
	public static void main(String[] args) {
		A a = new A();
		B b = new A();
		C c = new A();
	}
}

class B {
}

interface C {
}

interface E {
}

interface D {
}

interface F extends C, D, E {
}

class A extends B implements F, C, D, E {
}

```



```java
A a = new B();
（1）继承关系
     class B extends A{
     
     }
     class A{
     }
（2）接口实现关系
     interface A{
     
     }
     class B implements A{
     
     }

class A extends B implements C,D,E{


}

C c = new A();
D b = new A();
E e = new A();
```

* java1.8 接口新技术

  * 函数式接口

    ```java
    /*
     * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
     *
     */
    package cn.webrx.db;
    
    /**
     * <p>Project: javaseapp - User
     * <p> 函数式接口，如果一个接口中，有且只有一个抽象方法，此接口默认是函数接口
     * <p> 可以使用在接口声明处，添加 @FunctionalInterface 注解来说明是函数式接口
     *
     * @author webrx [webrx@126.com]
     * @version 1.0
     * @since 16
     */
    @FunctionalInterface
    public interface User {
        int pf(int i);
    }
    
    ```

    ```java
    public class UserTest {
        public static void main(String[] args) {
            //User 是函数式接口，可以使用lambda 表达式来编写，代码精简，必须出现内部类
            User u = (User) e -> 0;
            System.out.println(u.pf(16));
        }
    }
    ```

    

  * default 默认实现方法

    ```java
    //默认实现方法,要求实现接口的类实现可以直接调用，实现类可以重写
    public default void write() {
        System.out.println("hello wirte()");
    }
    ```

    ```java
    public class Test {
        public static void main(String[] args) {
            DbDao dd = new MyDbDaoImpl();
            //调用接口的default方法
            dd.write();
            //调用我们实现接口的抽象方法
            System.out.println(dd.pf(19));
            //使用接口可以直接调用静态方法
            DbDao.print();
    
    
        }
    }
    ```

    ```java
    /*
     * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
     *
     */
    package cn.webrx.db;
    
    /**
     * <p>Project: javaseapp - MyDbDaoImpl
     * <p>Powered by webrx On 2021-08-05 14:42:00
     * <p>Created by IntelliJ IDEA
     *
     * @author webrx [webrx@126.com]
     * @version 1.0
     * @since 16
     */
    public class MyDbDaoImpl implements DbDao {
    
        @Override
        public int pf(int i) {
            return i * i;
        }
    
        @Override
        public int lf(int i) {
            return 0;
        }
    
        @Override
        public void show() {
    
        }
    }
    
    ```

    

  * static 静态实现方法

    ```java
    /*java 1.8 增加了3个功能(1) 函数接口  (2) default默认实现方法 (3) static静态实现*/
    //静态实现方法，直接使用接口，直接调用
    public static void print() {
        System.out.println("hello show()");
    }
    ```

    ```java
    public class Test {
        public static void main(String[] args) {
            DbDao dd = new MyDbDaoImpl();
            //调用接口的default方法
            dd.write();
            //调用我们实现接口的抽象方法
            System.out.println(dd.pf(19));
            //使用接口可以直接调用静态方法
            DbDao.print();
    
    
        }
    }
    ```

    

### 5.14 抽象类和接口区别

>abstract class Db{}
>
>​	特点：是一个类，可有抽象方法，也可以有实现方法，如果有抽象方法，则必须为抽象类，使用它的子类，如果子类没有实现所有抽象方法，那子类也必须为抽象类。
>
>interface Db{}  
>
>​	特点：全局常量，全局抽象方法，全局static 静态实现方法，全局default默认实现方法。`java1.8对接口已经可以有直接实现的方法功用，还有如果一个接口只有一个抽象方法，必须自动为函数式接口`
>
>企业框架开发中，使用接口比较多。比如：java体系内部JDBC全是接口
>
>jdbc Connection 是个类是个接口
>
>一般写抽象类和接口是什么人？管理人员、架构师，mybatis interface
>
>java api  api “10”

instanceof 判断一个对象是不个类的实例  

```java
public static int max(Object... o) {
		int m = Integer.MIN_VALUE;
		if (o.length == 0) {
			m = 0;
		} else if (o.length == 1) {
			if (o[0] instanceof Integer) {
				m = Integer.parseInt(o[0].toString());
			} else if (o[0] instanceof int[]) {
				for (int oo : (int[]) o[0]) {
					if (m < oo)
						m = oo;
				}
			}else if(o[0] instanceof int[][]) {
				for(int[] ii : (int[][])o[0]) {
					m = max(ii);
				}
			}
		} else {
			for (Object a : o) {
				if (a instanceof int[]) {
					if (m < max(a)) {
						m = max(a);
					}
				} else {
					if (a instanceof Integer && m < Integer.parseInt(a.toString())) {
						m = Integer.parseInt(a.toString());
					}
				}
			}
		}
		return m;
	}
```

```java
//声明接口A继承C D E F相关的接口
interface A extends C,D,E,F{

}
//声明类F 并继承E类，同时实现D,C,B,A 等接口 这就是我们说的java单继承多实现
publc class F extends E implements D,C,B,A{

}
//如果说如下一行代码正确，A和B关 
//（1）A继承了B类，A是子类，B父类。
// (2)B是个接口，A是B接口的实现类，
/*
  java 内部的参考代码：
      List<String> list = new ArrayList<>();
      Set<String> set = new HashSet<>();
      Map<String,Object> map = new HashMap<>();
*/
B b = new A();
Date d = new Date();
```

作业：编写 static int max(Object…o);

### 5.15 装箱、拆箱（基本类型和对象类型转换）

```
Integer i1 = 128;  // 装箱，相当于 Integer.valueOf(128);
int t = i1; //相当于 i1.intValue() 拆箱

基本类型和包装器类型的转换  List<int>错误，泛型不支持基本类型   List<Integer>正确

8大基本类型　byte short int long float double char boolean
int i = 10;
Integer a = 20; //装箱操作

List<int>  错误的
List<Integer> 正确的 
```

| 基本类型 | 包装器类型(对象类型) valueOf() .parse() |
| -------- | --------------------------------------- |
| boolean  | Boolean                                 |
| char     | Character                               |
| int      | Integer                                 |
| byte     | Byte                                    |
| short    | Short                                   |
| long     | Long                                    |
| float    | Float                                   |
| double   | Double                                  |

### 5.16 静态

1. 静态程序段
2. 静态属性，类成员，可以直接通过类名直接访问，多个实例对象，内存只分配一次空间存储，能实现多实现共享一个变量
3. 静态方法，可以直接通过类名直接调用
4. 静态导入 import static java.lang.Math.PI; 这样导入后可以直接使用PI

```java
package cn.ex;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class StaticTest {
    /*静态程序段，一个类中可以有多少静态程序段，第一次实例化类时会先执行所有静态程序段，再执行构造方法*/
	static {
		for (int i = 1; i <= 5; i++) {
			System.out.println(i);
		}
	}
    /* i 属性 成员属性 类型为int 前边添加了static*/
	static int i = 30;

	public static void main(String[] args) {
		System.out.println(PI);
		System.out.println(i);
		System.out.println(pow(2, 3));

		System.out.println(getPf(3));

		System.out.println(StaticTest.getPf(6));

	}

	public static int getPf(int i) {
		return i * i;
	}

}


//静态导入
import static java.lang.Math.*;

System.out.println(PI);
System.out.println(random());
System.out.println(round(6.5));

```

### 5.17 内部类

1. 类中包含
   1. 静态程序段
   2. 属性
   3. 方法
   4. 成员内部类，可以使用private修饰

```java
package com.baidu.oa.dao;

public class A {
    /* 成员内部类 */
	private class B {
	}

	public void show() {
        /* 成员方法中声明的内部类 */
		class C{
			
		}
		
		/*DB是个接口，会产生内部匿名类，如果DB是函数式接口，可以使用lambda表达式推荐使用*/
		DB dd = new DB() {
			
		};
	}
}

```

内部类的共性内部类分为: 成员内部类、静态嵌套类、匿名内部类。

(1)、内部类仍然是一个独立的类，在编译之后内部类会被编译成独立的.class文件，但是前面冠以外部类的类名和$符号。

(2)、内部类不能用普通的方式访问。内部类是外部类的一个成员，因此内部类可以自由地访问外部类的成员变量，无论是否是private的。

(3)、外部类不能直接访问其内部类，想要访问内部类，需实例化内部类

(4)、内部类声明成静态的，就不能随便的访问外部类的成员变量了，此时内部类只能访问外部类的静态成员变量。

(5)、其他类想要直接访问内部类，可直接实例化内部类，方法如下外部类名.内部类 对象名 = new 外部类().new 内部类();

例：Out.In in = new Out().new In();



如果内部类是静态的，当其他类调用内部类可直接通过类名调用，格式如下：

外部类.内部类 对象名 = new 外部类.内部类()

例：Out.In in2 = new Out.In();

当内部类是静态的，且方法也是静态的，此时可不需实例化对象

外部类.内部类.静态方法名();

例：Out.In.fun();

System.out.print();

![image-20210111102522531](https://gitee.com/webrx/wx_note/raw/master/images/image-20210111102522531.png)

![image-20210111102537315](https://gitee.com/webrx/wx_note/raw/master/images/image-20210111102537315.png)

内部匿名类代码演示：

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.db;

/**
 * <p>Project: javaseapp - Demo
 * <p>Powered by webrx On 2021-08-05 17:27:39
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Demo {
    public static void main(String[] args) {
        //java程序中不允许new 接口
        //Stu stu = new Stu() { };

        //此没有内部匿名类  推荐使用 
        //Stu stu = (i) -> i * i;
        //System.out.println(stu.pf(3));

        //不使用lambda表达会产生内部匿名类 Demo$1.class 
        Stu stu = new Stu() {
            public int pf(int i) {
                return i * i;
            }
        };
        System.out.println(stu.pf(4));
    }
}

interface Stu {
    int pf(int i);
}
```

> 类 属性 方法 最终类 继承 父类  子类 重写 重载 构造方法 抽象类 抽象方法 接口 内部类 静态程序段 静态成员 静态方法 静态导入 java1.8 接口 static实现方法 default实现方法
>
> static 
>
> final
>
> private public protected
>
> this
>
> super
>
> extends 
>
>  implements
>
> abstract
>
> package org.beiyou.oa;

### 作业

1. 编写一个类，包含两个方法，一个方法是求和，别外一个方法是求出最大值。

   ```java
   public int sum(int ... n){
       
   }
   
   public int max(int ... n){
       
   }
   public int max(int[] nums){
       
   }
   ```

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   /**
    * <p>Project: javaseapp - MyUtil
    * <p>Powered by webrx On 2021-08-06 09:02:55
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class MyUtil {
       public int sum(int a, int b) {
           return a + b;
       }
   
       /**
        * 在方法有形参中，...代表是数组参数，可以为不填写
        *
        * @param numbs int[]
        * @return int
        */
       public int sum(int... numbs) {
           int s = 0;
           for (int i : numbs) {
               s += i;
           }
           return s;
       }
   
       /**
        * 求最大值
        *
        * @param a int[]
        * @return int
        */
       public static int max(int... a) {
           int max = 0;
           //a.length 返回参数的个数，本质就是数组元素的个数
           if (a.length > 0) {
               max = a[0];
               for (int i = 1; i < a.length; i++) {
                   if (max < a[i]) {
                       max = a[i];
                   }
               }
           }
           return max;
       }
   
       public static int min(int... a) {
           int min = 0;
           if (a.length > 0) {
               min = a[0];
               for (int i = 1; i < a.length; i++) {
                   if (min > a[i]) {
                       min = a[i];
                   }
   
               }
           }
           return min;
       }
   }
   ```
   
   
   
2. 问题

   1）父类和子类的构造方法哪个先执行？

   2）父类多个的构造函数中那个被执行？

   3）如何指定执行父类某个特定的构造函数？

   4）子类可以访问父类的私有成员吗？private 只本类中

   5）父子类都有静态程序段，实现化子类，父子类构造方法和静态程序段执执行顺序？
   
   

##  第六章 字符串、正则表达式

### 6.1 String（java.lang.String)

>不可变的字符串对象，尽量少使用+=操作

* 字符串转换符号为 \

*  一.常见的转义字符

转移字符对应的英文是escape character  , 转义字符串（Escape Sequence）

字母前面加上捺斜线"\"来表示常见的那些不能显示的ASCII字符.称为转义字符.如\0,\t,\n等，就称为转义字符，因为后面的字符，都不是它本来的ASCII字符意思了。

常用方法

1. length()  字符个数
2. equals   equalsIgnoreCase
3. trim
4. substring
5. concat
6. contains()
7. indexOf
8. lastIndexOf
9. replace
10. split
11. toLowerCase（） toUpperCase（）

所有的转义字符和所对应的意义：

| 转义字符 | 意义                                    | ASCII码值（十进制） |
| -------- | --------------------------------------- | ------------------- |
|          |                                         |                     |
| \b       | 退格(BS) ，将当前位置移到前一列         | 008                 |
| \f       | 换页(FF)，将当前位置移到下页开头        | 012                 |
| **\n**   | **换行(LF) ，将当前位置移到下一行开头** | 010                 |
| **\r**   | **回车(CR) ，将当前位置移到本行开头**   | 013                 |
| **\t**   | **水平制表(HT) （跳到下一个TAB位置）**  | 009                 |
| \v       | 垂直制表(VT)                            | 011                 |
| **\\**   | **代表一个反斜线字符''\'**              | 092                 |
| **\'**   | **代表一个单引号（撇号）字符**          | 039                 |
| **\"**   | **代表一个双引号字符**                  | 034                 |
| \0       | 空字符(NULL)                            | 000                 |
| \ddd     | 1到3位八进制数所代表的任意字符          | 三位八进制          |
| \uhhhh   | 1到2位十六进制所代表的任意字符          | 二位十六进制        |

```java
package cn.webrx;
public class Str1 {

	public static void main(String[] args) {
		//java.lang.String
		String s = "ok";
		
		//输出 hello 'java' "oop" \n
		System.out.println("hello 'java' \"oop\" \\n");
		
		System.out.println(s);
		
		s += "java";
		System.out.println(s);
	}
}

```

* java15 新的字符串块

```java
package cn.webrx;

public class Str2 {

	public static void main(String[] args) {
		//java 15 新的字符串
		String s = """
				int i = 30;
				
				
				
				<font > abc汉字</font> \n"java"
				""";
		
		System.out.println(s.length());//字符数
		System.out.println(s.getBytes().length);//字节数
		System.out.println(s);
	}
}

```

* 字符串常用操作

  ```java
  package cn.webrx;
  public class Str3 {
  
  	public static void main(String[] args) {
  		var s = "ok";
  		String s2 = new String("ok");
  		System.out.println(s == s2);//false
  		System.out.println(s.equals(s2));//true
  		System.out.println("Java".equalsIgnoreCase("JAVA"));//true
  		
  		var ss = "中国河南郑州";
  		System.out.println(ss.length());//字数
  		System.out.println(ss.substring(0));
  		System.out.println(ss.substring(0,2));
  		System.out.println(ss.substring(2,4));
  		
  		System.out.println(ss.charAt(2));//河
  		int w = 1;
  		System.out.println("星期" + "日一二三四五六".charAt(w));
  		
  		//判断 
  		System.out.println("javascript2021".startsWith("java"));//true
  		System.out.println("javascript2021".endsWith("21"));//true
  		System.out.println("javascript2021".contains("java"));//true
  		
  		System.out.println("java".indexOf("a"));//1
  		System.out.println("java".lastIndexOf("a"));//3
  		System.out.println("java".lastIndexOf("p"));//-1
  		//字符串替换
  		System.out.println("java".replace('a', 'b'));
  		//
  		System.out.println("1234java2021".replaceAll("\\d+",""));
  	}
  }
  ```
  
  作业：

  1. 将一个字符串数组进行分组输出，每组中的字符串都由相同的字符组成。

  举个例子：输入[“eat”,”tea”,”tan”,”ate”,”nat”,”bat”]
  
  输出[[“ate”,”eat”,”tea”],[“nat”,”tan”],[“bat”]]
  
  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.ex;
  
  import java.util.Arrays;
  
  /**
   * <p>Project: javase - Ex1
   * <p>Powered by webrx On 2021-12-04 11:06:42
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 17
   */
  public class Ex1 {
      public static void main(String[] args) {
          String[] ls = new String[]{"age", "eat", "tea", "tan", "ate", "nat", "bat", "book", "bkoo"};
          System.out.println(Arrays.toString(ls));
          for (int n = 0; n < ls.length; n++) {
              String t = ls[n];
              if ("-".equals(t)) continue;
              for (int i = n + 1; i < ls.length; i++) {
                  if (is(t, ls[i])) {
                      ls[n] += "-" + ls[i];
                      ls[i] = "";
                  }
              }
          }
  
          System.out.println(Arrays.deepToString(ls));
  
          String ok = "";
          for (String t : ls) {
              if ("".equals(t) || "-".equals(t)) continue;
              ok += "," + t;
          }
          String[] oo = ok.substring(1).split(",");
          System.out.println(Arrays.toString(oo));
  
  
      }
  
  
      public static boolean is(String a, String b) {
          boolean f = false;
          if (a.length() == b.length()) {
              var aa = a.toCharArray();
              Arrays.sort(aa);
              var bb = b.toCharArray();
              Arrays.sort(bb);
              f = new String(aa).equalsIgnoreCase(new String(bb));
          }
          return f;
      }
  
  
      public static boolean isSame(String a, String b) {
          boolean f = true;
          if (a.length() == b.length()) {
              for (int i = 0; i < a.length(); i++) {
                  if (b.indexOf(a.charAt(i)) == -1) {
                      f = false;
                      break;
                  }
              }
  
              for (int i = 0; i < a.length(); i++) {
                  if (a.indexOf(b.charAt(i)) == -1) {
                      f = false;
                      break;
                  }
              }
          }
          return f;
      }
  }
  
  ```
  
  
  
  2. ```
     // linux /usr/local/jdk/
        windows d:\\jdk\\jdk-17
     
     String file = "c:/user/abc/upload/xxsfasf.afasf-asfas.jpg";
     //求出文件目录 c:/user/abc/upload/
     //求出文件名称 xxsfasf.afasf-asfas.jpg
     //求出文件的扩展名 jpg
     //将字符串替换为c:/user/abc/upload/20211202154333.jpg
     ```
     
     ```java
     /*
      * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
      *
      */
     package cn.ex;
     
     import java.io.File;
     import java.util.UUID;
     
     /**
      * <p>Project: javase - Ex2
      * <p>Powered by webrx On 2021-12-06 09:10:50
      * <p>Created by IntelliJ IDEA
      *
      * @author webrx [webrx@126.com]
      * @version 1.0
      * @since 17
      */
     public class Ex2 {
         public static void main(String[] args) {
             String p = "d:/xxx/user/user_abc.jpg";
             System.out.println(p);
             //File.separator 是根据系统返回相关的符号 linux / windows \ \\
             String fc = File.separator;
             fc = p.indexOf(fc) == -1 ? "/" : fc;
             System.out.println(fc);
             String path = p.substring(0, p.lastIndexOf(fc)).concat(fc);
             System.out.println(path);
             String name = p.substring(p.lastIndexOf(fc) + 1);
             System.out.println(name);
             String ext = name.lastIndexOf(".") == -1 ? "" : name.substring(name.lastIndexOf(".") + 1);
             System.out.println(ext);
             UUID uuid = UUID.randomUUID();
             String newp = String.format("%s%s.%s", path, uuid, ext);
             System.out.println(newp);
         }
     }
     
     ```
     
     
     
     
  
  java 11 string api
  
  ```java
  // 判断字符串是否为空白
  " ".isBlank();                // true
  
  // 去除首尾空格
  " Javastack ".strip();          // "Javastack"
  
  // 去除尾部空格 
  " Javastack ".stripTrailing();  // " Javastack"
  
  // 去除首部空格 
  " Javastack ".stripLeading();   // "Javastack "
  
  // 复制字符串
  "Java".repeat(3);             // "JavaJavaJava"
  
  // 行数统计
  "A\nB\nC".lines().count();    // 3
  
  String str = "Java";
  
  // 小于0：java.lang.IllegalArgumentException
  System.out.println(str.repeat(-2));
  
  // 等于0：空白串（""）
  System.out.println(str.repeat(0));
  
  // JavaJavaJava
  System.out.println(str.repeat(3));
  
  // java.lang.OutOfMemoryError
  System.out.println(str.repeat(Integer.MAX_VALUE));
  
  // 4
  System.out.println("A\nB\nC\rD".lines().count());
  ```
  
  判断一个对象是否属于一个类可以用关键字instanceof，它是二元操作符，格式为：
  
  对象 instanceof 类名
  
  式子的值为一个布尔值(boolean)
  
  ```java
  Object sth;
  boolean isString = sth instanceof String;
  ```
  
  或者
  
  ```java
  if (sth instanceof String) {
      // your code
  }
  ```
  
  

### 6.2 StringBuilder、StringBuffer

它们都是可变字符串，使用上功能上基本一样，StringBuilder不支持多线程（非线程安全），它们都支持动态修改。

![img](https://gitee.com/webrx/wx_note/raw/master/images/java-string-20201208.png)

| 序号 | 方法描述                                                     |
| :--- | :----------------------------------------------------------- |
| 1    | public StringBuffer append(String s) 将指定的字符串追加到此字符序列。 |
| 2    | public StringBuffer reverse()  将此字符序列用其反转形式取代。 |
| 3    | public delete(int start, int end) 移除此序列的子字符串中的字符。 |
| 4    | public insert(int offset, int i) 将 `int` 参数的字符串表示形式插入此序列中。 |
| 5    | insert(int offset, String str) 将 `str` 参数的字符串插入此序列中。 |
| 6    | replace(int start, int end, String str) 使用给定 `String` 中的字符替换此序列的子字符串中的字符。 |

| 1    | int capacity() 返回当前容量。                                |
| ---- | ------------------------------------------------------------ |
| 2    | char charAt(int index) 返回此序列中指定索引处的 `char` 值。  |
| 3    | void ensureCapacity(int minimumCapacity) 确保容量至少等于指定的最小值。 |
| 4    | void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin) 将字符从此序列复制到目标字符数组 `dst`。 |
| 5    | int indexOf(String str) 返回第一次出现的指定子字符串在该字符串中的索引。 |
| 6    | int indexOf(String str, int fromIndex) 从指定的索引处开始，返回第一次出现的指定子字符串在该字符串中的索引。 |
| 7    | int lastIndexOf(String str) 返回最右边出现的指定子字符串在此字符串中的索引。 |
| 8    | int lastIndexOf(String str, int fromIndex) 返回 String 对象中子字符串最后出现的位置。 |
| 9    | int length()  返回长度（字符数）。                           |
| 10   | void setCharAt(int index, char ch) 将给定索引处的字符设置为 `ch`。 |
| 11   | void setLength(int newLength) 设置字符序列的长度。           |
| 12   | CharSequence subSequence(int start, int end) 返回一个新的字符序列，该字符序列是此序列的子序列。 |
| 13   | String substring(int start) 返回一个新的 `String`，它包含此字符序列当前所包含的字符子序列。 |
| 14   | String substring(int start, int end) 返回一个新的 `String`，它包含此序列当前所包含的字符子序列。 |
| 15   | String toString() 返回此序列中数据的字符串表示形式。         |

![img](https://gitee.com/webrx/wx_note/raw/master/images/2021-03-01-java-stringbuffer.svg)

### 6.3 正则表达式

正则表达式定义了字符串的模式。

正则表达式可以用来搜索、编辑或处理文本。

正则表达式并不仅限于某一种语言，但是在每种语言中有细微的差别。

**正则表达式实例**

一个字符串其实就是一个简单的正则表达式，例如 **Hello World** 正则表达式匹配 "Hello World" 字符串。

**.**（点号）也是一个正则表达式，它匹配任何一个字符如：“a” 或 “1”

作业：

1. 判断字符串有没有大写字母
2. 判断字符串有没有汉字
3. 判断字符串有没有数字
4. 判断一个字符串是不是标签的手机号

```java
package cn.webrx;
import java.util.Arrays;
public class Reg1 {
    public static void main(String[] args) {
        String str = "hello16java16mysql8html";
        System.out.println(str);

        //删除指定的数字
        System.out.println(str.replace("6", ""));

        //删除所有数字
        for (int i = 0; i <= 9; i++) {
            str = str.replace(String.valueOf(i), "");
        }
        System.out.println(str);

        //------------使用正则表达式，删除所有数字
        String str2 = "hello16789java16mysql8html";
        System.out.println(str2);
        //hello16java16mysql8html

        //这两个方法，支持正则表达式 \\d 代表数字   \\D 代表非数字
        //String.replaceAll()  String.replaceFirst();
        System.out.println(str2.replaceAll("\\d", ""));//删除字符串数字内容
        //hellojavamysqlhtml
        System.out.println(str2.replaceAll("\\D", ""));//删除不是数字的内容
        //16168
        //删除首次出现的字符串匹配删除 \\d+
        System.out.println(str2.replaceFirst("\\d+", ""));

        String[] langs = str2.split("\\d+");
        System.out.println(Arrays.toString(langs));
    }
}
```

> String 支持正则表达式方法
>
> .replaceAll()
>
> .replaceFirst()
>
> .split()
>
> .matches()

```java
//判断字符串有没有大写
String str = "hello";
//.*[A-Z].*
// .代表一个符号
if (str.matches("^.*[A-Z].*$")) {
    System.out.println(String.format("%s:有大写字母", str));
} else {
    System.err.println(String.format("%s:没有大写字母", str));
}

// 判断字符串有没有数字
System.out.println("hello".matches("^.*\\d.*$"));

//判断有没有汉字
System.out.println("china".matches("^.*[\u4e00-\u9fa5].*$"));

// 判断是不是手机号
String phone = "13014577066";
String pattern = "^1[3,5,8]\\d{9}$";
if (phone.matches(pattern)) {
    System.out.println("yes");
} else {
    System.out.printf("手机号：%s不是合法的手机号。 \n", phone);
}

// 判断是不是纯中文
String name = "jack";
String p = "^[\u4e00-\u9fa5]+$";
if (name.matches(p)) {
    System.out.println(name);
} else {
    System.out.println("姓名必须为纯中文");
}
```

java.util.regex 包主要包括以下三个类：

**Pattern** **类：**pattern 对象是一个正则表达式的编译表示。Pattern 类没有公共构造方法。要创建一个 Pattern 对象，你必须首先调用其公共静态编译方法，它返回一个 Pattern 对象。该方法接受一个正则表达式作为它的第一个参数。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Project: javase - Reg4
 * <p>Powered by webrx On 2021-12-06 15:25:56
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Reg4 {
    public static void main(String[] args) {
        //java.util.regex.Pattern
        //var p1 = Pattern.compile("\\d");
        var p1 = Pattern.compile("[a-z]{1,}");

        //var p2 = Pattern.compile("[a-zA-Z]{1,}");
        var p2 = Pattern.compile("[a-z]{1,}", Pattern.CASE_INSENSITIVE);
        //var p2 = Pattern.compile("[a-z]{1,}", 2);

        //输出正则表达式，字符串
        //System.out.println(p1);
        //System.out.println(p1.toString());
        //System.out.println(p1.pattern());

        //java.util.regex.Matcher
        var matcher = p2.matcher("java123mysql8html5javascript-Book-student");
        while (matcher.find()) {
            System.out.printf("%s%n", matcher.group());
        }

        //123456789
        //12+3+4+56-7-8-9 = 100
        Set<String> set = new HashSet<String>();
        int index = 0;
        while (true) {
            String[] s1 = {"", "+", "-"};
            Random rand = new Random();
            StringBuilder sbu = new StringBuilder("1");
            for (int i = 2; i <= 9; i++) {
                sbu.append(s1[rand.nextInt(s1.length)]);
                sbu.append(i);
            }
            //System.out.printf("%s%n", sbu);
            var p3 = Pattern.compile("[+-]*[0-9]+");
            var m3 = p3.matcher(sbu);
            int sum = 0;
            while (m3.find()) {
                int num = Integer.parseInt(m3.group());
                sum += num;
            }
            if (sum == 100 && !set.contains(sbu.toString())) {
                System.out.printf("%d、%s = 100%n", ++index, sbu);
                set.add(sbu.toString());
            }
            if (set.size() == 11) {
                break;
            }
        }
        for (String s : set) {
            System.out.printf("%s = 100%n", s);
        }
    }
}

```

作业：

求123456789 字符串1..9 之间不改数字位置随机+-要求表达结果和为100,写出这样表达式

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Random;
import java.util.regex.Pattern;

/**
 * <p>Project: javase - Reg6
 * <p>Powered by webrx On 2021-12-07 10:09:16
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Reg6 {
    public static void main(String[] args) {
        var rand = new Random();
        var sbu = new StringBuilder("1");
        var ops = new String[]{"", "+", "-"};
        for (int i = 2; i <= 9; i++) {
            sbu.append(ops[rand.nextInt(3)].concat(String.valueOf(i)));
        }
        System.out.printf("%s%n", sbu.toString());

        String str = "123-4-5-67+8-9";
        //使用正则表达式搜索数字 "-?\\d+"
        var p = Pattern.compile("-{0,1}[0-9]{1,}");
        var m = p.matcher(str);
        int sum = 0;
        while (m.find()) {
            int num = Integer.parseInt(m.group());
            System.out.println(num);
            sum += num;
        }
        System.out.println(sum);
    }
}

```



**Matcher** **类：**Matcher 对象是对输入字符串进行解释和匹配操作的引擎。与Pattern 类一样，Matcher 也没有公共构造方法。你需要调用 Pattern 对象的 matcher 方法来获得一个 Matcher 对象。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * <p>Project: javase - Reg5
 * <p>Powered by webrx On 2021-12-07 09:29:25
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Reg5 {
    public static void main(String[] args) {
        //Pattern
        //[a-c] 匹配一个字符abc其中的一个字符
        //[a-c]+ [a-c]{1,}
        //[a-c]{3}
        var p = Pattern.compile("[a-c,x,y,z\\-]{3}", Pattern.CASE_INSENSITIVE);
        //Matcher
        var m = p.matcher("---");
        System.out.println(m.matches());

        int a3 = 6;
        int _userage = 7;
        //7-17
        // + 代表前边元字符次数｛1，｝
        // * 代表前边元字符次数{0,}
        // . 代表一个任意符号  ...  李.   .*   .{0,}   ".*[a-z]+.*"
        // ? [a-c]?   [a-c]{0,1}
        // ^ 开头
        // $ 结尾
        // ^ 取反 \\d [0-9] \\D [^0-9]
        String ppp = "[$_a-zA-Z]{1}[a-zA-Z$_0-8]*";
        System.out.println("iabc2423".matches(ppp));
    }
}

```

**PatternSyntaxException：**PatternSyntaxException 是一个非强制异常类，它表示一个正则表达式模式中的语法错误。

* 正则表达式基本语法规则

  | 字符                 | 说明                          | 实例                |
  | -------------------- | ----------------------------- | ------------------- |
  | ^                    | 表达式开始                    |                     |
  | \$                   | 表达式结尾                    |                     |
  | [3,5,8,a-z]          | 一个符号，符号范围在3,5,8,a-z |                     |
  | \d  === [0-9]        | 代表一位数字                  |                     |
  | \d+                  | 代表1个数字，或多个数字       |                     |
  | \d{3}                | 代表3个数字                   |                     |
  | [\u4e00-\u9fa5]{2,8} | 代表2到8个汉字字符            |                     |
  | \*                   | 代表0个或多个前边的符号       | [a-z]\*             |
  | .                    | 代表1个任意的符号             | str.matches("^.+$") |
  | +                    | 代表1个或多个前边的符号       | \\\\d+              |
  | ?                    | 代表0个或1个前边的符号 {0,1}  |                     |
  | {n}                  | n个                           |                     |
  | {n,}                 | n个或多个                     |                     |
  | {m,n}                |                               |                     |
  | \D                   | [^0-9]                        |                     |
  | \w                   | [a-zA-Z0-9_]                  |                     |
  | \W                   | [^a-zA-Z0-9_]                 |                     |
  | (\|)                 | 匹配或正则表达式或，分组功能  |                     |
  | ()                   | 分组                          |                     |
  |                      |                               |                     |
  |                      |                               |                     |
  |                      |                               |                     |

  ```java
  package cn.webrx;
  
  import java.util.Arrays;
  
  public class Reg4 {
  	public static void main(String[] args) {
  		String str = "5hell123456o java800.";
  		// 如下方法支持表达式
  		// str.split(str)
  		// str.replaceAll(str, str)
  		// str.replaceFirst(str, str)
  		// str.matches(str)
  
  		// str.contains(str) 不支持正则表达式
  
  		// 查检字符串中有没有java
  		System.out.println(str.matches(".*java.*"));// true
  
  		System.out.println(str.contains("java"));// true 推荐使用
  
  		// 检查字符串有没有数字
  		System.out.println(str.matches(".*\\d+.*"));
  		
  		String ss = "java8000php123python66html5css3mysql";
  		
  		String[] s2 = ss.split("\\d+");
  		System.out.println(Arrays.toString(s2));
  		System.out.println(ss.replaceAll("\\d+", "-"));
  		System.out.println(ss.replaceFirst("[0-9]+", ""));
  
  	}
  }
  ```


* 正则贪婪械，禁用贪婪

```java
package cn.webrx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reg9 {
	public static void main(String[] args) {
		String a = "java8php123javascript6mysql88888html5";
		System.out.println(a);
		System.out.println(a.replaceAll("\\d+?", "-"));//?禁用贪婪
		
		String str = """
				<div>hello</div><div>java 15</div><div><p>java 15</p><span>java 8</p><div>java 15</div></div>
				""";
		
		Pattern p = Pattern.compile("<div>.*?java.*?</div>");
		Matcher m = p.matcher(str);
		while(m.find()) {
			System.out.println(m.group());
		}
	}
}



String h = "<div>hello</div><span>java</span><div>java</div>";
//打印输出<div></div>标签中的内容
//hello
//java

// 此处?是禁用贪婪模式
var pattern = Pattern.compile("<div>(.*?)</div>");
var m = pattern.matcher(h);
while (m.find()) {
    System.out.println(m.group(1));
}
```

* 正则表达式分组

```java
package cn.webrx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reg10 {
	public static void main(String[] args) {
		String a = "java8php123javascript6mysql88888html5";
		//System.out.println(a);
		//System.out.println(a.replaceAll("\\d+?", "-"));//?禁用贪婪
		
		String str = """
				<div>hello</div><div>java 15</div><div><p>java 15</p><span>java 8</p><div>java 13</div><div>java 162423423</div>
				""";
		
		
		System.out.println(str.replaceAll("<[a-z]+>|</[a-z]+>", ""));
		System.exit(0);
		
		Pattern p = Pattern.compile("(<div>java )(\\d+)(</div>)");
		
		//System.out.println(str.replaceAll(p.pattern(), "$18$3"));
		
		System.out.println(str.replaceAll("<(div)(>java \\d+</)(div)>", "<h3$2h3>"));
		
		Matcher m = p.matcher(str);
		while(m.find()) {
			System.out.println(m.group().replaceAll("<div>(.*)</div>", "$1"));
		}
		
		//<div>java 数字</div> 修改为  <div>java 8</div>
	}
}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.regex.Pattern;

/**
 * <p>Project: javase - Reg9
 * <p>Powered by webrx On 2021-12-07 15:31:14
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Reg9 {
    public static void main(String[] args) {
        //显示div标签有java的标签直接显示
        String h = "java<div>hello-java</div><br><div>htm</div><span>java</span><div>java</div>java<div>hellojavascriptok</div>";
        var p = Pattern.compile("<div>[\\-a-z\\s]*?java[\\-a-z\\s]*?</div>", Pattern.MULTILINE);
        var m = p.matcher(h);
        while (m.find()) {
            System.out.println(m.group());
        }

        System.out.println(h);
        //div标签中包括java字符串，全部替换为javb字符串
        String t = h.replaceAll("(<div>[\\-a-z\\s]*?)(java)([\\-a-z\\s]*?</div>)", "$1javb$3");
        System.out.println(t);
    }
}

```

### 6.4 文本块（Text Blocks）

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Str1
 * <p>Powered by webrx On 2021-08-06 10:36:52
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Str2 {
    public static void main(String[] args) {
        //java 13 
        String s = """
                public static void main(String[] args) {
                       String s = ""\"
                               ""\";
                              \s
                        System.out.println(s);
                }
                """;
        System.out.println(s);
    }
}
```

### 作业

1. 要求实现字符逆序。

   ```java
   String str = "ABC123ABC";
   
   StringBuilder stringBuilder = new StringBuilder(str);
   stringBuilder.reverse();
   
   String newStr = stringBuilder.toString();
   
   System.out.println("反转前：" + str);
   System.out.println("反转后：" + newStr);
   
   String str = "ABC123ABC";
   
   char[] charArray = str.toCharArray();
   Stack<Character> stack = new Stack<>();
   StringBuilder newStr = new StringBuilder();
   
   for (char item : charArray) {
       stack.push(item);
   }
   
   for (int i = 0; i < charArray.length; i++) {
       newStr.append(stack.pop());
   }
   
   System.out.println("反转前：" + str);
   System.out.println("反转后：" + newStr.toString());
   ```

   

2. **判断字符出现次数**

   ```java
   public static void main(String[] args) {
       String str = "ABC123ABC";
       char searchChar = 'B';
   
       int count = 0;
       char[] charArray = str.toCharArray();
       for (char item : charArray) {
           if (item == searchChar) {
               count++;
           }
       }
       System.out.println("字符" + searchChar + "出现的次数为：" + count);
   }
   ```
   
   ```java
   public static void main(String[] args) {
       String s = "java-ajavajavajavabc-mysqljavascript-aajava 16ajav";
       String d = "java";
       int num = 0;
       if (s.contains(d)) {
           int index = s.indexOf(d);
           while (index != -1) {
               ++num;
               index = s.indexOf(d, index + d.length());
           }
       }
       System.out.printf("%s 在 %s 中出现了 %d 次。%n", d, s, num);
   
       //正则表达式
       Pattern p = Pattern.compile("java");
       Matcher m = p.matcher(s);
       int nn = 0;
       while (m.find()) {
           ++nn;
           //System.out.printf("%s ", m.group());
       }
       System.out.printf("%s 在 %s 中出现了 %d 次。%n", d, s, nn);
   
   }
   ```
   
   
   
3. 描述一下String、StringBuilder、StringBuffer的区别？

   | string  固定不可变字符串                                     | StringBuffer 可变                                            | StringBuilder    |
   | ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------- |
   | String的值是不可变的，这就导致每次对String的操作都会生成新的String对象，不仅效率低下，而且浪费大量优先的内存空间 | StringBuffer是可变类，和线程安全的字符串操作类，任何对它指向的字符串的操作都不会产生新的对象。每个StringBuffer对象都有一定的缓冲区容量，当字符串大小没有超过容量时，不会分配新的容量，当字符串大小超过容量时，会自动增加容量 | 可变类，速度更快 |
   | 不可变                                                       | 可变                                                         | 可变             |
   |                                                              | 线程安全                                                     | 线程不安全       |
   |                                                              | 多线程操作字符串                                             | 单线程操作字符串 |

4. 将一个数字字符串转换成逗号分隔的数字串，即从右边开始每三个数字用逗号分隔

   ```java
   //"12345678911"  
   //12,345,678,911 
   ```

   

5. 随机生成指定长度的密码或指定长度的验证码字符串。

   ```java
   public class Util {
       public String getPassword(int... n) {
           int num = n.length == 0 ? 6 : n[0];
           String str = "0123456789_-=+'\",`~!@#$%^&*()abcdefghijklmnolpqrstuvwxyzABCDEFGHIJKLMNOLPQRSTUVWXYZ";
           StringBuilder sbu = new StringBuilder(6);
           Random rand = new Random();
           for (int i = 0; i < num; i++) {
               sbu.append(str.charAt(rand.nextInt(str.length())));
           }
           return sbu.toString();
       }
   
       public String checkcode() {
           return checkcode(4);
       }
   
       public String checkcode(int n) {
           String str = "0123456789abcdefghijklmnolpqrstuvwxyzABCDEFGHIJKLMNOLPQRSTUVWXYZ";
           StringBuilder sbu = new StringBuilder(6);
           Random rand = new Random();
           for (int i = 0; i < n; i++) {
               sbu.append(str.charAt(rand.nextInt(str.length())));
           }
           return sbu.toString();
       }
       //md5(32) sha1(40)
       public String getMdString(String password, String algorithm) {
           StringBuilder sbu = new StringBuilder();
           MessageDigest md = null;
           try {
               md = MessageDigest.getInstance(algorithm);
           } catch (Exception e) {
               e.printStackTrace();
           }
           md.update(password.getBytes());
           for (byte b : md.digest()) {
               sbu.append(String.format("%02x", b));
           }
           return sbu.toString();
       }
   }
   ```

   

6. 查询身份证号归属地，查询ip所有地

![image-20210113094928775](https://gitee.com/webrx/wx_note/raw/master/images/image-20210113094928775.png)

```java
package cn.exam;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;


public class Ex1 {
	public static void main(String[] args) {
		String id = "511181200109256813";
		String u = "https://qq.ip138.com/idsearch/index.asp?userid=" + id + "&action=idcard";
		System.out.println(id);
		try {
			String t = Jsoup.connect(u).get().toString();
			Pattern p = Pattern.compile("[\\u4e00-\\u9fa5\\s]+?<br>");
			
			Pattern p2 = Pattern.compile("男|女");
			Matcher m2 = p2.matcher(t);
			m2.find();
			System.out.println(m2.group());
			
			Pattern p3 = Pattern.compile("\\d{4}年\\d{2}月\\d{2}日");
			Matcher m3 = p3.matcher(t);
			m3.find();
			System.out.println(m3.group());
			
			Matcher m = p.matcher(t);
			while (m.find()) {
				System.out.println(m.group().replace(" <br>", "").replace(" ",""));
				System.out.println(m.group().replaceAll("[^\\u4e00-\\u9fa5]", ""));
			}
		} catch (IllegalStateException e) {
			System.out.println("未找到");
		} catch (IOException e) {
			System.out.println("网络异常");
		} catch(Exception e) {
			System.out.println("未知错误异常");
		}
	}
}

```

![image-20210115145837917](https://gitee.com/webrx/wx_note/raw/master/images/image-20210115145837917.png)

查询手机号归属地

> ​	https://ip138.com/mobile.asp?mobile=13014577033&action=mobile

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.exam;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Project: javaseapp - Ex2
 * <p>Powered by webrx On 2021-08-07 11:32:40
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Ex4 {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.print("请输入手机号：");
        String tel = sc.nextLine();

        String u = String.format("https://ip138.com/mobile.asp?mobile=%s&action=mobile", tel);
        //System.out.println(u);
        String html = Jsoup.connect(u).get().toString();
        //System.out.println(html);

        Pattern pattern = Pattern.compile("<td><span>(.*)</span></td>");
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            System.out.println(matcher.group(1).replace("&nbsp;", " "));
        }
    }
}

```

![image-20210807152621087](https://gitee.com/webrx/wx_note/raw/master/images/image-20210807152621087.png)



1. 注册项目时，用户的姓名要么是全中文，要么是全英文，不能其它符号，不可以使用混合。

```java
public class Ex6 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入姓名：");
        String name = sc.nextLine();
        String p = "([a-zA-Z ]+|[\u4e00-\u9fa5 ]+)";
        if (name.matches(p)) {
            System.out.printf("注册成功，欢迎：%s。%n", name);
        } else {
            System.err.printf("注册失败，姓名必须全中文 或 全英文，不能是中央混合。%n");
        }
    }
}
```



1. 姓名是张三丰，显示 张\*丰，手机号：13014577033   显示的是 130\*\*\*\*7033

```java
package cn.webrx;

public class EX1 {

	public static void main(String[] args) {
		String name = "张三丰";
		System.out.println(name);
		System.out.println(name.replaceAll("(.).(.*)", "$1*$2"));

		String phone = "13014577033";
		System.out.println(phone);
		System.out.println(phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));
		
		
		System.out.println(maskStr("李四六","#"));
		System.out.println(maskStr("13014577088","#"));
	}

	public static String maskStr(String str, String mask) {
		StringBuilder s = new StringBuilder(str);
		if (str.matches("^1\\d{10}$")) {
			return s.toString().replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1" + mask.repeat(4) + "$2");
		} else {
			return s.toString().replaceAll("(.).(.*)", "$1" + mask + "$2");
		}
	}

}

```

![image-20210806164504882](https://gitee.com/webrx/wx_note/raw/master/images/image-20210806164504882.png)

计算一个字符串所有数字的和

![image-20210806175000391](https://gitee.com/webrx/wx_note/raw/master/images/image-20210806175000391.png)



1. 在http://www.51beiyou.org/coutus.html 取出邮件地址和手机号。


```java
package cn.webrx;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class EX1 {

	public static void main(String[] args) {
		String u = "http://www.51beiyou.org/coutus.html";
		try {
			Document doc = Jsoup.connect(u).get();
			System.out.println(Jsoup.connect("http://www.baidu.com").get().title());
			String str = doc.toString();
			
			//取出所有数字
			Pattern p1 = Pattern.compile("<img.*?>");
			Matcher m1 = p1.matcher(str);
			while(m1.find()) {

				System.out.printf("%s %n",m1.group());
			}
			
			
			
			//获取的手机号，邮箱地址
			Pattern p = Pattern.compile("\\d{11}|\\w+@[\\w,\\.]+");
			Matcher m = p.matcher(str);
			while (m.find()) {
				System.out.println(m.group());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

```

## 第七章 异常处理

### 7.1 包装器

Java中的基本数据类型不是对象型（引用类型）。但是在程序中有时需要对对象而不是基本数据类型进行操作。因此，java里提供了一种叫做包装类(wrapper)，它能够**把基本数据类型包装成对象类型**。

Java中的包装器类有两个主要的作用

  1．提供一种机制，将基本值“包装”到对象中，从而使基本值能够包含在为对象而保留的操作中，或者从带对象返回值的方法中返回。注意，java5增加了自动装箱和拆箱，程序员过去需手工执行的许多包装操作，现在可以由java自动处理了。

  2．为基本值提供分类功能。这些功能大多数于各种转换有关：在基本值和String 对象间相互转换，在基本值和String 对象之间按不同基数转换，如二进制、八进制和十六进制等。  

基本数据类型及包装类型的对应关系

  boolean  Boolean 

  byte  Byte   char  Character  double  Double  float  Float   int  Integer long  Long  short  Short 

### 7.2 装箱拆箱

自动装箱和拆箱从Java 1.5开始引入，目的是将原始类型值转自动地转换成对应的对象。自动装箱与拆箱的机制可以让我们在Java的变量赋值或者是方法调用等情况下使用原始类型或者对象类型更加简单直接。

如果你在Java1.5下进行过编程的话，你一定不会陌生这一点，你不能直接地向集合(Collections)中放入原始类型值，因为集合只接收对象。通常这种情况下你的做法是，将这些原始类型的值转换成对象，然后将这些转换的对象放入集合中。使用Integer,Double,Boolean等这些类我们可以将原始类型值转换成对应的对象，但是从某些程度可能使得代码不是那么简洁精炼。为了让代码简练，Java 1.5引入了具有在原始类型和对象类型自动转换的装箱和拆箱机制。但是自动装箱和拆箱并非完美，在使用时需要有一些注意事项，如果没有搞明白自动装箱和拆箱，可能会引起难以察觉的bug。

```java
package cn.webrx;

public class MyVar1 {
	public static void main(String[] args) {
		int i = 30;
		Integer n = 20;//自动装箱
		
		Integer nn = Integer.valueOf("30");
		System.out.println(n.intValue());
	}
}

```

### 7.3 异常概念

异常指的是在程序运行过程中发生的异常事件，通常是由外部问题（如硬件错误、输入错误）所导致的。在Java等面向对象的编程语言中异常属于对象。

> java异常处理：try catch finally throw thorws   java.lang.Exception

异常（Exception）都是运行时的。编译时产生的不是异常，而是错误（Error）。

最开始大家都将程序设计导致的错误（Error）认定文不属于异常（Exception）。

但是一般都将Error作为异常的一种，所以异常一般分两类，Error与Exception。

```java
package cn.webrx;

public class Excep1 {
	public static void main(String[] args) {
		int a = 30;
		int b = 0;
		System.out.println(a / b);  //java.lang.ArithmeticException
		
		//java.lang.StringIndexOutOfBoundsException
		//"hello".substring(100);
		
		int[] ns = {1,2,3};
		System.out.println(ns.length);
		System.out.println(ns[2]);
		System.out.println(ns[3]);
		
		String[] ss = {"aa","php"};
		
		show(ss);
		show(new String[]{"a","b"});
		show(ns);
	}
	
	public static void show(String[] sss) {
		
	}
	
	public static void show(int[] sss) {
		
	}
}

```

### 7.4 异常处理

> try catch finally throw throws
>
> try{}catch(){}
>
> try{}catch(){}finally{}
>
> try{}catch(){ }catch(){}catch(){}
>
> try{}catch(){ }catch(){}catch(){}finally{}
>
> final 常量
>
> finally 在异常处理时，代表最终有没有异常，有没有return continue break 都要执行代码段

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - EXce4
 * <p>Powered by webrx On 2021-08-09 11:43:06
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class EXce4 {
    public static void main(String[] args) {
        System.out.println(testReturn1());
    }

    public static int testReturn1() {
        int i = 1;
        try {
            i++;
            System.out.println("try:" + i);
            return i;
        } catch (Exception e) {
            i++;
            System.out.println("catch:" + i);
        } finally {
            i++;
            System.out.println("finally:" + i);
        }
        return i;
    }
}

```

![image-20210809114951763](https://gitee.com/webrx/wx_note/raw/master/images/image-20210809114951763.png)

```java
package cn.webrx;

import java.util.Scanner;

public class Excep2 {
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);
			System.out.print("\n请输入数字a");
			// java.util.InputMismatchException
			int a = sc.nextInt();
			System.out.print("\n请输入数字b");
			int b = sc.nextInt();

			// java.lang.ArithmeticException
			System.out.printf("%d / %d = %d \n", a, b, a / b);
		} catch (Exception e) {
			System.out.println("\n输入有误，必须都输入数字，且第二个b不能输入0.");
		}
		System.out.println("谢谢使用");
	}
}


try {
    Scanner sc = new Scanner(System.in);
    System.out.print("\n请输入数字a");
    // java.util.InputMismatchException
    int a = sc.nextInt();
    System.out.print("\n请输入数字b");
    int b = sc.nextInt();
    // java.lang.ArithmeticException
    System.out.printf("%d / %d = %d \n", a, b, a / b);
} catch (ArithmeticException e) {
    System.out.println("第二个b不能输入0");
} catch (InputMismatchException e) {
    System.out.println("输入有误，必须都输入数字");
} catch (Exception e) {
    System.out.println("\n系统未知异常。");
}finally {
    System.out.println("最终要执行的。");
}
```

### 7.5 声明异常类

```java
package cn.webrx;

public class SexException extends Exception{

	public SexException() {
	}
	public SexException(String msg) {
		super(msg);
	}
}
```

### 7.6 抛出异常

```java
package cn.webrx;

public class Student {
	private int id;
	private String name;
	private String gender;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) throws SexException {
		if("男".equals(gender) || "女".equals(gender)) {
			
		}else {
			throw new SexException("性别设置异常，必须为男或女");
		}
		this.gender = gender;
	}
}

```

测试

```java
package cn.webrx;

public class Test {
	public static void main(String[] args) {
		Student stu = new Student();
		stu.setId(10);
		stu.setName("jack");
		try {
			stu.setGender("男2");
			int a = 10 / 20;
		} catch (SexException e) {
			System.out.println("性别设置错误。");
		} catch (ArithmeticException e) {
			System.out.println("分母为0了");
		} catch (Exception e) {
			System.out.println("未知exception");
		}
	}
}

```

### 7.7 finally块执行

```java
package cn.webrx;

import java.io.IOException;

import org.jsoup.Jsoup;

public class Test2 {
	public static void main(String[] args) {
		Student stu = new Student();
		stu.setId(10);
		stu.setName("jack");
		try {
			stu.setGender("男");
			Jsoup.connect("http://mi.com").get();
		} catch (SexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		show();
	}
	
	public static void show() {
		try {
			Jsoup.connect("http://www.baidu.com").get();
			System.out.println("try...");
			return;
		} catch (IOException e) {
			System.out.println("catch....");
			return;
		}finally {
			System.out.println("finally....");
		}
	}
}
```

### 作业：

1. 猜数游戏程序，使用异常处理。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Random;
import java.util.Scanner;

/**
 * <p>Project: javaseapp - Exce1
 * <p>Powered by webrx On 2021-08-09 10:12:05
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Exce2 {
    public static void main(String[] args) {
        Random rand = new Random();
        int num = rand.nextInt(100) + 1;
        Scanner sc = new Scanner(System.in);
        int temp = 0;
        while (true) {
            System.out.print("请输入数字：");
            try {
                temp = sc.nextInt();
            } catch (Exception e) {
                System.out.println("请认真输入，请必须输入数字。");
                sc.next();
                continue;
            }

            if (temp > num) {
                System.out.println("太大了");
            } else if (temp < num) {
                System.out.println("太小了");
            } else {
                System.out.println("恭喜，猜对了.");
                break;
            }
        }
    }
}

```



## 第八章 集合泛型

![img](https://gitee.com/webrx/wx_note/raw/master/images/u=2048693045,3938195127&fm=26&gp=0.jpg)

### 8.1 Collection List Set

* Collection接口-》List接口-》ArrayList是List的实现类

```java
package cn.webrx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class List1 {

	public static void main(String[] args) {
		//实例化ArrayList
		List list1 = new ArrayList();
        
		ArrayList list2 = new ArrayList();
		var list = new ArrayList();
		var list3 = List.of(1,2,3,"hello","php",new Date());
		
		//array list相互转换
		int[] arr = new int[]{1,2,3};
		List lis = new ArrayList(Arrays.asList(arr));
		System.out.println(lis.get(0));
		System.out.println(lis.size());

		
		//增加元素
		list.add("java");
		list.add(100);
		list.add(new Date());
		list.add(0, "javascript");
		
		//输出集合容器中的元素个数
		System.out.println(list.size());
		list.remove(2);//删除一个元素
		list.remove(2);//删除一个元素
		list.add(1,new Date());
		System.out.println(String.format("%1$tF %1$tT",list.get(1)));
		
		//清空集合，删除所有元素  .size() 为0
		//list.clear();
		list.add("python");
		System.out.println(list.indexOf("java"));
		list.set(2, "python");//修改指定索引位置的对象
		
		System.out.println(list.contains("java"));
		System.out.println("-".repeat(50));
		//list.remove("python");
		
		//list.removeIf(e->e.toString().contains("py"));
		//list.removeIf(e->true);
		
		System.out.println(list.size());
		for(Object o : list) {
			System.out.println(o);
		}
		
		Object[] os = list.toArray();
		System.out.println(os.length);
		
		System.out.println("-".repeat(50));
		list.addAll(2, List.of(100,200,300,400,500));//将另一个list增加进来
		list.forEach(System.out::println);
	}
}

```

* ArrayList和Array数组类型转换

```java
package cn.webrx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class List2 {

	public static void main(String[] args) {
		Integer[] nums = new Integer[] { 10, 20, 30, 40, 50 };

		//固定尺寸的List
		List<Integer> list = Arrays.asList(nums);
		List list2 = List.of(1, 2, 3);
		System.out.println(list.size());
		System.out.println(list2.size());
		//list.clear(); 异常
		//list2.clear(); 异常
		
	    //数组 转 List
		List list3 = new ArrayList(list);
		System.out.println(list3.size());
		list3.add(600);
		list3.addAll(List.of(700,800,900));
		
		System.out.println(list3.size());
		System.out.println(list3);
		System.out.println(Arrays.toString(nums));//数组
		
		//List 转到 数组
		Object[] ns = list3.toArray();
		System.out.println(ns.length);
		for(Object i : ns) {
			System.out.println(i);
		}
	}
}

```

```java
package cn.webrx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class List3 {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.addAll(List.of("java", "php", "python"));
		list.add(0, "html");
		list.add(2, "javascript");
		list.set(3, "mysql");

		System.out.println(list.size());// 5
		System.out.println(list.get(1));// java
		System.out.println(list.indexOf("mysql"));// 3
		System.out.println(list.contains("python"));// true

		// list.forEach(System.out::println);
		list.stream().filter(e -> e.startsWith("java")).forEach(System.out::println);

		for (String s : list) {
			System.out.println(s);
		}

		System.out.println(list);

		Integer[] nn = { 10, 20, 30, 40 };
		System.out.println(nn);
		System.out.println(Arrays.toString(nn));
		System.out.println(List.of(nn));//固定尺寸的List
		System.out.println(new ArrayList<Integer>(List.of(nn)));//List
		
		
		var lista = new ArrayList<String>(List.of("a","b","c"));
		var listb = new ArrayList<String>(List.of("aa","bb","cc","dd"));
		lista.addAll(listb);
		lista.addAll(listb);
		lista.addAll(listb);
		System.out.println(lista);
	}
}

```

List 排序，检索过滤方式

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.*;
import java.util.stream.IntStream;

/**
 * <p>Project: javase - List5
 * <p>Powered by webrx On 2021-12-09 09:16:59
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class List5 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.addAll(List.of(1, 6, 3, 20, 9, 5));
        //数字
        System.out.println(list);
        Collections.sort(list);
        list.sort((a, b) -> b - a); //降序
        System.out.println(list);
        list.sort((a, b) -> a - b); //升序
        System.out.println(list);
        Collections.shuffle(list); //乱序
        System.out.println(list);

        list = randomList((ArrayList) list);
        System.out.println(list);
        //字符串

        //对象
    }

    public static ArrayList randomList(ArrayList sourceList) {
        if (sourceList == null || sourceList.size() == 0) {
            return sourceList;
        }
        ArrayList randomList = new ArrayList(sourceList.size());
        do {
            int randomIndex = Math.abs(new Random().nextInt(sourceList.size()));
            randomList.add(sourceList.remove(randomIndex));
        } while (sourceList.size() > 0);
        return randomList;
    }

}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * <p>Project: javaseapp - List1
 * <p>Powered by webrx On 2021-08-09 14:47:51
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class List3 {
    public static void main(String[] args) {
        //固定的List，相当于一个特殊的数组
        List<String> list = List.of("java", "javascript", "php", "python", "c", "go", "ruby");
        list = new ArrayList<String>(list);
        System.out.println(list);
        //升序 a-z
        list.sort(String::compareTo);
        System.out.println(list);
        //降序 z-a
        list.sort(Comparator.reverseOrder());
        System.out.println(list);

        //按字母个数升序
        list.sort(Comparator.comparingInt(String::length));
        //按字母个数降序
        list.sort((a, b) -> b.length() - a.length());
        System.out.println(list);

        //list.clear(); 错误，原因是
        //System.out.println(list.size());

        //new ArrayList() 将固定的List
        List<String> listok = new ArrayList<>(list);
        //listok.clear();
        //System.out.println(listok.size());
        //System.out.println(listok);

        List<Integer> list2 = new ArrayList<>(Arrays.asList(10, 1, 0, 9, 8, 3, 25, 18, 60, 33));
        System.out.println(list2);
        //升序
        list2.sort((a, b) -> a - b);
        System.out.println(list2);
        //降序
        list2.sort((a, b) -> b - a);
        System.out.println(list2);
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>Project: javase - List7
 * <p>Powered by webrx On 2021-12-09 09:40:24
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class List7 {
    public static void main(String[] args) {
        var list = new ArrayList<User>(List.of(new User(100, "李四", 60), new User(2, "张三丰", 20), new User(3, "李四", 30), new User(4, "周五", 70), new User(5, "王六", 80), new User(9, "李丽", 20), new User(11, "李四", 99)));

        //打乱顺序
        Collections.shuffle(list);
        System.out.println(list);
        //根据成绩升序
        list.sort((a, b) -> b.getScore() - a.getScore());
        list.forEach(System.out::println);

        //根据ID降序
        list.sort(Comparator.comparingInt(User::getId));
        list.forEach(System.out::println);
    }
}
```

* Collection->Set->HashSet 实现类

```java
package cn.webrx;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

public class Set1 {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Set<Integer> sets = new HashSet<>();

		sets.add(10);
		sets.add(10);
		sets.add(10);
		sets.add(10);
		sets.add(10);
		// System.out.println(sets.size());

		// sets.clear();
		// sets.contains(10)
		// sets.remove(10);

		// 编写双色球效果
		Random rand = new Random();
		// 生成蓝色 1 （1-16）
		int blue = rand.nextInt(16) + 1;

		// 生成红色 5 （1-33）
		Set<Integer> reds = new HashSet<>();
		while (reds.size() < 5)
			reds.add(rand.nextInt(33) + 1);
		System.out.println(reds);
		System.out.println(blue);
		List<Integer> list = new ArrayList<Integer>(reds);
		list.sort((a, b) -> a - b);
		list.add(blue);
		System.out.println(list);

		BufferedImage i = new BufferedImage(350, 100, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = i.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

		g.setFont(new Font("", Font.BOLD, 25));
		g.setStroke(new BasicStroke(3));
		for (int n = 0; n < list.size(); n++) {
			g.setColor(Color.BLUE);
			int x = n * 55 + 12;
			int y = 28;
			String sc = String.format("%02d", list.get(n));
			if (n < 5) {
				g.setColor(Color.RED);
				g.drawOval(x, y, 50, 50);
				g.drawString(sc, x + 12, y + 33);
				continue;
			}
			g.drawOval(x, y, 50, 50);
			g.drawString(sc, x + 12, y + 33);

		}
		g.dispose();
		ImageIO.write(i, "png", new FileOutputStream("ok.png"));

		//Runtime run = Runtime.getRuntime();
		//run.exec("cmd /k start ok.png");

	}
}

```

* LinkedList

1、LinkedList类是List接口的一个具体实现类

2、LinkedList 类用于创建链表数据结构

3、插入或者删除元素时，它提供更好的性能

**ArrayList**是一个可以处理变长数组的类型，可以存放任意类型的对象。ArrayList的所有方法都是默认在单一线程下进行的，因此ArrayList不具有线程安全性。

**LinkedList**可以看做为一个双向链表，LinkedList也是线程不安全的，在LinkedList的内部实现中，并不是用普通的数组来存放数据的，而是使用结点<Node>来存放数据的，有一个指向链表头的结点first和一个指向链表尾的结点last。LinkedList的插入方法的效率要高于ArrayList，但是查询的效率要低一点。

**Vector**也是一个类似于ArrayList的可变长度的数组类型，它的内部也是使用数组来存放数据对象的。值得注意的是Vector与ArrayList唯一的区别是，Vector是线程安全的。在扩展容量的时候，Vector是扩展为原来的2倍，而ArrayList是扩展为原来的1.5倍。



为什么说**ArrayList**和**LinkedList**是线程不安全的，而**Vector**是线程安全的呢？

首先来看下线程安全不安全的解释：

**线程安全**就是多线程访问时，采用了加锁机制，当一个线程访问该类的某个数据时，进行保护，其他线程不能进行访问直到该线程读取完，其他线程才可使用。不会出现数据不一致或者数据污染。

**线程不安全**就是不提供数据访问保护，有可能出现多个线程先后更改数据造成所得到的数据是脏数据。

阅读Vector类的源码容易知道，Vector的方法前面加了synchronized关键字，意思指同步。无论多少线程执行此程序，当一个线程没有执行完的时候，其他线程只有等待。所以它被设计成了线程安全的，但是在效率上并没有ArrayList高效。例如Vector类的add方法源码：

```java
package cn.webrx;

import java.util.LinkedList;
import java.util.List;

public class LinkedList1 {
	public static void main(String[] args) {
		LinkedList<String> list = new LinkedList<>();
		list.add("java");
		list.add("html");
		list.add("mysql");
		//list.clear();
		//list.isEmpty();
		list.remove("java");
		list.remove(0);
		
		list.addFirst("java");
		list.addLast("python");
	
		
		System.out.println(list);
	}
}

```

```java
package cn.webrx;

import java.util.LinkedList;

public class Stack<E> {
	private E e;
	private LinkedList<E> stack = new LinkedList<>();
	private int i = 0;

	public Stack() {

	}

	public int size() {
		return this.i;
	}

	public void add(E e) {
		++i;
		stack.addLast(e);
	}
	public E get() {
		--i;
		return stack.removeLast();
	}
}
```



### 8.2 Map HashMap TreeMap

> Map<String,Object> map = new HashMap<String,Object>();
>
> 键值对数据结构的容器集合
>
> map.put(k,v);
>
> map.get(k);

* HashMap

```java
package cn.webrx;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Map1 {

	public static void main(String[] args) {
		// Book b1 = new Book(100,"《java项目实战开发》",18);
		// Book b2 = b1;
		// b2.setPrice(90);
		// System.out.println(b1 == b2);//true false
		// System.out.println(b1.getPrice());

		// keys values 键值对数组
		Map<String, Book> m = new HashMap<>();
		m.put("java1", new Book(100, "《java项目实战开发》", 18));
		m.put("java2", new Book(100, "《java项目实战开发》", 18));
		m.put("java3", new Book(100, "《java项目实战开发》", 18));
		m.put("java3", new Book(100, "《java项目实战开发》", 18));
		m.put("php", new Book(101, "《php入门开发》", 15));
		
		System.out.println(m.containsKey("php"));//true
		System.out.println(m.containsValue(m.get("php")));//true

		// m.clear();
		// m.size();
		// m.isEmpty()
		// 移除元素
		 Book book = m.remove("java3");
		// System.out.println(book);
		 System.out.println(m.containsValue(book));//false

		// System.out.println(m.size());

		Set<String> keys = m.keySet();
		for (String k : keys) {
			System.out.printf("%s = %s %n", k, m.get(k));
		}

		// 遍历值集合Collection
		Collection<Book> vs = m.values();
		for (Book bk : vs) {
			System.out.println(bk);
		}

		m.forEach((k, v) -> {
			System.out.println(k);
			System.out.println(v);
		});
	}
}
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <p>Project: javaseapp - Map1
 * <p>Powered by webrx On 2021-08-10 08:46:28
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Map1 {
    public static void main(String[] args) {
        //List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        //Map<String, Object> map = new HashMap<>();
        //var set = new HashSet<String>();
        Map<String, String> map = new HashMap<>();
        map.put("1", "java");
        System.out.println(map.size());
        map.clear();//清空map
        System.out.println(map.isEmpty());//true

        map.put("k1", "java");
        map.put("k2", "javascript");
        map.put("k2", "myjava");
        map.put("k3", "php");
        map.put("k4", "mysql");
        map.put("k5", "redis");
        System.out.println(map.containsKey("k6"));//false
        //map.containsKey()
        System.out.println(map.containsValue("html"));//false
        System.out.println(map.containsValue("redis"));//true
        //map.containsValue()
        System.out.println(map.size());//5

        System.out.println(map.get("k2"));//myjava
        map.put("k9", "python");
        map.put("k6", "typescript");
        //读取map中指定的key的值，如果有就返回，如果没有就使用第二个参数为默认值返回
        Object lang = map.getOrDefault("k6", "c");
        System.out.println(lang);

        map.remove("k6");
        map.remove("k9");
        map.remove("k9", "python");


        Collection<String> col = map.values();
        System.out.println(col);

        for (Object obj : col) {
            System.out.println(obj);
        }

        Set<String> keys = map.keySet();
        System.out.println(keys);
        for (String k : keys) {
            System.out.printf("%s = %s %n", k, map.get(k));
        }
        System.out.println("----------------------------------");
        // 遍历方法二
        map.forEach((k, v) -> System.out.printf("%s = %s %n", k, v));


    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Project: javase - Map1
 * <p>Powered by webrx On 2021-12-10 09:49:57
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Map1 {
    public static void main(String[] args) {
        //Map 接口 键值对  "s100"=new Student();
        //key = value
        //键 = 值

        //实现化HashMap集合工具类
        Map<String, String> map = new HashMap<>();
        //添加数据
        map.put("s1", "java");
        map.put("s5", "javascript");
        //添加元素时，如果key存在，则值覆盖
        map.put("s1", "html");
        System.out.println(map);
        //求集合元素个数
        System.out.println(map.size());
        //判断有没有"java"对象
        System.out.println(map.containsValue("java"));
        //map集合判断有没有相关的key键
        System.out.println(map.containsKey("s2"));
        System.out.println(map.containsKey("s1"));
        //清空map集合
        //map.clear();

        //删除指定key的元素,并返回这个元素
        //String a = map.remove("s1");
        //String b = map.remove("s1");
        //System.out.println("ok".equals(b));
        System.out.println(map.size());
        //System.out.println(a);

        //删除元素要求key value都匹配，删除成功返回true 否则返回false
        boolean f = map.remove("s1", "html");
        System.out.println(f);
        System.out.println(map);
    }
}
```

map values 排序 keySet() 排序

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.*;

/**
 * <p>Project: javaseapp - Map1
 * <p>Powered by webrx On 2021-08-10 08:46:28
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Map2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>(List.of("java", "c", "php", "mysql"));
        System.out.println(list);
        //list 是变的
        Collections.sort(list);
        System.out.println(list);

        Map<String, String> map = new HashMap<>();
        map.put("k5", "go");
        map.put("k6", "ruby");
        map.put("k7", "php");
        map.put("k1", "c");
        map.put("k2", "java");
        map.put("k3", "python");
        map.put("k4", "javascript");

        Collection<String> v = map.values();
        System.out.println(v);
        List<String> vs = new ArrayList<String>(v);
        //Collections.sort(vs); 升序
        //降序
        Collections.sort(vs, Comparator.reverseOrder());
        System.out.println(vs);

        //将map 的key集合 排序
        Set<String> keys = map.keySet();
        List<String> likey = new ArrayList<>(keys);
        Collections.sort(likey);//默认升序
        //降序
        Collections.sort(likey, Comparator.reverseOrder());
        System.out.println(likey);
        for (String k : likey) {
            System.out.println(map.get(k));
        }
    }
}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.*;

/**
 * <p>Project: javase - Map2
 * <p>Powered by webrx On 2021-12-10 14:22:18
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Map2 {
    public static void main(String[] args) {

        var m = new HashMap<String, String>();
        m.put("k1", "java");
        m.put("k5", "html");
        m.put("k234241", "c");
        m.put("k3", "go");
        m.put("abc", "mysql");
        m.put("k0", "css");
        m.put("k16", "java");
        System.out.println(m);
        System.out.println(m.keySet());
        //Set<String> 是key类型
        var k = new ArrayList<>(m.keySet());
        System.out.println(k);
        //list排升序
        //k.sort(Comparator.naturalOrder());
        //list排降序
        //k.sort(Comparator.reverseOrder());
        //使用Collections工具类shffle(List) 乱序
        //Collections.shuffle(k);

        //根据字符串长度进行排序 升序
        k.sort(Comparator.comparingInt(String::length));
        //根据字符串长度进行排序 降序
        k.sort((x, y) -> y.length() - x.length());
        System.out.println(k);


        //List<String> 是value类型
        var v = new ArrayList<>(m.values());
        System.out.println(v);


        System.out.println(m);
    }
}

```

* TreeMap

  > 在Map集合框架中，除了HashMap以外，TreeMap也是常用到的集合对象之一。
  > 与HashMap相比，TreeMap是一个能比较元素大小的Map集合，会对传入的key进行了大小排序。其中，可以使用元素的自然顺序，也可以使用集合中自定义的比较器来进行排序；
  > 不同于HashMap的哈希映射，TreeMap实现了红黑树的结构，形成了一颗二叉树。

  ![img](https://gitee.com/webrx/wx_note/raw/master/images/20191211103308557.png)

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.HashMap;
  import java.util.Map;
  import java.util.TreeMap;
  
  /**
   * <p>Project: javaseapp - TreeMap1
   * <p>Powered by webrx On 2021-08-12 11:41:21
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class TreeMap1 {
      public static void main(String[] args) {
          Map<String, Integer> tm = new TreeMap<>();
          tm.put("java", 100);
          tm.put("php", 10);
          tm.put("c", 90);
          tm.put("python", 70);
          tm.put("go", 30);
          tm.put("ruby", 15);
          System.out.println(tm);
          System.out.println(tm.keySet());
          System.out.println(tm.values());
      }
  }
  
  ```

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.Map;
  import java.util.TreeMap;
  
  /**
   * <p>Project: javaseapp - TreeMap1
   * <p>Powered by webrx On 2021-08-12 11:41:21
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class TreeMap2 {
      public static void main(String[] args) {
          //TreeMap 默认key按自然排序
          Map<Integer, String> tm = new TreeMap<>();
          tm.put(100, "java");
          tm.put(10, "php");
          tm.put(90, "c");
          tm.put(70, "python");
          tm.put(30, "go");
          tm.put(15, "ruby");
          System.out.println(tm);
          System.out.println(tm.keySet());
          System.out.println(tm.values());
      }
  }
  
  ```

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.Map;
  import java.util.Set;
  import java.util.TreeMap;
  
  /**
   * <p>Project: javaseapp - TreeMap1
   * <p>Powered by webrx On 2021-08-12 11:41:21
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class TreeMap3 {
      public static void main(String[] args) {
          //创建TreeMap对象：
          TreeMap<String, Integer> treeMap = new TreeMap<>();
          System.out.println("初始化后,TreeMap元素个数为：" + treeMap.size());
  
          //新增元素:
          treeMap.put("hello", 1);
          treeMap.put("world", 2);
          treeMap.put("my", 3);
          treeMap.put("name", 4);
          treeMap.put("is", 5);
          treeMap.put("huangqiuping", 6);
          treeMap.put("i", 6);
          treeMap.put("am", 6);
          treeMap.put("a", 6);
          treeMap.put("developer", 6);
          System.out.println("添加元素后,TreeMap元素个数为：" + treeMap.size());
          System.out.println(treeMap);
          System.out.println(treeMap.keySet());
          System.out.println(treeMap.values());
  
          //遍历元素：
          Set<Map.Entry<String, Integer>> entrySet = treeMap.entrySet();
          for (Map.Entry<String, Integer> entry : entrySet) {
              String key = entry.getKey();
              Integer value = entry.getValue();
              System.out.println("TreeMap元素的key:" + key + ",value:" + value);
          }
      }
  }
  
  ```

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.Map;
  import java.util.TreeMap;
  
  /**
   * <p>Project: javaseapp - TreeMap1
   * <p>Powered by webrx On 2021-08-12 11:41:21
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class TreeMap4 {
      public static void main(String[] args) {
          Map<Integer, String> m = new TreeMap<>((a, b) -> b - a);
          m.put(1, "java1");
          m.put(3, "java122");
          m.put(10, "java2");
          m.put(4, "java4");
  
          System.out.println(m.size());
          System.out.println(m);
      }
  }
  ```
  

作业：

  List ArrayList Set HashSet  Map HashMap

1. 掌握HashMap
2. 掌握TreeMap
3. HashMap和TreeMap的区别？

### 8.3 泛型(generic)编程

> **泛型程序设计**（generic programming）是程序设计语言的一种风格或范式。泛型允许程序员在强类型程序设计语言中编写代码时使用一些以后才指定的类型，在实例化时作为参数指明这些类型。各种程序设计语言和其编译器、运行环境对泛型的支持均不一样。

泛型是jdk5引入的类型机制，就是将类型参数化，它是早在1999年就制定的jsr14的实现。

泛型机制将类型转换时的类型检查从运行时提前到了编译时，使用泛型编写的代码比杂乱的使用Object并在需要时再强制类型转换的机制具有更好的可读性和安全性。

泛型程序设计意味着程序可以被不同类型的对象重用，类似c++的模版。

泛型对于集合类尤其有用，如ArrayList。这里可能有疑问，既然泛型为了适应不同的对象，ArrayList本来就可以操作不同类型的对象呀？那是因为没有泛型之前采用继承机制实现的，实际上它只维护了一个Object对象的数组。结果就是对List来说它只操作了一类对象Object，而在用户看来却可以保存不同的对象。

泛型提供了更好的解决办法——类型参数，如：

List list = new ArrayList()；

这样解决了几个问题：

1 可读性，从字面上就可以判断集合中的内容类型；

2 类型检查，避免插入非法类型。

3 获取数据时不在需要强制类型转换。

![image-20210114145833782](https://gitee.com/webrx/wx_note/raw/master/images/image-20210114145833782.png)



![image-20210114145841413](https://gitee.com/webrx/wx_note/raw/master/images/image-20210114145841413.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.*;

/**
 * <p>Project: javaseapp - MyUtils
 * <p>Powered by webrx On 2021-08-10 11:35:04
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class MyUtils<A, V> {
    public static void main(String[] args) {
        MyUtils<User, Student> mu = new MyUtils<>();
        mu.save(new User(1, "jack", 80));

        var ms = new MyUtils<Student, User>();
        ms.save(new Student(1, "李四", 30d));

    }

    public void add(V v) {

    }

    public void save(A t) {
        System.out.println("保存数据：" + t);
    }


    public static void set1(String[] args) {
        //泛型不支持基本类型int，请使用包装器类型 Integer
        List<String> list = new ArrayList<>();
        for (String s : list) {

        }
        Set<String> set = new HashSet<String>();
        Map<String, Object> map = new HashMap<String, Object>();
        var list2 = new ArrayList<Map<String, Object>>();

        Set<User> su = new HashSet<>();

    }

    public static void list1(String[] args) {
        List list = new ArrayList();
        list.add(1);
        list.add("hello");
        list.add(new Date());
        list.add(Calendar.getInstance());
        for (Object obj : list) {
            System.out.println(obj);
        }
    }
}

```

### 作业：

1. User(1,"jack",80)

   List<User>  按照user的成绩排序

2. 编写双色球 

3. linkedlist和arraylist区别





## 第九章 IO操作

![img](https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimage.bubuko.com%2Finfo%2F201912%2F20191220114833991044.png&refer=http%3A%2F%2Fimage.bubuko.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630907771&t=f97d32e54705a5565d0ae51858531038)

### 9.1 目录操作

建立目录`file.mkdir()、file.mkdirs()`，判断是不是目录存在不存在`file.exists()、file.isDirectory()`

```java
package cn.webrx;
import java.io.File;
public class Dir1 {
	public static void main(String[] args) {
		//判断目录是否存在
		File f = new File("user");
		if(f.exists()) {
			System.out.println("user目录存在");
		}else {
			f.mkdir();//建立一级目录
			System.out.println("user目录建立成功");
			System.out.println(f.exists());
		}
		//建立目录 .mkdir()
		File f2 = new File("c:\\javaok\\book\\my\\user");
		if(!f2.exists()) f2.mkdirs(); //建立多级目录
		
		//建立多级目录  .mkdirs()
		File f3 = new File("c:/code/user");
		if(f3.mkdirs()) {
			System.out.println("建立成功");
		}else{
			System.out.println("已经存在不用建立");
		}
		
		//删除目录 只能删除一级空目录
		File f5 = new File("c:/code/abc");
		if(f5.exists() && f5.isDirectory()) {
			f5.delete(); //删除文件或删除一级空目录
		}
		File f6 = new File("c:/code");
		f6.delete();
	}
}

```

```java
package cn.webrx;
import java.io.IOException;
public class Dir2 {

	public static void main(String[] args) throws IOException {
		//windows 系统下删除非空目录
		Runtime run = Runtime.getRuntime();
		run.exec("cmd /k rd /S /Q c:\\code");
	}
}
```

![image-20211213144147306](https://gitee.com/webrx/wx_note/raw/master/images/image-20211213144147306.png)

作业：

实现删除有内容的目录。

（1）调用系统命令

（2）使用递归方法（推荐）

（3）使用第三方的组件jar来实现删除非空目录（推荐）

遍历目录

```java
public class Dir4 {
    public static void main(String[] args) {
        File f = new File("c:/myuser");
        if (f.exists() && f.isDirectory()) {
            //遍历目录
            File[] fs = f.listFiles();
            for (File t : fs) {
                System.out.println(t.getAbsolutePath());
                System.out.println(t.isFile());
                System.out.println(t.isHidden());
                System.out.println(t.isDirectory());
                System.out.println(t.getName());
                System.out.println(t.getPath());
                System.out.println(t.getParent());
                System.out.println("-".repeat(50));
            }
        }

    }
}
```



删除非空目录

```java
File f = new File("c:/abc");
if (f.exists() && f.isDirectory()) {
    System.out.println(f.delete()); //只能删除空目录，没有任何对象
} else {
    f.mkdir(); //建立一级目录 c:/user
    f.mkdirs();//递归建立多级目录 c:/user/jav/html
}
```

删除目录 方法一 递归

```java
/**
 * 递归方法，实现删除非空目录
 */
public static void del(File file) {
    if (file.exists() && file.isDirectory()) {
        for (File t : file.listFiles()) {
            if (t.isDirectory()) {
                del(t);
            } else {
                t.delete();
            }
        }
    } else {
        file.delete();
    }
    file.delete();
}
```

删除目录 方法二 调用系统命令

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.io.IOException;

/**
 * <p>Project: javaseapp - Dir7
 * <p>Powered by webrx On 2021-08-11 11:30:08
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Dir7 {
    public static void main(String[] args) throws IOException {
        //会根据系统自动的返回值  windows \\ linux /
        //System.out.println(File.separator);
        //System.out.println(System.getProperty("os.name"));

        Runtime run = Runtime.getRuntime();
        //c:/c目录  中间不使用/
        String dir = "c:c";
        //判断系统
        String os = System.getProperty("os.name");
        if (os.contains("Linux")) {
            //linux mkdir a b c use/a/cc -p
            run.exec(String.format("rm -rf %s", dir));
        } else if (os.contains("Windows")) {
            run.exec(String.format("cmd /k rmdir /S /Q %s", dir));
            System.out.println("delete c");
        }
    }
}
```

作业：

1. 递归统计某目录中有多少java文件？

2. 递归统计某目录，并列出些目录中，某文件类型的文件个数？

   jpg 18

   png 6

   java 12

   



### 9.2 文件操作

* java.io.File

  ```java
  package cn.webrx;
  
  import java.io.File;
  import java.io.IOException;
  
  public class File1 {
  
  	public static void main(String[] args) throws IOException {
  		File f = new File("user");
  		System.out.println(f.isAbsolute());
  		System.out.println(f.isDirectory());
  		System.out.println(f.isHidden());
  		System.out.println(f.isFile());
  		System.out.println(f.exists());
  		
  		//建立文件
  		File uf1 = new File("user1.txt");
  		File uf2 = new File(".","user2.txt");
  		System.out.println(uf1.createNewFile());
  		System.out.println(uf2.createNewFile());
  		
  		//删除文件
  		if(uf1.exists()) uf1.delete();
  		if(uf2.exists()) uf2.deleteOnExit();
  
  	}
  
  }
  
  
  
  package cn.webrx;
  import java.io.File;
  public class File2 {
  	public static void main(String[] args)  {
  		//File f = new File("c:/javaok/a.rar");
  		//f.deleteOnExit();
  		
  		File dir = new File("c:/javaok");
  		if(dir.exists()) {
  			File[] fs = dir.listFiles(); //列出当前目录下的所有文件对象
  			//System.out.println(fs.length);
  			for(File t : fs) {
  				if(t.isFile() && t.getName().endsWith(".rar")) {
  					t.delete();
  				}
  			}
  		}
  	}
  }
  
  
  package cn.webrx;
  import java.io.File;
  import java.text.SimpleDateFormat;
  import java.util.Date;
  public class File3 {
  	public static void main(String[] args) {
  		String path = "D:\\eclipse-workspace\\part02\\src\\cn\\Bin2.java";
  		File f = new File(path);
  		System.out.println(f.length());
  		System.out.printf("%1$tF %1$tT \n",f.lastModified());
  		Date d = new Date(f.lastModified());
  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  		System.out.println(sdf.format(d));
  	}
  }
  
  ```

  统计文件代码行，空行数

  ```java
  package cn.webrx;
  
  import java.io.BufferedReader;
  import java.io.FileNotFoundException;
  import java.io.FileReader;
  
  public class File5 {
  	static int num = 0;
  
  	public static void main(String[] args) throws FileNotFoundException {
  		String path = "D:\\eclipse-workspace\\part02\\src\\cn\\Bin2.java";
  		BufferedReader br = new BufferedReader(new FileReader(path));
  		System.out.println(br.lines().filter(e->e.trim().length()>0).count());
  	}
  
  }
  
  ```

  递归统计文件个数及文件行数实现

  ```java
  package cn.webrx;
  
  import java.io.BufferedReader;
  import java.io.File;
  import java.io.FileNotFoundException;
  import java.io.FileReader;
  
  public class File4 {
  	static int num = 0;
      static int rows = 0;
      static int space = 0;
  	public static void main(String[] args) throws Exception {
  		String path = "c:/javaok";
  		
  		//del(new File(path));
  		
  		count(new File("d:/eclipse-workspace"));
  		
  		
  		System.out.printf("%s 共有%d个java程序文件%n","d:/eclipse-workspace",num); //java文件个数
  		System.out.printf("共 %d 行代码%n",rows);
  		System.out.printf("共 %d 空行代码%n",space);
  
  	}
  	
  	public static void count(File dir) throws FileNotFoundException  {
  		if (dir.isDirectory()) {
  			File[] f = dir.listFiles();
  			for (File t : f) {
  				if (t.isDirectory())
  					count(t);
  				if (t.isFile() && t.getName().endsWith(".java")) { 
  					++num;
  					BufferedReader br = new BufferedReader(new FileReader(t));
  					//rows+=br.lines().filter(e->e.trim().length()>0).count();
  					rows+=br.lines().count();
  					
  					BufferedReader br2 = new BufferedReader(new FileReader(t));
  					space+=br2.lines().filter(e->e.trim().length()==0).count();
  				}
  					
  			}
  		} 
  		if (dir.isFile() && dir.getName().endsWith(".rar")) {
  			++num;
  			BufferedReader br = new BufferedReader(new FileReader(dir));
  			//rows+=br.lines().filter(e->e.trim().length()>0).count();
  			rows+=br.lines().count();
  			
  			BufferedReader br2 = new BufferedReader(new FileReader(dir));
  			space+=br2.lines().filter(e->e.trim().length()==0).count();
  		}
  	}
  
  	public static void del(File dir) {
  		if (dir.isDirectory()) {
  			File[] f = dir.listFiles();
  			for (File t : f) {
  				if (t.isDirectory())
  					del(t);
  				if (t.isFile() && t.getName().endsWith(".rar"))
  					t.delete();
  			}
  		} 
  		if (dir.isFile() && dir.getName().endsWith(".rar"))
  			dir.delete();
  	}
  
  }
  
  ```


作业：

1. 建立一个文件，并写入内容。

2. 读取一个文本文件。

3. 实现文件删除，更名、文件复制、移动（剪切，粘贴）

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.exam;
   
   import java.io.*;
   import java.util.UUID;
   
   /**
    * <p>Project: javase - Ex1
    * <p>Powered by webrx On 2021-12-14 14:34:49
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 17
    */
   public class Ex1 {
       public static void main(String[] args) {
           //建立文件
           try (var fos = new FileOutputStream("user.txt")) {
               for (int i = 0; i < 110; i++) {
                   var u = UUID.randomUUID().toString().toLowerCase().concat(".jpg\r\n");
                   fos.write(u.getBytes());
               }
           } catch (FileNotFoundException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           }
   
           //删除文件 linux rmo
           //var f = new File("user.txt");
           //if (f.exists()) f.deleteOnExit();
   
           //文件更名，也可实现文件的移动相当于linux mv
           //var f = new File("user.txt");
           //f.renameTo(new File("c:/x.txt"));
           //f.renameTo(new File("userabc.txt"));
   
           //文件复制
           try (var b = new FileOutputStream("c:/abcu.txt"); var a = new FileInputStream("userabc.txt")) {
               //b.write(a.readAllBytes());
               a.transferTo(b);
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
   }
   
   ```

   

4. 将所有java源文件读取内容，一并写入一个code.txt文件

   

### 9.3 字节流

![image-20210118103556888](https://gitee.com/webrx/wx_note/raw/master/images/image-20210118103556888.png)

* java.io.InputStream 输入流，主要是用来读取文件内容的。

* java.io.OutputStream  输出流，主要是用来将内容写入文件的。

  `FileInputStream ByteArrayInputStream ObjectInputStream DataInputStream BufferedInputStream`

* FileInputStream、 FileOutputStream

  ```java
  package cn.webrx;
  
  import java.io.FileInputStream;
  import java.io.FileNotFoundException;
  import java.io.FileOutputStream;
  import java.io.IOException;
  
  public class FileStreamTest {
  	
  	public static void main(String[] args) {
  		//读取文件
  		String f = "D:\\java\\log\\20201222-type.txt";
  		try {
  			FileInputStream fis = new FileInputStream(f);
  			//System.out.println(new String(fis.readAllBytes(),"utf-8"));
  			
  			byte[] buf = new byte[1024];
  			int len = 0;
  			while((len = fis.read(buf))>0) {
  				System.out.println(new String(buf,"utf-8"));
  			}
  			fis.close();
  		}catch(Exception e) {
  			
  		}
  	}
  
  	
  	public static void write(String str) {
  		try {
  			//建立文件输出流，用来写入内容，true,追加模式
  			FileOutputStream fos = new FileOutputStream("c:/u.txt",true);
  			fos.write(String.format("hello world 中文效果(%1$tF %1$tT)\r\n", System.currentTimeMillis()).getBytes());
  			fos.close();
  		} catch (FileNotFoundException e) {
  			e.printStackTrace();
  		} catch (IOException e) {
  			e.printStackTrace();
  		}
  	}
  }
  
  ```
  
* 实现文件复制功能

  ```java
  package cn.webrx;
  
  import java.io.DataInputStream;
  import java.io.FileInputStream;
  import java.io.FileNotFoundException;
  import java.io.FileOutputStream;
  import java.io.IOException;
  public class FileCopy {
  	public static void main(String[] args) throws IOException {
  		//实现文件复制
  		FileInputStream fis = new FileInputStream("c:/u.jpg");
  		//DataInputStream dis = new DataInputStream(fis);
  		FileOutputStream fos = new FileOutputStream("d:/u2.jpg");
  		//fos.write(fis.readAllBytes());
  		byte[] buf = new byte[1024];
  		int len = 0;
  		while((len = fis.read(buf))!=-1) {
  			fos.write(buf, 0, len);
  		}
  		fos.close();
  	}
  }
  
  ```

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.io.*;
  
  /**
   * <p>Project: javaseapp - Ex03
   * <p>Powered by webrx On 2021-08-12 08:49:30
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class Ex04 {
      public static void main(String[] args) {
          //字节流实现文件复制
          //d:/user.txt 复制到当前项目中 user.txt
          File file = new File("d:/", "user.txt");
          try (FileInputStream fis = new FileInputStream(file); FileOutputStream fos = new FileOutputStream("user33.txt")) {
              //fis.transferTo() 将文件输入流的所有字节传输到指定的OutputStream输出流中
              //fis.readAllBytes() 读取文件输入流中的所有字节
              //fis.transferTo(fos); java 9.0 实现了文件复制功能
              //fos.write(fis.readAllBytes());
              byte[] buf = new byte[10240];
              int len = 0;
              while ((len = fis.read(buf)) != -1) {
                  fos.write(buf, 0, len);
              }
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          } catch (IOException e) {
              e.printStackTrace();
          }
  
          //rename 重新命名，就移动文件（剪切粘贴）
          //File file = new File("user2022.txt");
          //file.delete() 删除
          //file.createNewFile(); 建立空文件
          //file.renameTo(new File("d:/user.txt"));
      }
  }
  ```

  

* RandomAccessFile

  ```java
  package cn.webrx;
  import java.io.FileNotFoundException;
  import java.io.IOException;
  import java.io.RandomAccessFile;
  public class StreamTest {
  	public static void main(String[] args) throws IOException {
  		RandomAccessFile randf = new RandomAccessFile("c:/u.jpg", "r");
  		
  		//System.out.println(randf.length());
  		//System.out.println(randf.readByte());//-1
  		//System.out.println(randf.readByte());//-40
  		//System.out.println(randf.readByte());//-1
  		//randf.skipBytes(0);//0 -32 1 0   2 16
  		//System.out.println(randf.readByte());//16
  		randf.skipBytes((int)randf.length()-1);
  		System.out.println(randf.readByte());
  	}
  
  }
  
  ```
  
* ObjectInputStream\ObjectOutputStream

  ```java
  package cn.webrx;
  
  import java.io.Serializable;
  
  public class User implements Serializable {
  	private int id;
  	private String name;
  	private int age;
  
  	public int getId() {
  		return id;
  	}
  
  	public void setId(int id) {
  		this.id = id;
  	}
  
  	public String getName() {
  		return name;
  	}
  
  	public void setName(String name) {
  		this.name = name;
  	}
  
  	public int getAge() {
  		return age;
  	}
  
  	public void setAge(int age) {
  		this.age = age;
  	}
  
  	public User(int id, String name, int age) {
  		super();
  		this.id = id;
  		this.name = name;
  		this.age = age;
  	}
  
  	public User() {
  		super();
  		// TODO Auto-generated constructor stub
  	}
  
  	@Override
  	public String toString() {
  		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
  	}
  
  }
  
  package cn.webrx;
  
  import java.io.FileInputStream;
  import java.io.FileNotFoundException;
  import java.io.FileOutputStream;
  import java.io.IOException;
  import java.io.ObjectInputStream;
  import java.io.ObjectOutputStream;
  import java.util.ArrayList;
  import java.util.List;
  
  public class ObjectStreamTest {
  
  	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
  		// 将java为的实例保存
  		//User u = new User(1, "杰克", 18);
  		 ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("user.dat", true));
  		 
  		 oos.writeObject("aa");
  		 
  
  
  		 List<User> us = new ArrayList<User>();
  		 oos.writeObject(us);
  
  		 
  		// oos.writeObject(u);
  		// oos.close();
  		
  		
  		// 读取Object
  		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("user.dat"));
  		User user = (User)ois.readObject();
  		System.out.println(user);
  	}
  }
  ```
  
  面试题：java中什么是序列化，反序列化？为什么要序列化？
  
  ​                如何获取文件真实的格式?
  
  ​				如何判断两个文件是不是同一个文件（内容一样）？
  
  ​               迅雷下载工具为什么速度很快？5G速度很快？（100万字节）
  
  ​                实现目录复制？



### 9.4 字符流

![image-20210118110221972](https://gitee.com/webrx/wx_note/raw/master/images/image-20210118110221972.png)

* Reader FileReader BufferedReader

* Writer FileWriter BufferedWriter

  ```java
  package cn.webrx;
  
  import java.io.BufferedReader;
  import java.io.FileReader;
  import java.io.FileWriter;
  import java.io.IOException;
  
  public class ReaderWriterTest {
  
  	public static void main(String[] args) throws IOException {
  		//写入文件
  		FileWriter fw = new FileWriter("user.dat", true);
  		//fw.write("hello world中文\n\r");
  		//fw.write("hello world中文\n\r");
  		//fw.write("hello world中文\n\r");
  		//fw.close();
  		
  		//读取
  		BufferedReader br = new BufferedReader(new FileReader("user.dat"));
  		//System.out.println(br.lines().count());
  		int rownum = 0;
  		
  		while(br.ready()) {
  			++rownum;
  			System.out.println(br.readLine());
  		}
  		System.out.println(rownum);
  		br.close();
  	}
  }
  ```

### 9.5 NIO和commons-io库

* NIO实现文件复制

  ```java
  Path source = Path.of("a.txt");
  Path target = Paths.get("b.txt");
  Files.copy(source,target,StandardCopyOption.REPLACE_EXISTING);
  ```

* commons-io 是一套io工具jar包

```text
IOUtils 包含一些工具类,用于处理读,写和拷贝,这些方法基于 InputStream, OutputStream, Reader 和 Writer工作.

FileUtils 包含一些工具类,它们基于File对象工作，包括读，写，拷贝和比较文件

FilenameUtils包含一些工具类,它们基于文件名工作而不是File对象。这个类旨在 在Unix和Windows环境下保持一致,帮助在两个环境下过渡(如从开发环境到生成环境)

FileSystemUtils包含一些工具类，基于文件系统访问功能不被JDK支持。目前，只有一个方法就是得到驱动器空余空间。注意这使用命令行而不是 native code。

 

EndianUtils 包含静态方法来交换Java基本类型和流的字节序

SwappedDataInputStream实现了DataInput接口。可以从文件中读取非本地字节序。

```



![image-20210118144127601](https://gitee.com/webrx/wx_note/raw/master/images/image-20210118144127601.png)



![image-20210118144804013](https://gitee.com/webrx/wx_note/raw/master/images/image-20210118144804013.png)



```java
package cn.webrx;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
public class IoTest {
	public static void main(String[] args) throws IOException {
		//FileUtils.deleteDirectory(new File("c:/javaok"));
	
		FileUtils.copyDirectory(new File("c:/aaa"), new File("c:/aaa2"));
		FileUtils.copyFile(new File("c:/u.jpg"), new File("c:/u2.jpg"));
	}
}

```

### 9.6 ImageIO图像操作

> 处理项目中，图片格式转换，验证码效果，缩略图，水印文字，水印图标，裁剪图像，webp格式使用。
>
> 二维码生成，解析

```text
jpg 真彩无损压缩格式，网络上图像一般都jpg
png 真彩无操损压缩格式，支持画布透明
gif 颜色不能超过256种颜色，所以此格式不是真彩，既支持帧静态图像，又支持多帧动态图像，是图像文件中唯一支持动画的
webp

WebP（发音：weppy）是一种同时提供了有损压缩与无损压缩（可逆压缩）的图片文件格式，派生自影像编码格式VP8，被认为是WebM多媒体格式的姊妹项目，是由Google在购买On2 Technologies后发展出来，以BSD授权条款发布。
WebP最初在2010年发布，目标是减少文件大小，但达到和JPEG格式相同的图片质量，希望能够减少图片档在网络上的发送时间。2011年11月8日，Google开始让WebP支持无损压缩和透明色（alpha通道）的功能，而在2012年8月16日的参考实做libwebp 0.2.0中正式支持。根据Google较早的测试，WebP的无损压缩比网络上找到的PNG档少了45%的文件大小，即使这些PNG档在使用pngcrush和PNGOUT处理过，WebP还是可以减少28%的文件大小。
WebP支持的像素最大数量是16383x16383。有损压缩的WebP仅支持8-bit的YUV 4:2:0格式。而无损压缩（可逆压缩）的WebP支持VP8L编码与8-bit之ARGB色彩空间。又无论是有损或无损压缩皆支持Alpha透明通道、ICC色彩配置、XMP诠释数据。
WebP有静态与动态两种模式。动态WebP（Animated WebP）支持有损与无损压缩、ICC色彩配置、XMP诠释数据、Alpha透明通道。
```

#### 9.6.1 二维码qrcode

maven项目，pom.xml添加依赖

```xml
<!-- com.google.zxing/javase -->
<dependency>
    <groupId>com.google.zxing</groupId>
    <artifactId>javase</artifactId>
    <version>3.4.1</version>
</dependency>
```



```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Project: javase - QrCode1
 * <p>Powered by webrx On 2021-12-16 15:43:13
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class QrCode1 {
    public static void main(String[] args) throws WriterException, IOException, ChecksumException, NotFoundException, FormatException {
        String qr = "c:/qr.png";

        //生成QRcode
        var bm = new QRCodeWriter().encode("http://img0.baidu.com/it/u=1119515913,3748293383&fm=26&fmt=auto", BarcodeFormat.QR_CODE, 200, 200);
        var i = MatrixToImageWriter.toBufferedImage(bm);
        ImageIO.write(i, "png", new FileOutputStream(qr));

        //读取QRcode
        var img = ImageIO.read(new File("c:/me.gif"));
        var src = new BufferedImageLuminanceSource(img);
        var binarizer = new HybridBinarizer(src);
        var binaryBitmap = new BinaryBitmap(binarizer);
        var result = new QRCodeReader().decode(binaryBitmap);
        System.out.println(result.getText());

    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.QRCodeWriter;
import org.jsoup.Jsoup;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Project: javase - Dict
 * <p>Powered by webrx On 2021-12-16 20:04:50
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Dict {
    public static void main(String[] args) throws IOException, WriterException {
        String w = JOptionPane.showInputDialog("请输入词汇：");
        String u = String.format("http://www.youdao.com/w/eng/%s/#keyfrom=dict2.index", w);
        String t = Jsoup.connect(u).get().select("div[class=trans-container]").get(0).text();
        //JOptionPane.showMessageDialog(null, t);

        String txt = String.format("%s : %s", w, t);
        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        var bm = new QRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, 200, 200, hints);
        var i = MatrixToImageWriter.toBufferedImage(bm);
        var dst = String.format("c:/%s.png", w);
        ImageIO.write(i, "png", new FileOutputStream(dst));

        var run = Runtime.getRuntime();
        run.exec("cmd /k start " + dst);

    }
}

```



* hello world imageio

  ```java
  package cn.img;
  
  import java.awt.Color;
  import java.awt.Graphics2D;
  import java.awt.RenderingHints;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  import java.util.Random;
  
  import javax.imageio.ImageIO;
  
  public class Img1 {
  
  	public static void main(String[] args) throws IOException {
  		BufferedImage img = new BufferedImage(600, 420, BufferedImage.TYPE_INT_ARGB);
  		int h = img.getHeight();
  		int w = img.getWidth();
  
  		// Graphics g1 = img.getGraphics();
  		// g1.setColor(Color.ORANGE);
  		// g1.drawOval(50, 50, 100, 100);
  		// g1.dispose();
  		// ImageIO.write(img,"png",new File("c:/g1.png"));
  
  		Graphics2D g2 = img.createGraphics();
  		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
  		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  		g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  		
  		
  		g2.setColor(Color.RED);
  		// g2.setStroke(new BasicStroke(5));
  		// g2.drawOval(100, 100, 50, 50); //空心圆
  
  		// 画点
  		// g2.drawLine(150, 150, 150,150);
  		Random rand = new Random();
  		for (int i = 0; i <= 200; i++) {
  			//g2.setColor(new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255),rand.nextInt(50)+50));
  			g2.setColor(new Color(255,0,0,50)); //#ff0000
  			int x = rand.nextInt(600);
  			int y = rand.nextInt(400);
  			int sw = rand.nextInt(100);
  			int sh = rand.nextInt(100);
  			g2.fillOval(x, y, sh,sh);//实心圆
  		}
  		// 画线
  
  		// 圆
  
  		// 矩形
  
  		// 多边形
  
  		g2.dispose();
  		ImageIO.write(img, "png", new File("/c:/g2.png"));
  
  	}
  
  }
  
  ```

  ![image-20210118173320129](https://gitee.com/webrx/wx_note/raw/master/images/image-20210118173320129.png)



* 案例效果

  ```java
  package cn.img;
  
  import java.awt.Color;
  import java.awt.Font;
  import java.awt.Graphics2D;
  import java.awt.RenderingHints;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  import java.util.Random;
  
  import javax.imageio.ImageIO;
  
  public class Img2 {
  
  	public static void main(String[] args) throws IOException {
  		int w = 800;
  		int h = 800;
  		BufferedImage i = new BufferedImage(w, h, 2);
  		Graphics2D g = i.createGraphics();
  		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
  		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  
  		g.setColor(Color.ORANGE);
  		g.fillRect(0, 0, w, h);
  
  		g.setColor(Color.BLACK);
  		g.fillArc(200, 200, 400, 400, 90, 180);
  
  		g.setColor(Color.WHITE);
  		g.fillArc(200, 200, 400, 400, 270, 180);
  		g.fillOval(300, 200, 200, 200);
  		g.setColor(Color.BLACK);
  		g.fillOval(300, 400, 200, 200);
  		g.fillOval(350, 250, 100, 100);
  		g.setColor(Color.WHITE);
  		g.fillOval(350, 450, 100, 100);
  
  		// 画字
  		Font font = new Font("zh74hfmss", Font.BOLD, 60);
  		Random rand = new Random();
  		g.setColor(new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
  		g.setFont(font);
  		g.drawString("新年快乐 更近一步！", 120, 720);
  
  		g.dispose();
  		ImageIO.write(i, "png", new File("c:/ok.png"));
  
  		Runtime run = Runtime.getRuntime();
  		run.exec("cmd /k start c:/ok.png");
  
  	}
  
  }
  
  ```

  字体转换网址：https://convertio.co/zh/woff-ttf/
  
  

![image-20210120113723891](https://gitee.com/webrx/wx_note/raw/master/images/image-20210120113723891.png)

* 验证码案例效果

![image-20210120142839917](https://gitee.com/webrx/wx_note/raw/master/images/image-20210120142839917.png)



![image-20210120173700067](https://gitee.com/webrx/wx_note/raw/master/images/image-20210120173700067.png)



```java
package cn.img;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class Img3 {

	public static void main(String[] args) throws IOException, FontFormatException {
		int w = 160;
		int h = 60;
		int len = 5;
		Random rand = new Random();
		BufferedImage i = new BufferedImage(w, h, 2);
		Graphics2D g = i.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

		g.setColor(new Color(251, 251, 251));
		g.fillRect(0, 0, w, h);

		Font font = Font.createFont(Font.TRUETYPE_FONT, new File("Comicbook Smash.ttf"));
		font = font.deriveFont(Font.PLAIN, 55f);
		g.setFont(font);
		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		double radianPercent = 0D;
		for (int n = 0; n < 20; n++) {
			font = font.deriveFont(Font.PLAIN, rand.nextFloat() * 15 + 15);
			g.setFont(font);
			g.setColor(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(10) + 50));
			int x = rand.nextInt(w);
			int y = rand.nextInt(h);
			g.drawString(String.valueOf(str.charAt(rand.nextInt(str.length()))), x, y);
		}

		StringBuilder sok = new StringBuilder(len);
		g.setColor(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(25) + 220));
		for (int n = 0; n < len; n++) {
			font = font.deriveFont(Font.PLAIN, rand.nextFloat() * 35 + 30);
			g.setFont(font);
			int x = n * 32 + 5;
			int y = rand.nextInt(35) + 20;
			radianPercent = Math.PI * (rand.nextInt(35) / 180D);
			if (rand.nextBoolean())
				radianPercent = -radianPercent;
			g.rotate(radianPercent, x, y);
			String sss = String.valueOf(str.charAt(rand.nextInt(str.length())));
			sok.append(sss);
			g.drawString(sss, x, y);
			g.rotate(-radianPercent, x, y);
		}

		Point[] points = { new Point(0, 0), new Point(10, 50), new Point(30, 6), new Point(60, 55), new Point(80, 3),
				new Point(160, 60)

		};

		GeneralPath path = new GeneralPath();
		path.moveTo(points[0].x, points[0].y);
		for (int i2 = 0; i2 < points.length - 1; ++i2) {
			Point sp = points[i2];
			Point ep = points[i2 + 1];
			Point c1 = new Point((sp.x + ep.x) / 2, sp.y);
			Point c2 = new Point((sp.x + ep.x) / 2, ep.y);
			path.curveTo(c1.x, c1.y, c2.x, c2.y, ep.x, ep.y);
		}
		g.setStroke(new BasicStroke(rand.nextInt(6) + 2));
		// g.draw(path);

		g.setColor(new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(25) + 220));
		int yy = rand.nextInt(15) + 15;// y
		int hh = rand.nextInt(20) + 10;// 高度
		int aa = rand.nextInt(60) + 20;//
		// x
		for (int x = 10; x < w; x++) {
			int y = (int) (yy + hh * Math.sin(x * Math.PI / aa));
			// g.drawLine(x, (int) y, x, (int) y);
			g.fillOval(x, y, 3, 3);
		}

		// 输出验证码
		System.out.println(sok.toString());

		g.dispose();
		ImageIO.write(i, "png", new File("c:/ok.png"));

		Runtime run = Runtime.getRuntime();
		run.exec("cmd /k start c:/ok.png");

	}

}

```

* 画直线、虚线、曲线

  ```java
  package cn.img;
  
  import java.awt.BasicStroke;
  import java.awt.Color;
  import java.awt.Font;
  import java.awt.Graphics2D;
  import java.awt.Point;
  import java.awt.RenderingHints;
  import java.awt.geom.GeneralPath;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  import java.util.Random;
  
  import javax.imageio.ImageIO;
  
  public class Img4 {
  	public static void main(String[] args) throws IOException {
  		int w = 800;
  		int h = 800;
  		BufferedImage i = new BufferedImage(w, h, 2);
  		Graphics2D g = i.createGraphics();
  		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
  		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  
  		g.setColor(Color.ORANGE);
  		g.fillRect(0, 0, w, h);
  
  		g.setColor(Color.RED);
  		// g.drawLine(0, 0, w, h);
  
  		g.setStroke(new BasicStroke(3));
  		g.setColor(Color.WHITE);
  		for (int n = 0; n < 800; n++) {
  			int x = n;
  			n += 18;
  			// g.drawLine(x, 400, x+5, 400);
  		}
  
  		// g.drawOval(10, 20, 30,30);
  
  		// g.drawRect(50, 50, 50, 50);
  
  		// g.drawArc(100, 100, 100, 100, 0, 180);
  
  		Random rand = new Random();
  		for (int n = 0; n < 50; n++) {
  			g.setColor(new Color(255, 255, 255, 220));
  			// g.drawArc(rand.nextInt(w),rand.nextInt(h),rand.nextInt(w)+20,rand.nextInt(h)+20,rand.nextInt(360),rand.nextInt(360));
  		}
  
  		// 画曲线
  		Point[] points = { new Point(0, 0), new Point(0, rand.nextInt(h)), new Point(w, rand.nextInt(h))
  
  		};
  
  		GeneralPath path = new GeneralPath();
  		path.moveTo(points[0].x, points[0].y);
  		for (int i2 = 0; i2 < points.length - 1; ++i2) {
  			Point sp = points[i2];
  			Point ep = points[i2 + 1];
  			Point c1 = new Point((sp.x + ep.x) / 2, sp.y);
  			Point c2 = new Point((sp.x + ep.x) / 2, ep.y);
  			path.curveTo(c1.x, c1.y, c2.x, c2.y, ep.x, ep.y);
  		}
  
  		// g.draw(path);
  		for (int n = 0; n <= 355; n += 10) {
  			// g.drawArc(100, 200, 600, 300, n, 5);
  		}
  
  		int yy = 100;// y
  		int hh = 100;// 高度
  		int aa = 200;//
  		// x
  		for (int x = 10; x < w ; x+=15) {
  			int y = (int) (yy + hh * Math.sin(x * Math.PI / aa));
  			//g.drawLine(x, (int) y, x, (int) y);
  			g.fillOval(x, y, 6, 6);
  		}
  
  		g.dispose();
  		ImageIO.write(i, "png", new File("c:/ok.png"));
  
  		Runtime run = Runtime.getRuntime();
  		run.exec("cmd /k start c:/ok.png");
  
  	}
  
  }
  
  ```

* 正弦曲线

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.imgs;
  
  import javax.imageio.ImageIO;
  import java.awt.*;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  
  /**
   * <p>Project: javaseapp - Img10
   * <p>Powered by webrx On 2021-08-16 11:02:02
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class Img10 {
      public static void main(String[] args) throws IOException {
          int w = 800, h = 500, t = 2;
          var i = new BufferedImage(w, h, t);
          var g = i.createGraphics();
          g.setColor(Color.WHITE);//设置面板背景色
          g.fillRect(0, 0, 400, 300);//填充面板
          g.setColor(Color.RED);//设置画线的颜色
          double y = 0;
          double x = 0;
          //一个周期
          for (x = 0; x <= 360; x += 0.1) {
              y = Math.sin(x * Math.PI / 180);//转化为弧度,1度=π/180弧度
              y = (100 + 80 * y);//便于在屏幕上显示
              //g.drawString(".",(int)x,(int)y);//用这种方式也可以
              g.drawLine((int) x, (int) y, (int) x, (int) y);//画点
          }
          ImageIO.write(i, "png", new File("c:/ok.png"));
          Runtime run = Runtime.getRuntime();
          run.exec("cmd /k start c:/ok.png");
      }
  }
  
  ```

  ![image-20210816111027876](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816111027876.png)
  
* 画正圆图案

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import javax.imageio.ImageIO;
  import java.awt.*;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  
  /**
   * <p>Project: javase - Img6
   * <p>Powered by webrx On 2021-12-17 19:06:43
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 17
   */
  public class Img6 {
      public static void main(String[] args) throws IOException {
  
          int w = 800;
          int h = 600;
          BufferedImage i = new BufferedImage(w, h, 1);
          Graphics g = i.getGraphics();
          g.setColor(Color.RED);
          g.fillRect(0, 0, w, h);
          g.setColor(Color.WHITE);
          int xx = 400, yy = 300;//坐标中心
          int angle = 0;//角度
          int cw = 200, ch = 200;//控制圆形的水平直径 垂直直径
          for (int n = 0; n <= 36; n++) {
              int x = xx - (int) (Math.sin(angle * Math.PI / 360) * cw);
              int y = yy - (int) (Math.cos(angle * Math.PI / 360) * ch);
              g.fillOval(x, y, 10, 10);
              angle += 20;
          }
          g.dispose();
          ImageIO.write(i, "jpg", new File("c:/b.jpg"));
  
      }
  }
  
  ```

  ![image-20211217191313806](https://gitee.com/webrx/wx_note/raw/master/images/image-20211217191313806.png)




* 黑白图像

```java
package cn.img;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Img5 {

	public static void main(String[] args) throws IOException {
		String f = "c:/aaa/www.jpg";//xxx_gray.jpg  abc.png  abc_gray.png
		//读取原来的图像
		BufferedImage i = ImageIO.read(new File(f));
		int w = i.getWidth();
		int h = i.getHeight();

		//建立新图像，使用黑白效果
		BufferedImage ii = new BufferedImage(w, h, 10);
		Graphics2D g = ii.createGraphics();
		g.drawImage(i, 0, 0, w, h, null);
		
		g.setColor(new Color(0,255,0,55));
		g.fillRect(0, 0, w, h);
		g.dispose();
		
		String ext = f.substring(f.lastIndexOf("."));
		String name = f.substring(0,f.lastIndexOf("."));
		String newname = name + "_gray"+ext;
		
		ImageIO.write(ii, "jpg", new File(newname));

	}

}

```

![image-20210121102725985](https://gitee.com/webrx/wx_note/raw/master/images/image-20210121102725985.png)

* 最新验证码

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.imgs;
  
  import javax.imageio.ImageIO;
  import java.awt.*;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  import java.util.Random;
  
  /**
   * <p>Project: javaseapp - Img7
   * <p>Powered by webrx On 2021-08-16 08:46:51
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class Img9 {
      public static void main(String[] args) throws IOException {
          Random rand = new Random();
          int w = 160, h = 60, t = 2;
          BufferedImage i = new BufferedImage(w, h, t);
          var g = i.createGraphics();
          g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
          g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  
          String text = str();
          Font font = null;
          try {
              font = Font.createFont(0, new File("font/Comicbook Smash.ttf"));
              font = font.deriveFont(Font.BOLD, 50);
              g.setFont(font);
          } catch (FontFormatException e) {
              e.printStackTrace();
          }
          String textmask = str(15);
          for (int j = 0; j < textmask.length(); j++) {
              g.setColor(new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(80) + 80));
              font = font.deriveFont(Font.BOLD, rand.nextInt(10) + 20);
              g.setFont(font);
              String strc = String.valueOf(textmask.charAt(j));
              int x = rand.nextInt(w);
              int y = rand.nextInt(h) + 20;
              g.drawString(strc, x, y);
          }
          //干扰曲线
          int linesize = rand.nextInt(5) + 1;
          for (int i1 = 0; i1 < linesize; i1++) {
              paint(g, w, h);
          }
          for (int j = 0; j < text.length(); j++) {
              g.setColor(getRandColor());
              font = font.deriveFont(Font.BOLD, rand.nextInt(20) + 30);
              g.setFont(font);
              String strc = String.valueOf(text.charAt(j));
              int x = j * 38 + 6;
              int y = rand.nextInt(35) + 20;
              double radianPercent = Math.PI * (rand.nextInt(35) / 180D);
              if (rand.nextBoolean())
                  radianPercent = -radianPercent;
              g.rotate(radianPercent, x, y);
              g.drawString(strc, x, y);
              g.rotate(-radianPercent, x, y);
          }
  
  
          g.dispose();
          String name = text + ".png";
          ImageIO.write(i, "png", new File("c:/" + name));
          System.out.println(text);
          Runtime run = Runtime.getRuntime();
          run.exec("cmd /k start c:/" + name);
      }
  
      public static Color getRandColor() {
          Random rand = new Random();
          return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
      }
  
      public static Color getRandColorAlpha() {
          Random rand = new Random();
          return new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
      }
  
      public static String str(int len) {
          String letter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
          Random rand = new Random();
          StringBuilder sbu = new StringBuilder();
          for (int i = 0; i < len; i++) {
              sbu.append(letter.charAt(rand.nextInt(letter.length())));
          }
          return sbu.toString();
      }
  
      public static String str() {
          return str(4);
      }
  
      public static void paint(Graphics2D g, int w, int h) {
          Random rand = new Random();
          g.setStroke(new BasicStroke(rand.nextInt(3) + 3));
          g.setColor(new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(5) + 1));//设置画线的颜色
          double y = 0;
          double x = 0;
          int a = rand.nextInt(50) + 10;
          int b = rand.nextInt(15) + 5;
          int c = rand.nextInt(150) + 30;
  
          //一个周期
          for (x = 0; x <= w; x += 0.1) {
              y = Math.sin(x * Math.PI / c);//转化为弧度,1度=π/180弧度
              //y = (200 + 5 * y);//便于在屏幕上显示
              y = (a + b * y);
               //g.drawString(".",(int)x,(int)y);//用这种方式也可以
              g.drawLine((int) x, (int) y, (int) x, (int) y);//画点
          }
      }
  }
  
  ```
  
  ![image-20210816115539434](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816115539434.png)
  
* 如何生成二维码，产品条形码

  ![image-20210121114241613](https://gitee.com/webrx/wx_note/raw/master/images/image-20210121114241613.png)
  
  pom.xml 添加依赖
  
  ```xml
  <dependency>
      <groupId>com.google.zxing</groupId>
      <artifactId>javase</artifactId>
      <version>3.4.1</version>
  </dependency>
  ```
  
  

```java
zxing 

package cn.img;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public class Img6 {

	public static void main(String[] args) throws Exception {
		// 二维码原理（将字符串信息通过一规则转换为图像）
		// 生成二维码
		String name = "c:/my.png";
		String content = "程序学习 2021";
		int width = 400;
		int height = 400;
		// BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height);

		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.MARGIN, 2);
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
		
		
		MatrixToImageWriter.writeToStream(bm, "png", new FileOutputStream(name));
		// BufferedImage bar = ImageIO.read(new File(name));
		// 解析读取二维码
	}

}

```

```java
package cn.img;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public class Img6 {

	public static void main(String[] args) throws Exception {
		// 二维码原理（将字符串信息通过一规则转换为图像）
		// 生成二维码
		String name = "c:/my.png";
		String content = "程序学习 2021";
		int width = 400;
		int height = 400;
		// BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE,
		// width, height);

		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.MARGIN, 2);
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);

		MatrixToImageWriter.writeToStream(bm, "png", new FileOutputStream(name));

		BufferedImage bar = ImageIO.read(new File(name));
		
		BufferedImage ok = new BufferedImage(width, height, 1);
		ok.getGraphics().drawImage(bar, 0, 0, width, height, null);
		
		
	

		//System.out.println(String.format("%x", Color.RED.getRGB()));
		// int[][] px = new int[width][height];
		Random rand = new Random();
		Color c = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				ok.setRGB(x, y, bar.getRGB(x,y)==-1 ? 0xffffffff : c.getRGB());
			}
			//if(x>5) break;
		}
		ImageIO.write(ok, "png", new File("c:/mymy.png"));

		// BufferedImage bar = ImageIO.read(new File(name));

		// 解压读取二维码

	}

}

```

![image-20210121152950956](https://gitee.com/webrx/wx_note/raw/master/images/image-20210121152950956.png)

```java
package cn.img;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

public class Img7 {
	public static void main(String[] args) throws Exception {
		// 二维码原理（将字符串信息通过一规则转换为图像）
		// 生成二维码 erweima.jpg g3.jpg

		// 解析读取二维码
		 //解析图像
        BufferedImage i = ImageIO.read(new File(name));
        LuminanceSource source = new BufferedImageLuminanceSource(i);
        BinaryBitmap image = new BinaryBitmap(new HybridBinarizer(source));
        try{
            Result result = new MultiFormatReader().decode(image);
            String info = result.getText();
            System.out.println(info);
            if(info==null || "".equals(info)){
                System.out.println("没有二维码信息");
            }else if(info.contains("weixin")){  //qq mp
                System.out.println("有微信二维码，不允许");
            }else{
                System.out.println("有二维，可以使用");
            }
        }catch(Exception e){
            System.out.println("图像中没有二维码");
        }

	}

}

```

![image-20210121163034782](https://gitee.com/webrx/wx_note/raw/master/images/image-20210121163034782.png)

```java
package cn.img;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public class Img8 {

	public static void main(String[] args) throws Exception {
		// 生成产品条码效果
		String name = "c:/bar.png";
		String content = "6923790798701";
		int width = 300;
		int height = 100;

		BitMatrix bm = new MultiFormatWriter().encode(content, BarcodeFormat.EAN_13, width, height);

		MatrixToImageWriter.writeToStream(bm, "png", new FileOutputStream(name));

		BufferedImage bar2 = ImageIO.read(new File(name));
		BufferedImage bar = new BufferedImage(bar2.getWidth(), bar2.getHeight() + 30, 1);

		Graphics2D g = bar.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, bar2.getWidth(), bar2.getHeight() + 30);
		g.drawImage(bar2, 0, 15, bar2.getWidth(), bar2.getHeight(), null);
		g.fillRect(62, 100, 85, 15);
		g.fillRect(155, 100, 83, 15);
		g.setColor(Color.BLACK);
		g.setFont(new Font("", Font.PLAIN, 22));
		
		
		g.drawString(content.substring(0, 1), 40, 120);
		g.drawString(content.substring(1, 7), 68, 120);
		g.drawString(content.substring(7), 158, 120);
		g.dispose();

		ImageIO.write(bar, "png", new File("c:/bar2.png"));

	}

}

```



* 水印效果文字水印图标水印

```java
package cn.img;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

public class Img9 {

	public static void main(String[] args) throws Exception {
		// 生成产品条码效果
		String name = "d:/w.webp/aa.jpg";
		BufferedImage bar = ImageIO.read(new File(name));
		Graphics2D g = bar.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

		g.setColor(new Color(255, 0, 0, 100));

		Font font = Font.createFont(Font.TRUETYPE_FONT, new File("user/zh74hfmss.ttf"));
		font = font.deriveFont(80f);
		font = font.deriveFont(Font.BOLD);
		g.setFont(font);

		String text = "版权所有";
		int x = 30;
		int y = 150;
		g.drawString(text, x, y);

		g.dispose();

		String n1 = name.substring(0, name.lastIndexOf("."));
		String n2 = name.substring(name.lastIndexOf("."));
		String nn = n1 + "_text" + n2;
		ImageIO.write(bar, "png", new File(nn));

		Runtime run = Runtime.getRuntime();
		run.exec("cmd /k start " + nn);
	}
}

```

![image-20210121173533299](https://gitee.com/webrx/wx_note/raw/master/images/image-20210121173533299.png)

* 水印图标效果（logo.png)

```java
public void logo(File logo, int pos, boolean flag) {
		try {
			BufferedImage log = ImageIO.read(logo);
			int lw = log.getWidth();
			int lh = log.getHeight();
			int x = 0, y = 0;
			switch (pos) {
			case 1:
				x = 30;
				y = 30;
				break;
			case 2:
				x = 0;
				y = 0;
				break;
			case 3:
				x = 0;
				y = 0;
				break;
			case 4:
				x = 0;
				y = 0;
				break;
			case 5:
				x = (int) ((this.width - lw) / 2.0);
				y = (int) ((this.height - lh) / 2.0);
				break;
			case 6:
				x = 0;
				y = 0;
				break;
			case 7:
				x = 0;
				y = 0;
				break;
			case 8:
				x = (int) ((this.width - lw) / 2.0);
				y = this.height - lh - 30;
				break;
			case 9:
				x = this.width - lw - 30;
				y = this.height - lh - 30;
				break;
			default:
				x = rand.nextInt(this.width - lw) + 30;
				y = rand.nextInt(this.height - lh) + 30;
				break;
			}
			this.g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, .5f));
			this.g.drawImage(log, x, y, null);
			this.g.dispose();
			this.save(flag, "_logo");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
```



![image-20210122155230866](https://gitee.com/webrx/wx_note/raw/master/images/image-20210122155230866.png)

* 文字线条平滑，文字坐标位置控制

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.imgs;
  
  import javax.imageio.ImageIO;
  import java.awt.*;
  import java.awt.font.LineMetrics;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.io.IOException;
  import java.util.Random;
  
  /**
   * <p>Project: javaseapp - Img3
   * <p>Powered by webrx On 2021-08-13 09:05:50
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class Img3 {
      public static void main(String[] args) {
          int w = 800, h = 500, t = 2;
          var i = new BufferedImage(w, h, t);
          var g = i.createGraphics();
          //设置图像中文本，线程的平滑
          g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
          g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  
          //设置随机的背景颜色
          g.setColor(getRandColor());
          g.fillRect(0, 0, w, h);
  
          //画布写字
          Font font = new Font("微软雅黑", Font.BOLD, 30);
          try {
              font = Font.createFont(Font.TRUETYPE_FONT, new File("font/zh210gfsnt.ttf"));
          } catch (FontFormatException e) {
              e.printStackTrace();
          } catch (IOException e) {
              e.printStackTrace();
          }
          font = font.deriveFont(Font.BOLD, 130f);
          g.setFont(font);
          Color c = getRandColor();
          g.setColor(new Color(0, 0, 0, 150));
          String text = "喜报";
          FontMetrics fm = g.getFontMetrics();
          int tw = fm.stringWidth(text);
          int th = fm.getHeight();
          LineMetrics line = fm.getLineMetrics(text, g);
  
          float ascent = line.getAscent();
          float descent = line.getDescent();
  
  
          float x = 0.0f;
          float y = ascent - descent / 2;
  
          x = w - tw;
          y = h - descent / 2;
  
          x = (w - tw) / 2;
          y = (h - th) / 2 + ascent;
          g.drawString(text, x, y);
  
  
          g.dispose();
          try {
              ImageIO.write(i, "png", new File("t.png"));
              Runtime run = Runtime.getRuntime();
              run.exec("cmd /k start t.png");
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
  
      public static Color getRandColor() {
          Random rand = new Random();
          Color c = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
          return c;
      }
  }
  
  ```

  

* 裁剪图像

  ```java
  package cn.img;
  import java.awt.AlphaComposite;
  import java.awt.Color;
  import java.awt.Font;
  import java.awt.FontMetrics;
  import java.awt.Graphics2D;
  import java.awt.RenderingHints;
  import java.awt.image.BufferedImage;
  import java.io.File;
  import java.util.Random;
  import javax.imageio.ImageIO;
  public class Img10 {
  	public static void main(String[] args) throws Exception {
  		String name = "c:/office.jpg";
  		BufferedImage src = ImageIO.read(new File(name));
  		int sw = src.getWidth();
  		int sh = src.getHeight();
  
  		int dw = 300;
  		int dh = (int) (sh * (1.0 * dw / sw));
  		dh = 300;
  		BufferedImage dst = new BufferedImage(dw, dh, 1);
  		Graphics2D g = dst.createGraphics();
  		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
  		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
  
  		// g.setColor(Color.BLUE);
  		// g.fillRect(0, 0, dw, dh);
  
  		// g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, .5f));
  		// g.drawImage(src, 0, 0, dw, dh, null);
  		int x = 620;
  		int y = 60;
  		g.drawImage(src, 0, 0, dw, dh, x, y, x + dw, dh + y, null);
  		g.dispose();
  		String n1 = name.substring(0, name.lastIndexOf("."));
  		String n2 = name.substring(name.lastIndexOf("."));
  		String nn = n1 + "_ok" + n2;
  		ImageIO.write(dst, name.substring(name.lastIndexOf(".") + 1), new File(nn));
  		Runtime run = Runtime.getRuntime();
  		run.exec("cmd /k start " + nn);
  	}
  }
  ```
  
  
  
  ![image-20210122163549034](https://gitee.com/webrx/wx_note/raw/master/images/image-20210122163549034.png)
  
* 缩略图效果和改变图像尺寸

```java
package cn.img;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
public class Img11 {
	public static void main(String[] args) throws Exception {
		String name = "c:/jd.jpg";
		BufferedImage src = ImageIO.read(new File(name));
		int sw = src.getWidth();
		int sh = src.getHeight();

		int dw = 350;
		int dh = (int) (sh * (1.0 * dw / sw));
		//dh = 300;
		
		dw = 350;
		dh = 350;
		BufferedImage dst = new BufferedImage(dw, dh, 1);
		Graphics2D g = dst.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);

		// g.setColor(Color.BLUE);
		// g.fillRect(0, 0, dw, dh);

		// g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, .5f));
		g.drawImage(src, 0, 0, dw, dh, null);
		g.dispose();
		String n1 = name.substring(0, name.lastIndexOf("."));
		String n2 = name.substring(name.lastIndexOf("."));
		String nn = n1 + String.format("_ok(%d_%d)",dw,dh) + n2;
		ImageIO.write(dst, name.substring(name.lastIndexOf(".") + 1), new File(nn));
		Runtime run = Runtime.getRuntime();
		run.exec("cmd /k start " + nn);
	}
}
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * <p>Project: javase - Img8
 * <p>Powered by webrx On 2021-12-21 09:08:21
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Img8 {
    public static void main(String[] args) throws IOException {
        var src = ImageIO.read(new File("c:/s.jpg"));
        var sw = src.getWidth();
        var sh = src.getHeight();

        var dw = 120;
        var dh = 120;
        var dst = new BufferedImage(dw, dh, 1);
        var g = dst.createGraphics();
        //缩略图 0,0 dw,dh 目标宽高
        //g.drawImage(src, 0, 0, dw, dh, null);
        //g.drawImage(src, 0, 0, dw, dh, 0, 0, sw, sh, null);

        //剪切图像 0,0 sw,sh 源图像宽高
        int x = 130;
        int y = 10;
        //g.drawImage(src, -x, -y, sw, sh, null);
        //裁剪图像，src 代码要裁剪的图像  0 0 dw dh 目标图像尺寸
        //x,y,x+dw,y+dh 被裁剪的图像的位置尺寸，null父依赖对象swing，
        g.drawImage(src, 0, 0, dw, dh, x, y, x + dw, y + dh, null);
        dst = rotateImage(dst, 90);
        //释放占用的内存
        g.dispose();
        //输出dst缓存图像到文件中,jpg是格式 gif png jpg
        ImageIO.write(dst, "jpg", new File("c:/s01.jpg"));

        System.out.println(sw);
        System.out.println(sh);

    }

    /**
     * 旋转图片为指定角度
     *
     * @param bufferedimage 目标图像
     * @param degree        旋转角度
     * @return
     */
    public static BufferedImage rotateImage(final BufferedImage bufferedimage,
                                            final int degree) {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(w, h, type))
                .createGraphics()).setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2);
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }

    /**
     * 变更图像为指定大小
     *
     * @param bufferedimage 目标图像
     * @param w             宽
     * @param h             高
     * @return
     */
    public static BufferedImage resizeImage(final BufferedImage bufferedimage,
                                            final int w, final int h) {
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = create(w, h, type))
                .createGraphics()).setRenderingHint(
                RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.drawImage(bufferedimage, 0, 0, w, h, 0, 0, bufferedimage
                .getWidth(), bufferedimage.getHeight(), null);
        graphics2d.dispose();
        return img;
    }

    /**
     * 水平翻转图像
     *
     * @param bufferedimage 目标图像
     * @return
     */
    public static BufferedImage flipImage(final BufferedImage bufferedimage) {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = create(w, h, bufferedimage
                .getColorModel().getTransparency())).createGraphics())
                .drawImage(bufferedimage, 0, 0, w, h, w, 0, 0, h, null);
        graphics2d.dispose();
        return img;
    }

    public static BufferedImage create(int w, int h, int type) {
        return new BufferedImage(w, h, type);
    }
}


```



### 作业：

1. 统计某目录有多少java文件

   ```java
   public class FileUtils {
       public static int num = 0;
   
       public FileUtils() {
       }
   
       public static int count(String dir, String ext) {
           File dirs = new File(dir);
           return count(dirs, ext);
       }
   
       public static int count(File dir, String ext) {
           for (File f : dir.listFiles()) {
               if (f.isDirectory()) {
                   count(f, ext);
               } else if (f.isFile() && f.getName().endsWith(ext)) {
                   ++num;
               }
           }
           return num;
       }
   }
   ```

2. 统计某目录有分别有多少种文件及个数   java 20,jpg 15,css 20

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   import java.io.File;
   import java.util.HashMap;
   import java.util.Map;
   
   /**
    * <p>Project: javaseapp - Ex03
    * <p>Powered by webrx On 2021-08-12 08:49:30
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class Ex03 {
       static Map<String, Integer> map = new HashMap<>();
   
       public static void main(String[] args) {
           File dir = new File("D:\\ftp");
           //System.out.println(dir.getAbsolutePath());
           count(dir);
   
           //System.out.println(map);
   
           System.out.println(map.size() + "种文件");
           map.forEach((k, v) -> System.out.printf("%s = %d%n", k, v));
       }
   
       public static void count(File f) {
           if (f.isDirectory()) {
               for (File file : f.listFiles()) {
                   if (file.isDirectory()) {
                       count(file);
                   } else if (file.isFile()) {
                       String name = file.getName();
                       int index = name.lastIndexOf(".");
                       String ext = index == -1 ? name : name.substring(index);
                       if (map.containsKey(ext)) {
                           map.put(ext, map.get(ext) + 1);
                       } else {
                           map.put(ext, 1);
                       }
                   }
               }
           }
       }
   }
   
   ```

   

3. 使用递归方法，实现删除非空目录

   ```java
   public static void delDir(String dir) {
       delDir(new File(dir));
   }
   
   public static void delDir(File dir) {
       File[] fs = dir.listFiles();
       if (fs.length == 0) {
           dir.delete(); //删除空目录
       } else {
           for (File f : fs) {
               if (f.isDirectory()) {
                   delDir(f);//再次递归调用，删除目录
               } else if (f.isFile()) {
                   f.delete();//删除文件
               }
           }
       }
       dir.delete();
   }
   ```

   

4. 统计自己编写了多行代码

   ```java
   public static long countSourceRows(File file) {
       long rows = 0;
       try (BufferedReader br = new BufferedReader(new FileReader(file))) {
           rows = br.lines().count();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return rows;
   }
   
   public static long countDirRows(File dir) {
       for (File f : dir.listFiles()) {
           if (f.isDirectory()) {
               countDirRows(f);
           } else if (f.isFile() && f.getName().endsWith(".java")) {
               rows += countSourceRows(f);
           }
       }
       return rows;
   }
   ```

   

5. 如何判断两个文件是不是一个文件

   >1)文件大小一样
   >
   >2)字节内容一样

   ```xml
   <!-- 字符串md5 文件md5 -->
   <!-- commons-codec -->
   <dependency>
       <groupId>commons-codec</groupId>
       <artifactId>commons-codec</artifactId>
       <version>1.15</version>
   </dependency>
   <!--
   String a = "C:\\Users\\Administrator\\Desktop\\aa.rar";
   String b = "C:\\Users\\Administrator\\Desktop\\cc.rar";
   var x = DigestUtils.md5Hex(new FileInputStream(a));
   var y = DigestUtils.md5Hex(new FileInputStream(b));
   if (x.equals(y)) {
    System.out.printf("%s 和 %s 是同一个文件", "aa.rar", "cc.rar");
   } else {
    System.out.printf("%s 和 %s 不是同一个文件", "aa.rar", "cc.rar");
   }
   
   System.out.println(DigestUtils.md5Hex("123"));
   -->
   
   <dependency>
       <groupId>org.projectlombok</groupId>
       <artifactId>lombok</artifactId>
       <version>1.18.20</version>
       <scope>provided</scope>
   </dependency>
   ```

   ```java
   public static boolean isSames(String one, String two) {
       return isSames(new File(one), new File(two));
   }
   
   public static boolean isOk(File one, File two) {
       //apache commons codec 文件 md5
       boolean f = false;
       try {
           FileInputStream fo = new FileInputStream(one);
           FileInputStream ft = new FileInputStream(two);
           String f1 = DigestUtils.md5Hex(fo);
           String f2 = DigestUtils.md5Hex(ft);
           if (f1.equalsIgnoreCase(f2)) {
               f = true;
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
       return f;
   }
   
   public static boolean isSames(File one, File two) {
       boolean f = false;
       if (one.length() == two.length()) {
           try (FileInputStream fo = new FileInputStream(one); FileInputStream ft = new FileInputStream(two)) {
               long i = 0;
               while (fo.read() == ft.read()) {
                   ++i;
                   if (i == one.length() * 0.01) {
                       f = true;
                       break;
                   }
               }
   
           } catch (Exception e) {
           }
       }
       return f;
   }
   ```

   

6. 如何识别一个文件本质是什么文件（user.jpg   abc.mp4)

   > .jpg .gif .png .rar .zip .pdf .mp4 .mp3

   ```java
   public static String getFileType(String file) {
       return getFileType(new File(file));
   }
   
   public static String getFileType(File file) {
       String e = "unknow";
       var map = new HashMap<String, String>();
       map.put("jpg", "FFD8FF"); //JPEG (jpg)
       map.put("png", "89504E47");  //PNG (png)
       map.put("gif", "47494638");  //GIF (gif)
       map.put("tif", "49492A00");  //TIFF (tif)
       map.put("bmp", "424D"); //Windows Bitmap (bmp)
       map.put("dwg", "41433130"); //CAD (dwg)
       map.put("html", "68746D6C3E");  //HTML (html)
       map.put("rtf", "7B5C727466");  //Rich Text Format (rtf)
       map.put("xml", "3C3F786D6C");
       map.put("zip", "504B0304");
       map.put("rar", "52617221");
       map.put("psd", "38425053");  //Photoshop (psd)
       map.put("eml", "44656C69766572792D646174653A");  //Email [thorough only] (eml)
       map.put("dbx", "CFAD12FEC5FD746F");  //Outlook Express (dbx)
       map.put("pst", "2142444E");  //Outlook (pst)
       map.put("xls", "D0CF11E0");  //MS Word
       map.put("doc", "D0CF11E0");  //MS Excel 注意：word 和 excel的文件头一样
       map.put("mdb", "5374616E64617264204A");  //MS Access (mdb)
       map.put("wpd", "FF575043"); //WordPerfect (wpd)
       map.put("eps", "252150532D41646F6265");
       map.put("ps", "252150532D41646F6265");
       map.put("pdf", "255044462D312E");  //Adobe Acrobat (pdf)
       map.put("qdf", "AC9EBD8F");  //Quicken (qdf)
       map.put("pwl", "E3828596");  //Windows Password (pwl)
       map.put("wav", "57415645");  //Wave (wav)
       map.put("avi", "41564920");
       map.put("ram", "2E7261FD");  //Real Audio (ram)
       map.put("rm", "2E524D46");  //Real Media (rm)
       map.put("mpg", "000001BA");  //
       map.put("mov", "6D6F6F76");  //Quicktime (mov)
       map.put("asf", "3026B2758E66CF11"); //Windows Media (asf)
       map.put("mid", "4D546864");  //MIDI (mid)
       String ext = getFileHeaderInfo(file, 14);
       for (String k : map.keySet()) {
           if (ext.startsWith(map.get(k))) {
               e = k;
               break;
           }
       }
       return e;
   }
   
   public static String getFileHeaderInfo(File file, int size) {
       StringBuilder sbu = new StringBuilder();
       byte[] buf = new byte[size];
       try (FileInputStream fis = new FileInputStream(file)) {
           fis.read(buf);
       } catch (IOException e) {
           e.printStackTrace();
       }
       for (byte b : buf) {
           sbu.append(String.format("%02X", b));
       }
       return sbu.toString();
   }
   ```

   

7. 实现复制一个目录到指定的位置（复制目录）

   ```java
   public static void copyFile(File src, File dst) {
       try (FileInputStream fis = new FileInputStream(src);
            FileOutputStream fos = new FileOutputStream(dst)) {
           int len;
           byte[] buffer = new byte[4096];
           while ((len = fis.read(buffer)) > 0) {
               fos.write(buffer, 0, len);
           }
       } catch (IOException e) {
           // ... handle IO exception
       }
   }
   
   public static void copyFile(String src, String dst) {
       copyFile(new File(src), new File(dst));
   }
   
   public static void copyDir(File src, File dst) {
       if (!src.isDirectory()) {
           return;
       }
       if (!dst.exists()) {
           dst.mkdirs();
       }
       File[] files = src.listFiles();
       for (File f : files) {
           if (f.isDirectory()) {
               copyDir(f, new File(dst.getPath(), f.getName()));
           } else if (f.isFile()) {
               copyFile(f, new File(dst.getPath(), f.getName()));
           }
       }
   }
   
   public static void copyDir(String src, String dst) {
       copyDir(new File(src), new File(dst));
   }
   ```
   
   
   
   ![image-20210818144028659](https://gitee.com/webrx/wx_note/raw/master/images/image-20210818144028659.png)
   
   ![image-20210818144131021](https://gitee.com/webrx/wx_note/raw/master/images/image-20210818144131021.png)
   
   apache commons io 有复制目录的功能
   
   ```xml
   <dependency>
       <groupId>commons-io</groupId>
       <artifactId>commons-io</artifactId>
       <version>2.11.0</version>
   </dependency>
   ```

![image-20210817114626546](assets/image-20210817114626546.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

/**
 * <p>Project: javaseapp - Demo
 * <p>Powered by webrx On 2021-08-17 11:37:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Demo {

    @Test
    public void m1() {
        System.out.println("Test Junit m1()");
    }

    @Test
    public void m2() {
        try {
            File src = new File("../dict");
            //System.out.println(src.exists());
            //System.out.println(src.isDirectory());
            File dst = new File("c:/mydict");
            //实现的目录复制
            FileUtils.copyDirectory(src, dst);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

```



8. 编写程序，实现对打字速度的那个文件，分析，要求输出每个人 平均速度，如果可以，对平均速度排序，输出到一个文件。
9. 编写程序统计java jdk源码有多少java文件，有多行少代码，有多少个单词，单词现出的频率。
10. 了解POI(操作办公文件word excel ppt )，了解加缩解压。
11. 编写一个类，调用一个方法，直接给图像添加水印文字效果。（控制水印文字位置）
12. 编写程序实现批量添加水印效果
13. 编写工具类StringUtil.java
14. 编写工具类FileUtil.java
15. 编写工具类ImgUtil.java
16. 编写工具类DateUtil.java

### 补充工具jar组件：

* thumbnailator 图像处理组件

  ```xml
  <!-- https://mvnrepository.com/artifact/net.coobird/thumbnailator -->
  <dependency>
      <groupId>net.coobird</groupId>
      <artifactId>thumbnailator</artifactId>
      <version>0.4.14</version>
  </dependency>
  <!-- https://mvnrepository.com/artifact/net.coobird/thumbnailator -->
  <dependency>
      <groupId>net.coobird</groupId>
      <artifactId>thumbnailator</artifactId>
      <version>0.4.15</version>
  </dependency>
  
  
  <!--
  从图像文件创建缩略图
  Thumbnails.of(new File("original.jpg"))
          .size(160, 160)
          .toFile(new File("thumbnail.jpg"));
  在此示例中，图像来自original.jpg调整大小，然后保存到thumbnail.jpg。
  
  或者，Thumbnailator将接受文件名作为String。File不需要使用对象指定图像文件：
  
  Thumbnails.of("original.jpg")
          .size(160, 160)
          .toFile("thumbnail.jpg");
  在编写快速原型代码或从脚本语言中使用Thumbnailator时，此表单非常有用。
  
  使用旋转和水印创建缩略图
  Thumbnails.of(new File("original.jpg"))
          .size(160, 160)
          .rotate(90)
          .watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("watermark.png")), 0.5f)
          .outputQuality(0.8)
          .toFile(new File("image-with-watermark.jpg"));
  在此示例中，original.jpg调整图像大小，然后顺时针旋转90度，然后在右下角放置一个半透明水印，然后image-with-watermark.jpg以80％压缩质量设置保存。
  
  创建缩略图并写入 OutputStream
  OutputStream os = ...;
  		
  Thumbnails.of("large-picture.jpg")
          .size(200, 200)
          .outputFormat("png")
          .toOutputStream(os);
  在此示例中，将文件中的图像large-picture.jpg调整为最大尺寸200 x 200（保持原始图像的纵横比），并将其写入指定OutputStream的PNG图像。
  
  创建固定大小的缩略图
  BufferedImage originalImage = ImageIO.read(new File("original.png"));
  
  BufferedImage thumbnail = Thumbnails.of(originalImage)
          .size(200, 200)
          .asBufferedImage();
  上面的代码采用图像originalImage并创建一个200像素乘200像素的缩略图，并使用并存储结果thumbnail。
  
  按给定因子缩放图像
  BufferedImage originalImage = ImageIO.read(new File("original.png"));
  
  BufferedImage thumbnail = Thumbnails.of(originalImage)
          .scale(0.25)
          .asBufferedImage();
  上面的代码将图像originalImage带入并创建一个缩略图，该缩略图是原始图像的25％，并使用默认缩放技术来制作存储在其中的缩略图thumbnail。
  
  创建缩略图时旋转图像
  BufferedImage originalImage = ImageIO.read(new File("original.jpg"));
  
  BufferedImage thumbnail = Thumbnails.of(originalImage)
          .size(200, 200)
          .rotate(90)
          .asBufferedImage();
  上面的代码采用原始图像并创建一个顺时针旋转90度的缩略图。
  
  使用水印创建缩略图
  BufferedImage originalImage = ImageIO.read(new File("original.jpg"));
  BufferedImage watermarkImage = ImageIO.read(new File("watermark.png"));
  
  BufferedImage thumbnail = Thumbnails.of(originalImage)
          .size(200, 200)
          .watermark(Positions.BOTTOM_RIGHT, watermarkImage, 0.5f)
          .asBufferedImage();
  如图所示，可以通过调用该watermark方法将水印添加到缩略图。
  
  可以从Positions枚举中选择定位。
  
  缩略图的不透明度（或相反地，透明度）可以通过改变最后一个参数来调整，其中0.0f缩略图是完全透明的，并且1.0f水印是完全不透明的。
  
  将缩略图写入特定目录
  File destinationDir = new File("path/to/output");
  
  Thumbnails.of("apple.jpg", "banana.jpg", "cherry.jpg")
          .size(200, 200)
          .toFiles(destinationDir, Rename.PREFIX_DOT_THUMBNAIL);
  此示例将获取源图像，并将缩略图作为文件写入destinationDir（path/to/output目录），同时thumbnail.在文件名前添加重命名。
  
  因此，缩略图将被写为以下文件：
  
  path/to/output/thumbnail.apple.jpg
  path/to/output/thumbnail.banana.jpg
  path/to/output/thumbnail.cherry.jpg
  写入指定目录时，也可以保留原始文件名：
  
  File destinationDir = new File("path/to/output");
  
  Thumbnails.of("apple.jpg", "banana.jpg", "cherry.jpg")
          .size(200, 200)
          .toFiles(destinationDir, Rename.NO_CHANGE);
  在上面的代码中，缩略图将写入：
  
  path/to/output/apple.jpg
  path/to/output/banana.jpg
  path/to/output/cherry.jpg
  
  
  
  -->
  ```

* EasyCaptcha 验证码组件

  ```xml
  <!-- Java图形验证码，支持gif、中文、算术等类型，可用于Java Web、JavaSE等项目。 -->
  <dependency>
      <groupId>com.github.whvcse</groupId>
      <artifactId>easy-captcha</artifactId>
      <version>1.6.2</version>
  </dependency>
  ```
  
  ```java
  @Test
  public void cc1() throws FileNotFoundException {
      // png类型
      SpecCaptcha captcha = new SpecCaptcha(130, 48);
      captcha.text();  // 获取验证码的字符
      captcha.textChar();  // 获取验证码的字符数组
  
      // gif类型
      var gif = new GifCaptcha(130, 48);
      gif.setLen(2);
  
      // 中文类型
      var cn = new ChineseCaptcha(130, 48);
  
      // 中文gif类型
      var cngif = new ChineseGifCaptcha(130, 48);
  
      // 算术类型
      var ari = new ArithmeticCaptcha(130, 48);
      ari.setLen(3);  // 几位数运算，默认是两位
      //captcha.getArithmeticString();  // 获取运算的公式：3+2=?
      //captcha.text();  // 获取运算的结果：5
  
      cn.out(new FileOutputStream("../dict/c2.png"));  // 输出验证码
  }
  ```
  
  
  
* The Apache Commons IO

  ```xml
  <!-- 复制目录 复制文件 剪切 删除目录 删除文件 FileUtils FileNameUtils -->
  <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.5</version>
  </dependency>
  
  ```

* zxing 二维码 条码

  ```xml
  <!-- https://mvnrepository.com/artifact/com.google.zxing/javase -->
  <dependency>
      <groupId>com.google.zxing</groupId>
      <artifactId>javase</artifactId>
      <version>3.4.1</version>
  </dependency>
  
  ```

  作业：
  
  求一个字符串表达式，计算结果
  
  1*6-3+1 = ?
  
  2*3/2-1=?
  
  1+2+3-5*2=?

## 第十章 Lambda 表达式

> Lambda 表达式”(lambda expression)是一个匿名函数，Lambda表达式基于数学中的λ演算得名，直接对应于其中的lambda抽象(lambda abstraction)，是一个匿名函数，即没有函数名的函数。Lambda表达式可以表示闭包（注意和数学传统意义上的不同）

### 10.1 lambda

Lambda 表达式，也可称为闭包，它是推动 Java 8 发布的最重要新特性。

Lambda 允许把函数作为一个方法的参数（函数作为参数传递进方法中）。

使用 Lambda 表达式可以使代码变的更加简洁紧凑，有效避免内部匿名类出现。

`javascript lambda =>`

`java lambda  ->   ::`

()->

->

()->{}

a->a

a->a*a

()->a

a->System.out::println;

![image-20210820092516735](https://gitee.com/webrx/wx_note/raw/master/images/image-20210820092516735.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * 函数式接口(这样的接口有且只有一个抽象方法)
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
@FunctionalInterface
public interface DbDao {
    int pf(int i);

    static int lf(int n) {
        return n * n * n;
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Demo
 * <p>Powered by webrx On 2021-08-20 08:49:01
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Demo {
    public static void main(String[] args) {
        DbDao db = new DbDao() {
            public int pf(int i) {
                return i * i;
            }
        };

        System.out.println(db.pf(19));

        //使用了lambda 表达式，
        var db2 = (DbDao) a -> a * a;
        DbDao db3 = e -> e * e;
        System.out.println(db2.pf(3));
        System.out.println(db3.pf(6));

        UnaryOperator<Integer> o = i -> i * i;
        System.out.println(o.apply(3));

        UnaryOperator<String> hello = (s) -> "hello:" + s;
        System.out.println(hello.apply("李四"));

        Comparator<Integer> c = Integer::compare;
        System.out.println(c.compare(1, 0));
    }
}

```

```java
 @Test
public void test6() {
        /*************** 方法的引用 ****************/
        // 类：：静态方法名
        Comparator<Integer> bb = Integer::compare;
        System.out.println(bb.compare(3, 2));
        Comparator<Integer> cc = (x, y) -> Integer.compare(x, y);
        System.out.println(cc.compare(3, 2));
 
        Comparator<Integer> dd = (x, y) -> x.compareTo(y);
        System.out.println(dd.compare(3, 2));
        Comparator<Integer> ee = Integer::compareTo;
        System.out.println(ee.compare(3, 2));
        // 类：：实例方法名
        BiPredicate<String, String> bp = (x, y) -> x.equals(y);
        System.out.println(bp.test("a", "b"));
        BiPredicate<String, String> bp1 = String::equals;
        System.out.println(bp1.test("a", "b"));
 
        // 对象：：实例方法名
        Consumer<String> con1 = x -> System.out.println(x);
        con1.accept("abc");
        Consumer<String> con = System.out::println;
        con.accept("abc");
 
        Emp emp = new Emp("上海", "xiaoMIng", 18);
        Supplier<String> supper1 = () -> emp.getAddress();
        System.out.println(supper1.get());
        Supplier<String> supper = emp::getAddress;
        System.out.println(supper.get());
 
        /*************** 构造器的引用 ****************/
        // 无参构造函数，创建实例
        Supplier<Emp> supper2 = () -> new Emp();
        Supplier<Emp> supper3 = Emp::new;
        Emp emp1 = supper3.get();
        emp1.setAddress("上海");
        // 一个参数
        Function<String, Emp> fun = address -> new Emp(address);
        Function<String, Emp> fun1 = Emp::new;
        System.out.println(fun1.apply("beijing"));
        // 两个参数
        BiFunction<String, Integer, Emp> bFun = (name, age) -> new Emp(name, age);
        BiFunction<String, Integer, Emp> bFun1 = Emp::new;
        System.out.println(bFun1.apply("xiaohong", 18));
 
    }
 
    static class Emp {
        private String address;
 
        private String name;
 
        private Integer age;
 
        public Emp() {
 
        }
 
        public Emp(String address) {
            this.address = address;
        }
 
        public Emp(String name, Integer age) {
            this.name = name;
            this.age = age;
        }
 
        public Emp(String address, String name, Integer age) {
            super();
            this.address = address;
            this.name = name;
            this.age = age;
        }
 
        public String getAddress() {
            return address;
        }
 
        public void setAddress(String address) {
            this.address = address;
        }
 
        public String getName() {
            return name;
        }
 
        public void setName(String name) {
            this.name = name;
        }
 
        public Integer getAge() {
            return age;
        }
 
        public void setAge(Integer age) {
            this.age = age;
        }
 
        @Override
        public String toString() {
            return "Emp [address=" + address + ", name=" + name + ", age=" + age + "]";
        }
 
}
```

### 10.2 排序实例

* 数字排序

```java
package part10;
import java.util.Arrays;
import java.util.Comparator;
public class Lam2 {
	public static void main(String[] args) {
		Integer[] num = new Integer[] { 1, 6, 2, 5, 10, 4 };
		System.out.println(Arrays.toString(num));

		Arrays.sort(num);
		System.out.println(Arrays.toString(num));

		//Arrays.sort(num, (a, b) -> a - b);
		Arrays.sort(num,new Comparator<Integer>() {
			public int compare(Integer a, Integer b) {
				return a-b;
			}
		});
		
		System.out.println(Arrays.toString(num));

		Arrays.sort(num, (a, b) -> b - a);
		/*
		Arrays.sort(num,new Comparator<Integer>() {
			public int compare(Integer a, Integer b) {
				return a-b;
			}
		}.reversed());
		*/
		System.out.println(Arrays.toString(num));
        
        //乱序
        Random rand = new Random();
        Arrays.sort(nums, (a, b) -> rand.nextBoolean() ? -1 : 1);
        Arrays.sort(nums, (a, b) -> rand.nextBoolean() ? -1 : 1);
        Arrays.sort(nums, (a, b) -> rand.nextBoolean() ? -1 : 1);
        Arrays.sort(nums, (a, b) -> rand.nextBoolean() ? -1 : 1);
        System.out.println(Arrays.toString(nums));
        
        //Array 转换为 List 使用 shuffle打乱顺序，并转换为Array
        var list = new ArrayList<>(Arrays.stream(nums).toList());
        Collections.shuffle(list);
        list.toArray(nums);
        System.out.println(Arrays.toString(nums));
	}
}

```

* 字符串排序方法

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.Arrays;
  import java.util.Comparator;
  import java.util.Random;
  
  /**
   * <p>Project: javase - StringSort
   * <p>Powered by webrx On 2021-12-21 14:29:10
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 17
   */
  public class StringSort {
      public static void main(String[] args) {
          String[] langs = {"java", "javascript", "c", "go", "python"};
          System.out.println(Arrays.toString(langs));
  
          //升序
          Arrays.sort(langs);
          System.out.println(Arrays.toString(langs));
  
          //降序
          //Arrays.sort(langs,(a,b)->b.compareTo(a));
          Arrays.sort(langs, Comparator.reverseOrder());
          System.out.println(Arrays.toString(langs));
  
          //乱序
          Random rand = new Random();
          Arrays.sort(langs, (a, b) -> rand.nextBoolean() ? 1 : -1);
          Arrays.sort(langs, (a, b) -> rand.nextBoolean() ? 1 : -1);
          Arrays.sort(langs, (a, b) -> rand.nextBoolean() ? 1 : -1);
          Arrays.sort(langs, (a, b) -> rand.nextBoolean() ? 1 : -1);
          System.out.println(Arrays.toString(langs));
  
          //根据字母个数
          //Arrays.sort(langs, (a, b) -> a.length() - b.length());
          Arrays.sort(langs, Comparator.comparingInt(String::length));
          System.out.println(Arrays.toString(langs));
  
          //根据字母个数
          Arrays.sort(langs, (a, b) -> b.length() - a.length());
          System.out.println(Arrays.toString(langs));
  
          //乱序
          Arrays.sort(langs, (a, b) -> Math.random() > .5 ? 1 : -1);
          Arrays.sort(langs, (a, b) -> Math.random() > .5 ? 1 : -1);
          Arrays.sort(langs, (a, b) -> Math.random() > .5 ? 1 : -1);
          Arrays.sort(langs, (a, b) -> Math.random() > .5 ? 1 : -1);
          System.out.println(Arrays.toString(langs));
      }
  }
  
  ```

  

```java
package part10;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符串数组排序实现
 * 
 * @author webrx
 *
 */
public class Lam2 {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>(List.of("java", "B", "c", "jcva", "basic", "javascript", "python", "go", "perl", "php"));
		// 没有排序
		System.out.println(list);

		// 降序
		list.sort((a, b) -> b.compareTo(a));
		System.out.println(list);

		// 升序
		list.sort((a, b) -> a.compareTo(b));
		System.out.println(list);

		
		//根据字符串字符的个数，按大到小排序
		list.sort((a, b) -> b.length() - a.length());
		System.out.println(list);
	}
}

```

* 对象排序方法

```java
User.java
/**
 * 
 */
package part10;

public class User {
	private int id;
	private String name;
	private int score;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public User(int id, String name, int score) {
		super();
		this.id = id;
		this.name = name;
		this.score = score;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", score=" + score + "]";
	}
}
/**
 * 
 */
package part10;
import java.util.ArrayList;
import java.util.List;

/**
 * 对象排序
 * 
 * @author webrx
 *
 */
public class Lam4 {

	public static void main(String[] args) {
		List<User> list = new ArrayList<>(List.of(new User(42,"Andy",18),new User(600,"张三丰",72),new User(1, "jack", 80), new User(2, "李四", 45), new User(35, "张三", 60), new User(60, "james", 90)));
		System.out.println(list);

        //User a,User b -> a.getScore() - b.getScore()
		list.sort((a, b) -> b.getScore() - a.getScore());
		System.out.println(list);
		
		//根据姓名升序
		list.sort((a,b)->a.getName().compareTo(b.getName()));
		System.out.println(list);

	}

}

```

* TreeMap(根据key排序，或根据value排序，TreeMap本质是根据key自动升序)

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn;
  
  import org.junit.Test;
  
  import java.util.ArrayList;
  import java.util.Comparator;
  import java.util.Map;
  import java.util.TreeMap;
  
  /**
   * <p>Project: javaseapp - Demo
   * <p>Powered by webrx On 2021-08-20 11:48:51
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  
  public class Demo {
      @Test
      public void test() {
          //key降序 ，如果构造方法中没有参数是，key升序
  //Map<String, Integer> maps = new TreeMap<>(Comparator.reverseOrder());
  
          //默认是升序
          Map<String, Integer> maps = new TreeMap<>();
          Map<String, Integer> maps = new TreeMap<String, Integer>();
          maps.put("张三", 22);
          maps.put("李四", 24);
          maps.put("王五", 18);
          maps.put("赵六", 28);
          maps.put("张三丰", 32);
          maps.put("李丽", 21);
          System.out.println(maps);
  
          //自定义比较器  Map.Entry 相当于 Student(name,age)
          Comparator<Map.Entry<String, Integer>> valCmp = new Comparator<Map.Entry<String, Integer>>() {
              public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                  return o2.getValue() - o1.getValue();  // 降序排序，如果想升序就反过来
              }
          };
  
          // 降序排序，如果想升序就反过来
          Comparator<Map.Entry<String, Integer>> com = (o1, o2) -> o2.getValue() - o1.getValue();
  
  
          //将map转成List，map的一组key，value对应list一个存储空间
          var list = new ArrayList<Map.Entry<String, Integer>>(maps.entrySet()); //传入maps实体
          //Collections.sort(list, (o1, o2) -> o2.getValue() - o1.getValue()); // 注意此处Collections 是java.util包下面的,传入List和自定义的valCmp比较器
          //根据值来排序
          list.sort(Comparator.comparingInt(Map.Entry::getValue));
          System.out.println(list);
  
          //根据key来排序,其它TreeMap默认是根据key排序升序
          list.sort((a, b) -> b.getKey().compareTo(a.getKey()));
  
          for (Map.Entry<String, Integer> e : list) {
              System.out.printf("%s = %d%n", e.getKey(), e.getValue());
          }
          //输出map
          //for (int i = 0; i < list.size(); i++) {
          //    System.out.println(list.get(i).getKey() + " = " + list.get(i).getValue());
          //}
      }
  }
  
  ```
  



### 10.3 函数式接口

`(a)->a*a`

`a->2*2`

`(a,b)->a-b`

![image-20210820093001033](https://gitee.com/webrx/wx_note/raw/master/images/image-20210820093001033.png)

```java
public class Java8UnaryOperator2 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        List<Integer> result = math(list, x -> x * 2);
        System.out.println(result); // [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

    }

    public static <T> List<T> math(List<T> list, UnaryOperator<T> uo) {
        List<T> result = new ArrayList<>();
        for (T t : list) {
            result.add(uo.apply(t));
        }
        return result;
    }
}
```



| 序号 | 接口 & 描述                                                  |
| :--- | :----------------------------------------------------------- |
| 1    | **BiConsumer<T,U>**代表了一个接受两个输入参数的操作，并且不返回任何结果 |
| 2    | **BiFunction<T,U,R>**代表了一个接受两个输入参数的方法，并且返回一个结果 |
| 3    | **BinaryOperator<T>**代表了一个作用于于两个同类型操作符的操作，并且返回了操作符同类型的结果 |
| 4    | **BiPredicate<T,U>**代表了一个两个参数的boolean值方法        |
| 5    | **BooleanSupplier**代表了boolean值结果的提供方               |
| 6    | **Consumer<T>**代表了接受一个输入参数并且无返回的操作        |
| 7    | **DoubleBinaryOperator**代表了作用于两个double值操作符的操作，并且返回了一个double值的结果。 |
| 8    | **DoubleConsumer**代表一个接受double值参数的操作，并且不返回结果。 |
| 9    | **DoubleFunction<R>**代表接受一个double值参数的方法，并且返回结果 |
| 10   | **DoublePredicate**代表一个拥有double值参数的boolean值方法   |
| 11   | **DoubleSupplier**代表一个double值结构的提供方               |
| 12   | **DoubleToIntFunction**接受一个double类型输入，返回一个int类型结果。 |
| 13   | **DoubleToLongFunction**接受一个double类型输入，返回一个long类型结果 |
| 14   | **DoubleUnaryOperator**接受一个参数同为类型double,返回值类型也为double 。 |
| 15   | **Function<T,R>**接受一个输入参数，返回一个结果。            |
| 16   | **IntBinaryOperator**接受两个参数同为类型int,返回值类型也为int 。 |
| 17   | **IntConsumer**接受一个int类型的输入参数，无返回值 。        |
| 18   | **IntFunction<R>**接受一个int类型输入参数，返回一个结果 。   |
| 19   | **IntPredicate**：接受一个int输入参数，返回一个布尔值的结果。 |
| 20   | **IntSupplier**无参数，返回一个int类型结果。                 |
| 21   | **IntToDoubleFunction**接受一个int类型输入，返回一个double类型结果 。 |
| 22   | **IntToLongFunction**接受一个int类型输入，返回一个long类型结果。 |
| 23   | **IntUnaryOperator**接受一个参数同为类型int,返回值类型也为int 。 |
| 24   | **LongBinaryOperator**接受两个参数同为类型long,返回值类型也为long。 |
| 25   | **LongConsumer**接受一个long类型的输入参数，无返回值。       |
| 26   | **LongFunction<R>**接受一个long类型输入参数，返回一个结果。  |
| 27   | **LongPredicate**R接受一个long输入参数，返回一个布尔值类型结果。 |
| 28   | **LongSupplier**无参数，返回一个结果long类型的值。           |
| 29   | **LongToDoubleFunction**接受一个long类型输入，返回一个double类型结果。 |
| 30   | **LongToIntFunction**接受一个long类型输入，返回一个int类型结果。 |
| 31   | **LongUnaryOperator**接受一个参数同为类型long,返回值类型也为long。 |
| 32   | **ObjDoubleConsumer<T>**接受一个object类型和一个double类型的输入参数，无返回值。 |
| 33   | **ObjIntConsumer<T>**接受一个object类型和一个int类型的输入参数，无返回值。 |
| 34   | **ObjLongConsumer<T>**接受一个object类型和一个long类型的输入参数，无返回值。 |
| 35   | **Predicate<T>**接受一个输入参数，返回一个布尔值结果。       |
| 36   | **Supplier<T>**无参数，返回一个结果。                        |
| 37   | **ToDoubleBiFunction<T,U>**接受两个输入参数，返回一个double类型结果 |
| 38   | **ToDoubleFunction<T>**接受一个输入参数，返回一个double类型结果 |
| 39   | **ToIntBiFunction<T,U>**接受两个输入参数，返回一个int类型结果。 |
| 40   | **ToIntFunction<T>**接受一个输入参数，返回一个int类型结果。  |
| 41   | **ToLongBiFunction<T,U>**接受两个输入参数，返回一个long类型结果。 |
| 42   | **ToLongFunction<T>**接受一个输入参数，返回一个long类型结果。 |
| 43   | **UnaryOperator<T>**接受一个参数为类型T,返回值类型也为T。一元运算，接受一个T类型参数，输出一个与入参一模一样的值 |

| **接口**                                       | **参数** | **返回内容** | **描述**                                                     |
| ---------------------------------------------- | -------- | ------------ | ------------------------------------------------------------ |
| Predicate<T>  **java.util.function.Predicate** | T        | boolean      | 用于判别一个对象。比如求一个人是否为男性                     |
| Consumer<T> 消费型                             | T        | void         | 用于接收一个对象进行处理但没有返回，比如接收一个人并打印他的名字 |
| Function<T,  R> 函数型                         | T        | R            | 转换一个对象为不同类型的对象                                 |
| Supplier<T> 供给型                             | None     | T            | 提供一个对象                                                 |
| UnaryOperator<T>                               | T        | T            | 接收对象并返回同类型的对象                                   |
| BinaryOperator<T>                              | (T,  T)  | T            | 接收两个同类型的对象，并返回一个原类型对象                   |

```java
/**
 * 
 */
package part10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * 显示及格的学生信息
 * 
 * @author webrx
 *
 */
public class Lam5 {

	public static void main(String[] args) {
		
	
		List<User> list = new ArrayList<>(List.of(new User(42, "Andy", 18), new User(600, "张三丰", 52), new User(1, "jack", 80), new User(2, "李四", 45), new User(35, "张三", 60), new User(60, "james", 90)));
		// System.out.println(list);

		//list.stream().filter(e -> e.getScore() < 60).forEach(System.out::println);
		//list.stream().filter(e -> e.getScore() < 60).forEach(e -> System.out.println(e));
		
		//list.stream().filter(e -> e.getScore() < 60 && !e.getName().startsWith("李")).forEach(System.out::println);
		Random rand = new Random();
		IntStream.generate(()->rand.nextInt(100)+1).limit(10).forEach(System.out::println);
	}
}
```

### 10.4 Stream流技术

Stream（流）是一个来自数据源的元素队列并支持聚合操作

- 元素是特定类型的对象，形成一个队列。 Java中的Stream并不会存储元素，而是按需计算。
- **数据源** 流的来源。 可以是集合，数组，I/O channel， 产生器generator 等。
- **聚合操作** 类似SQL语句一样的操作， 比如filter, map, reduce, find, match, sorted等。

和以前的Collection操作不同， Stream操作还有两个基础的特征：

- **Pipelining**: 中间操作都会返回流对象本身。 这样多个操作可以串联成一个管道， 如同流式风格（fluent style）。 这样做可以对操作进行优化， 比如延迟执行(laziness)和短路( short-circuiting)。
- **内部迭代**： 以前对集合遍历都是通过Iterator或者For-Each的方式, 显式的在集合外部进行迭代， 这叫做外部迭代。 Stream提供了内部迭代的方式， 通过访问者模式(Visitor)实现。

* IntStream

```java
/**
 * 
 */
package part10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.IntConsumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * IntStream
 * 
 * @author webrx
 *
 */
public class IntStreamTest {

	public static void main(String[] args) {

		IntStream is = IntStream.of(10, 20, 30, 40, 50);
		is.forEach(System.out::println);

		System.out.println(IntStream.rangeClosed(1, 100).sum());
		System.out.println(IntStream.generate(() -> 5).limit(3).count());

		IntStream i2 = IntStream.generate(() -> {
			Random rand = new Random();
			return rand.nextInt(100) + 1;
		}).limit(10);

		IntConsumer ic = (a) -> {
			System.out.printf("%d ", a);
		};

		i2.forEach(ic);
		System.out.println();

		IntStream.iterate(1, a -> a + 2).limit(5).forEach(ic);
		System.out.println();

		IntStream.iterate(2, a -> a + 2).skip(3).limit(15).forEach(ic);
		
		

		// IntStream 转换为 List<Integer>
		List<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(1);
		list.add(1);

		System.out.println();
		// list.stream().forEach(System.out::println); //Stream<Integer>

		//Stream<Integer> ss = list.stream();
		IntStream ii2 = list.stream().mapToInt(i -> i);
		
		//Stream<Integer> sa = ii2.boxed();
		List<Integer> lis = ii2.boxed().collect(Collectors.toList());
		System.out.println(lis.size());

		// IntStream.iterate(1, a->a+2).limit(5).col
	
		
		List<String> as = List.of("java","php");
		Stream<String> sss = as.stream();
	
	}
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Comparator;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * <p>Project: javaseapp - IntStream3
 * <p>Powered by webrx On 2021-08-23 08:35:46
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class IntStream3 {
    public static void main(String[] args) {
        IntStream is = IntStream.empty();
        is = IntStream.of(10, 20, 30, 40);
        is = IntStream.range(1, 5);
        is = IntStream.rangeClosed(1, 10);
        Random rand = new Random();
        is = IntStream.generate(() -> rand.nextInt(100) + 1).limit(8).skip(2);
        //升序
        //is.sorted().forEach(System.out::println);
        
        //降序
        //ss.sorted((a, b) -> b - a).forEach(e -> System.out.printf("%02d ", e));
        Stream<Integer> si = is.boxed();
        //si.forEach(System.out::println);
        si.sorted(Comparator.reverseOrder()).forEach(System.out::println);
        
        //乱序
        ss = Stream.of(1, 5, 2, 35, 15, 9);
        ss.sorted((a, b) -> rand.nextBoolean() ? 1 : -1).forEach(e -> System.out.printf("%02d ", e));
    }
}
```

* Stream

  ```java
  /**
   * 
   */
  package part10;
  
  import java.io.BufferedReader;
  import java.util.Arrays;
  import java.util.stream.Stream;
  
  /**
   * 显示及格的学生信息
   * @author webrx
   */
  public class Stream1 {
  
  	public static void main(String[] args) {
  		int[] nums = new int[] { 1, 2, 3, 10, 20, 30, 50 };
  		System.out.println(Arrays.stream(nums).min().getAsInt());
  		System.out.println(Arrays.stream(nums).max().getAsInt());
  		System.out.println(Arrays.stream(nums).count());// 3
  		System.out.println(Arrays.stream(nums).sum());// 6
  //average() 平均值		System.out.printf("%.2f%n",Arrays.stream(nums).average().getAsDouble());
  
  		Arrays.stream(nums).forEach(e -> System.out.printf("%02d ", e));
  		//BufferedReader br = null;
  		//Stream<String> c = br.lines();
           System.out.println();
  
          String str = "java\njavascript\nmysql\nhtml5\njava8\njava17";
          var sss = str.lines();
          //sss.sorted().forEach(System.out::println);
          //sss.filter(e -> e.startsWith("java")).sorted(Comparator.reverseOrder()).forEach(System.out::println);
          sss
                  .filter(e -> e.startsWith("java") && e.length() > 5)
                  .map(e -> String.format("hello:%s", e.toUpperCase()))
                  .filter(e -> e.length() > 5)
                  .sorted(Comparator.comparingInt(String::length))
                  .forEach(System.out::println);
  	}
  }
  
  
  /**
   * 
   */
  package part10;
  
  import java.util.Arrays;
  import java.util.stream.DoubleStream;
  import java.util.stream.IntStream;
  import java.util.stream.Stream;
  
  public class Stream2 {
  	public static void main(String[] args) {
  
  		// Stream<String> ss = Stream.of("java", "javascript", "mysql", "php",
  		// "python");
  
  		// ss.filter(e -> e.startsWith("java")).map(e ->
  		// e.toUpperCase()).forEach(System.out::println);;
  
  		int[] nums = new int[] { 30,1, 3, 5 ,20,15,100};
  		// IntStream is = Arrays.stream(nums).map(e -> e + 5);
  		IntStream is = Arrays.stream(nums).map(e -> e % 5 == 0 ? e + 5 : e);
  		is.forEach(System.out::println);
  //DoubleStream.generate(Math::random).limit(10).forEach(System.out::println);
  
  		double[] dd = new double[10];
  		Arrays.stream(dd).map(e->Math.random()).forEach(e->System.out.printf("%.2f ",e));
  		
  		System.out.println();
  		System.out.println(Stream.of(1,2,3,1,6,8,1,9,10,1,22).distinct().count());
  	}
  }
  
  /**
   * 
   */
  package part10;
  import java.util.Arrays;
  import java.util.stream.DoubleStream;
  import java.util.stream.IntStream;
  import java.util.stream.Stream;
  public class Stream3 {
  	public static void main(String[] args) {
  		int num = 6;
  		IntStream is = IntStream.rangeClosed(1, 6);
  		int ok = is.reduce(1, (a, b) -> a * b);
  		System.out.println(ok);
  		System.out.println(1 * 1 * 2 * 3 * 4 * 5 * 6);
  		Stream<String> aa = Stam.of("mysql","php","java","javascript");
  		System.out.println(aa.reduce("", (a,b)->a.concat(b)));
          
          
          var ii = IntStream.rangeClosed(1, 6);
          System.out.println(1 * 2 * 3 * 4 * 5 * 6);
          //System.out.println(ii.reduce((a, b) -> a * b).getAsInt());
          System.out.println(ii.reduce(1, (a, b) -> a * b));
  
          var ss = Stream.of("java", "javascript", "html");
          //System.out.println(ss.reduce("", String::concat));
          System.out.println(ss.reduce(String::concat).get());
  	}
  }
  ```
  

#### 10.5.1 流的常用创建方法

使用Collection下的 stream() 和 parallelStream() 方法

![image-20210823081530889](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081530889.png)

```java
var s = IntStream.range(1, 135);//串行流
var s2 = s.parallel(); //返回并行流，多线程输出
//s.forEach(System.out::println);
s2.forEach(e -> {
    String tn = Thread.currentThread().getName();
    System.out.printf("%s:%d%n", tn, e);
});
```



使用Arrays 中的 stream() 方法，将数组转成流

![image-20210823081721152](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081721152.png)

使用Stream中的静态方法：of()、iterate()、generate()

![](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081635232-16296778277441.png)

```java
var s10 = Stream.of(1, 2, 3);
var s11 = Stream.generate(() -> "ok").limit(5);
s11.forEach(System.out::println);
//0 1 2
//var s12 = Stream.iterate(0, (a) -> a + 1).limit(3);
//1 3 5
var s12 = Stream.iterate(1, (a) -> a + 2).limit(3);
s12.forEach(System.out::println);

```



使用 BufferedReader.lines() 方法，将每行内容转成流

![image-20210823081649098](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081649098.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * <p>Project: javase - Stream4
 * <p>Powered by webrx On 2021-12-22 16:04:52
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Stream4 {
    public static void main(String[] args) {
        var src = new File("C:\\javase\\10lambda\\src\\main\\java\\cn\\webrx\\Stream4.java");
        try (var read = new BufferedReader(new FileReader(src))) {
            var i = new AtomicInteger(1);
            //每行输出一行
            read.lines().forEach(e -> System.out.printf("%02d、 %s%n", i.getAndIncrement(), e));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void test(String[] args) {
        Pattern p = Pattern.compile("[+]");
        String str = "1+2+3-5-1+9".replace("-", "+-");
        p.splitAsStream(str).forEach(System.out::println);

        var s = IntStream.range(1, 135);//串行流
        var s2 = s.parallel(); //返回并行流，多线程输出
        //s.forEach(System.out::println);
        s2.forEach(e -> {
            String tn = Thread.currentThread().getName();
            System.out.printf("%s:%d%n", tn, e);
        });

        var s10 = Stream.of(1, 2, 3);
        var s11 = Stream.generate(() -> "ok").limit(5);
        s11.forEach(System.out::println);
        //0 1 2
        //var s12 = Stream.iterate(0, (a) -> a + 1).limit(3);
        //1 3 5
        var s12 = Stream.iterate(1, (a) -> a + 2).limit(3);
        s12.forEach(System.out::println);


    }
}

```

使用 Pattern.splitAsStream() 方法，将字符串分隔成流

![image-20210823081635232](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081635232.png)



#### 10.4.2  流的中间操作

![image-20210823081836513](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081836513.png)



![image-20210823081900042](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081900042.png)

![image-20210823081920643](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081920643.png)



![image-20210823081958133](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823081958133.png)

```java
var ss = Stream.of(new Student(1, "jack", 28), new Student(2, "james", 58));
//ss.peek(o -> o.setAge(o.getAge() + 5)).forEach(System.out::println);
ss.map(o -> {
    o.setAge(o.getAge() + 5);
    return o;
}).forEach(System.out::println);
```



#### 10.4.3 流的终止操作

![image-20210823082143201](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823082143201.png)

![image-20210823082204052](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823082204052.png)

![image-20210823082233773](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823082233773.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * <p>Project: javase - Stream5
 * <p>Powered by webrx On 2021-12-22 17:54:11
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Stream8 {
    public static void main(String[] args) {
        var ss = Stream.of(1, 2, 3, 4, 5);
        //var sum = ss.reduce(Integer::sum).get();
        //System.out.println(sum);

        //var j = ss.reduce((a, b) -> a * b).get();
        //System.out.println(j);

        var ii = IntStream.rangeClosed(1, 5);
        System.out.println(ii.sum());

    }
}

```

>  collect：接收一个Collector实例，将流中元素收集成另外一个数据结构。
>         Collector<T, A, R> 是一个接口，有以下5个抽象方法：
>             Supplier<A> supplier()：创建一个结果容器A
>             BiConsumer<A, T> accumulator()：消费型接口，第一个参数为容器A，第二个参数为流中元素T。
>             BinaryOperator<A> combiner()：函数接口，该参数的作用跟上一个方法(reduce)中的combiner参数一样，将并行流中各                                                                 个子进程的运行结果(accumulator函数操作后的容器A)进行合并。
>             Function<A, R> finisher()：函数式接口，参数为：容器A，返回类型为：collect方法最终想要的结果R。
>             Set<Characteristics> characteristics()：返回一个不可变的Set集合，用来表明该Collector的特征。有以下三个特征：
>                 CONCURRENT：表示此收集器支持并发。
>                 UNORDERED：表示该收集操作不会保留流中元素原有的顺序。
>                 IDENTITY_FINISH：表示finisher参数只是标识而已，可忽略。

```java
Student s1 = new Student("aa", 10,1);
Student s2 = new Student("bb", 20,2);
Student s3 = new Student("cc", 10,3);
List<Student> list = Arrays.asList(s1, s2, s3);
 
//装成list
List<Integer> ageList = list.stream().map(Student::getAge).collect(Collectors.toList()); // [10, 20, 10]
 
//转成set
Set<Integer> ageSet = list.stream().map(Student::getAge).collect(Collectors.toSet()); // [20, 10]
 
//转成map,注:key不能相同，否则报错
Map<String, Integer> studentMap = list.stream().collect(Collectors.toMap(Student::getName, Student::getAge)); // {cc=10, bb=20, aa=10}
 
//字符串分隔符连接
String joinName = list.stream().map(Student::getName).collect(Collectors.joining(",", "(", ")")); // (aa,bb,cc)
 
//聚合操作
//1.学生总数
Long count = list.stream().collect(Collectors.counting()); // 3
//2.最大年龄 (最小的minBy同理)
Integer maxAge = list.stream().map(Student::getAge).collect(Collectors.maxBy(Integer::compare)).get(); // 20
//3.所有人的年龄
Integer sumAge = list.stream().collect(Collectors.summingInt(Student::getAge)); // 40
//4.平均年龄
Double averageAge = list.stream().collect(Collectors.averagingDouble(Student::getAge)); // 13.333333333333334
// 带上以上所有方法
DoubleSummaryStatistics statistics = list.stream().collect(Collectors.summarizingDouble(Student::getAge));
System.out.println("count:" + statistics.getCount() + ",max:" + statistics.getMax() + ",sum:" + statistics.getSum() + ",average:" + statistics.getAverage());
 
//分组
Map<Integer, List<Student>> ageMap = list.stream().collect(Collectors.groupingBy(Student::getAge));
//多重分组,先根据类型分再根据年龄分
Map<Integer, Map<Integer, List<Student>>> typeAgeMap = list.stream().collect(Collectors.groupingBy(Student::getType, Collectors.groupingBy(Student::getAge)));
 
//分区
//分成两部分，一部分大于10岁，一部分小于等于10岁
Map<Boolean, List<Student>> partMap = list.stream().collect(Collectors.partitioningBy(v -> v.getAge() > 10));
 
//规约
Integer allAge = list.stream().map(Student::getAge).collect(Collectors.reducing(Integer::sum)).get(); //40
```

```java

//toList 源码
public static <T> Collector<T, ?, List<T>> toList() {
    return new CollectorImpl<>((Supplier<List<T>>) ArrayList::new, List::add,
            (left, right) -> {
                left.addAll(right);
                return left;
            }, CH_ID);
}
 
//为了更好地理解，我们转化一下源码中的lambda表达式
public <T> Collector<T, ?, List<T>> toList() {
    Supplier<List<T>> supplier = () -> new ArrayList();
    BiConsumer<List<T>, T> accumulator = (list, t) -> list.add(t);
    BinaryOperator<List<T>> combiner = (list1, list2) -> {
        list1.addAll(list2);
        return list1;
    };
    Function<List<T>, List<T>> finisher = (list) -> list;
    Set<Collector.Characteristics> characteristics = Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.IDENTITY_FINISH));
 
    return new Collector<T, List<T>, List<T>>() {
        @Override
        public Supplier supplier() {
            return supplier;
        }
 
        @Override
        public BiConsumer accumulator() {
            return accumulator;
        }
 
        @Override
        public BinaryOperator combiner() {
            return combiner;
        }
 
        @Override
        public Function finisher() {
            return finisher;
        }
 
        @Override
        public Set<Characteristics> characteristics() {
            return characteristics;
        }
    };
}
```

#### 10.4.4 Optional容器

Optional 类是一个可以为null的容器对象。如果值存在则isPresent()方法会返回true，调用get()方法会返回该对象。

Optional 是个容器：它可以保存类型T的值，或者仅仅保存null。Optional提供很多有用的方法，这样我们就不用显式进行空值检测。

Optional 类的引入很好的解决空指针异常。

| 序号 | 方法 & 描述                                                  |
| :--- | :----------------------------------------------------------- |
| 1    | **static <T> Optional<T> empty()**返回空的 Optional 实例。   |
| 2    | **boolean equals(Object obj)**判断其他对象是否等于 Optional。 |
| 3    | **Optional<T> filter(Predicate<? super <T> predicate)**如果值存在，并且这个值匹配给定的 predicate，返回一个Optional用以描述这个值，否则返回一个空的Optional。 |
| 4    | **<U> Optional<U> flatMap(Function<? super T,Optional<U>> mapper)**如果值存在，返回基于Optional包含的映射方法的值，否则返回一个空的Optional |
| 5    | **T get()**如果在这个Optional中包含这个值，返回值，否则抛出异常：NoSuchElementException |
| 6    | **int hashCode()**返回存在值的哈希码，如果值不存在 返回 0。  |
| 7    | **void ifPresent(Consumer<? super T> consumer)**如果值存在则使用该值调用 consumer , 否则不做任何事情。 |
| 8    | **boolean isPresent()**如果值存在则方法会返回true，否则返回 false。 |
| 9    | **<U>Optional<U> map(Function<? super T,? extends U> mapper)**如果有值，则对其执行调用映射函数得到返回值。如果返回值不为 null，则创建包含映射返回值的Optional作为map方法返回值，否则返回空Optional。 |
| 10   | **static <T> Optional<T> of(T value)**返回一个指定非null值的Optional。 |
| 11   | **static <T> Optional<T> ofNullable(T value)**如果为非空，返回 Optional 描述的指定值，否则返回空的 Optional。 |
| 12   | **T orElse(T other)**如果存在该值，返回值， 否则返回 other。 |
| 13   | **T orElseGet(Supplier<? extends T> other)**如果存在该值，返回值， 否则触发 other，并返回 other 调用的结果。 |
| 14   | **<X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier)**如果存在该值，返回包含的值，否则抛出由 Supplier 继承的异常 |
| 15   | **String toString()**返回一个Optional的非空字符串，用来调试  |

```java
import java.util.Optional;
 
public class Java8Tester {
   public static void main(String args[]){
   
      Java8Tester java8Tester = new Java8Tester();
      Integer value1 = null;
      Integer value2 = new Integer(10);
        
      // Optional.ofNullable - 允许传递为 null 参数
      Optional<Integer> a = Optional.ofNullable(value1);
        
      // Optional.of - 如果传递的参数是 null，抛出异常 NullPointerException
      Optional<Integer> b = Optional.of(value2);
      System.out.println(java8Tester.sum(a,b));
   }
    
   public Integer sum(Optional<Integer> a, Optional<Integer> b){
    
      // Optional.isPresent - 判断值是否存在
        
      System.out.println("第一个参数值存在: " + a.isPresent());
      System.out.println("第二个参数值存在: " + b.isPresent());
        
      // Optional.orElse - 如果值存在，返回它，否则返回默认值
      Integer value1 = a.orElse(new Integer(0));
        
      //Optional.get - 获取值，值需要存在
      Integer value2 = b.get();
      return value1 + value2;
   }
}
```

`注意事项：`

> `Optional` 无法被序列化。所以不要试图将 `Optional` 作为方法参数进行定义，也不要在类当中声明 `Optional` 类型的成员变量。`Optional` 通常只作为方法的返回值，用来规避空指针异常。
>
> 在使用 `Optional` 时，应该使用函数式的编程风格。



### 10.5  lambda表达式双冒号

双冒号运算符就是java中的方法引用，方法引用的格式是类名：：方法名。

这里只是方法名，方法名的后面没有括号“（）”。--------> 这样的式子并不代表一定会调用这个方法。这种式子一般是用作Lambda表达式，Lambda有所谓的懒加载，不要括号就是说，看情况调用方法。

表达式：person ->person.getAge();可以替换为 `Person::getAge`

表达式：（）-> new HashMap<>();可以替换为`HashMap::new`

这种方法引用或者是双冒号运算对应的参数类型是Function<T,R>T表示传入的类型，R表示返回的类型。比如表达式person -> person.getAge();传入的参数是person，返回值是peron.getAge(),那么方法引用Person::getAge

就对应着Funciton<Person,Integer>类型。

案例代码：

```java
@Test
public void convertTest(){
    List<String> collected = new ArrayList<>();
    collected.add("alpha");
    collected.add("beta");
    collected = collected.stream().map(string -> string.toUpperCase()).collect(collectors.toList());
    System.out.println(collected);     
}

@Test
public void TestConverTest(){
     List<String> collected = new ArrayList<>();
     collected.add("alpha");
     collected.add("beta");
     collected = collected.stream().map(String::toUpperCase).collect(collectors.toCollection(ArrayList::new));
    System.out.println(collected);    
}
```





```java
package cn.exam;

import java.util.Random;

public class User {
    private String name;
    private String sex;
    private boolean isMan;

    public User(String name,String sex){
        this.name = name;
        this.sex =sex;
        this.isMan = "男".equals(this.sex) ? true : false;
    }

    public static void println(User u){
        System.out.println(u);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setMan(boolean man) {
        isMan = man;
    }

    private static Random rand = new Random();
    public static int rand(){
        return rand.nextInt(100)+1;
    }


    public static void println(int i) {
        System.out.printf("%02d ",i);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", isMan=" + isMan +
                '}';
    }
}


package cn.exam;
import java.io.*;
import java.net.InetAddress;
import java.util.Random;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class My {
    public static void main(String[] args) throws IOException {
        //()->Math.random()  Math::random()
        DoubleStream ds1 = DoubleStream.generate(Math::random).limit(5);
        DoubleStream ds2 = DoubleStream.generate(()->Math.random()).limit(5);

        //()->new Ranom()   Random::new
        Stream<Random> sr1 = Stream.generate(Random::new).limit(5);
        Stream<Random> sr2 = Stream.generate(()->new Random()).limit(5);

        //产生10 1-100的随机数 调用的是User 静态的 rand()方法
        IntStream is = IntStream.generate(User::rand).limit(10);
        //()->user.rand()   User::rand


        is.forEach(User::println);

        //Stream<User> su1 = Stream.generate(User::new).limit(5);
        Random ran = new Random();
        Stream<User> su2 = Stream.generate(()->new User("jack"+Math.random(),ran.nextBoolean() ? "男":"女")).limit(15);
        //su2.filter((u)->u.isMan()).forEach(System.out::println);
        //su2.filter(User::isMan).forEach(User::println);
        su2.filter(User::isMan).forEach(e->e.println(e));

    }
}
```



### 作业：

1. 求1+2+3+…+100 = ? 不使用循环和递归

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.stream.IntStream;

/**
 * <p>Project: javaseapp - IntStreamDemo
 * <p>Powered by webrx On 2021-08-20 17:21:18
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class IntStreamDemo {
    public static int num = 0;


    public static void main(String[] args) {
        //建立IntStream 空流
        var is = IntStream.empty();
        System.out.println(is.count());

        //计算1+2+3+...+100=?
        //方法一
        int sum = 0;
        for (int i = 1; i <= 100; i++) sum += i;
        System.out.println("1+2+3+...+100=" + sum);

        //方法二 递归
        int sum2 = sum(100);
        System.out.println("1+2+3+...+100=" + sum2);

        //java8 IntStream
        int sum3 = IntStream.rangeClosed(1, 100).sum();
        System.out.println("1+2+3+...+100=" + sum3);
        var n1 = IntStream.rangeClosed(1, 100);
        //System.out.println(n1.sum());

        //System.out.println(n1.reduce(Integer::sum).getAsInt());

        var n2 = Stream.iterate(1, a -> a + 1).limit(100);
        System.out.println(n2.reduce((a, b) -> a + b).get());
    }

    public static int sum(int i) {
        num += i--;
        if (i == 0) {
            return num;
        } else {
            return sum(i);
        }
    }
}

```

2. 斐波那契数列

   ```java
   /**
    * 使用IntStream技术
    */ 
   
   package cn.webrx;
   
   import java.util.List;
   import java.util.stream.Stream;
   
   /**
    * <p>Project: javaseapp - IntSteam2
    * <p>Powered by webrx On 2021-08-20 18:16:34
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class IntSteam2 {
       public static void main(String[] args) {
           //IntStream is = IntStream.range(0, 2);
           //IntStream is2 = IntStream.iterate(0, (e) -> e + 1).limit(5);
           //is2.forEach(System.out::println); // 0 1 2 3 4
           //IntStream is3 = IntStream.iterate(0, e -> true, e -> e + 1).limit(3);
           //is3.forEach(System.out::println);
   
           //java Stream Fibonacci series
           var s = Stream.iterate(List.of(0, 1), i -> List.of(i.get(1), i.get(0) + i.get(1))).limit(15);
           s.forEach(e -> {
               System.out.printf("%d ", e.get(0));
           });
       }
   }
   
   ```

   ```java
   /**
   * 斐波那契数列算法，从第三个数开始，每个数是前两个数之和：1 1 2 3 5 8 13 21 34 55
   * 求第N个数的两种算法，分递归和非递归两种
   */
   public class Fib {
           public static void main(String[] args) {
                   System.out.println(f(20));
                   System.out.println(fx(20));
           }
   
           //递归方式
           public static int f(int n) {
                   //参数合法性验证
                   if (n < 1) {
                           System.out.println("参数必须大于1！");
                           System.exit(-1);
                   }
                   if (n == 1 || n == 2) return 1;
                   else return f(n - 1) + f(n - 2);
           }
   
           //非递归方式
           public static int fx(int n) {
                   //参数合法性验证
                   if (n < 1) {
                           System.out.println("参数必须大于1！");
                           System.exit(-1);
                   }
                   //n为1或2时候直接返回值
                   if (n == 1 || n == 2) return 1;
   
                   //n>2时候循环求值
                   int res = 0;
                   int a = 1;
                   int b = 1;
                   for (int i = 3; i <= n; i++) {
                           res = a + b;
                           a = b;
                           b = res;
                   }
                   return res;
           }
   }
   ```

   ![image-20210823153007494](https://gitee.com/webrx/wx_note/raw/master/images/image-20210823153007494.png)




## 第十一章 多线程编程
synchronized volatile CountDownLatch ReenTrantLock TimeUnit  Semaphore lockInterruptibly lock unlock tryLock wait sleep notify 

### 11.1 多线程概念

#### 11.1.1 多线程相关的概念

> 什么是程序？一个程序可以有多个进程
>
> 程序是一段静态的代码，它是应用程序执行的蓝本。
>
> 什么是进程？一个进程可以有多线程
>
> 进程是指一种正在运行的程序，有自己的地址空间。
>
> 作为蓝本的程序可以被多次加载到系统的不同内存区域分别执行，形成不同的进程。
>
> 基于进程的特点是允许计算机同时运行两个或更多的程序。
>
> 什么是线程?
>
> 线程是进程内部单一的一个顺序控制流。 一个进程在执行过程中，可以产生多个线程。每个线程也有自己产生、存在和消亡的过程。

```text
并发当有多个线程在操作时,如果系统只有一个CPU,则它根本不可能真正同时进行一个以上的线程，它只能把CPU运行时间划分成若干个时间段,再将时间 段分配给各个线程执行，在一个时间段的线程代码运行时，其它线程处于挂起状。.这种方式我们称之为并发(Concurrent)。
并行：当系统有一个以上CPU时,则线程的操作有可能非并发。当一个CPU执行一个线程时，另一个CPU可以执行另一个线程，两个线程互不抢占CPU资源，可以同时进行，这种方式我们称之为并行(Parallel)。
区别：并发和并行是即相似又有区别的两个概念，并行是指两个或者多个事件在同一时刻发生；而并发是指两个或多个事件在同一时间间隔内发生。在多道程序环境下，并发性是指在一段时间内宏观上有多个程序在同时运行，但在单处理机系统中，每一时刻却仅能有一道程序执行，故微观上这些程序只能是分时地交替执行。倘若在计算机系统中有多个处理机，则这些可以并发执行的程序便可被分配到多个处理机上，实现并行执行，即利用每个处理机来处理一个可并发执行的程序，这样，多个程序便可以同时执行。 
```

![image-20210128141457317](https://gitee.com/webrx/wx_note/raw/master/images/image-20210128141457317.png)

#### 11.1.2 JUC

在 Java 5.0 提供了 `java.util.concurrent`(简称JUC)包,在此包中增加了在并发编程中很常用的工具类,
用于定义类似于线程的自定义子系统,包括线程池,异步 IO 和轻量级任务框架;还提供了设计用于多线程上下文中
的 Collection 实现等

### 11.2 实现多线程类方法

#### 11.2.1 Runnable接口

```java
/*
 * Copyright 2006-2021 webrx Group.
 */
package cn.webrx;

/**
 * <p> java.lang.Runnable  实现Runnable接口，作为Thread构造参数来使用 
 * <pre>
 * </pre>
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @date 2021-01-28 14:24:09
 */
public class Thread2 implements Runnable{
    public void run() {
        System.out.println("Thread2....");
    }
}

```

#### 11.2.2 继承Thread类

```java
/*
 * Copyright 2006-2021 webrx Group.
 */
package cn.webrx;

/**
 * <p> java.lang.Thread 
 * <pre>
 * </pre>
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @date 2021-01-28 14:27:03
 */
public class Thread3 extends Thread{

    public void run() {
        System.out.println("Thread3");
    }
}
```

#### 11.2.3 Callable 接口

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * <p>Project: javase - T3
 * <p>Powered by webrx On 2021-12-24 10:48:59
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class T3 implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        System.out.println("call()");
        return 300;
    }

    public static void main(String[] args) {
        T3 t3 = new T3();

        // 执行 Callable 方式,需要 FutureTask 实现类的支持
        // FutureTask 实现类用于接收运算结果, FutureTask 是 Future 接口的实现类
        FutureTask<Integer> result = new FutureTask<>(t3);
        new Thread(result).start();

        // 接收线程运算后的结果
        try {
            // 只有当 Thread 线程执行完成后,才会打印结果;
            // 因此, FutureTask 也可用于闭锁
            System.out.println(result.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
```





#### 11.2.4 综合线程实现

```java
/*
 * Copyright 2006-2021 webrx Group.
 */
package cn.webrx;

/**
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @date 2021-01-28 14:19:03
 */
public class Thread1 {
    public static void main(String[] args)  {
        System.out.println("hello world main thread");
        //建立方法一 实现Runnable接口，并作为Thread类参数
        new Thread(() -> {
            System.out.println("Thread1");
        }).start();

        Thread t2 = new Thread(new Thread2());
        t2.start();

        //建立方法二　继承Thread 类，重写run方法
        var t3 = new Thread3();
        t3.setName("线程三");
        t3.start();

    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javaseapp - Thread1
 * <p>Powered by webrx On 2021-08-23 17:44:19
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Thread1 {
    public static void main(String[] args) {
        //多线程 方法一 继承Thread
        var t1 = new Thread() {
            @Override
            public void run() {
                this.setName("one thread");
                System.out.println(this.getName());
            }
        };
        t1.setName("one1111 thread");
        t1.start();

        //多线程 方法二 实现接口，实现Thread作为参数
        var t2 = new Runnable() {
            @Override
            public void run() {
                String n = Thread.currentThread().getName();
                System.out.println(n);
            }
        };
        var t22 = new Thread(t2, "two thread");
        t22.setName("two2222 thread");
        t22.start();

        //main线程输出
        try {
            TimeUnit.SECONDS.sleep(2);
            Thread.currentThread().setName("main thread");
            System.out.println(Thread.currentThread().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```



### 11.3 多线程状态及常用方法

![image-20210128150722176](https://gitee.com/webrx/wx_note/raw/master/images/image-20210128150722176.png)





![image-20210128150737763](https://gitee.com/webrx/wx_note/raw/master/images/image-20210128150737763.png)

> java中线程优先级：10个级别
>
> ```java
> var t = new Thread(() -> {
>     System.out.println("ttttt");
> });
> t.setPriority(Thread.MAX_PRIORITY);
> t.start();
> //10个级别
> //Thread.MAX_PRIORITY 10
> //Thread.MIN_PRIORITY 1
> //Thread.NORM_PRIORITY 5
> System.out.println(Thread.currentThread().getPriority());
> System.out.println("mainmmmmmm");
> ```



```java
/*
 * Copyright 2006-2021 webrx Group.
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @date 2021-01-28 14:27:03
 */
public class Thread4 {

    public static void main(String[] args) {
        Thread t0 = new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName());
            System.out.println(Thread.currentThread().getPriority());
        },"ttt1");

        t0.setName("ttt-000");
        t0.setPriority(10);
        t0.start();


        //设置线程
        Thread.currentThread().setName("主线程");
        //设置优先级 1 - 10
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        try {
            //Thread.sleep(10000);
            TimeUnit.SECONDS.sleep(2);
            System.out.println(Thread.currentThread().getName());
            System.out.println(Thread.currentThread().getPriority());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

### 11.4 java多线程实现

#### 11.4.1 多线程模拟售票

```java
/*
 * Copyright 2006-2021 webrx Group.
 */
package cn.exam;

import java.util.concurrent.TimeUnit;

/**
 * <p> synchronized 同步 
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @date 2021-01-29 10:35:33
 */
public class SellTicket {
    public static void main(String[] args) {
        //实现Runnable接口
        Ticket t = new Ticket(50);
        Thread t1 = new Thread(t,"科学大道");
        Thread t2 = new Thread(t,"东风路");
        Thread t3 = new Thread(t,"火车站");
        Thread t4 = new Thread(t,"瑞达路");
        t1.start();t2.start();t3.start();t4.start();
    }
}

class Ticket implements Runnable {
    private int num = 100;
    public Ticket() {
    }
    public Ticket(int num) {
        this.num = num;
    }

    public void run() {
        while(true){
            synchronized (this) {
                if (num > 0) {
                    System.out.printf("[%s] 售出一张票，剩余%d张票%n", Thread.currentThread().getName(), --num);
                } else {
                    System.out.printf("%n[%s] 票已售完，停止售票。", Thread.currentThread().getName());
                    break;
                }
            }
            try{
                TimeUnit.SECONDS.sleep(1);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
```

#### 11.4.2 定时关机

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class MyClose {
    public static void main(String[] args) {

        //多长时间后关机 5s
        new Thread(()->{
            long t1 = System.currentTimeMillis();
            while(true){
                long ok = System.currentTimeMillis() - t1;
                if(ok >= 5 * 1000) break;
            }
            System.out.println("系统正在关机5...");
        }).start();



        //
        new Thread(()->{
            try{
                TimeUnit.SECONDS.sleep(10);
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.println("系统正在关机10");
        }).start();

        //指定时间关机 2021-1-30 21:30:00
        new Thread(()->{
            String s = "2021-1-29 15:13:59";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long end = 0;
            try {
                end = sdf.parse(s).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try{
                TimeUnit.MILLISECONDS.sleep(end-System.currentTimeMillis());
            }catch(Exception e){
                e.printStackTrace();
            }
            System.out.printf("%s - 系统正在关机%n",s);
        }).start();
    }
}
```



### 11.5 多线程工具类

#### 11.5.1 Timer TimerTask

Timer类的常用其他方法：

`cancel()` 终止此计时器，丢弃所有当前已安排的任务。

`purge()` 从此计时器的任务队列中移除所有已取消的任务。

`schedule(TimerTask task, Date time)` 安排在指定的时间执行指定的任务。

TimerTask类的常用其他方法：

`cancel()` 取消此计时器任务。

`run()` 此计时器任务要执行的操作。

`scheduledExecutionTime()` 返回此任务最近行的已安排执行时间。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.exam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class TimerTest {
    public static void main(String[] args) throws ParseException {
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                System.out.printf("%1$tF %1$tT%n",System.currentTimeMillis());
                //t.cancel();
            }
        }, 5000,1000);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        t.schedule(new TimerTask() {
           public void run() {
               t.cancel();
           }
       },sdf.parse("2021-01-29 15:28:30"));

        Timer t2 = new Timer();
        TimerTask tt = new TimerTask() {
           public void run() {
               System.out.println("hello world Timer");
               t2.cancel();
           }
       };
       t2.schedule(tt,10000);
    }
}
```

#### 11.5.2 TimeUnit

![image-20210129153450192](https://gitee.com/webrx/wx_note/raw/master/images/image-20210129153450192.png)



#### 11.5.3 CountDownLatch

`java.util.concurrent.CountDownLatch`

* 实现方法一   一个线程实现功能添加元素，一个线程统计

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.ArrayList;
  import java.util.List;
  import java.util.concurrent.TimeUnit;
  
  /**
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class Sync2 {
      public static void main(String[] args) {
          MySync2 ms = new MySync2();
  
          new Thread(() -> {
              for (int i = 1; i <= 20; i++) {
                  ms.add();
                  try {
                      TimeUnit.SECONDS.sleep(1);
                  } catch (Exception e) {
                      e.printStackTrace();
                  }
              }
          }, "t1").start();
  
          new Thread(() -> {
              System.out.println("t2");
              while (!ms.count()) ;
              System.out.println("已经有五个了");
          }, "t2").start();
  
  
      }
  }
  
  class MySync2 {
      private volatile List<String> list = new ArrayList<>();
  
      public void add() {
          list.add("add" + list.size());
          System.out.println(String.format("%d = %s", list.size(), list.get(list.size() - 1)));
  
      }
  
      public boolean count() {
          boolean f = false;
          if (list.size() >= 5) {
              System.out.println(Thread.currentThread().getName() + "线程启动");
              f = true;
          }
          return f;
      }
  }
  ```
  
  方法二 CountDownLatch
  
  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.ArrayList;
  import java.util.List;
  import java.util.concurrent.CountDownLatch;
  import java.util.concurrent.TimeUnit;
  
  /**
   * <p>Project: javaseapp - Ex3
   * <p>Powered by webrx On 2021-08-25 08:59:35
   * <p>Created by IntelliJ IDEA
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class Ex3 {
      static List<String> list = new ArrayList<String>();
      static CountDownLatch latch = new CountDownLatch(1);
  
      public static void work() {
          String tn = Thread.currentThread().getName();
          System.out.println(tn + ":线程启动");
          while (true) {
              try {
                  TimeUnit.SECONDS.sleep(2);
              } catch (Exception e) {
                  e.printStackTrace();
              }
              list.add("item" + (list.size() + 1));
              System.out.println(String.format("%s:%d = %s", tn, list.size(), list.get(list.size() - 1)));
              if (list.size() == 5) {
                  latch.countDown();//开启门栓，此时会提醒线程二向下执行。
              }
          }
      }
  
      public static void check() {
          String tn = Thread.currentThread().getName();
          System.out.println(tn + ":线程启动");
          try {
              latch.await();//此线程中断在此处，等待
              System.out.println(tn + ":容器中已经有五个元素了,线程结束 。");
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
      }
  
      public static void main(String[] args) {
          //CountDownLatch
          //T1 任务向容器中添加元素，到5个元素时，打开门栓通知线程T2
          new Thread(Ex3::work, "T1").start();
          //T2 启动就进入就绪等待，接到通知就开始向下执行。
          new Thread(Ex3::check, "T2").start();
      }
  }
  ```
  
  
  
  案例二
  
  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.concurrent.CountDownLatch;
  import java.util.concurrent.TimeUnit;
  
  /**
   * 是一个计数器，线程完成一个记录一个，计数器递减，只能只用一次
   * //参数count为计数值
   * public CountDownLatch(int count) {  };
   * 调用await()方法的线程会被挂起，它会等待直到count值为0才继续执行
   * public void await() throws InterruptedException { };
   * 和await()类似，只不过等待一定的时间后count值还没变为0的话就会继续执行
   * public boolean await(long timeout, TimeUnit unit) throws InterruptedException { };
   * 将count值减1 public void countDown() { };
   * CountDownLatch CyclicBarrier、Semaphore、concurrentHashMap和BlockingQueue
   * <p>Project: untitled - Demo
   * <p>Powered by webrx On 2021-01-31 18:56:45
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class Test{
      static void m(){
          for(int i=0;i<5;i++) {
              try{
                  TimeUnit.SECONDS.sleep(1);
                  System.out.println(Thread.currentThread().getName() + i);
              }catch(Exception e){
                  e.printStackTrace();
              }
          }
          latch.countDown(); --
      }
      static CountDownLatch latch = new CountDownLatch(5);
      public static void main(String[] args) {
  
          new Thread(()->{
              System.out.println("收尾开始");
              try {
                  latch.await();
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
              //latch.countDown();
              System.out.println("真正程序的大结局");
          },"END").start();
          new Thread(Test::m,"t1").start();
          new Thread(Test::m,"t2").start();
          new Thread(Test::m,"t3").start();
          new Thread(Test::m,"t4").start();
          m();
          System.out.println("main - 大结局");
      }
  }
  ```
  
  ```java
  实现多个线程，有一个线程收尾工作
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   */
  package cn.thread;
  import java.util.concurrent.CountDownLatch;
  import java.util.concurrent.TimeUnit;
  /**
   * <p>Project: part12 - T8
   * <p>Powered By webrx
   * <p>Created By IntelliJ IDEA On 2021-02-01 10:46:15
   * <p>Description : 实现多个线程，有一个线程收尾工作。
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class T8 {
      static CountDownLatch latch = new CountDownLatch(5);
      void work(){
          for(int i=0;i<=5;i++){
              try{
                  TimeUnit.SECONDS.sleep(1);
                  String str = String.format("%s - %d",Thread.currentThread().getName(),i);
                  System.out.println(str);
              }catch(Exception e){
                  e.printStackTrace();
              }
          }
          latch.countDown();
      }
  
      void gameover(){
          System.out.println("结束线程启动....");
          try {
              latch.await();
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          System.out.println("程序结束");
      }
  
      public static void main(String[] args) {
          var t = new T8();
          new Thread(t::work,"T1").start();
          new Thread(t::work,"T2").start();
          new Thread(t::work,"T3").start();
          new Thread(t::work,"T4").start();
          new Thread(t::gameover,"OVER").start();
          for(int i=0;i<=5;i++){
              try{
                  TimeUnit.SECONDS.sleep(1);
                  String str = String.format("%s - %d",Thread.currentThread().getName(),i);
                  System.out.println(str);
              }catch(Exception e){
                  e.printStackTrace();
              }
          }
          latch.countDown();
      }
  }
  ```
  
  ```
  CyclicBarrier 循环栅栏
  ```
  
  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   */
  package cn.thread;
  import java.util.concurrent.*;
  /**
   * <p>Project: part12 - T8
   * <p>Powered By webrx
   * <p>Created By IntelliJ IDEA On 2021-02-01 10:46:15
   * <p>Description : 三个一起吃饭，到起开饭。
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class T10 {
      CyclicBarrier bar = new CyclicBarrier(3,()->System.out.println("大家到起了，开饭了."));
      void eat(int t) {
          System.out.println(Thread.currentThread().getName()+"开始动身...");
          try {
              TimeUnit.SECONDS.sleep(t);
              System.out.println(Thread.currentThread().getName()+"到达。");
              bar.await();
              //bar.await(6,TimeUnit.SECONDS);
          } catch (InterruptedException e) {
              e.printStackTrace();
          } catch (BrokenBarrierException e) {
              e.printStackTrace();
          }
      }
      public static void main(String[] args) {
          var t = new T10();
          new Thread(()->t.eat(6),"李四").start();
          new Thread(()->t.eat(2),"张三丰").start();
          new Thread(()->t.eat(12),"赵六").start();
      }
  }
  ```
  

#### 11.5.4 ReentrantLock 重入锁

<font color=red>ReentrantLock常常对比着synchronized来分析，我们先对比着来看然后再一点一点分析。</font>

（1）synchronized是独占锁，加锁和解锁的过程自动进行，易于操作，但不够灵活。ReentrantLock也是独占锁，加锁和解锁的过程需要手动进行，不易操作，但非常灵活。

（2）synchronized可重入，因为加锁和解锁自动进行，不必担心最后是否释放锁；ReentrantLock也可重入，但加锁和解锁需要手动进行，且次数需一样，否则其他线程无法获得锁。

（3）synchronized不可响应中断，一个线程获取不到锁就一直等着；ReentrantLock可以相应中断。

ReentrantLock好像比synchronized关键字没好太多，我们再去看看synchronized所没有的，一个最主要的就是ReentrantLock还可以实现公平锁机制。什么叫公平锁呢？也就是在锁上等待时间最长的线程将获得锁的使用权。通俗的理解就是谁排队时间最长谁先执行获取锁。

![image-20210129143137509](https://gitee.com/webrx/wx_note/raw/master/images/image-20210129143137509.png)

* 公平锁

  ![image-20210129143110008](https://gitee.com/webrx/wx_note/raw/master/images/image-20210129143110008.png)
  
  ```java
  ReentrantLock
  
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.concurrent.TimeUnit;
  import java.util.concurrent.locks.Lock;
  import java.util.concurrent.locks.ReentrantLock;
  
  /**
   * <p>Project: untitled - T
   * <p>Powered by webrx On 2021-01-31 19:16:40
   * <p>Created by IntelliJ IDEA
   * <p></p>
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class T {
      Lock lock = new ReentrantLock();
      void m1(){
          lock.lock();
          for(int i=0; i<10; i++) {
              try {
                  TimeUnit.SECONDS.sleep(1);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
              System.out.println(i);
          }
          lock.unlock();
      }
      void m2(){
          lock.lock();
          System.out.println("m2()...");
          lock.unlock();
      }
      public static void main(String[] args) {
  
          var t = new T();
          new Thread(t::m1,"t1").start();
          try {
              TimeUnit.SECONDS.sleep(1);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          new Thread(t::m2,"t2").start();
      }
  }
  
  
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.concurrent.TimeUnit;
  import java.util.concurrent.locks.Lock;
  import java.util.concurrent.locks.ReentrantLock;
  
  /**
   * <p>Project: untitled - T
   * <p>Powered by webrx On 2021-01-31 19:16:40
   * <p>Created by IntelliJ IDEA
   * <p></p>
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class T2 {
      Lock lock = new ReentrantLock();
      void m1(){
          lock.lock();
          System.out.println("线程1启动");
          try{
              TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
          }catch(Exception e){
              e.printStackTrace();
          }finally {
              lock.unlock();
          }
  
      }
      void m2(){
          try {
              lock.lockInterruptibly();
              System.out.println("线程2终于打断了");
          } catch (InterruptedException e) {
              System.out.println("编程2....");
          }finally {
              if(lock.tryLock())
              lock.unlock();
              System.out.println("线程2结束了");
              System.exit(0);
          }
      }
      public static void main(String[] args) {
  
          var t = new T2();
          new Thread(t::m1,"T1").start();
          Thread t2 = new Thread(t::m2,"T2");
          try {
              TimeUnit.SECONDS.sleep(1);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          t2.start();
          t2.interrupt();
          System.out.println(Thread.activeCount());
      }
  }
  
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.webrx;
  
  import java.util.concurrent.TimeUnit;
  import java.util.concurrent.locks.Lock;
  import java.util.concurrent.locks.ReentrantLock;
  
  /**
   * <p>Project: untitled - T
   * <p>Powered by webrx On 2021-01-31 19:16:40
   * <p>Created by IntelliJ IDEA
   * <p></p>
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 15
   */
  public class T3 {
      Lock lock = new ReentrantLock(true);//设置为true公平锁，效率低但公平
  
      void m() {
          for (int i = 0; i <= 20; i++) {
              lock.lock();
              System.out.println(Thread.currentThread().getName());
              try{
                  TimeUnit.SECONDS.sleep(1);
              }catch(Exception e){
                  e.printStackTrace();
              }
              lock.unlock();
          }
      }
  
      public static void main(String[] args) {
          var t = new T3();
          new Thread(t::m,"T1").start();
          new Thread(t::m,"T2").start();
          new Thread(t::m,"T3").start();
          new Thread(t::m,"T4").start();
      }
  }
  
  ```

#### 11.5.5 ReadWriteLock读写锁

`ReentrantReadWriteLock`

```java
// 测试类
public class TestReadWriteLock{
    public static void main(String[] args){
        ReadWriteLockDemo rw = new ReadWriteLockDemo();

        // 一个线程进行写
        new Thread(new Runnable(){
            public void run(){
                rw.set((int)(Math.random()*100));
            }
        },"Write:").start();


        // 100个线程进行读操作
        for(int i=0; i<100; i++){
            new Thread(new Runnable(){
                public void run(){
                rw.get();
                }
            },"Read:").start();
        }
    }
}
 
class ReadWriteLockDemo{
	private int number = 0;
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	// 读
	public void get(){
		lock.readLock().lock(); // 上锁
        try{
        	System.out.println(Thread.currentThread().getName()+":"+number);
        }finally{
        	lock.readLock().unlock(); // 释放锁
        }
	}
    // 写
    public void set(int number){
        lock.writeLock().lock();
        try{
        	System.out.println(Thread.currentThread().getName());
        	this.number = number;
        }finally{
        	lock.writeLock().unlock();
        }
    }
}
```



#### 11.5.6 wait()和sleep()

```text
1 sleep()实现线程阻塞的方法，我们称之为“线程睡眠”，方式是超时等待
2 wait()方法实现线程阻塞的方法，我们称之为“线程等待”和sleep()方法一样，通过传入“睡眠时间”作为参数，时间到了就“醒了”；
不传入时间，进行一次“无限期的等待”，只用通过notify()方法来“唤醒”。
3 sleep()释放CPU执行权，但不释放同步锁；
4 wait()释放CPU执行权，也释放同步锁，使得其他线程可以使用同步控制块或者方法。
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javase - T15
 * <p>Powered by webrx On 2021-12-27 11:43:13
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class T15 {
    Object lock = new Object();

    public static void main(String[] args) {
        var ex = new T15();
        new Thread(ex::work, "T1").start();
        new Thread(ex::work, "T2").start();
        new Thread(ex::work, "T3").start();
        new Thread(ex::work, "T4").start();
        new Thread(ex::work, "ok").start();
    }


    void work() {
        String tn = Thread.currentThread().getName();
        System.out.println(tn + "线程启动");
        if ("ok".equals(tn)) {
            try {
                TimeUnit.SECONDS.sleep(2);
                synchronized (lock) {
                    lock.notifyAll();
                    //lock.notify();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            for (int i = 0; i < 3; i++) {
                System.out.printf("%s = %d%n", tn, i);
                synchronized (lock) {
                    try {
                        if (i < 1) {
                            System.out.println(tn + "wait()....");
                            lock.wait(); //没有唤醒   notify()
                        }
                        //TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        System.out.println(tn + "线程结束...");
    }
}
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javaseapp - Ex11
 * <p>Powered by webrx On 2021-08-26 09:08:48
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Ex11 {
    Object b = new Object();
    List<String> list = new ArrayList<>();

    void work() {
        while (true) {
            synchronized (b) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String ok = "item" + (list.size() + 1);
                System.out.println(ok);
                list.add(ok);
                if (list.size() == 5) {
                    try {
                        b.notify();//唤醒其它线程，不释放当前占用的锁
                        b.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    void count() {
        synchronized (b) {
            try {
                b.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("统计提示：已经到5个元素。");
            b.notify();
        }
    }

    public static void main(String[] args) {
        var t = new Ex11();
        new Thread(t::work, "T1").start();
        new Thread(t::count, "T2").start();

    }
}


/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javase - T14
 * <p>Powered by webrx On 2021-12-27 11:11:53
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class T14 {
    Object lock = new Object();
    List<String> list = new ArrayList<>();

    void add() {
        String t = Thread.currentThread().getName();
        System.out.println(t + "增加线程启动...");
        for (int i = 0; i < 6; i++) {
            synchronized (this) {
                synchronized (lock) {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String item = "Item" + list.size();
                    list.add(item);
                    System.out.println(t + ":增加" + item);
                    if (list.size() == 6) {
                        try {
                            lock.notify();
                            lock.wait();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    }

    void count() {
        String t = Thread.currentThread().getName();
        System.out.println(t + "统计线程启动...");
        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(t + ":容器到6个元素了.");
            System.out.println(t + ":监控线程结束...");
            lock.notify();
        }
    }

    public static void main(String[] args) {
        var t = new T14();

        new Thread(t::add, "Add1").start();
        new Thread(t::add, "Add2").start();
        new Thread(t::add, "Add3").start();
        new Thread(t::add, "Add4").start();
        new Thread(t::add, "Add5").start();
        new Thread(t::add, "Add6").start();
        new Thread(t::count, "Count").start();
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javase - WaitAndNotify
 * <p>Powered by webrx On 2021-12-27 12:18:53
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class WaitAndNotify {
    public static void main(String[] args) {
        Object co = new Object();
        System.out.println(co);

        for (int i = 0; i < 5; i++) {
            MyThread t = new MyThread("Thread" + i, co);
            t.start();
        }

        try {
            TimeUnit.SECONDS.sleep(2);
            System.out.println("-----Main Thread notify-----");
            synchronized (co) {
                //co.notify();
                co.notifyAll();
            }

            TimeUnit.SECONDS.sleep(2);
            System.out.println("Main Thread is end.");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class MyThread extends Thread {
        private String name;
        private Object co;

        public MyThread(String name, Object o) {
            this.name = name;
            this.co = o;
        }

        @Override
        public void run() {
            System.out.println(name + " is waiting.");
            try {
                synchronized (co) {
                    co.wait();
                }
                System.out.println(name + " has been notified.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

```



#### 11.5.7 join()、Thread.yield( )

```text
ThreadTest t1=new ThreadTest("A");
ThreadTest t2=new ThreadTest("B");
t1.start();
t1.join();
t2.start();
先执行t1线程，再执行t2线程。

ThreadTest t1=new ThreadTest("A");
ThreadTest t2=new ThreadTest("B");
ThreadTest t3=new ThreadTest("C");
t1.start();
t2.start();
t1.join();
t3.start();
t1线程和t2线程交替执行，当t1线程执行完后开始t3线程，执行过程中和t2没有关系

多次实验可以看出，主线程在t1.join()方法处停止，并需要等待A线程执行完毕后才会执行t3.start()，然而，并不影响B线程的执行。因此，可以得出结论，t.join()方法只会使主线程进入等待池并等待t线程执行完毕后才会被唤醒。并不影响同一时刻处在运行状态的其他线程。

thread.Join把指定的线程加入到当前线程，可以将两个交替执行的线程合并为顺序执行的线程。
比如在线程B中调用了线程A的Join()方法，直到线程A执行完毕后，才会继续执行线程B。
t.join();      //调用join方法，等待线程t执行完毕
t.join(1000);  //等待 t 线程，等待时间是1000毫秒。
PS:join源码中，只会调用wait方法，并没有在结束时调用notify，这是因为线程在die的时候会自动调用自身的notifyAll方法，来释放所有的资源和锁。
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javaseapp - Ex12
 * <p>Powered by webrx On 2021-08-26 11:35:34
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Ex12 {
    void work() {
        String tn = Thread.currentThread().getName();
        for (int i = 0; i < 5; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.printf("%s = %d%n", tn, i);
        }
    }

    public static void main(String[] args) {
        var ex = new Ex12();
        var t1 = new Thread(ex::work, "T1");
        var t2 = new Thread(ex::work, "T2");
        var t3 = new Thread(ex::work, "T3");
        var t4 = new Thread(ex::work, "T4");
        //并行
        //t2->t3 串行执行
        //t1->t3 串行执行 先执行t1再执行t3
        t1.start();
        t2.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t3.start();
        try {
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t4.start();
    }

    public static void test(String[] args) {
        var ex = new Ex12();
        var t1 = new Thread(ex::work, "T1");
        var t2 = new Thread(ex::work, "T2");
        var t3 = new Thread(ex::work, "T3");
        var t4 = new Thread(ex::work, "T4");

        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
    }
}
```

<strong>Thread.yield( )</strong>

使当前线程从执行状态（运行状态）变为可执行态（就绪状态）。cpu会从众多的可执行态里选择，也就是说，当前也就是刚刚的那个线程还是有可能会被再次执行到的，并不是说一定会执行其他线程而该线程在下一次中不会执行到了。

Java线程中有一个Thread.yield( )方法，很多人翻译成线程让步。顾名思义，就是说当一个线程使用了这个方法之后，它就会把自己CPU执行的时间让掉，让自己或者其它的线程运行。

<p>打个比方：现在有很多人在排队上厕所，好不容易轮到这个人上厕所了，突然这个人说：“我要和大家来个竞赛，看谁先抢到厕所！”，然后所有的人在同一起跑线冲向厕所，有可能是别人抢到了，也有可能他自己有抢到了。我们还知道线程有个优先级的问题，那么手里有优先权的这些人就一定能抢到厕所的位置吗? 不一定的，他们只是概率上大些，也有可能没特权的抢到了。</p>

```java
public class YieldTest extends Thread {
	public YieldTest(String name) {
		super(name);
	}
	@Override
	public void run() {
		for (int i = 1; i <= 50; i++) {
			System.out.println("" + this.getName() + "-----" + i);
			// 当i为30时，该线程就会把CPU时间让掉，让其他或者自己的线程执行（也就是谁先抢到谁执行）
			if (i == 30) {
				this.yield();
			}
		}
	}
	public static void main(String[] args) {
		YieldTest yt1 = new YieldTest("张三");
		YieldTest yt2 = new YieldTest("李四");
		yt1.start();
		yt2.start();
	}
}
```

#### 11.5.8 interrupt() 打断

```text
方法一
    public static volatile boolean exit = false;  //退出标志
    public static void main(String[] args) {
        new Thread() {
            public void run() {
                System.out.println("线程启动了");
                while (!exit) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("线程结束了");
            }
        }.start();
        
        try {
            Thread.sleep(1000 * 5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exit = true;//5秒后更改退出标志的值,没有这段代码，线程就一直不能停止
   }  
   
方法二 interrupt();
package cn.webrx;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
public class T {
    public static void main(String[] args) {
        var t = new Thread(()->{
            while(true){
                try {
                    System.out.println(Thread.currentThread().getName());
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    System.out.println(Thread.currentThread().getName() + "被中断了");
                    break;
                }
            }
        },"t1");
        t.start();
        
        try {
            System.out.println(Thread.currentThread().getName());
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.interrupt();
    }
}

方法三
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javaseapp - Thread9
 * <p>Powered by webrx On 2021-08-26 15:13:51
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Thread9 {
    public static void work() {
        Thread tt = Thread.currentThread();
        String tn = tt.getName();
        System.out.printf("%s启动了%n", tn);
        System.out.printf("%s.isInterrupted() %s%n", tn, tt.isInterrupted());
        while (!tt.isInterrupted()) {
            System.out.printf("%s.isInterrupted() %s%n", tn, tt.isInterrupted());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                break;
            }

        }
        System.out.printf("%s.isInterrupted() %s%n", tn, tt.isInterrupted());
        System.out.printf("%s结束了...%n", tn);
    }

    public static void main(String[] args) {
        //开启线程
        var t = new Thread(Thread9::work, "T1");
        t.start();

        try {
            TimeUnit.SECONDS.sleep(5);
            t.interrupt();
            System.out.println("线程是否被中断：" + t.isInterrupted());//true
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: part12 - T
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T13 {
    final Object lock = new Object();

    void m() {
        System.out.println(Thread.currentThread().getName() + "启动等待...");
        try {
            synchronized (lock) {
                wait();
            }
            System.out.println(Thread.currentThread().getName() + "被唤醒了...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        var t = new T13();
        for (int i = 0; i < 5; i++) new Thread(t::m, "T" + i).start();

        try {
            TimeUnit.SECONDS.sleep(5);
            synchronized (t.lock) {
                t.notify();
                //t.lock.notifyAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

```



#### 11.5.9 wait()、notify()、notifyAll()

如果线程调用了对象的 wait()方法，那么线程便会处于该对象的等待池中，等待池中的线程不会去竞争该对象的锁。

当有线程调用了对象的 notifyAll()方法（唤醒所有 wait 线程）或 notify()方法（只随机唤醒一个 wait 线程），被唤醒的的线程便会进入该对象的锁池中，锁池中的线程会去竞争该对象锁。也就是说，调用了notify后只要一个线程会由等待池进入锁池，而notifyAll会将该对象等待池内的所有线程移动到锁池中，等待锁竞争

优先级高的线程竞争到对象锁的概率大，假若某线程没有竞争到该对象锁，它还会留在锁池中，唯有线程再次调用 wait()方法，它才会重新回到等待池中。而竞争到对象锁的线程则继续往下执行，直到执行完了 synchronized 代码块，它会释放掉该对象锁，这时锁池中的线程会继续竞争该对象锁。

所谓唤醒线程，另一种解释可以说是将线程由等待池移动到锁池，notifyAll调用后，会将全部线程由等待池移到锁池，然后参与锁的竞争，竞争成功则继续执行，如果不成功则留在锁池等待锁被释放后再次参与竞争。而notify只会唤醒一个线程。

```java
public class WaitAndNotify {
public static void main(String[] args) {
        Object co = new Object();
        System.out.println(co);
        for (int i = 0; i < 5; i++) {
            MyThread t = new MyThread("Thread" + i, co);
            t.start();
        }

try {
            TimeUnit.SECONDS.sleep(2);
            System.out.println("-----Main Thread notify-----");
            synchronized (co) {
                co.notify();
            }

            TimeUnit.SECONDS.sleep(2);
            System.out.println("Main Thread is end.");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

static class MyThread extends Thread {
		private String name;
        private Object co;
        public MyThread(String name, Object o) {
			this.name = name;
            this.co = o;
        }
        public void run() {
            System.out.println(name + " is waiting.");
            try {
				synchronized (co) {
					co.wait();
                }
                System.out.println(name + " has been notified.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
```

#### 11.5.10 AtomicInteger AtomicBoolean

在`JDK8`之前，针对原子操作，我们基本上可以通过上面提供的这些类来完成我们的多线程下的原子操作，不过在并发高的情况下，上面这些单一的 `CAS` + 自旋操作的性能将会是一个问题，所以上述这些类一般用于低并发操作。
而针对这个问题，`JDK8`又引入了下面几个类：`DoubleAdder`，`LongAdder`，`DoubleAccumulator`，`LongAccumulator`，这些类是对`AtomicLong`这些类的改进与增强，这些类都继承自`Striped64`这个类。

![img](https://gitee.com/webrx/wx_note/raw/master/images/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2Nja2V2aW5jeWg=,size_16,color_FFFFFF,t_70.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>Project: part12 - T
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T {
    AtomicInteger n = new AtomicInteger(50);
    int num = 50;
    void minus() {
        while (true) {
            if (n.get() > 0) {
                n.decrementAndGet();
                System.out.printf("%s 售出一张票，剩余%d张票。 %n", Thread.currentThread().getName(), n.get());
            } else {
                break;
            }

            /*
            if (num > 0) {
                num--;
                System.out.printf("%s 售出一张票，剩余%d张票。 %n",Thread.currentThread().getName(),num);
            }else{
                break;
            }
			*/
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        var t = new T();
        new Thread(t::minus, "T1").start();
        new Thread(t::minus, "T2").start();
        new Thread(t::minus, "T3").start();
        new Thread(t::minus, "T4").start();
        new Thread(t::minus, "T5").start();
        new Thread(t::minus, "T6").start();
        new Thread(t::minus, "T7").start();
        new Thread(t::minus, "T8").start();
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * <p>Project: part12 - T
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T0 {
    AtomicBoolean flag = new AtomicBoolean(true);
    void op() {
        System.out.printf("%s 启动计算中 %n", Thread.currentThread().getName());
        String[] o = new String[]{"", "+", "-"};
        Random rand = new Random();
        StringBuffer str = new StringBuffer("1");
        Pattern p = Pattern.compile("(\\d+|-\\d+)");
        while (flag.get()) {
            for (int i = 2; i < 10; i++)
                str.append(String.format("%s%d", o[rand.nextInt(o.length)], i));
            String s = str.toString();//123456789 = 100
            Matcher m = p.matcher(s);
            List<Integer> ln = new ArrayList<>();
            while (m.find()) {
                ln.add(Integer.parseInt(m.group()));
            }
            int sum = ln.stream().reduce(0, Integer::sum);
            if (sum == 100) {
                flag.set(false);
                System.out.printf("[%s]:%s = 100%n", Thread.currentThread().getName(), s);
                break;
            } else {
                str.delete(1, str.length());
            }
        }
        System.out.printf("%s 结束 %n", Thread.currentThread().getName());
    }
    public static void main(String[] args) {
        var t = new T0();
        for (int i = 0; i < 5; i++)
            new Thread(t::op, "T" + i).start();
    }

    public static void ok(String[] args) {
        String[] o = new String[]{"", "+", "-"};
        Random rand = new Random();
        StringBuilder str = new StringBuilder("1");
        List<String> list = new ArrayList<>();
        while (true) {
            for (int i = 2; i < 10; i++)
                str.append(String.format("%s%d", o[rand.nextInt(o.length)], i));
            String s = str.toString();
            Pattern p = Pattern.compile("(\\d+|-\\d+)");
            Matcher m = p.matcher(s);
            List<Integer> ln = new ArrayList<>();
            while (m.find()) {
                ln.add(Integer.parseInt(m.group()));
            }
            int sum = ln.stream().reduce(0, Integer::sum);
            if (sum == 100) {
                System.out.printf("%s = 100%n", s);
                break;
            } else {
                str.delete(1, str.length());
            }
        }
    }
}
```

```java
public class Counter {

    public static AtomicInteger count = new AtomicInteger(0);

    public static void inc(){
        try{
            Thread.sleep(1); //延迟1毫秒

        }catch (InterruptedException e){ //catch住中断异常，防止程序中断
            e.printStackTrace();

        }
        count.getAndIncrement();//count值自加1
    }


    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(100);
        for(int i=0;i<100;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter.inc();
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
        System.out.println("运行结果："+Counter.count);
    }
}
```

```java
public class Counter {
    public volatile  static int count = 0;
    public static void inc(){
        try{
            Thread.sleep(1); //延迟1毫秒
        }catch (InterruptedException e){ //catch住中断异常，防止程序中断
            e.printStackTrace();
        }
        count++;//count值自加1
    }
    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(100);
        for(int i=0;i<100;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter.inc();
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
        System.out.println("运行结果："+Counter.count);
   }
}
运行结果：98
    
    
import java.util.concurrent.CountDownLatch;
public class Counter {
    public volatile static  Integer count = 0;
    public synchronized static void inc(){
        try{
            Thread.sleep(1); //延迟1毫秒
        }catch (InterruptedException e){ //catch住中断异常，防止程序中断
            e.printStackTrace();
        }
          count++;//count值自加1
    }

    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(100);
        for(int i=0;i<100;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Counter.inc();
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
        System.out.println("运行结果："+Counter.count);
    }
}

运行结果：100
```

### 11.6 volatile synchronized

- volatile 关键字: 当多个线程进行操作共享数据时,可以保证内存中的数据是可见的;相较于 synchronized 是一种较为轻量级的同步策略

- volatile 不具备"互斥性"

- volatile 不能保证变量的"原子性"

- **Java 内存模型中的可见性、原子性和有序性。**

  ```text
  可见性：
  　　可见性是一种复杂的属性，因为可见性中的错误总是会违背我们的直觉。通常，我们无法确保执行读操作的线程能适时地看到其他线程写入的值，有时甚至是根本不可能的事情。为了确保多个线程之间对内存写入操作的可见性，必须使用同步机制。
  　　可见性，是指线程之间的可见性，一个线程修改的状态对另一个线程是可见的。也就是一个线程修改的结果。另一个线程马上就能看到。比如：用volatile修饰的变量，就会具有可见性。volatile修饰的变量不允许线程内部缓存和重排序，即直接修改内存。所以对其他线程是可见的。但是这里需要注意一个问题，volatile只能让被他修饰内容具有可见性，但不能保证它具有原子性。比如 volatile int a = 0；之后有一个操作 a++；这个变量a具有可见性，但是a++ 依然是一个非原子操作，也就是这个操作同样存在线程安全问题。
  
  　　在 Java 中 volatile、synchronized 和 final 实现可见性。
  原子性：
  　　原子是世界上的最小单位，具有不可分割性。比如 a=0；（a非long和double类型） 这个操作是不可分割的，那么我们说这个操作时原子操作。再比如：a++； 这个操作实际是a = a + 1；是可分割的，所以他不是一个原子操作。非原子操作都会存在线程安全问题，需要我们使用同步技术（sychronized）来让它变成一个原子操作。一个操作是原子操作，那么我们称它具有原子性。java的concurrent包下提供了一些原子类，我们可以通过阅读API来了解这些原子类的用法。比如：AtomicInteger、AtomicLong、AtomicReference等。
  
  在 Java 中 synchronized 和在 lock、unlock 中操作保证原子性。
  
  有序性：
  　　Java 语言提供了 volatile 和 synchronized 两个关键字来保证线程之间操作的有序性，volatile 是因为其本身包含“禁止指令重排序”的语义，synchronized 是由“一个变量在同一个时刻只允许一条线程对其进行 lock 操作”这条规则获得的，此规则决定了持有同一个对象锁的两个同步块只能串行执行。
  ```

Java语言提供了一种稍弱的同步机制，即volatile变量，用来确保将变量的更新操作通知到其他线程。当把变量声明为volatile类型后，编译器与运行时都会注意到这个变量是共享的，因此不会将该变量上的操作与其他内存操作一起重排序。volatile变量不会被缓存在寄存器或者对其他处理器不可见的地方，因此在读取volatile类型的变量时总会返回最新写入的值。

在访问volatile变量时不会执行加锁操作，因此也就不会使执行线程阻塞，因此volatile变量是一种比sychronized关键字更轻量级的同步机制。

![img](https://gitee.com/webrx/wx_note/raw/master/images/731716-20160708224602686-2141387366.png)

当对非 volatile 变量进行读写的时候，每个线程先从内存拷贝变量到CPU缓存中。如果计算机有多个CPU，每个线程可能在不同的CPU上被处理，这意味着每个线程可以拷贝到不同的 CPU cache 中。

而声明变量是 volatile 的，JVM 保证了每次读变量都从内存中读，跳过 CPU cache 这一步。

当一个变量定义为 volatile 之后，将具备两种特性：

1.保证此变量对所有的线程的可见性，这里的“可见性”，如本文开头所述，当一个线程修改了这个变量的值，volatile 保证了新值能立即同步到主内存，以及每次使用前立即从主内存刷新。但普通变量做不到这点，普通变量的值在线程间传递均需要通过主内存来完成。

2.禁止指令重排序优化。有volatile修饰的变量，赋值后多执行了一个“load addl $0x0, (%esp)”操作，这个操作相当于一个**内存屏障**（指令重排序时不能把后面的指令重排序到内存屏障之前的位置），只有一个CPU访问内存时，并不需要内存屏障；（什么是指令重排序：是指CPU采用了允许将多条指令不按程序规定的顺序分开发送给各相应电路单元处理）。

volatile 性能：volatile 的读性能消耗与普通变量几乎相同，但是写操作稍慢，因为它需要在本地代码中插入许多内存屏障指令来保证处理器不发生乱序执行。

```java
/**
 * volatile 关键字，使一个变量在多个线程间可见
 * A B线程都用到一个变量，java默认是A线程中保留一份copy，这样如果B线程修改了该变量，则A线程未必知道
 * 使用volatile关键字，会让所有线程都会读到变量的修改值
 * 
 * 在下面的代码中，running是存在于堆内存的t对象中
 * 当线程t1开始运行的时候，会把running值从内存中读到t1线程的工作区，在运行过程中直接使用这个copy，并不会每次都去
 * 读取堆内存，这样，当主线程修改running的值之后，t1线程感知不到，所以不会停止运行
 * 
 * 使用volatile，将会强制所有线程都去堆内存中读取running的值
 * 
 * volatile并不能保证多个线程共同修改running变量时所带来的不一致问题，也就是说volatile不能替代synchronized
 *
 */
package cn.webrx.c_012;
import java.util.concurrent.TimeUnit;
public class T {
	/*volatile*/ boolean running = true; //对比一下有无volatile的情况下，整个程序运行结果的区别
	void m() {
		System.out.println("m start");
		while(running) {
			/*
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		}
		System.out.println("m end!");
	}

	public static void main(String[] args) {
		T t = new T();
		new Thread(t::m, "t1").start();
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t.running = false;
	}
}
```

```java
/**
 * volatile并不能保证多个线程共同修改running变量时所带来的不一致问题，也就是说volatile不能替代synchronized
 * 运行下面的程序，并分析结果
 *
 */
package cn.webrx.c_013;

import java.util.ArrayList;
import java.util.List;

public class T {
	volatile int count = 0; 
	void m() {
		for(int i=0; i<10000; i++) count++;
	}
	
	public static void main(String[] args) {
		T t = new T();
		
		List<Thread> threads = new ArrayList<Thread>();
		
		for(int i=0; i<10; i++) {
			threads.add(new Thread(t::m, "thread-"+i));
		}
		
		threads.forEach((o)->o.start());
		
		threads.forEach((o)->{
			try {
				o.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		
		System.out.println(t.count);
	}
}
```

```java
/**
 * 对比上一个程序，可以用synchronized解决，synchronized可以保证可见性和原子性，volatile只能保证可见性
 *
 */
package cn.webrx.c_014;

import java.util.ArrayList;
import java.util.List;


public class T {
	/*volatile*/ int count = 0;

	synchronized void m() { 
		for (int i = 0; i < 10000; i++)
			count++;
	}

	public static void main(String[] args) {
		T t = new T();

		List<Thread> threads = new ArrayList<Thread>();

		for (int i = 0; i < 10; i++) {
			threads.add(new Thread(t::m, "thread-" + i));
		}

		threads.forEach((o) -> o.start());

		threads.forEach((o) -> {
			try {
				o.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		System.out.println(t.count);

	}

}
```

```java
﻿/**
 * 解决同样的问题的更高效的方法，使用AtomXXX类
 * AtomXXX类本身方法都是原子性的，但不能保证多个方法连续调用是原子性的
 *
 */
package cn.webrx.c_015;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class T {
	/*volatile*/ //int count = 0;
	
	AtomicInteger count = new AtomicInteger(0); 

	/*synchronized*/ void m() { 
		for (int i = 0; i < 10000; i++)
			//if count.get() < 1000
			count.incrementAndGet(); //count++
	}

	public static void main(String[] args) {
		T t = new T();

		List<Thread> threads = new ArrayList<Thread>();

		for (int i = 0; i < 10; i++) {
			threads.add(new Thread(t::m, "thread-" + i));
		}

		threads.forEach((o) -> o.start());

		threads.forEach((o) -> {
			try {
				o.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		System.out.println(t.count);

	}

}
```

```java
/**
 * synchronized优化
 * 同步代码块中的语句越少越好
 * 比较m1和m2
 *
 */
package cn.webrx.c_016;

import java.util.concurrent.TimeUnit;


public class T {
	
	int count = 0;

	synchronized void m1() {
		//do sth need not sync
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//业务逻辑中只有下面这句需要sync，这时不应该给整个方法上锁
		count ++;
		
		//do sth need not sync
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	void m2() {
		//do sth need not sync
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//业务逻辑中只有下面这句需要sync，这时不应该给整个方法上锁
		//采用细粒度的锁，可以使线程争用时间变短，从而提高效率
		synchronized(this) {
			count ++;
		}
		//do sth need not sync
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
```



### 11.7 线程池

在Java 5之后，并发编程引入了一堆新的启动、调度和管理线程的API。Executor框架便是Java 5中引入的，其内部使用了线程池机制，它在java.util.cocurrent 包下，通过该框架来控制线程的启动、执行和关闭，可以简化并发编程的操作。因此，在Java 5之后，通过Executor来启动线程比使用Thread的start方法更好，除了更易管理，效率更好（用线程池实现，节约开销）外，还有关键的一点：有助于避免this逃逸问题——如果我们在构造器中启动一个线程，因为另一个任务可能会在构造器结束之前开始执行，此时可能会访问到初始化了一半的对象用Executor在构造器中。Eexecutor作为灵活且强大的异步执行框架，其支持多种不同类型的任务执行策略，提供了一种标准的方法将任务的提交过程和执行过程解耦开发，基于生产者-消费者模式，其提交任务的线程相当于生产者，执行任务的线程相当于消费者，并用Runnable来表示任务，Executor的实现还提供了对生命周期的支持，以及统计信息收集，应用程序管理机制和性能监视等机制。、

![img](https://gitee.com/webrx/wx_note/raw/master/images/20180530120605822.png)

Executor框架包括：线程池，Executor，Executors，ExecutorService，CompletionService，Future，Callable等。

- 线程池提供了一个线程队列,队列中保存着所有等待状态的线程;

- 避免了创建与销毁线程的额外开销,提高了响应速度;

- 线程池的体系结构

  - `java.util.concurrent.Executor`: 负责线程的使用和调度的根接口;
  - `ExecutorService`: 子接口,线程池的主要接口;
  - `ThreadPoolExecutor`: 线程池的实现类;
  - `ScheduledExecutorService`: 子接口,负责线程的调度;
  - `ScheduledThreadPoolExecutor`: 继承了线程池的实现类,实现了负责线程调度的子接口;

- 工具类: 

  ```
  Executors
  ```

  - `ExecutorService newFixedThreadPool()`: 创建固定大小的线程池;
  - `ExecutorService newCachedThreadPool()`: 缓存线程池,线程池中线程的数量不固定,可以根据需求自动更改数量;
  - `ExecutorService newSingleThreadExecutor()`: 创建单个线程池, 线程池中只有一个线程;
  - `ScheduledExecutorService newScheduledThreadPool()`: 创建固定大小的线程,可以延时或定时的执行任务;

```java
public class TestThreadPool{
	public static void main(String[] args){
        // 1. 创建线程池
        ExecutorService pool = Executors.newFixedThreadPool(5);
        ThreadPoolDemo tpd = new ThreadPoolDemo();
        // 2. 为线程池中线程分配任务
        // submit(Callable<T> task)
        // submit(Runnable task)
        for(int i=0; i<10; i++){
            pool.submit(tpd);
        }
        // 3. 关闭线程池
        pool.shutdown();
    }
}
 
class ThreadPoolDemo implements Runnable{ 
    private int i=0;
    public void run(){
        while(i <= 100){
        	System.out.println(Thread.currentThread().getName()+" : "+ i++)
        }
    }
}
```

```java
public class TestScheduledThreadPool{
    public static void main(String[] args) throws Exception{
        // 1. 创建线程池
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        // 2. 分配任务
        // pool.shedule(Callalbe<T> callable, long delay, TimeUnit unit(时间单位))
        for(int i=0; i < 10; i++){
        	Future<Integer> result = pool.schedule(new Callable<Integer>(){
        		public Integer call() throws Exception{
                    // 产生100以内的随机数
                    int num = new Random().nextInt(100);
                    System.out.println(Thread.currentThread().getName()+ ":" + num);
                    return num;
                }
        	}, 3, TimeUnit.SECONDS);
        	System.out.println(result.get());
        }
        //3. 关闭线程池
        pool.shutdown();
    }
}
```

```java
import java.util.ArrayList;   
import java.util.List;   
import java.util.concurrent.*;   
  
public class CallableDemo{   
    public static void main(String[] args){   
        ExecutorService executorService = Executors.newCachedThreadPool();   
        List<Future<String>> resultList = new ArrayList<Future<String>>();   
  
        //创建10个任务并执行   
        for (int i = 0; i < 10; i++){   
            //使用ExecutorService执行Callable类型的任务，并将结果保存在future变量中   
            Future<String> future = executorService.submit(new TaskWithResult(i));   
            //将任务执行结果存储到List中   
            resultList.add(future);   
        }   
 
        //遍历任务的结果   
        for (Future<String> fs : resultList){   
                try{   
                    while(!fs.isDone);//Future返回如果没有完成，则一直循环等待，直到Future返回完成  
                    System.out.println(fs.get());     //打印各个线程（任务）执行的结果   
                }catch(InterruptedException e){   
                    e.printStackTrace();   
                }catch(ExecutionException e){   
                    e.printStackTrace();   
                }finally{   
                    //启动一次顺序关闭，执行以前提交的任务，但不接受新任务  
                    executorService.shutdown();   
                }   
        }   
    }   
}   
  
class TaskWithResult implements Callable<String>{   
    private int id;   
  
    public TaskWithResult(int id){   
        this.id = id;   
    }   
  
    /**  
     * 任务的具体过程，一旦任务传给ExecutorService的submit方法， 
     * 则该方法自动在一个线程上执行 
     */   
    public String call() throws Exception {  
        System.out.println("call()方法被自动调用！！！    " + Thread.currentThread().getName());   
        //该返回结果将被Future的get方法得到  
        return "call()方法被自动调用，任务返回的结果是：" + id + "    " + Thread.currentThread().getName();   
    }   
}
```



```java
public class ThreadPoolTest{   
    public static void main(String[] args){   
        //创建等待队列   
        BlockingQueue<Runnable> bqueue = new ArrayBlockingQueue<Runnable>(20);   
        //创建线程池，池中保存的线程数为3，允许的最大线程数为5  
        ThreadPoolExecutor pool = new ThreadPoolExecutor(3,5,50,TimeUnit.MILLISECONDS,bqueue);   
        //创建七个任务   
        Runnable t1 = new MyThread();   
        Runnable t2 = new MyThread();   
        Runnable t3 = new MyThread();   
        Runnable t4 = new MyThread();   
        Runnable t5 = new MyThread();   
        Runnable t6 = new MyThread();   
        Runnable t7 = new MyThread();   
        //每个任务会在一个线程上执行  
        pool.execute(t1);   
        pool.execute(t2);   
        pool.execute(t3);   
        pool.execute(t4);   
        pool.execute(t5);   
        pool.execute(t6);   
        pool.execute(t7);   
        //关闭线程池   
        pool.shutdown();   
    }   
}   
  
class MyThread implements Runnable{   
    @Override   
    public void run(){   
        System.out.println(Thread.currentThread().getName() + "正在执行。。。");   
        try{   
            Thread.sleep(100);   
        }catch(InterruptedException e){   
            e.printStackTrace();   
        }   
    }   
}
```

> corePoolSize：线程池中所保存的核心线程数，包括空闲线程。
>
> maximumPoolSize：池中允许的最大线程数。
>
> keepAliveTime：线程池中的空闲线程所能持续的最长时间。
>
> unit：持续时间的单位。
>
> workQueue：任务执行前保存任务的队列，仅保存由execute方法提交的Runnable任务。
>
>
> 根据ThreadPoolExecutor源码前面大段的注释，我们可以看出，当试图通过excute方法将一个Runnable任务添加到线程池中时，按照如下顺序来处理：
>     1、如果线程池中的线程数量少于corePoolSize，即使线程池中有空闲线程，也会创建一个新的线程来执行新添加的任务；
>     2、如果线程池中的线程数量大于等于corePoolSize，但缓冲队列workQueue未满，则将新添加的任务放到workQueue中，按照FIFO的原则依次等待执行（线程池中有线程空闲出来后依次将缓冲队列中的任务交付给空闲的线程执行）；
>
>    3、如果线程池中的线程数量大于等于corePoolSize，且缓冲队列workQueue已满，但线程池中的线程数量小于maximumPoolSize，则会创建新的线程来处理被添加的任务；
>
>
>   4、如果线程池中的线程数量等于了maximumPoolSize，有4种处理方式（该构造方法调用了含有5个参数的构造方法，并将最后一个构造方法为RejectedExecutionHandler类型，它在处理线程溢出时有4种方式，这里不再细说，要了解的，自己可以阅读下源码）。
>
>     总结起来，也即是说，当有新的任务要处理时，先看线程池中的线程数量是否大于corePoolSize，再看缓冲队列workQueue是否满，最后看线程池中的线程数量是否大于maximumPoolSize。
>     
>     另外，当线程池中的线程数量大于corePoolSize时，如果里面有线程的空闲时间超过了keepAliveTime，就将其移除线程池，这样，可以动态地调整线程池中线程的数量。

顺序执行的线程：

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.sql.SQLOutput;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * <p>Project: javase - T20
 * <p>Powered by webrx On 2021-12-27 13:35:36
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class T20 {
    void work() {
        String tn = Thread.currentThread().getName();
        System.out.printf("%s:线程启动%n", tn);
        for (int i = 0; i < 2; i++) {
            System.out.printf("%s:%d%n", tn, i);
        }
        System.out.printf("%s:线程结束%n", tn);
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService es = Executors.newCachedThreadPool();
        //建立固定大小的线程池
        //es = Executors.newFixedThreadPool(3);
        es = Executors.newSingleThreadExecutor();

        var t = new T20();
        //es.submit(t::work, "T1");
        //es.submit(t::work, "T2");
        //es.submit(t::work, "T3");
        //es.submit(t::work, "T4");

        es.execute(t::work);
        es.execute(t::work);
        es.execute(t::work);
        es.execute(t::work);

        System.out.println(es.isShutdown());

        // 关闭启动线程
        es.shutdown();
        // 等待子线程结束，再继续执行下面的代码
        es.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        System.out.println("all thread complete");

    }
}

```



<font color=red>new Thread的弊端如下：</font>

a. 每次new Thread新建对象性能差。

b. 线程缺乏统一管理，可能无限制新建线程，相互之间竞争，及可能占用过多系统资源导致死机或oom。
c. 缺乏更多功能，如定时执行、定期执行、线程中断。
相比new Thread，Java提供的四种线程池的好处在于：

1. 重用存在的线程，减少对象创建、消亡的开销，性能佳。
2.  可有效控制最大并发线程数，提高系统资源的使用率，同时避免过多资源竞争，避免堵塞。
3. 提供定时执行、定期执行、单线程、并发数控制等功能。

### 11.8 Fork/Join 框架

java.util.concurrent.ForkJoinPool由Java大师Doug Lea主持编写，它可以将一个大的任务拆分成多个子任务进行并行处理，最后将子任务结果合并成最后的计算结果，并进行输出。本文中对Fork/Join框架的讲解，基于JDK1.8+中的Fork/Join框架实现，参考的Fork/Join框架主要源代码也基于JDK1.8+。

这几篇文章将试图解释Fork/Join框架的知识点，以便对自己、对各位读者在并发程序的设计思路上进行一些启发。文章将首先讲解Fork/Join框架的基本使用，以及其中需要注意的使用要点；接着使用Fork/Join框架解决一些实际问题；最后再讲解Fork/Join框架的工作原理。

这里是一个简单的Fork/Join框架使用示例，在这个示例中我们计算了1-1001累加后的值

```java

/**
 * 这是一个简单的Join/Fork计算过程，将1—1001数字相加
 */
public class TestForkJoinPool {
 
    private static final Integer MAX = 200;
 
    static class MyForkJoinTask extends RecursiveTask<Integer> {
        // 子任务开始计算的值
        private Integer startValue;
 
        // 子任务结束计算的值
        private Integer endValue;
 
        public MyForkJoinTask(Integer startValue , Integer endValue) {
            this.startValue = startValue;
            this.endValue = endValue;
        }
 
        @Override
        protected Integer compute() {
            // 如果条件成立，说明这个任务所需要计算的数值分为足够小了
            // 可以正式进行累加计算了
            if(endValue - startValue < MAX) {
                System.out.println("开始计算的部分：startValue = " + startValue + ";endValue = " + endValue);
                Integer totalValue = 0;
                for(int index = this.startValue ; index <= this.endValue  ; index++) {
                    totalValue += index;
                }
                return totalValue;
            }
            // 否则再进行任务拆分，拆分成两个任务
            else {
                MyForkJoinTask subTask1 = new MyForkJoinTask(startValue, (startValue + endValue) / 2);
                subTask1.fork();
                MyForkJoinTask subTask2 = new MyForkJoinTask((startValue + endValue) / 2 + 1 , endValue);
                subTask2.fork();
                return subTask1.join() + subTask2.join();
            }
        }
    }
 
    public static void main(String[] args) {
        // 这是Fork/Join框架的线程池
        ForkJoinPool pool = new ForkJoinPool();
        ForkJoinTask<Integer> taskFuture =  pool.submit(new MyForkJoinTask(1,1001));
        try {
            Integer result = taskFuture.get();
            System.out.println("result = " + result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace(System.out);
        }
    }
}
```

以上代码很简单，在关键的位置有相关的注释说明。这里本文再对以上示例中的要点进行说明。首先看看以上示例代码的可能执行结果：

```text
开始计算的部分：startValue = 1;endValue = 126
开始计算的部分：startValue = 127;endValue = 251
开始计算的部分：startValue = 252;endValue = 376
开始计算的部分：startValue = 377;endValue = 501
开始计算的部分：startValue = 502;endValue = 626
开始计算的部分：startValue = 627;endValue = 751
开始计算的部分：startValue = 752;endValue = 876
开始计算的部分：startValue = 877;endValue = 1001
result = 501501
```

下图展示了以上代码的工作过程概要，但实际上Fork/Join框架的内部工作过程要比这张图复杂得多，例如如何决定某一个recursive task是使用哪条线程进行运行；再例如如何决定当一个任务/子任务提交到Fork/Join框架内部后，是创建一个新的线程去运行还是让它进行队列等待。

所以如果不深入理解Fork/Join框架的运行原理，只是根据之上最简单的使用例子观察运行效果，那么我们只能知道子任务在Fork/Join框架中被拆分得足够小后，并且其内部使用多线程并行完成这些小任务的计算后再进行结果向上的合并动作，最终形成顶层结果。不急，一步一步来，我们先从这张概要的过程图开始讨论。

![img](https://gitee.com/webrx/wx_note/raw/master/images/SouthEast.png)

之前所举的的例子是使用Fork/Join框架完成1-1000的整数累加。这个示例如果只是演示Fork/Join框架的使用，那还行，但这种例子和实际工作中所面对的问题还有一定差距。我们使用Fork/Join框架解决一个实际问题，就是高效排序的问题。

排序问题是我们工作中的常见问题。目前也有很多现成算法是为了解决这个问题而被发明的，例如多种插值排序算法、多种交换排序算法。而并归排序算法是目前所有排序算法中，平均时间复杂度较好（O(nlgn)），算法稳定性较好的一种排序算法。它的核心算法思路将大的问题分解成多个小问题，并将结果进行合并。

![img](https://gitee.com/webrx/wx_note/raw/master/images/SouthEast-16405729219042.png)



```java
package test.thread.pool.merge;
import java.util.Arrays;
import java.util.Random;
/**
 * 归并排序
 * @author yinwenjie
 */
public class Merge1 {
    private static int MAX = 10000;
    private static int inits[] = new int[MAX];
    // 这是为了生成一个数量为MAX的随机整数集合，准备计算数据
    // 和算法本身并没有什么关系
    static {
        Random r = new Random();
        for(int index = 1 ; index <= MAX ; index++) {
            inits[index - 1] = r.nextInt(10000000);
        }
    }
    public static void main(String[] args) {
        long beginTime = System.currentTimeMillis();
        int results[] = forkits(inits); 
        long endTime = System.currentTimeMillis();
        // 如果参与排序的数据非常庞大，记得把这种打印方式去掉
        System.out.println("耗时=" + (endTime - beginTime) + " | " + Arrays.toString(results));       
    }
 
    // 拆分成较小的元素或者进行足够小的元素集合的排序
    private static int[] forkits(int source[]) {
        int sourceLen = source.length;
        if(sourceLen > 2) {
            int midIndex = sourceLen / 2;
            int result1[] = forkits(Arrays.copyOf(source, midIndex));
            int result2[] = forkits(Arrays.copyOfRange(source, midIndex , sourceLen));
            // 将两个有序的数组，合并成一个有序的数组
            int mer[] = joinInts(result1 , result2);
            return mer;
        } 
        // 否则说明集合中只有一个或者两个元素，可以进行这两个元素的比较排序了
        else {
            // 如果条件成立，说明数组中只有一个元素，或者是数组中的元素都已经排列好位置了
            if(sourceLen == 1
                || source[0] <= source[1]) {
                return source;
            } else {
                int targetp[] = new int[sourceLen];
                targetp[0] = source[1];
                targetp[1] = source[0];
                return targetp;
            }
        }
    }
 
    /**
     * 这个方法用于合并两个有序集合
     * @param array1
     * @param array2
     */
    private static int[] joinInts(int array1[] , int array2[]) {
        int destInts[] = new int[array1.length + array2.length];
        int array1Len = array1.length;
        int array2Len = array2.length;
        int destLen = destInts.length;
 
        // 只需要以新的集合destInts的长度为标准，遍历一次即可
        for(int index = 0 , array1Index = 0 , array2Index = 0 ; index < destLen ; index++) {
            int value1 = array1Index >= array1Len?Integer.MAX_VALUE:array1[array1Index];
            int value2 = array2Index >= array2Len?Integer.MAX_VALUE:array2[array2Index];
            // 如果条件成立，说明应该取数组array1中的值
            if(value1 < value2) {
                array1Index++;
                destInts[index] = value1;
            }
            // 否则取数组array2中的值
            else {
                array2Index++;
                destInts[index] = value2;
            }
        }
        return destInts;
    }
}
```

### 11.9 阿里巴巴规范创建Java线程池

![img](https://gitee.com/webrx/wx_note/raw/master/images/1371272-20191231160605978-586888795.png)

 **1.newSingleThreadExecutor**

介绍：创建一个单线程的线程池。这个线程池只有一个线程在工作，也就是相当于单线程串行执行所有任务。如果这个唯一的线程因为异常结束，那么会有一个新的线程来替代它。
此线程池保证所有任务的执行顺序按照任务的提交顺序执行。

优点：单线程的线程池，保证线程的顺序执行

缺点：不适合并发

**2.newFixedThreadPool**

介绍：创建固定大小的线程池。每次提交一个任务就创建一个线程，直到线程达到线程池的最大大小。线程池的大小一旦达到最大值就会保持不变，如果某个线程因为执行异常而结束，那么线程池会补充一个新线程。

优点：固定大小线程池，超出的线程会在队列中等待

缺点：不支持自定义拒绝策略，大小固定，难以扩展

**3.newCachedThreadPool**

介绍：创建一个可缓存的线程池。如果线程池的大小超过了处理任务所需要的线程，那么就会回收部分空闲（60秒不执行任务）的线程，当任务数增加时，此线程池又可以智能的添加新线程来处理任务。此线程池不会对线程池大小做限制，线程池大小完全依赖于操作系统（或者说JVM）能够创建的最大线程大小。

优点：很灵活，弹性的线程池线程管理，用多少线程给多大的线程池，不用后及时回收，用则新建

缺点：一旦线程无限增长，会导致内存溢出

**4.newScheduledThreadPool**

介绍：创建一个定长线程池，支持定时及周期性任务执行

优点：一个固定大小线程池，可以定时或周期性的执行任务

缺点：任务是单线程方式执行，一旦一个任务失败其他任务也受影响

**总结**

1）以上线程池都不支持自定义拒绝策略。

2）newFixedThreadPool 和 newSingleThreadExecutor：主要问题是堆积的请求处理队列可能会耗费非常大的内存，甚至 OOM。

3）newCachedThreadPool 和 newScheduledThreadPool：主要问题是线程数最大数是 Integer.MAX_VALUE，可能会创建数量非常多的线程，甚至 OOM(Out Of Memory)。

```java
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue,
                          ThreadFactory threadFactory,
                          RejectedExecutionHandler handler);
```

**参数解释：**

　　corePoolSize ： 线程池核心池的大小。

　　maximumPoolSize ： 线程池的最大线程数。

　　keepAliveTime ： 当线程数大于核心时，此为终止前多余的空闲线程等待新任务的最长时间。

　　unit ： keepAliveTime 的时间单位。

　　workQueue ： 用来储存等待执行任务的队列。

　　threadFactory ： 线程工厂。

　　handler 拒绝策略。

　　**原理：**

　　有请求时，创建线程执行任务，当线程数量等于corePoolSize时，请求加入阻塞队列里，当队列满了时，接着创建线程，线程数等于maximumPoolSize。 当任务处理不过来的时候，线程池开始执行拒绝策略。

　　阻塞队列：

　　ArrayBlockingQueue ：一个由数组结构组成的有界阻塞队列。

　　LinkedBlockingQueue ：一个由链表结构组成的有界阻塞队列。

　　PriorityBlockingQueue ：一个支持优先级排序的无界阻塞队列。

　　DelayQueue： 一个使用优先级队列实现的无界阻塞队列。

　　SynchronousQueue： 一个不存储元素的阻塞队列。

　　LinkedTransferQueue： 一个由链表结构组成的无界阻塞队列。

　　LinkedBlockingDeque： 一个由链表结构组成的双向阻塞队列。

　　拒绝策略：

　　ThreadPoolExecutor.AbortPolicy: 丢弃任务并抛出RejectedExecutionException异常。 (默认)

　　ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。

　　ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务。（重复此过程）

　　ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务。

DEMO

```java
package com.xxx;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.util.concurrent.*;
/**
 * 线程池
 * @author xhq
 */
 public class ThreadPoolService {
    /**
     * 自定义线程名称,方便的出错的时候溯源
     */
     private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("test-pool-%d").build();

    /**
     * corePoolSize    线程池核心池的大小
     * maximumPoolSize 线程池中允许的最大线程数量
     * keepAliveTime   当线程数大于核心时，此为终止前多余的空闲线程等待新任务的最长时间
     * unit            keepAliveTime 的时间单位
     * workQueue       用来储存等待执行任务的队列
     * threadFactory   创建线程的工厂类
     * handler         拒绝策略类,当线程池数量达到上线并且workQueue队列长度达到上限时就需要对到来的任务做拒绝处理
      */
     private static ExecutorService service = new ThreadPoolExecutor(
            4,
            40,
            0L,
            TimeUnit.MILLISECONDS,
             new LinkedBlockingQueue<>(1024),
             namedThreadFactory,
             new ThreadPoolExecutor.AbortPolicy()
    );
 
    /**
      * 获取线程池
     * @return 线程池
      */
    public static ExecutorService getEs() {
         return service;
    }

    /**
      * 使用线程池创建线程并异步执行任务
      * @param r 任务
      */
    public static void newTask(Runnable r) {
         service.execute(r);
     }
}
```

### 作业：

1. 定时器操作（实现电脑定时关机）。

   ```java
   //指定时间输出一句  2021-08-24 09:01:30
   var t2 = new Thread(() -> {
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       Date d = null;
       try {
           d = sdf.parse("2021-08-24 09:03:00");
       } catch (ParseException e) {
           e.printStackTrace();
       }
       long end = d.getTime();
       try {
           while (true) {
               long now = System.currentTimeMillis();
               if (now >= end) {
                   System.out.println("时间到");
                   //调用关机程序 shutdown /s /t
                   //shutdown /a 取消关机进程
                   //Runtime run = Runtime.getRuntime();
                   //run.exec("cmd /k shutdown /s /t 0");
                   break;
               }
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
   });
   t2.start();
   ```

2. 每个月的月末(02:00:00) 执行一次代码

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   import java.text.SimpleDateFormat;
   import java.util.Calendar;
   import java.util.concurrent.TimeUnit;
   
   /**
    * <p>Project: javaseapp - Thread5
    * <p>Powered by webrx On 2021-08-24 14:50:14
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class Thread5 {
       public static void main(String[] args) {
           new Thread(() -> {
               while (true) {
                   try {
                       //TimeUnit.SECONDS.sleep(5);
                       //TimeUnit.DAYS.sleep(30);
   
                       var c = Calendar.getInstance();
                       //System.out.println(c.get(Calendar.MONTH) + 1);
                       c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
                       c.set(Calendar.DAY_OF_MONTH, 0);
                       c.set(Calendar.HOUR_OF_DAY, 2);
                       c.set(Calendar.MINUTE, 0);
                       c.set(Calendar.SECOND, 0);
                       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                       //System.out.println(sdf.format(c.getTime()));
                       if (c.getTimeInMillis() == System.currentTimeMillis()) {
                           //每月的月末，要执行的代码
                           System.out.println(sdf.format(c.getTime()));
                           System.out.printf("%1$tF %1$tT%n", System.currentTimeMillis());
                           //--------------每月的月末，要执行的代码
                           TimeUnit.SECONDS.sleep(2);
                       }
                       //System.out.printf("%1$tF %1$tT%n", System.currentTimeMillis());
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
               }
           }).start();
       }
   }
   
   ```

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   import java.util.Calendar;
   import java.util.concurrent.TimeUnit;
   
   /**
    * <p>Project: javaseapp - Thread6
    * <p>Powered by webrx On 2021-08-24 15:28:25
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class Thread6 {
       public static void main(String[] args) {
           new Thread(() -> {
               long start = System.currentTimeMillis();
               var c = Calendar.getInstance();
               c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
               c.set(Calendar.DAY_OF_MONTH, 0);
               c.set(Calendar.HOUR_OF_DAY, 2);
               c.set(Calendar.MINUTE, 0);
               c.set(Calendar.SECOND, 0);
               long end = c.getTimeInMillis();
               while (true) {
                   System.out.println("ok");
                   try {
                       TimeUnit.MILLISECONDS.sleep(end - start);
                       System.out.println("执行数据库备份命令！！！");
                       start = System.currentTimeMillis();
                       var cc = Calendar.getInstance();
                       cc.set(Calendar.MONTH, cc.get(Calendar.MONTH) + 1);
                       cc.set(Calendar.DAY_OF_MONTH, 0);
                       cc.set(Calendar.HOUR_OF_DAY, 2);
                       cc.set(Calendar.MINUTE, 0);
                       cc.set(Calendar.SECOND, 0);
                       end = cc.getTimeInMillis();
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
               }
           }).start();
       }
   }
   
   ```

   

3. 模拟售票。

4. 用五个线程实现，求123456789 之间放+-和100的表达式，如果一个线程求出结果，立即告诉其它停止。

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    */
   package cn.thread;
   import java.util.ArrayList;
   import java.util.List;
   import java.util.Random;
   import java.util.concurrent.atomic.AtomicBoolean;
   import java.util.regex.Matcher;
   import java.util.regex.Pattern;
   /**
    * <p>Project: part12 - T
    * <p>Powered By webrx
    * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
    * <p>Description :
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 15
    */
   public class T00 {
       volatile List<String> list = new ArrayList<>();
       void op() {
           System.out.printf("%s 启动计算中 %n", Thread.currentThread().getName());
           String[] o = new String[]{"", "+", "-"};
           Random rand = new Random();
           StringBuffer str = new StringBuffer("1");
           Pattern p = Pattern.compile("(\\d+|-\\d+)");
           while (list.size()!=11) {
               for (int i = 2; i < 10; i++)
                   str.append(String.format("%s%d", o[rand.nextInt(o.length)], i));
               String s = str.toString();
               Matcher m = p.matcher(s);
               List<Integer> ln = new ArrayList<>();
               while (m.find()) {
                   ln.add(Integer.parseInt(m.group()));
               }
               int sum = ln.stream().reduce(0, Integer::sum);
               if (sum == 100 && !list.contains(s)) {
                   list.add(s);
                   System.out.printf("[%s]:%s = 100%n", Thread.currentThread().getName(), s);
               } else {
                   str.delete(1, str.length());
               }
           }
           System.out.printf("%s 结束 %n", Thread.currentThread().getName());
       }
       public static void main(String[] args) {
           var t = new T00();
           for (int i = 0; i < 15; i++)
               new Thread(t::op, "T" + i).start();
       }
   }
   ```
   
5. 实现一个容器，提供两个方法，add，count 写两个线程，线程1添加10个元素到容器中，线程2实现监控元素的个数，当个数到5个时，线程2给出提示并结束。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * <p>Project: part12 - T1
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 08:57:40
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T7 {
    List<String> list = new ArrayList<>();
    CountDownLatch latch = new CountDownLatch(1);

    void add() {
        for (int i = 0; i < 10; i++) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String msg = String.format("%s-%d", "hello", i);
            list.add(msg);
            System.out.printf("%s : %s%n", Thread.currentThread().getName(), msg);
            if(list.size() ==5){
                latch.countDown();
            }

        }
    }

    void count() {
        try {
            latch.await();//等待 当latch为0时
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("%s 已经有5个元素%n", Thread.currentThread().getName());
}

    public static void main(String[] args) {
        var t = new T7();

        //添加元素
        new Thread(t::add, "T1").start();

        //查看元素个数
        new Thread(t::count, "T2").start();
    }
}

```

6. 编写程序模拟线程死锁

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: part12 - T
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T14 {
    final Object a = new Object();
    final Object b = new Object();

    void m1() {
        System.out.println(Thread.currentThread().getName() + "启动等待...");
        synchronized (a){
            try{
                TimeUnit.SECONDS.sleep(1);
            }catch(Exception e){
                e.printStackTrace();
            }
            synchronized (b){
            }
        }
    }
    void m2() {
        System.out.println(Thread.currentThread().getName() + "启动等待...");
        synchronized (b){
            try{
                TimeUnit.SECONDS.sleep(1);
            }catch(Exception e){
                e.printStackTrace();
            }
            synchronized (a){
            }
        }
    }
    public static void main(String[] args) {
        var t = new T14();
        new Thread(t::m1,"T1").start();
        new Thread(t::m2,"T2").start();
    }

}

```

![image-20210202115720016](https://gitee.com/webrx/wx_note/raw/master/images/image-20210202115720016.png)



```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.concurrent.TimeUnit;

/**
 * <p>Project: part12 - T
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-01 14:47:51
 * <p>Description : 模拟多线程死锁
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class T15 {
    public static void main(String[] args) {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
            synchronized(T15.class){
                try{
                    Thread.sleep(1000);
                }catch(Exception e){
                    e.printStackTrace();
                }
                synchronized (Object.class){

                }
            }
        },"T1").start();
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
            synchronized(Object.class){
                try{
                    Thread.sleep(1000);
                }catch(Exception e){
                    e.printStackTrace();
                }
                synchronized (T15.class){
                }
            }
        },"T2").start();

    }
}
```

7. 编写程序，实现三个线程，运行输出 A1 B2 C3 A4 B5 C6 …..

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.thread;
   
   import java.util.concurrent.TimeUnit;
   import java.util.concurrent.atomic.AtomicInteger;
   import java.util.concurrent.locks.ReentrantLock;
   
   /**
    * <p>Project: javaseapp - T4
    * <p>Powered by webrx On 2021-08-26 18:05:31
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   public class T4 {
       AtomicInteger num = new AtomicInteger(0);
       ReentrantLock lock = new ReentrantLock(true);
   
       void work() {
           String tn = Thread.currentThread().getName();
           while (true) {
               lock.lock();
               try {
                   TimeUnit.SECONDS.sleep(1);
               } catch (Exception e) {
                   e.printStackTrace();
               }
               System.out.printf("%s%d  ", tn, num.incrementAndGet());
               if ("C".equals(tn)) {
                   System.out.println();
               }
               lock.unlock();
           }
       }
   
       public static void main(String[] args) {
           var t = new T4();
           var a = new Thread(t::work, "A");
           a.start();
   
           var b = new Thread(t::work, "B");
           b.start();
   
           var c = new Thread(t::work, "C");
           c.start();
   
       }
   }
   
   ```

   ![image-20210826181513880](https://gitee.com/webrx/wx_note/raw/master/images/image-20210826181513880-16300526612715.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>Project: javaseapp - T8
 * <p>Powered by webrx On 2021-08-27 11:21:32
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T8 {
    AtomicInteger num = new AtomicInteger(0);

    synchronized void work() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String n = Thread.currentThread().getName();
        System.out.printf("%s%d ", n, num.incrementAndGet());
        if ("C".equals(n)) {
            System.out.println();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        var t = new T8();
        while (true) {
            var tt1 = new Thread(t::work, "A");
            tt1.start();
            tt1.join();
            var tt2 = new Thread(t::work, "B");
            tt2.start();
            tt2.join();
            var tt3 = new Thread(t::work, "C");
            tt3.start();
            tt3.join();
        }
    }
}

```



### 面试题：

```text
1、A线程正在执行一个对象中的同步方法，B线程是否可以同时执行同一个对象中的非同步方法？ `可以`
2、同上，B线程是否可以同时执行同一个对象中的另一个同步方法？不可以
3、线程抛出异常会释放锁吗？会的
4、volatile和synchronized区别？
5、写一个程序，证明AtomXXX类比synchronized更高效
6、AtomXXX类可以保证可见性吗？请写一个程序来证明  可以
7、写一个程序证明AtomXXX类的多个方法并不构成原子性
8、写一个程序模拟死锁
9、写一个程序，在main线程中启动100个线程，100个线程完成后，主线程打印“完成”，使用join()和countdownlatch都可以完成，请比较异同，循环栅栏。
答案参考：https://blog.csdn.net/cckevincyh/article/details/103845640
```

![image-20210827115850017](https://gitee.com/webrx/wx_note/raw/master/images/image-20210827115850017.png)![image-20210827144741930](https://gitee.com/webrx/wx_note/raw/master/images/image-20210827144741930.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 写一个程序，证明Atomxxx类比synchronized更高效
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T11 {
    AtomicInteger atomicCount = new AtomicInteger(0);
    int count = 0;

    void m() {
        for (int i = 0; i < 1000000; i++) {
            atomicCount.incrementAndGet(); //原子操作
        }
    }

    void m2() {
        for (int i = 0; i < 1000000; i++) {
            synchronized (this) {
                count++;
            }
        }
    }

    public static void main(String[] args) {
        var t1 = new T11();
        var t2 = new T11();
        long time1 = time(t1::m);
        System.out.println(t1.atomicCount);
        long time2 = time(t2::m2);
        System.out.println(t2.count);

        System.out.println(time1);
        System.out.print("synchronized:");
        System.out.println(time2);
    }

    private static long time(Runnable runnable) {
        List<Thread> threads = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            threads.add(new Thread(runnable, "thread-" + i));
        }
        threads.forEach(Thread::start);
        threads.forEach(o -> {
            try {
                o.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}

```

* 不要以字符串常量作为锁的对象 `synchronized(“hello”){}`

  ```java
  package demo19;
  
  /**
   * 不要以字符串常量作为锁定对象
   * 在下面m1 m2 其实锁定的是同一个对象
   * 这种情况下还会发生比较诡异的现象,比如你用到了一个类库,在该类库中的代码锁定了"Hello",
   * 但是你读不到源码,所以你在自己的代码中锁定了"Hello",这时候有可能发生非常诡异的死锁阻塞,
   * 因为你的程序和你用到的类库不经意间使用了同一把锁.
   */
  public class T {
  
      String s1 = "Hello";
      String s2 = "Hello";
  
      void m1() {
          synchronized (s1) {
              while (true) {
                  System.out.println("m1");
              }
  
          }
      }
  
      void m2() {
          synchronized (s2) {
              while (true) {
                  System.out.println("m2");
              }
          }
      }
  
      public static void main(String[] args) {
          T t = new T();
          new Thread(t::m1).start();
          new Thread(t::m2).start();
      }
  }
  ```

* 原子操作类的多个方法调用并不构成原子性

  ```java
  package demo16;
  
  import java.util.ArrayList;
  import java.util.List;
  import java.util.concurrent.atomic.AtomicInteger;
  
  /**
   * 写一个程序证明AtomXXX类的多个方法并不构成原子性
   */
  public class T {
      AtomicInteger count = new AtomicInteger(0);
  
      void m() {
          for (int i = 0; i < 10000; i++) {
              if (count.get() < 100 && count.get() >= 0) { //如果未加锁,之间还会有其他线程插进来
                  count.incrementAndGet();
              }
          }
      }
  
      public static void main(String[] args) {
          T t = new T();
          List<Thread> threads = new ArrayList<>();
          for (int i = 0; i < 10; i++) {
              threads.add(new Thread(t::m, "thread" + i));
          }
          threads.forEach(Thread::start);
          threads.forEach((o) -> {
              try {
                  //join()方法阻塞调用此方法的线程,直到线程t完成，此线程再继续。通常用于在main()主线程内，等待其它线程完成再结束main()主线程。
                  o.join(); //相当于在main线程中同步o线程，o执行完了，main线程才有执行的机会
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          });
          System.out.println(t.count);
      }
  }
  ```
  
  
  
* 写一个程序，在main线程中启动100个线程，100个线程完成后，主线程打印“完成”

  ```java
  /*
   * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
   *
   */
  package cn.thread;
  
  import java.util.concurrent.CountDownLatch;
  
  /**
   * 写一个程序，在main线程中启动100个线程，100个线程完成后，主线程打印“完成”
   *
   * @author webrx [webrx@126.com]
   * @version 1.0
   * @since 16
   */
  public class T12 {
      public static void main(String[] args) {
          CountDownLatch latch = new CountDownLatch(100);
          for (int i = 0; i < 100; i++) {
              new Thread(() -> {
                  String tn = Thread.currentThread().getName();
                  System.out.printf("%s : 开始执行...%n", tn);
                  System.out.printf("%s : 执行完成，程序结束。%n", tn);
                  latch.countDown();
              }, "T" + i).start();
          }
  
          try {
              latch.await();
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          System.out.println("---------------------------------------");
          System.out.println("100个线程执行完了。");
          String tn = Thread.currentThread().getName();
          System.out.printf("%s : 执行完成，程序结束。%n", tn);
      }
  }
  ```
  

## 第十二章 注解反射

### 12.1 注解(Annotation)

>Java 注解（Annotation）又称 Java 标注，是 JDK5.0 引入的一种注释机制。
>Java 语言中的类、方法、变量、参数和包等都可以被标注。和 Javadoc 不同，Java 标注可以通过反射获取标注内容。在编译器生成类文件时，标注可以被嵌入到字节码中。Java 虚拟机可以保留标注内容，在运行时可以获取到标注内容 。 当然它也支持自定义 Java 标注。
>
>内置注解：@Override @Deprecated @SuppressWarnings @FunctionalInterface @SafeVarargs
>
>元注解：注解的注解，用来定义注解类时使用的注解，
>
>@Retention
>		@Documented
>		@Target
>		@Inherited
>
>@Repeatable - Java 8
>

#### 内置注解

Java 定义了一套注解，共有 7 个，3 个在 java.lang 中，剩下 4 个在 java.lang.annotation 中。

作用在代码的注解是

@Override - 检查该方法是否是重写方法。如果发现其父类，或者是引用的接口中并没有该方法时，会报编译错误。
		@Deprecated - 标记过时方法。如果使用该方法，会报编译警告。
		@SuppressWarnings - 指示编译器去忽略注解中声明的警告。
作用在其他注解的注解(或者说 元注解)是:

| 关键字                   | 用途                                                         |
| ------------------------ | ------------------------------------------------------------ |
| all                      | to suppress all warnings（抑制所有警告）                     |
| boxing                   | to suppress warnings relative to boxing/unboxing operations（要抑制与箱/非装箱操作相关的警告） |
| cast                     | to suppress warnings relative to cast operations（为了抑制与强制转换操作相关的警告） |
| dep-ann                  | to suppress warnings relative to deprecated annotation（要抑制相对于弃用注释的警告） |
| deprecation              | to suppress warnings relative to deprecation（要抑制相对于弃用的警告） |
| fallthrough              | to suppress warnings relative to missing breaks in switch statements（在switch语句中，抑制与缺失中断相关的警告） |
| finally                  | to suppress warnings relative to finally block that don’t return（为了抑制警告，相对于最终阻止不返回的警告） |
| hiding                   | to suppress warnings relative to locals that hide variable（为了抑制本地隐藏变量的警告） |
| incomplete-switch        | to suppress warnings relative to missing entries in a switch statement (enum case)（为了在switch语句（enum案例）中抑制相对于缺失条目的警告） |
| nls                      | to suppress warnings relative to non-nls string literals（要抑制相对于非nls字符串字面量的警告） |
| null                     | to suppress warnings relative to null analysis（为了抑制与null分析相关的警告） |
| rawtypes                 | to suppress warnings relative to un-specific types when using generics on class params（在类params上使用泛型时，要抑制相对于非特异性类型的警告） |
| restriction              | to suppress warnings relative to usage of discouraged or forbidden references（禁止使用警告或禁止引用的警告） |
| serial                   | to suppress warnings relative to missing serialVersionUID field for a serializable class（为了一个可串行化的类，为了抑制相对于缺失的serialVersionUID字段的警告） |
| static-access            | o suppress warnings relative to incorrect static access（o抑制与不正确的静态访问相关的警告） |
| synthetic-access         | to suppress warnings relative to unoptimized access from inner classes（相对于内部类的未优化访问，来抑制警告） |
| unchecked                | to suppress warnings relative to unchecked operations（相对于不受约束的操作，抑制警告） |
| unqualified-field-access | to suppress warnings relative to field access unqualified（为了抑制与现场访问相关的警告） |
| unused                   | to suppress warnings relative to unused code（抑制没有使用过代码的警告） |

A.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - A
 * <p>Powered by webrx On 2021-08-27 17:43:42
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class A {
    void show() {

    }
}

```



B.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Project: javaseapp - B
 * <p>Powered by webrx On 2021-08-27 17:43:56
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
//@SuppressWarnings("")
@SuppressWarnings(value = {"unused", "rawtypes", "unchecked"})
public class B extends A {
    //unused
    int age = 18;

    //rawtypes
    List list = new ArrayList();

    @Override
    public void show() {
        //unchecked
        list.add("hello");
    }

    @Deprecated
    public void print() {
    }
}

```



从 Java 7 开始，额外添加了 3 个注解:

@SafeVarargs - Java 7 开始支持，忽略任何使用参数为泛型变量的方法或构造函数调用产生的警告。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.util.Arrays;
import java.util.List;

/**
 * <p>Project: javaseapp - D
 * <p>Powered by webrx On 2021-08-30 08:42:55
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class D {
    public static void main(String[] args) {
        List<String> list1 = Arrays.asList("one", "two");
        List<String> list2 = Arrays.asList("three", "four");
        show(list1, list2);
    }

    @SafeVarargs // 其实并不安全！
    static void show(List<String>... stringLists) {
        Object[] array = stringLists;
        List<Integer> tmpList = Arrays.asList(42, 56);
        array[0] = tmpList; // tmpList是一个List对象（类型已经擦除），赋值给Object类型的对象是允许的（向上塑型），能够编译通过
        String s = stringLists[0].get(0); // 运行时抛出ClassCastException！
    }
}
```

@FunctionalInterface - Java 8 开始支持，标识一个匿名函数或函数式接口。

```java
@FunctionalInterface
public interface C {
    void pf();
}
```

#### 元注解

@Retention - 标识这个注解怎么保存，是只在代码中，还是编入class文件中，或者是在运行时可以通过反射访问。
		@Documented - 标记这些注解是否包含在用户文档中。
		@Target - 标记这个注解应该是哪种 Java 成员。
		@Inherited - 标记这个注解是继承于哪个注解类(默认 注解并没有继承于任何子类)

@Repeatable - Java 8 开始支持，标识某注解可以在同一个声明上使用多次。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.ex;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * <p>Project: javase - AnnotationTest
 * <p>Powered by webrx On 2021-12-07 18:27:25
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
@Ann(name = "李四", value = 18, sex = "男")
public class AnnotationTest {
    @Ann(name = "李强", value = 88)
    public static void main(String[] args) throws NoSuchMethodException {
        var me = AnnotationTest.class;
        var m = me.getMethod("main", String[].class).getAnnotation(Ann.class);
        System.out.println(m);
        System.out.println(m.sex());
        System.out.println(m.name());
        System.out.println(m.value());
        if (me.isAnnotationPresent(Ann.class)) {
            var an = (Ann) me.getAnnotation(Ann.class);
            System.out.println(an.sex());
            System.out.println(an.value());
            System.out.println(an.name());
        }
    }
}

@Retention(RUNTIME)
@Target({TYPE, METHOD})
@interface Ann {
    String name();

    int value();

    String sex() default "女";
}
```

#### Annotation 架构



![img](https://gitee.com/webrx/wx_note/raw/master/images/28123151-d471f82eb2bc4812b46cc5ff3e9e6b82.jpg)

```text
@Deprecated  -- @Deprecated 所标注内容，不再被建议使用。
@Override    -- @Override 只能标注方法，表示该方法覆盖父类中的方法。
@Documented  -- @Documented 所标注内容，可以出现在javadoc中。
@Inherited   -- @Inherited只能被用来标注“Annotation类型”，它所标注的Annotation具有继承性。
@Retention   -- @Retention只能被用来标注“Annotation类型”，而且它被用来指定Annotation的RetentionPolicy属性。
@Target      -- @Target只能被用来标注“Annotation类型”，而且它被用来指定Annotation的ElementType属性。
@SuppressWarnings -- @SuppressWarnings 所标注内容产生的警告，编译器会对这些警告保持静默。
```

#### Annotation 组成部分(interface,enum,enum)

```java
package java.lang.annotation;
public interface Annotation {
    boolean equals(Object obj);
    int hashCode();
    String toString();
    Class<? extends Annotation> annotationType();
}
```

```java
package java.lang.annotation;
public enum ElementType {
    TYPE,               /* 类、接口（包括注释类型）或枚举声明  */
    FIELD,              /* 字段声明（包括枚举常量）  */
    METHOD,             /* 方法声明  */
    PARAMETER,          /* 参数声明  */
    CONSTRUCTOR,        /* 构造方法声明  */
    LOCAL_VARIABLE,     /* 局部变量声明  */
    ANNOTATION_TYPE,    /* 注释类型声明  */
    PACKAGE             /* 包声明  */
}
```

```java
package java.lang.annotation;
public enum RetentionPolicy {
    SOURCE,            /* Annotation信息仅存在于编译器处理期间，编译器处理完之后就没有该Annotation信息了  */
    CLASS,             /* 编译器将Annotation存储于类对应的.class文件中。默认行为  */
    RUNTIME            /* 编译器将Annotation存储于class文件中，并且可由JVM读入 */
}
```

```text

说明：
(01) Annotation 就是个接口。

"每 1 个 Annotation" 都与 "1 个 RetentionPolicy" 关联，并且与 "1～n 个 ElementType" 关联。可以通俗的理解为：每 1 个 Annotation 对象，都会有唯一的 RetentionPolicy 属性；至于 ElementType 属性，则有 1~n 个。

(02) ElementType 是 Enum 枚举类型，它用来指定 Annotation 的类型。

"每 1 个 Annotation" 都与 "1～n 个 ElementType" 关联。当 Annotation 与某个 ElementType 关联时，就意味着：Annotation有了某种用途。例如，若一个 Annotation 对象是 METHOD 类型，则该 Annotation 只能用来修饰方法。

(03) RetentionPolicy 是 Enum 枚举类型，它用来指定 Annotation 的策略。通俗点说，就是不同 RetentionPolicy 类型的 Annotation 的作用域不同。

"每 1 个 Annotation" 都与 "1 个 RetentionPolicy" 关联。

a) 若 Annotation 的类型为 SOURCE，则意味着：Annotation 仅存在于编译器处理期间，编译器处理完之后，该 Annotation 就没用了。 例如，" @Override" 标志就是一个 Annotation。当它修饰一个方法的时候，就意味着该方法覆盖父类的方法；并且在编译期间会进行语法检查！编译器处理完后，"@Override" 就没有任何作用了。
b) 若 Annotation 的类型为 CLASS，则意味着：编译器将 Annotation 存储于类对应的 .class 文件中，它是 Annotation 的默认行为。
c) 若 Annotation 的类型为 RUNTIME，则意味着：编译器将 Annotation 存储于 class 文件中，并且可由JVM读入。
这时，只需要记住"每 1 个 Annotation" 都与 "1 个 RetentionPolicy" 关联，并且与 "1～n 个 ElementType" 关联。学完后面的内容之后，再回头看这些内容，会更容易理解。
```

#### java 自带的 Annotation

![img](https://gitee.com/webrx/wx_note/raw/master/images/28124653-adf73c4cdcce4a63b7bf78efbe1a9cdf.jpg)



```text
@Deprecated  -- @Deprecated 所标注内容，不再被建议使用。
@Override    -- @Override 只能标注方法，表示该方法覆盖父类中的方法。
@Documented  -- @Documented 所标注内容，可以出现在javadoc中。
@Inherited   -- @Inherited只能被用来标注“Annotation类型”，它所标注的Annotation具有继承性。
@Retention   -- @Retention只能被用来标注“Annotation类型”，而且它被用来指定Annotation的RetentionPolicy属性。
@Target      -- @Target只能被用来标注“Annotation类型”，而且它被用来指定Annotation的ElementType属性。
@SuppressWarnings -- @SuppressWarnings 所标注内容产生的警告，编译器会对这些警告保持静默。
```

#### Annotation 的作用

Annotation 是一个辅助类，它在 Junit、Struts、Spring 等工具框架中被广泛使用。

我们在编程中经常会使用到的 Annotation 作用有：

Annotation 具有 让编译器进行编译检查的作用

例如，@SuppressWarnings, @Deprecated 和 @Override 都具有编译检查作用。

(01) 关于 @SuppressWarnings 和 @Deprecated。

(02) 若某个方法被 @Override 的标注，则意味着该方法会覆盖父类中的同名方法。如果有方法被 @Override 标示，但父类中却没有"被 @Override 标注"的同名方法，则编译器会报错。

![image-20210422084719181](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422084719181.png)

#### @Target

![image-20210422084741170](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422084741170.png)



案例

```java
@Target(ElementType.TYPE)
public @interface Table {
    /**
     * 数据表名称注解，默认值为类名称
     * @return
     */
    public String tableName() default "className";
}

@Target(ElementType.FIELD)
public @interface NoDBColumn {

}
//注解Table 可以用于注解类、接口(包括注解类型) 或enum声明,而注解NoDBColumn仅可用于注解类的成员变量。
```

#### @Retention

![image-20210422084939034](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422084939034.png)

```java
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    public String name() default "fieldName";
    public String setFuncName() default "setField";
    public String getFuncName() default "getField"; 
    public boolean defaultDBValue() default false;
}
//Column注解的的RetentionPolicy的属性值是RUTIME,这样注解处理器可以通过反射，获取到该注解的属性值，从而去做一些运行时的逻辑处理
```

#### @Documented

![image-20210422085043841](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422085043841.png)

```java
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {
    public String name() default "fieldName";
    public String setFuncName() default "setField";
    public String getFuncName() default "getField"; 
    public boolean defaultDBValue() default false;
}
```

#### @Inherited

![image-20210422085124182](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422085124182.png)

```java
/**
 * 
 * @author peida
 *
 */
@Inherited
public @interface Greeting {
    public enum FontColor{ BULE,RED,GREEN};
    String name();
    FontColor fontColor() default FontColor.GREEN;
}

```

**自定义注解：**

　　使用@interface自定义注解时，自动继承了java.lang.annotation.Annotation接口，由编译程序自动完成其他细节。在定义注解时，不能继承其他的注解或接口。@interface用来声明一个注解，其中的每一个方法实际上是声明了一个配置参数。方法的名称就是参数的名称，返回值类型就是参数的类型（返回值类型只能是基本类型、Class、String、enum）。可以通过default来声明参数的默认值。

　　**定义注解格式：**
　　public @interface 注解名 {定义体}

　　**注解参数的可支持数据类型：**

　　1.所有基本数据类型（int,float,boolean,byte,double,char,long,short)
　　　　2.String类型
　　　　3.Class类型
　　　　4.enum类型
　　　　5.Annotation类型
　　　　6.以上所有类型的数组

　　Annotation类型里面的参数该怎么设定:
　　第一,只能用public或默认(default)这两个访问权修饰.例如,String value();这里把方法设为defaul默认类型；　 　
　　第二,参数成员只能用基本类型byte,short,char,int,long,float,double,boolean八种基本数据类型和 String,Enum,Class,annotations等数据类型,以及这一些类型的数组.例如,String value();这里的参数成员就为String;　　
　　第三,如果只有一个参数成员,最好把参数名称设为"value",后加小括号.例:下面的例子FruitName注解就只有一个参数成员。



(1)

```java
package annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 水果名称注解
 * @author peida
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FruitName {
    String value() default "";
}
```

(2)

```java
package annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 水果颜色注解
 * @author peida
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FruitColor {
    /**
     * 颜色枚举
     * @author peida
     *
     */
    public enum Color{ BULE,RED,GREEN};
    
    /**
     * 颜色属性
     * @return
     */
    Color fruitColor() default Color.GREEN;

}
```

(3)

```java
package annotation;

import annotation.FruitColor.Color;

public class Apple {
    
    @FruitName("Apple")
    private String appleName;
    
    @FruitColor(fruitColor=Color.RED)
    private String appleColor;
    
    
    
    
    public void setAppleColor(String appleColor) {
        this.appleColor = appleColor;
    }
    public String getAppleColor() {
        return appleColor;
    }
    
    
    public void setAppleName(String appleName) {
        this.appleName = appleName;
    }
    public String getAppleName() {
        return appleName;
    }
    
    public void displayName(){
        System.out.println("水果的名字是：苹果");
    }
}
```

![image-20210422085318640](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422085318640.png)

```java
 package annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 水果供应者注解
 * @author peida
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FruitProvider {
    /**
     * 供应商编号
     * @return
     */
    public int id() default -1;

    /**
     * 供应商名称
     * @return
     */
    public String name() default "";

    /**
     * 供应商地址
     * @return
     */
    public String address() default "";
}
/*
定义了注解，并在需要的时候给相关类，类属性加上注解信息，如果没有响应的注解信息处理流程，注解可以说是没有实用价值。如何让注解真真的发挥作用，主要就在于注解处理方法*/
```



```text
User.java

enum User{
    LISI,JACK,MARY,ANDY,WANGWU
}

javac User.java 
User.class

class java
interface java
enum java
annotation java
module-info.java

class<T> User{
    public void save(T t){
	
    }
}

User<Book> u = new User<Book>();
u.save(new Book());

User<Teacher> u = new User<>();
u.save(new Teacher());


List<User>

泛型

枚举

注解  
comment 
annotation 
@Data

public enum RetentionPolicy {
    SOURCE,CLASS,RUNTIME
}

package cn.org;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Target;
/**
 * <p>Project: part13 - Webrx
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-03 10:12:22
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@Documented
@Target({TYPE,FIELD,METHOD})
@Retention(RUNTIME) //RUNTIME > CLASS > SOURCE
public @interface Webrx {
    public String font() default "";
    public int age() default 25;
    public String[] values() default {"java","html","mysql"};
    public int pos() default 5;
}
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 */
package cn.org;
@Webrx(age=35,pos=9)
public class MyUtil {
    public void showAge(){
        //反射
        int a = this.getClass().getAnnotation(Webrx.class).age();
        System.out.println(a);
    }

    public static void main(String[] args) {
        MyUtil mu = new MyUtil();
        mu.showAge();
    }
}
```

#### 定义注解案例

（1）AnimalName.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;

import java.lang.annotation.*;

/**
 * <p>Project: demo - AnimalName
 * <p>Powered by webrx On 2021-04-22 09:59:40
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AnimalName {
    String value() default "米米";
}



```

 (2) AnimalColor.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
//静态导入
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>Project: demo - AnimalColor
 * <p>Powered by webrx On 2021-04-22 14:05:07
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@Target(FIELD)
@Retention(RUNTIME)
@Documented
public @interface AnimalColor {
    enum Color {BLACK, WHITE, YELLOW, RED, GREEN, BLUE};
    Color animalColor() default Color.BLACK;
}

```

(3) AnimalMaster.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>Project: demo - AnimalMaster
 * <p>Powered by webrx On 2021-04-22 14:15:58
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@Documented
@Target({TYPE, FIELD})   //TYPE 代表类上 FIELD属性上
@Retention(RUNTIME)
public @interface AnimalMaster {
    int id() default 10;
    String name() default "李四";
    String address() default "郑州市";
}

```

(4) Master.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;

/**
 * <p>Project: demo - Master
 * <p>Powered by webrx On 2021-04-22 14:15:07
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@AnimalMaster
public class Master {
    private int id;
    private String name;
    private String address;
}
```

(5) Animal.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;

/**
 * <p>Project: demo - Animal
 * <p>Powered by webrx On 2021-04-22 10:32:17
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */

public class Animal {
    @AnimalName("mimi")
    private String name;
    //Color枚举为AnimalColor类的内部成员（内部类）
    @AnimalColor(animalColor = AnimalColor.Color.GREEN)
    private AnimalColor.Color animalColor;


    @AnimalMaster(name = "jack", address = "USA", id = 300)
    private Master master;
}

```



(5) Demo.java

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.anno;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>Project: demo - Demo
 * <p>Powered by webrx On 2021-04-22 09:25:49
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@SuppressWarnings("all")
public class Demo {

    public static void main(String[] args) {
        //Arrays.stream(Animal.class.getDeclaredFields()).forEach(System.out::println);
        Class an = Animal.class;
        Field[] fs = an.getDeclaredFields();
        for(Field f : fs){
            //System.out.println(f.getAnnotation(AnimalName.class));
            //System.out.println(f.isAnnotationPresent(AnimalName.class));
            if(f.isAnnotationPresent(AnimalName.class)){
                System.out.println(f.getAnnotation(AnimalName.class).value());
            }

            if(f.isAnnotationPresent(AnimalColor.class)){
                System.out.println(f.getAnnotation(AnimalColor.class).animalColor());
            }

            if(f.isAnnotationPresent(AnimalMaster.class)){
                var cc = f.getAnnotation(AnimalMaster.class);
                System.out.println(cc.id());
                System.out.println(cc.name());
                System.out.println(cc.address());
            }else{
                Class cl = f.getType();
                if(cl == Master.class){
                    var mm = Master.class;
                    if(mm.isAnnotationPresent(AnimalMaster.class)) {
                        System.out.println(mm.getAnnotation(AnimalMaster.class).id());
                        System.out.println(mm.getAnnotation(AnimalMaster.class).name());
                        System.out.println(mm.getAnnotation(AnimalMaster.class).address());
                    }
                }
            }

        }

        //单独读取Master.java 类注解信息
        Class<?> m = Master.class;
        if(m.isAnnotationPresent(AnimalMaster.class)) {
            System.out.println(m.getAnnotation(AnimalMaster.class).id());
            System.out.println(m.getAnnotation(AnimalMaster.class).name());
            System.out.println(m.getAnnotation(AnimalMaster.class).address());
        }


        /*
        Arrays.stream(Animal.class.getDeclaredFields()).forEach(e->{
            System.out.println(e.isAnnotationPresent(AnimalName.class));
            System.out.println(e.getAnnotation(AnimalName.class));
        });

         */
    }

}
```

![image-20210422145820539](https://gitee.com/webrx/wx_note/raw/master/images/image-20210422145820539.png)

#### @Repeatable

> 被元注解@Repeatable修饰的注解，可以在同一个地方使用多次。这是JAVA8加入的新特性，在此之前注解在同一个地方只能使用一次（实际上有解决方案，但可读性不好）。
>
> @Repeatable的使用有以下几个要点：
> 在需要重复使用的注解上修饰 @Repeatable
> @Repeatable中的参数为被修饰注解的容器的类对象（class对象）
> 容器包含一个value方法，返回一个被修饰注解的数组
> 这样就完成了一个可重复使用的注解的定义，然后在对应类、方法、属性上多次使用该注解，并通过反射的方法获取注解数组，根据需要获取数据。



```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>Project: javaseapp - MyDemo
 * <p>Powered by webrx On 2021-08-30 10:08:21
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class MyDemo {
    @Target(METHOD)
    @Retention(RUNTIME)
    public @interface FileTypes {
        FileType[] value();
    }

    @Target(METHOD)
    @Retention(RUNTIME)
    @Repeatable(FileTypes.class)
    public @interface FileType {
        String value() default ".class";
    }

    @FileType(".java")
    @FileType(".html")
    @FileType(".css")
    @FileType(".js")
    @FileType
    public void work() {

        try {
            FileType[] fileTypes = this.getClass().getMethod("work").getAnnotationsByType(FileType.class);
            System.out.println("将从如下后缀名的文件中查找文件内容");
            for (FileType fileType : fileTypes) {
                System.out.println(fileType.value());
            }
            System.out.println("查找过程略。。。");
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MyDemo().work();
    }
}

```

####   总结（注释程序人员查看源码时看，注解 jvm查看信息）

1. 如果注解难于理解，你就把它类同于标签，标签为了解释事物，注解为了解释代码。
2. 注解的基本语法，创建如同接口，但是多了个 @ 符号。 public @interface An{}
3. 注解的元注解。
4. 注解的属性 String value();  String name() default “james”。
5. 注解主要给编译器及工具类型(框架)的软件用的。
6. 注解的提取需要借助于 Java 的反射技术，反射比较慢，所以注解使用时也需要谨慎计较时间成本。

### 12.2 反射（Reflection）

Java的反射（reflection）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为动态语言的关键技术。

```text

Java 反射，就是在运行状态中。

获取任意类的名称、package信息、所有属性、方法、注解、类型、类加载器等
获取任意对象的属性，并且能改变对象的属性
调用任意对象的方法
判断任意一个对象所属的类
实例化任意一个类的对象
Java 的动态就体现在这。通过反射我们可以实现动态装配，降低代码的耦合度；动态代理等。反射的过度使用会严重消耗系统资源。

JDK 中 java.lang.Class 类，就是为了实现反射提供的核心类之一。
```

#### 12.2.1  反射基本概念



> 正射

```java
Apple apple = new Apple(); //直接初始化，「正射」
apple.setPrice(4);
```



> 反射

```java
Class clz = Class.forName("cn.webrx.Apple");
Method method = clz.getMethod("setPrice", int.class);
Constructor constructor = clz.getConstructor();
Object object = constructor.newInstance(); //相当于 new Apple();
method.invoke(object, 4);//相当于 apple.setPrice(4);
```

***反射就是在运行时才知道要操作的类是什么，并且可以在运行时获取类的完整构造，并调用对应的方法。***



![image-20210203115906905](https://gitee.com/webrx/wx_note/raw/master/images/image-20210203115906905.png)



![image-20210203115629441](https://gitee.com/webrx/wx_note/raw/master/images/image-20210203115629441.png)



```java
public class Apple {
    private int price;
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public static void main(String[] args) throws Exception{
        //正常的调用
        Apple apple = new Apple();
        apple.setPrice(5);
        System.out.println("Apple Price:" + apple.getPrice());
        //使用反射调用
        Class clz = Class.forName("cn.webrx.Apple");
        Method setPriceMethod = clz.getMethod("setPrice", int.class);
        Constructor appleConstructor = clz.getConstructor();
        Object appleObj = appleConstructor.newInstance();
        setPriceMethod.invoke(appleObj, 14);
        Method getPriceMethod = clz.getMethod("getPrice");
        System.out.println("Apple Price:" + getPriceMethod.invoke(appleObj));
    }
}
```



```text
反射
Reflection
	User user = new User("b");
  2010 java java反射机制，打印出java.lang.String 的所有方法
        Class<String> s = String.class;
        Class a = new String().getClass();
        Class c = Class.forName("java.lang.String");
        //Class
        //Method
        //Field
        System.out.println(a==c);
        //Arrays.stream(s.getDeclaredMethods()).forEach(e->System.out.println(e.getName()));
        Arrays.stream(s.getMethods()).forEach(e->System.out.println(e.getName()));

```

![image-20210203115850531](https://gitee.com/webrx/wx_note/raw/master/images/image-20210203115850531.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.reflection;

/**
 * <p>Project: javaseapp - Ref4
 * <p>Powered by webrx On 2021-08-30 17:23:01
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Ref4 {
    public static void main(String[] args) {
        var c1 = Student.class;
        System.out.println(c1.isRecord());//true

        //java.lang.String
        //java.lang 包下的所有对象，不需要导入，可以在任何java程序中直接使用
        

        // java 14 推出 record 类型 父类 java.lang.Record
        System.out.println(c1.getSuperclass().getName());

        var c2 = Color.class;
        System.out.println(c2.isEnum());//true
        //java.lang.Enum
        System.out.println(c2.getSuperclass().getName());

        var c3 = int[][].class;
        var c4 = String[].class;
        var c5 = User[].class;
        System.out.println(c3.isArray());//true
        System.out.println(c4.isArray());//true
        System.out.println(c5.isArray());//true

        //java.lang.Object
        System.out.println(c3.getSuperclass().getName());
    }
}

```



![image-20210203115935143](https://gitee.com/webrx/wx_note/raw/master/images/image-20210203115935143.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.reflect;

import cn.org.Webrx;

/**
 * <p>Project: part13 - Book
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-03 15:20:52
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
@Webrx
public class Book {
    private int id;
    private String name;
    private double price;
    int i = 50;
    protected String user = "lisi";
    public String addr = "郑州市";

    public void info(){
        System.out.println("hello world info()...");
    }

    public Book() {
    }

    public Book(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * <p>Project: part13 - ReflectTest
 * <p>Powered By webrx
 * <p>Created By IntelliJ IDEA On 2021-02-03 15:20:46
 * <p>Description :
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 15
 */
public class ReflectTest {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {

        System.out.println(Class.forName("java.sql.Connection").isInterface());

        System.out.println(Integer.class.isPrimitive());

        //正常方式
        //var book = new Book();

        //反射方式 Class<String>  Class<?>
        var c1 = Book.class;
        String c = "cn.webrx.reflect.Book";

        var c2 = Class.forName(c);
        //System.out.println(c2.getName());//cn.webrx.reflect.Book
        //System.out.println(c2.getSimpleName());//Book

        //Method info = c2.getMethod("info");
        //System.out.println(info.invoke(c2.getConstructor().newInstance()));

        Arrays.stream(c2.getDeclaredFields()).forEach(e->{
            System.out.println(e.getName());
        });

        Field fi = c2.getDeclaredField("i");
        System.out.println(fi.getType());

        System.out.println(fi.get(c2.getConstructor().newInstance()));

        //0 1 2 3 4
        System.out.println(c2.getDeclaredField("id").getModifiers());
        System.out.println(c2.getDeclaredField("i").getModifiers());
        System.out.println(c2.getDeclaredField("user").getModifiers());
        System.out.println(c2.getDeclaredField("addr").getModifiers());

        //var c3 = book.getClass();
        //System.out.println(c1.hashCode());
        //System.out.println(c2.hashCode());
        //System.out.println(c3.hashCode());

        //通过反射来实现对象
        //Book bk = (Book)c2.getDeclaredConstructors()[0].newInstance(3,"java",40d);
        //bk.info();
        //System.out.println(bk);


        var c4 = String.class;
        var c5 = int[][].class;
        var c6 = Class.class;
        var c7 = void.class;
        var c8 = Object.class;
        var c9 = String[][][].class;

    }
}


```

![image-20210421134940130](https://gitee.com/webrx/wx_note/raw/master/images/image-20210421134940130.png)

#### 12.2.2 反射常用API

#### 获取反射中的Class对象

![image-20210421135142050](https://gitee.com/webrx/wx_note/raw/master/images/image-20210421135142050.png)



#### 通过反射创建类对象

![image-20210421135202954](https://gitee.com/webrx/wx_note/raw/master/images/image-20210421135202954.png)

#### 通过反射获取类属性、方法、构造器

![image-20210421135228103](https://gitee.com/webrx/wx_note/raw/master/images/image-20210421135228103.png)

#### 12.2.3 反射获取注解属性

（1）、UserName.java

```java
package cn.reflection;

import java.lang.annotation.*;

/**
 * <p>Project: javaseapp - UserName
 * <p>Powered by webrx On 2021-08-30 17:37:28
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface UserName {
    String name() default "李四";
}

```



（2）、User

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.reflection;

/**
 * <p>Project: javaseapp - User
 * <p>Powered by webrx On 2021-08-30 15:20:37
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class User {
    @UserName(name = "王五")
    //@UserName
    private String name;

    public int pf(int i) {
        return i * i;
    }
}

```



（3）、Demo.java 读取User.class属性注解的内容

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.reflection;

import java.lang.reflect.Field;

/**
 * <p>Project: javaseapp - Ref7
 * <p>Powered by webrx On 2021-08-30 17:42:12
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Ref7 {
    public static void main(String[] args) {
        Class u = User.class;
        Field n = null;
        try {
            n = u.getDeclaredField("name");
            System.out.println(n.getName());
            var un = n.getAnnotation(UserName.class);
            System.out.println(un.name());
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }
}

```



### 作业：

1. 利用反射打印出String类的所有方法名

   ```java
   public class Ex01 {
       public static void main(String[] args) {
           //打印出java.lang.String类的所有方法名称
           var s = String.class;
           var ms = s.getDeclaredMethods();
           StringBuilder sbu = new StringBuilder();
           int rows = 0;
           for (Method m : ms) {
               var p = m.getParameters();
               for (Parameter pa : p) sbu.append(pa.getType()).append(" ").append(pa.getName()).append(",");
               if (sbu.length() > 0) sbu.deleteCharAt(sbu.length() - 1);
               System.out.printf("%d、String.%s(%s);%n", ++rows, m.getName(), sbu);
               sbu.delete(0, sbu.length());
           }
       }
   }
   ```
   
   
   
   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.reflection;
   
   import java.lang.reflect.Constructor;
   import java.lang.reflect.Method;
   
   /**
    * <p>Project: javaseapp - Ref1
    * <p>Powered by webrx On 2021-08-30 15:04:46
    * <p>Created by IntelliJ IDEA
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 16
    */
   @SuppressWarnings("all")
   public class Ref1 {
       public static void main(String[] args) {
   
   
           //反射类 获取String的反射类
           Class<String> c = String.class;
           var c2 = String.class;
           System.out.println(c == c2); //true
   
           Method[] ms = c.getDeclaredMethods();
           //Arrays.stream(ms).forEach(System.out::println);
           var cc = c.getDeclaredConstructors();
           //反射输出所有构造方法名称
           for (Constructor c1 : cc) {
               System.out.println(c1.getName());
           }
           //反射输出所有方法名称
           for (var m : ms) {
               System.out.println(m.getName());
           }
       }
   }
   ```
   
   
   
2. 在当前程序中加载/src/main/resources/db.properties文件内容

   db.properties

   ```properties
   db.driver=com.mysql.cj.jdbc.Driver
   db.url=jdbc:mysql:/mysql
   db.username=root
   db.password=123456
   ```

   Demo.java 测试程序

   ```java
   /*
    * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
    *
    */
   package cn.webrx;
   
   import java.io.IOException;
   import java.io.InputStream;
   import java.util.Properties;
   
   /**
    * <p>Project: demo - Reflect1
    * <p> C:\Users\admin\IdeaProjects\oop\src\main\resources\db.properties
    *
    * @author webrx [webrx@126.com]
    * @version 1.0
    * @since 15
    */
   public class Reflect4 {
   
       public static void main(String[] args) throws IOException {
           Properties prop = new Properties();
           InputStream is = null;
           //不推荐
           //is = new FileInputStream("C:\\Users\\admin\\IdeaProjects\\oop\\src\\main\\resources\\db.properties");
   
           //推荐
           //is =  Reflect4.class.getClassLoader().getResourceAsStream("db.properties");
           ClassLoader loader = Thread.currentThread().getContextClassLoader();
           is = loader.getResourceAsStream("db.properties");
   
           prop.load(is);
   
           prop.forEach((k, v) -> {
               System.out.printf("%s = %s%n", k, v);
           });
       }
   }
   
   ```

   

## 第十三章  Java1.8 日期API

在Java8以前，Date日期API对我们非常的不友好，它无法表示日期，只能以毫秒的精度来表示时间，并且可以修改，他的线程还不是安全的。所以Java8中引入了全新的日期和时间API就是为了解决这一问题。

> java.util.Date 日期类
>
> java.util.Calendar 日历 
>
> java.text.SimpleDateFormat 格式化类
>
> 
>
> java.time.LocalDate 年月日
>
> java.time.LocalTime 时分秒
>
> java.time.LocalDateTime 年月日时分秒
>
> java.time.format.DateTimeFormatter 格式化 日期 时间  日期时间

小时（h）分钟（min）秒（s）
毫秒（ms）微秒（us）纳秒（ns）皮秒（ps）飞秒（fs）

![img](https://gitee.com/webrx/wx_note/raw/master/images/70.png)

```java
@Test
public void test1() {
    LocalDate of = LocalDate.of(2019, 11, 11);//2019-11-11
    int year = of.getYear();                //年份：2019
    Month month = of.getMonth();             //月份：NOVEMBER
    int dayOfMonth = of.getDayOfMonth();    //这月的第几天：11
    DayOfWeek dayOfWeek = of.getDayOfWeek();//这周的第几天：MONDAY
    int dayOfYear = of.getDayOfYear();      //这年的第几天：315
    boolean leapYear = of.isLeapYear();     //是否是润年:false
}
```

```java
@Test
public void test2() {
    LocalTime localTime = LocalTime.of(11, 12, 13);
    int hour = localTime.getHour();         //小时：11
    int minute = localTime.getMinute();     //分钟：12
    int second = localTime.getSecond();     //秒数：13
}

@Test
public void test3() {
    //2019-11-12T13:14:15
    LocalDateTime localDateTime = LocalDateTime.of(2019, 11, 12, 13, 14, 15);
    //2019-11-12
    LocalDate localDate = localDateTime.toLocalDate();
    //13:14:15
    LocalTime localTime = localDateTime.toLocalTime();
    //可以从LocalDateTime获取所有的当前的信息，比如，年份，月份。。
    LocalDateTime now = LocalDateTime.now(); //获取当前系统的日期，时间
}
```

```java
@Test
public void test4() {
    LocalDateTime localDateTime = LocalDateTime.now();
    String format = localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE); 
    //20191210
    String format1 = localDateTime.format(DateTimeFormatter.ISO_DATE_TIME); 
    //2019-12-10T09:35:22.27
    String format2 = localDateTime.format(DateTimeFormatter.ISO_DATE);      
    //2019-12-10
    String format3 = localDateTime.format(DateTimeFormatter.ISO_TIME);      
    //09:37:52.778

    //自定义规则
    String format4 = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")); 
    //2019-12-10 09:39:18
    String format5 = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")); 
    //2019-12-10
    String format6 = localDateTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")); 
    //09:40:06
    
    //解晰字符串
    String mYDateTime = "2019-12-10 09:39:18";
    String mYDate = "2019-12-10";
    LocalDate parse = LocalDate.parse(mYDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    //2019-12-10
    LocalDateTime parse1 = LocalDateTime.parse(mYDateTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    //2019-12-10T09:39:18
}
```

### 13.1 Clock 时钟

Clock类提供了访问当前日期和时间的方法，Clock是时区敏感的，可以用来取代 System.currentTimeMillis() 来获取当前的微秒数。某一个特定的时间点也可以使用Instant类来表示，Instant类也可以用来创建老的java.util.Date对象。

```java
import java.time.Clock;
import java.time.Instant;
import java.util.Date;
public class Test {
	public static void main(String[] args) {
		Clock clock = Clock.systemDefaultZone();
		long millis = clock.millis();
	
		Instant instant = clock.instant();
		Date from = Date.from(instant);
		
		System.out.println("millis :" + millis);
		System.out.println("from :" + from);
	}
}
```

### 13.2 Timezones 时区

```java
import java.time.ZoneId;
import java.util.Set;
public class Test {
	public static void main(String[] args) {
		Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
		System.out.println(availableZoneIds);
		ZoneId of = ZoneId.of("Asia/Shanghai");
		ZoneId of1 = ZoneId.of("Asia/Aden");//亚丁湾
		System.out.println(of.getRules());
		System.out.println(of1.getRules());
		//Asia/Shanghai
        System.out.println(ZoneOffset.systemDefault());
        
        //美国纽约时间
        ZoneId zid = ZoneId.of("America/New_York");
        System.out.println(zid);
        System.out.println(zid.getRules());
	}
}
```

### 13.3 LocalTime 本地时间

1、可以通过静态工厂方法of创建一个LocalDate实例

2、同过方法parse 解析时间

3、操作方式和LocalDate类似

```java
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Set;

public class Test {
 
	public static void main(String[] args) {
		
		Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
		System.out.println(availableZoneIds);
	
		//获取当前时间
		ZoneId zoneId = ZoneId.of("Asia/Shanghai");
		LocalTime localTimeOld = LocalTime.now(zoneId);
		System.out.println("localTime :"+localTimeOld);
		
		//获取定时时间及时、分、秒
		LocalTime localTime = LocalTime.of(12, 25, 55);
		int hour = localTime.getHour();
		int minute = localTime.getMinute();
		int second = localTime.getSecond();
		System.out.println("localTime "+localTime+"\nhour :"+hour + "\nminute :" + minute + "second :"+second);
		
		//获取两个时间时间差(时间小的第一个参数结果是负数)
		long hoursBetween = ChronoUnit.HOURS.between(localTimeOld, localTime);
		long minutesBetween = ChronoUnit.MINUTES.between(localTimeOld, localTime);
		System.out.println("hoursBetween :"+hoursBetween);
		System.out.println("minutesBetween :"+minutesBetween);
		
		//多种工厂方法简化对象创建，包括字符串的解析
		DateTimeFormatter germanFormatter =
			    DateTimeFormatter
			        .ofLocalizedTime(FormatStyle.SHORT)
			        .withLocale(Locale.GERMAN);
		LocalTime parse = LocalTime.parse("12:25", germanFormatter);
		System.out.println("parse :"+parse);
		
	}
}
```

### 13.4 LocalDate 本地日期

LocalDate 表示了一个确切的日期，该对象值是不可变的，用起来和LocalTime基本一致。另外要注意的是这些对象是不可变的，操作返回的总是一个新实例。

```java
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class Test {

	public static void main(String[] args) {
		LocalDate today = LocalDate.now();
		LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
		LocalDate yesterday = tomorrow.minusDays(2);
		System.out.println(yesterday); 
		
		LocalDate independenceDay = LocalDate.of(2014, Month.JULY, 4);
		DayOfWeek dayOfWeek = independenceDay.getDayOfWeek();
		System.out.println(dayOfWeek);  
		
		
		DateTimeFormatter germanFormatter =  DateTimeFormatter
			                                 .ofLocalizedDate(FormatStyle.MEDIUM)
			                                 .withLocale(Locale.GERMAN);

			LocalDate xmas = LocalDate.parse("01.11.2019", germanFormatter);
			System.out.println(xmas);
	}
}
```

### 13.5 LocalDateTime 本地时间日期

1、LocalDateTime，是LocalDate和LocalTime的合体。它同时表示了日期和时间，但不带有时区信息，可以直接创建，也可以通过合并日期和时间对象构造。

2、LocalDateTime和LocalTime还有LocalDate一样，都是不可变的。LocalDateTime提供了一些能访问具体字段的方法。

```java
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;
public class Test {
	public static void main(String[] args) {
		LocalDateTime sylvester = LocalDateTime.of(2019, Month.DECEMBER, 31, 23, 59, 59);
		DayOfWeek dayOfWeek = sylvester.getDayOfWeek();
		System.out.println(dayOfWeek);      

		Month month = sylvester.getMonth();
		System.out.println(month);          
		long minuteOfDay = sylvester.getLong(ChronoField.MINUTE_OF_DAY);
		System.out.println(minuteOfDay);   
	}
}
```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * <p>Project: javaseapp - T3
 * <p>Powered by webrx On 2021-08-30 15:40:49
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T3 {
    public static void main(String[] args) {
        var d = LocalDateTime.now();
        System.out.println(d);
        var dt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(d.format(dt));
        System.out.println(d.toLocalDate());
        System.out.println(d.toLocalTime());


        //获取秒数
        Long second = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
        //获取毫秒数
        Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        System.out.println(milliSecond);

        //LocalDateTime 转换 Date
        Date d2 = new Date(milliSecond);
        System.out.println(d2.getTime());
        System.out.println(System.currentTimeMillis());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(d2));

        //LocalDateTime与String互转
        //DateTimeFormatter 时间转字符串格式化
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        String dateTime = LocalDateTime.now(ZoneOffset.of("+8")).format(formatter);

        //字符串转时间
        String dateTimeStr = "2018-07-28 14:11:15";
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime ldt = LocalDateTime.parse(dateTimeStr, df);


        //Date与LocalDateTime互转
        var da = new Date();
        var db = da.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
        System.out.println(db);
        System.out.println(db instanceof LocalDateTime);
        var dc = LocalDateTime.ofInstant(da.toInstant(), ZoneOffset.of("+8"));
        System.out.println(dc instanceof LocalDateTime);

        var dd = LocalDateTime.now();
        var de = Date.from(dd.toInstant(ZoneOffset.of("+8")));
        Long ms = dd.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        var df = new Date(ms);
        System.out.println(dc);
        System.out.println(dd);
    }
}

```



### 13.6 Instant

从计算机的角度来看，建模时间最自然的格式是表示一个持续时间段上某个点的单一大整型数, Instant的设计初衷是为了便于机器使用，它包含的是由秒及纳秒所构成的数字。Instant类也支持静态工厂方法now，它能够帮你获取当前时刻的时间戳。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.time.Instant;
import java.util.Date;

/**
 * <p>Project: javaseapp - T1
 * <p>Powered by webrx On 2021-08-30 10:44:25
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T1 {
    public static void main(String[] args) {
        //Instant
        var i = Instant.now();
        System.out.println(i);
        System.out.println(i.getEpochSecond());//秒
        System.out.println(System.currentTimeMillis());//毫秒
        //.getNano() 纳秒

        System.out.println(i.getEpochSecond() * 1000 + i.getNano() / 1000 / 1000);
        System.out.println(System.currentTimeMillis());


        var d = new Date();
        System.out.println(d);


        //java.util.Date 转换 java.time.Instant
        var is = d.toInstant();
        System.out.println(is);
        System.out.printf("%1$tF %1$tT %n", is.getEpochSecond() * 1000);

        // java.time.Instant 转换 java.util.Date
        var d3 = Date.from(Instant.now());
        System.out.printf("%1$tF %1$tT %n", d3);
    }
}

```



```java
import java.time.Instant;
public class Test {
	public static void main(String[] args) {
		Instant now = Instant.now();
		System.out.println(now.getEpochSecond() + "." + now.getNano());
		System.out.println(System.currentTimeMillis());
	}
}
```



### 13.7 时间类的通用方法

> LocalDate、 LocalTime、 LocalDateTime以及Instant这样表示时间点的日期时间类提供了大量通用的方法。

|  方法名  | 是否是静态方法 | 描述                                                         |
| :------: | :------------: | ------------------------------------------------------------ |
|   form   |       是       | 根据传入的Temporal对象创建对象实例                           |
|   now    |       是       | 根据系统时钟创建Temporal对象                                 |
|    of    |       是       | 由Temporal对象的某个部分创建该对象的实例                     |
|  parse   |       是       | 由字符串创建Temporal对象的实例                               |
| atOffset |       否       | 将Temporal对象和某个时区偏移相结合                           |
|  atZone  |       否       | 将Temporal对象和某个时区相结合                               |
|  format  |       否       | 使用某个指定的格式器将Temporal对象转化为字符串（Instant 类不提供该方法） |
|   get    |       否       | 读取Temporal对象某一个部分的值                               |
|  minus   |       否       | 创建Temporal对象的一个副本，通过将当前Temporal对象的值减去一定的时长创建该副本 |
|   plus   |       否       | 创建Temporal对象的一个副本，通过将当前Temporal对象的值加上一定的时长创建该副本 |
|   with   |       否       | 以该Temporal对象对象为模板，对某些状态进行修改创建该对象的副本 |

### 13.8 格式化

DateTimeFormatter通常用来格式化日期和时间，里面预定义了像BASIC_ISO_DATE和ISO_LOCAL_DATE这样的常量方便格式化。

![image-20210830124412925](https://gitee.com/webrx/wx_note/raw/master/images/image-20210830124412925-16302986800971.png)![](https://gitee.com/webrx/wx_note/raw/master/images/image-20210830124641839.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>Project: javaseapp - T2
 * <p>Powered by webrx On 2021-08-30 12:28:20
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T2 {
    public static void main(String[] args) {
        var d2 = LocalDate.now();
        System.out.println(d2);
        System.out.println(d2.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println(d2.format(DateTimeFormatter.ISO_DATE));
        System.out.println(d2.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(d2.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日")));
        System.out.println(d2.format(DateTimeFormatter.ofPattern("E")));


        var t2 = LocalTime.now();
        t2 = LocalTime.of(12, 33, 32);
        System.out.println(t2);
        var dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        System.out.println(t2.format(dtf));

        var d3 = LocalDateTime.now();
        var d3f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss E");
        System.out.println(d3.format(d3f));

    }
}

```

![image-20210830125340057](https://gitee.com/webrx/wx_note/raw/master/images/image-20210830125340057.png)

### 13.9 日期API常用操作

#### 13.9.1 获取总的日期间隔

```java
// 指定转换格式
DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");

LocalDate startDate = LocalDate.parse("2019-03-01",fmt);
LocalDate endDate = LocalDate.parse("2020-04-02",fmt);

System.out.println("总相差的天数:" + startDate.until(endDate, ChronoUnit.DAYS));
System.out.println("总相差的月数:" + startDate.until(endDate, ChronoUnit.MONTHS));
System.out.println("总相差的年数:" + startDate.until(endDate, ChronoUnit.YEARS));

//求出两个日期之间的天数
var da = LocalDate.of(1985, 10, 25);
var db = LocalDate.now();
Duration days = Duration.between(da.atStartOfDay(), db.atStartOfDay());
System.out.println(days.toDays());//13094 天

long ddd = ChronoUnit.DAYS.between(da, db);
System.out.println(ddd);//13094天

System.out.println(MONTHS.between(da, db));//430个月
System.out.println(ChronoUnit.YEARS.between(da, db));//35年
```

#### 13.9.2 获取年月日单独的日期间隔

![image-20210830132953866](https://gitee.com/webrx/wx_note/raw/master/images/image-20210830132953866.png)

#### 13.9.3 获取当前月的第一天与最后一天

```java
//当前月1日 日期 2021-08-01
var one = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
System.out.println(one);
//下月1日 日期 2021-09-01
var onenext = LocalDate.now().with(TemporalAdjusters.firstDayOfNextMonth());
System.out.println(onenext);
//上月1日 日期 2021-07-01
var oneprev = LocalDate.now().minus(1, ChronoUnit.MONTHS).with(TemporalAdjusters.firstDayOfMonth());
System.out.println(oneprev);
//上月1日 日期 2021-07-01
var oneprev2 = LocalDate.now().with((e) -> e.with(DAY_OF_MONTH, 1).plus(-1, MONTHS));
System.out.println(oneprev2);

//当前月最后一天 日期 2021-08-31
var two = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
System.out.println(two);
var two2 = LocalDate.now().with(t -> t.with(DAY_OF_MONTH, t.range(DAY_OF_MONTH).getMaximum()));
System.out.println(two2);

//获取下个月最后一天日期 2021-09-30
var twonext = LocalDate.now().plus(1, MONTHS).with(TemporalAdjusters.lastDayOfMonth());
System.out.println(twonext);
//获取上个月最后一天日期  2021-07-31
var twoprev = LocalDate.now().minus(1, MONTHS).with(TemporalAdjusters.lastDayOfMonth());
System.out.println(twoprev);

LocalDateTime date = LocalDateTime.now();
LocalDateTime firstday = date.with(TemporalAdjusters.firstDayOfMonth());
LocalDateTime lastDay = date.with(TemporalAdjusters.lastDayOfMonth());
System.out.println(“firstday:” + firstday);
System.out.println(“lastDay:” + lastDay);
```

#### 13.9.4 判断闰年，指定日期当月最多有多少天

```java
//判断当前年是不是闰年  java.time.LocalDate类
var d1 = LocalDate.now();
String info = String.format("%s年是%s%n", d1.getYear(), d1.isLeapYear() ? "闰年" : "平年");
System.out.println(info);

//判断指定年份  java.time.Year 类
var d2 = Year.of(2020);
String d2info = String.format("%s年是%s%n", d2.getValue(), d2.isLeap() ? "闰年" : "平年");
System.out.println(d2info);

var m = Month.of(2);
System.out.println(m.maxLength());//29
System.out.println(m.minLength());//28

//求出指定日期最多有多少天
var d3 = LocalDate.of(2020, 2, 1).lengthOfMonth();
System.out.println(d3);//29
System.out.println(LocalDate.now().lengthOfMonth());//31
```

#### 13.9.5 生成随机日期LocalDate(指定范围）

```java
//生成随机日期(指定范围）
//2000-01-01  2021-08-31
var dd1 = LocalDate.of(2000, 1, 1);
var dd2 = LocalDate.of(2021, 8, 31);
Random rand = new Random();
var yy = rand.nextInt(dd2.getYear() - dd1.getYear() + 1) + dd1.getYear();
var mm = rand.nextInt(11) + 1;
var dd = rand.nextInt(LocalDate.of(yy, mm, 1).lengthOfMonth() + 1) + 1;
var drand = LocalDate.of(yy, mm, dd);
System.out.println(drand);
```

#### 13.9.6 显示星期几

```java
//显示当前日期星期几 java.time.DayOfWeek 类
var d5 = LocalDate.now().getDayOfWeek();
System.out.println(d5);//TUESDAY
DayOfWeek dw = LocalDate.now().getDayOfWeek();
//Returns: //the day-of-week, from 1 (Monday) to 7 (Sunday)
System.out.println(dw.getValue());
System.out.printf("%s 是：星期%s %n", LocalDate.now(), "一二三四五六日".charAt(dw.getValue() - 1));
```

#### 13.9.7 LocalDateTime 转换操作

```java
var d = LocalDateTime.now();
System.out.println(d);
var dt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
System.out.println(d.format(dt));
System.out.println(d.toLocalDate());
System.out.println(d.toLocalTime());


//获取秒数
Long second = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));
//获取毫秒数
Long milliSecond = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
System.out.println(milliSecond);

//LocalDateTime 转换 Date
Date d2 = new Date(milliSecond);
System.out.println(d2.getTime());
System.out.println(System.currentTimeMillis());

SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
System.out.println(sdf.format(d2));

//LocalDateTime与String互转
//DateTimeFormatter 时间转字符串格式化
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
String dateTime = LocalDateTime.now(ZoneOffset.of("+8")).format(formatter);

//字符串转时间
String dateTimeStr = "2018-07-28 14:11:15";
DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
LocalDateTime ldt = LocalDateTime.parse(dateTimeStr, df);


//Date与LocalDateTime互转
var da = new Date();
var db = da.toInstant().atOffset(ZoneOffset.of("+8")).toLocalDateTime();
System.out.println(db);
System.out.println(db instanceof LocalDateTime);
var dc = LocalDateTime.ofInstant(da.toInstant(), ZoneOffset.of("+8"));
System.out.println(dc instanceof LocalDateTime);

var dd = LocalDateTime.now();
var de = Date.from(dd.toInstant(ZoneOffset.of("+8")));
Long ms = dd.toInstant(ZoneOffset.of("+8")).toEpochMilli();
var df2 = new Date(ms);
System.out.println(dc);
System.out.println(dd);

```



## 附录一 JDK配置变量配置

### 1.1 JDK 1.8环境变量配置

![image-20210726092229358](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726092229358.png)

![image-20210726092420252](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726092420252.png)

![image-20210726092552914](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726092552914.png)![image-20210726092633444](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726092633444.png)

建立JAVA_HOME环境变量

![image-20210726092826547](assets/image-20210726092826547.png)

建立CLASSPATH环境变量

![image-20210726093106554](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726093106554.png)

![image-20210726093402666](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726093402666.png)

A.java

```java
class A{
public static void main(String[] a){
System.out.println("Hello world");
}
}
```

```cmd
C:\Users\Administrator>java -version
java version "1.8.0_301"
Java(TM) SE Runtime Environment (build 1.8.0_301-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.301-b09, mixed mode)

C:\Users\Administrator>java --version
Unrecognized option: --version
Error: Could not create the Java Virtual Machine.
Error: A fatal exception has occurred. Program will exit.

C:\Users\Administrator>javac -version
javac 1.8.0_301

C:\Users\Administrator>notepad

C:\Users\Administrator>java A.java
错误: 找不到或无法加载主类 A.java

# cd desktop 进入桌面
C:\Users\Administrator>cd desktop

C:\Users\Administrator\Desktop>java A.java
错误: 找不到或无法加载主类 A.java

C:\Users\Administrator\Desktop>javac A.java

C:\Users\Administrator\Desktop>java A
Hello world
```

![image-20211119092613207](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119092613207.png)

![image-20211119092843025](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119092843025.png)

![](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119093029350-16372854554671.png)

![image-20211119093352707](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119093352707.png)

![image-20211119094106712](https://gitee.com/webrx/wx_note/raw/master/images/image-20211119094106712.png)

### 1.2 JDK 16 环境变量配置

 ```text
 JAVA_HOME -> D:\jdk\jdk-16.0.2
 CLASSPATH -> .;%JAVA_HOME%\lib
 CLASSPATH -> .;D:\jdk\jdk-16.0.2\lib
 
 在path环境变量中添加 .  再添加%JAVA_HOME%\bin,如果还要再完美一点儿，再添加%JAVA_HOME%\lib
 ```

![image-20210726095136677](https://gitee.com/webrx/wx_note/raw/master/images/image-20210726095136677.png)

### 1.3 linux环境变量jdk16.0.2

* vmware workstation pro
* ubuntu server iso
* SecureCRT SSH 客户端工具(Xshell  Securecrt   FinalShell国产管理)
* jdk-16.0.2_linux-x64_bin.tar.gz

```bash
ubuntu 虚拟机安装，删除声卡 打印机设置，建立最小安装，只有网络功能，什么都不安装。用到什么软件安装什么软件，这样服务器非常稳定。

http://mirrors.aliyun.com/ubuntu/
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3B4FE6ACC0B21F32 3B4FE6ACC0B21F32 3B4FE6ACC0B21F32  3B4FE6ACC0B21F32

http://mirrors.cloud.tencent.com/ubuntu/

webrx - ubuntu

# ubuntu 系统更新
sudo apt update
sudo list --upgradable
sudo apt upgrade 

# 查看ubuntu系统版本信息
cat /proc/version
uname -a
lsb_release -a

# 下载jdk并解压
wget 下载
tar 解压
webrx@userver:~$ tar -zxvf jdk-16.0.2_linux-x64_bin.tar.gz 
# 移动目录jdk-16.0.2 到/usr/local/jdk-16.0.2 目录
webrx@userver:~$ sudo mv jdk-16.0.2 /usr/local


#配置环境变量
sudo vim /etc/profile
    #export JAVA_HOME=/usr/local/jdk-15.0.2
    #export CLASSPATH=.:${JAVA_HOME}/lib
    #export PATH=${CLASSPATH}:${JAVA_HOME}/bin:$PATH
#最新版配置
29 export JAVA_HOME=/usr/local/jdk-16.0.2
30 export CLASSPATH=.:${JAVA_HOME}/lib
31 export PATH=${JAVA_HOME}/bin:${CLASSPATH}:$PATH
# 保存并退出
esc :wq
# 让环境变量立即生效
source /etc/profile
java -version
java --version


#linux
$sudo apt install tree sl
$sudo apt remove sl 卸载
$sudo poweroff 关机
$sudo reboot 重新启动
```

![image-20210810153302322](https://gitee.com/webrx/wx_note/raw/master/images/image-20210810153302322.png)

在ubuntu系统中，使用wget下载jdk

![image-20210810164349510](https://gitee.com/webrx/wx_note/raw/master/images/image-20210810164349510.png)

ubuntu /etc/profile

```conf
# /etc/profile: system-wide .profile file for the Bourne shell (sh(1))
# and Bourne compatible shells (bash(1), ksh(1), ash(1), ...).

if [ "${PS1-}" ]; then
  if [ "${BASH-}" ] && [ "$BASH" != "/bin/sh" ]; then
    # The file bash.bashrc already sets the default PS1.
    # PS1='\h:\w\$ '
    if [ -f /etc/bash.bashrc ]; then
      . /etc/bash.bashrc
    fi
  else
    if [ "`id -u`" -eq 0 ]; then
      PS1='# '
    else
      PS1='$ '
    fi
  fi
fi

if [ -d /etc/profile.d ]; then
  for i in /etc/profile.d/*.sh; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi

export JAVA_HOME=/usr/local/jdk-16.0.2
export CLASSPATH=.:${JAVA_HOME}/lib
export PATH=${JAVA_HOME}/bin:${CLASSPATH}:$PATH

```

```java
public class Demo{
   public static void main(String[] args){
       System.out.println("hello world");
       System.out.println(System.getProperty("java.home"));
       System.out.println(System.getProperty("java.version"));
       System.out.println(System.getProperty("os.name"));
       int sum = 0;
       for(int i=1;i<=100;i++){
			sum+=i;
       }
       System.out.println(sum);
   }
}

```



![image-20210810175049159](https://gitee.com/webrx/wx_note/raw/master/images/image-20210810175049159.png)

### 1.4 Java关键字

```java
Java 编程语言具有 50 多个保留关键字，这些保留关键字对于编译器具有特殊含义，不能用作变量名。 以下是按功能分类的 Java 关键字列表。
有关 Java 关键字还有一些需要注意的要点：
const 和 goto 是保留关键字，目前还没有实际用途。
true， false 和 null 是常量而不是关键字。
从 Java 8 开始，default 关键字也用于声明接口的默认方法。
从 Java 10 开始，单词 var 可用于声明局部变量（局部类型推断）。为了向前兼容，var 可以用作变量名，因此 var 只是保留字而不是关键字。
Java 14 添加了两个新的关键字 record 和 yield（switch表达式语句）。
所有关键字都是小写字母。

访问标识符：private, protected, public
类，方法，变量标识符：abstract, class, default, extends, final, implements, interface, native, new, static, strictfp, synchronized, transient, var, volatile, record
流程控制：break, case, continue, default, do, else, for, if, instanceof, return, switch, while, yield
包管理：import, package
原始数据类型：boolean, byte, char, double, float, int, long, short
错误处理：assert, catch, finally, throw, throws, try
枚举：enum
其它：super, this, void
无用（保留）：const, goto
```

### 1.5 项目打jar包

（1）jar 打包文件

```cmd
C:\jar\img>jar -cvfe i.jar m.txt .
已添加清单
正在添加: g1.gif(输入 = 10602) (输出 = 8118)(压缩了 23%)
正在添加: g1.png(输入 = 3505) (输出 = 3492)(压缩了 0%)
正在添加: g10.png(输入 = 4159) (输出 = 4164)(压缩了 0%)
正在添加: g11.png(输入 = 3868) (输出 = 3860)(压缩了 0%)
正在添加: g12.png(输入 = 3823) (输出 = 3828)(压缩了 0%)
正在添加: g13.png(输入 = 4405) (输出 = 4410)(压缩了 0%)
正在添加: g14.png(输入 = 3284) (输出 = 3270)(压缩了 0%)
正在添加: g15.png(输入 = 3616) (输出 = 3621)(压缩了 0%)
正在添加: g16.png(输入 = 4165) (输出 = 4170)(压缩了 0%)
正在添加: g17.png(输入 = 3839) (输出 = 3836)(压缩了 0%)
正在添加: g18.png(输入 = 4661) (输出 = 4655)(压缩了 0%)
正在添加: g19.png(输入 = 4251) (输出 = 4256)(压缩了 0%)
正在添加: g2.png(输入 = 4017) (输出 = 4022)(压缩了 0%)
正在添加: g3.png(输入 = 4232) (输出 = 4218)(压缩了 0%)
正在添加: g4.png(输入 = 4423) (输出 = 4408)(压缩了 0%)
正在添加: g5.png(输入 = 3880) (输出 = 3885)(压缩了 0%)
正在添加: g6.png(输入 = 4172) (输出 = 4177)(压缩了 0%)
正在添加: g7.png(输入 = 3761) (输出 = 3766)(压缩了 0%)
正在添加: g8.png(输入 = 4063) (输出 = 4044)(压缩了 0%)
正在添加: g9.png(输入 = 3962) (输出 = 3967)(压缩了 0%)

C:\jar\img>cd ..

C:\jar>jar -cvfe i.jar m.txt img
已添加清单
正在添加: img/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: img/g1.gif(输入 = 10602) (输出 = 8118)(压缩了 23%)
正在添加: img/g1.png(输入 = 3505) (输出 = 3492)(压缩了 0%)
正在添加: img/g10.png(输入 = 4159) (输出 = 4164)(压缩了 0%)
正在添加: img/g11.png(输入 = 3868) (输出 = 3860)(压缩了 0%)
正在添加: img/g12.png(输入 = 3823) (输出 = 3828)(压缩了 0%)
正在添加: img/g13.png(输入 = 4405) (输出 = 4410)(压缩了 0%)
正在添加: img/g14.png(输入 = 3284) (输出 = 3270)(压缩了 0%)
正在添加: img/g15.png(输入 = 3616) (输出 = 3621)(压缩了 0%)
正在添加: img/g16.png(输入 = 4165) (输出 = 4170)(压缩了 0%)
正在添加: img/g17.png(输入 = 3839) (输出 = 3836)(压缩了 0%)
正在添加: img/g18.png(输入 = 4661) (输出 = 4655)(压缩了 0%)
正在添加: img/g19.png(输入 = 4251) (输出 = 4256)(压缩了 0%)
正在添加: img/g2.png(输入 = 4017) (输出 = 4022)(压缩了 0%)
正在添加: img/g3.png(输入 = 4232) (输出 = 4218)(压缩了 0%)
正在添加: img/g4.png(输入 = 4423) (输出 = 4408)(压缩了 0%)
正在添加: img/g5.png(输入 = 3880) (输出 = 3885)(压缩了 0%)
正在添加: img/g6.png(输入 = 4172) (输出 = 4177)(压缩了 0%)
正在添加: img/g7.png(输入 = 3761) (输出 = 3766)(压缩了 0%)
正在添加: img/g8.png(输入 = 4063) (输出 = 4044)(压缩了 0%)
正在添加: img/g9.png(输入 = 3962) (输出 = 3967)(压缩了 0%)
```

![image-20211222091828432](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222091828432.png)

（2）jar打包类工具包

![image-20211222092308066](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222092308066.png)

编译程序，并打软件包utils.jar

```cmd
C:\Users\Administrator\Desktop\jar>dir
 驱动器 C 中的卷是 Win 10 Pro x64
 卷的序列号是 D830-6A82

 C:\Users\Administrator\Desktop\jar 的目录

2021/12/22  09:20    <DIR>          .
2021/12/22  09:20    <DIR>          ..
2021/12/22  09:20                85 Utils.java
               1 个文件             85 字节
               2 个目录 40,928,169,984 可用字节

C:\Users\Administrator\Desktop\jar>javac -d . *.java

C:\Users\Administrator\Desktop\jar>dir
 驱动器 C 中的卷是 Win 10 Pro x64
 卷的序列号是 D830-6A82

 C:\Users\Administrator\Desktop\jar 的目录

2021/12/22  09:23    <DIR>          .
2021/12/22  09:23    <DIR>          ..
2021/12/22  09:23    <DIR>          cn
2021/12/22  09:20                85 Utils.java
               1 个文件             85 字节
               3 个目录 40,919,842,816 可用字节

C:\Users\Administrator\Desktop\jar>jar -cvfe utils.jar m.txt cn
已添加清单
正在添加: cn/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: cn/webrx/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: cn/webrx/Utils.class(输入 = 247) (输出 = 193)(压缩了 21%)

C:\Users\Administrator\Desktop\jar>dir
 驱动器 C 中的卷是 Win 10 Pro x64
 卷的序列号是 D830-6A82

 C:\Users\Administrator\Desktop\jar 的目录

2021/12/22  09:24    <DIR>          .
2021/12/22  09:24    <DIR>          ..
2021/12/22  09:23    <DIR>          cn
2021/12/22  09:24               860 utils.jar
2021/12/22  09:20                85 Utils.java
               2 个文件            945 字节
               3 个目录 40,918,331,392 可用字节

C:\Users\Administrator\Desktop\jar>


```

项目使用jar

![image-20211222093007619](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222093007619.png)

maven项目pom.xml 也可以添加依赖

```xml
<!-- maven直接引入本地jar -->
<dependency>
    <groupId>cn.webrx</groupId>
    <artifactId>utils</artifactId>
    <version>1.0</version>
    <scope>system</scope>
    <systemPath>C:/Users/Administrator/Desktop/jar/utils.jar</systemPath>
</dependency>
```



![image-20211222093028759](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222093028759.png)

（3）jar打可以执行的程序包

App.java

```java
package cn.webrx;
public class App {
	public static void main(String[] args){
	    System.out.println("Hello World");
	}
}
```



```cmd
C:\Users\Administrator\Desktop\jar>javac -d . *.java

C:\Users\Administrator\Desktop\jar>java cn.webrx.App
Hello World

C:\Users\Administrator\Desktop\jar>jar -cvfe app.jar cn.webrx.App cn
已添加清单
正在添加: cn/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: cn/webrx/(输入 = 0) (输出 = 0)(存储了 0%)
正在添加: cn/webrx/App.class(输入 = 420) (输出 = 295)(压缩了 29%)

C:\Users\Administrator\Desktop\jar>java -jar app.jar
Hello World
```

(4) idea开发工具发行项目jar

![image-20211222100313266](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222100313266.png)

![image-20211222100551537](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222100551537.png)



![image-20211222100527647](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222100527647.png)

(5) maven 打jar

![image-20211222110822692](https://gitee.com/webrx/wx_note/raw/master/images/image-20211222110822692.png)

打可执行的jar  在maven 执行package并打包，并执行就可以了

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cn.webrx</groupId>
    <artifactId>app</artifactId>
    <version>1.0</version>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <java-version>17</java-version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>


    </dependencies>

    <build>
        <finalName>${project.artifactId}</finalName>
        <testSourceDirectory>src/test/java</testSourceDirectory>
        <sourceDirectory>src/main/java</sourceDirectory>

        <plugins>
            <!-- 编译插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>${java-version}</source>
                    <target>${java-version}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            
            <!-- 打jar包插件，可以指定执行程序 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>cn.webrx.App</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- 打包时 执行程序 -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <!-- test install complie package -->
                        <phase>package</phase>
                        <goals>
                            <goal>java</goal>
                        </goals>
                        <configuration>
                            <mainClass>cn.webrx.App</mainClass>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>

    </build>

</project>

```





## 附录二 java项目构建工具Maven

![image-20210816145201783](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816145201783.png)![image-20210816145237692](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816145237692.png)

> maven 官方网址：http://maven.apache.org/
>
> 下载地址：http://maven.apache.org/download.cgi
>
> maven中央仓库搜索源 : https://mvnrepository.com/
>
> https://maven.aliyun.com/mvn/guide

### 2.1 官方下载工具 maven.apache.org

![image-20211216101245893](https://gitee.com/webrx/wx_note/raw/master/images/image-20211216101245893.png)

### 2.2 解压到c:/maven

![image-20211216142047713](https://gitee.com/webrx/wx_note/raw/master/images/image-20211216142047713.png)

修改配置c:/maven/conf/settings.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 https://maven.apache.org/xsd/settings-1.2.0.xsd">
  <localRepository>c:/maven/repo</localRepository>
  <mirrors>
	<mirror>
	  <id>aliyunmaven</id>
	  <mirrorOf>*</mirrorOf>
	  <name>阿里云公共仓库</name>
	  <url>https://maven.aliyun.com/repository/public</url>
	</mirror>
  </mirrors>
  <profiles>
	<profile>
		<id>jdk-17</id>
		<activation>
			<activeByDefault>true</activeByDefault>
			<jdk>17</jdk>
		</activation>
		<properties>
			<maven.compiler.source>17</maven.compiler.source>
			<maven.compiler.target>17</maven.compiler.target>
			<maven.compiler.compilerVersion>17</maven.compiler.compilerVersion>
		</properties>
            </profile>
  </profiles>
</settings>

```

### 2.3 maven的环境变量path

jdk 开发环境，环境变量必须配置好，再配置maven path

![image-20211216142958421](https://gitee.com/webrx/wx_note/raw/master/images/image-20211216142958421.png)



### 2.4 打开命令控制台，查看版本信息

![image-20210816150828461](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816150828461.png)

### 2.5 配置maven 国内镜像和jdk版本

> d:/apache-maven-3.8.2/conf/settings.xml 核心配置文件

打原文件：

```xml
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 https://maven.apache.org/xsd/settings-1.2.0.xsd">
  <!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
  <localRepository>/path/to/local/repo</localRepository>
  -->

  <!-- interactiveMode
   | This will determine whether maven prompts you when it needs input. If set to false,
   | maven will use a sensible default value, perhaps based on some other setting, for
   | the parameter in question.
   |
   | Default: true
  <interactiveMode>true</interactiveMode>
  -->

  <!-- offline
   | Determines whether maven should attempt to connect to the network when executing a build.
   | This will have an effect on artifact downloads, artifact deployment, and others.
   |
   | Default: false
  <offline>false</offline>
  -->

  <!-- pluginGroups
   | This is a list of additional group identifiers that will be searched when resolving plugins by their prefix, i.e.
   | when invoking a command line like "mvn prefix:goal". Maven will automatically add the group identifiers
   | "org.apache.maven.plugins" and "org.codehaus.mojo" if these are not already contained in the list.
   |-->
  <pluginGroups>
    <!-- pluginGroup
     | Specifies a further group identifier to use for plugin lookup.
    <pluginGroup>com.your.plugins</pluginGroup>
    -->
  </pluginGroups>

  <!-- proxies
   | This is a list of proxies which can be used on this machine to connect to the network.
   | Unless otherwise specified (by system property or command-line switch), the first proxy
   | specification in this list marked as active will be used.
   |-->
  <proxies>
    <!-- proxy
     | Specification for one proxy, to be used in connecting to the network.
     |
    <proxy>
      <id>optional</id>
      <active>true</active>
      <protocol>http</protocol>
      <username>proxyuser</username>
      <password>proxypass</password>
      <host>proxy.host.net</host>
      <port>80</port>
      <nonProxyHosts>local.net|some.host.com</nonProxyHosts>
    </proxy>
    -->
  </proxies>

  <!-- servers
   | This is a list of authentication profiles, keyed by the server-id used within the system.
   | Authentication profiles can be used whenever maven must make a connection to a remote server.
   |-->
  <servers>
    <!-- server
     | Specifies the authentication information to use when connecting to a particular server, identified by
     | a unique name within the system (referred to by the 'id' attribute below).
     |
     | NOTE: You should either specify username/password OR privateKey/passphrase, since these pairings are
     |       used together.
     |
    <server>
      <id>deploymentRepo</id>
      <username>repouser</username>
      <password>repopwd</password>
    </server>
    -->

    <!-- Another sample, using keys to authenticate.
    <server>
      <id>siteServer</id>
      <privateKey>/path/to/private/key</privateKey>
      <passphrase>optional; leave empty if not used.</passphrase>
    </server>
    -->
  </servers>

  <!-- mirrors
   | This is a list of mirrors to be used in downloading artifacts from remote repositories.
   |
   | It works like this: a POM may declare a repository to use in resolving certain artifacts.
   | However, this repository may have problems with heavy traffic at times, so people have mirrored
   | it to several places.
   |
   | That repository definition will have a unique id, so we can create a mirror reference for that
   | repository, to be used as an alternate download site. The mirror site will be the preferred
   | server for that repository.
   |-->
  <mirrors>
    <!-- mirror
     | Specifies a repository mirror site to use instead of a given repository. The repository that
     | this mirror serves has an ID that matches the mirrorOf element of this mirror. IDs are used
     | for inheritance and direct lookup purposes, and must be unique across the set of mirrors.
     |
    <mirror>
      <id>mirrorId</id>
      <mirrorOf>repositoryId</mirrorOf>
      <name>Human Readable Name for this Mirror.</name>
      <url>http://my.repository.com/repo/path</url>
    </mirror>
     -->
    <mirror>
      <id>maven-default-http-blocker</id>
      <mirrorOf>external:http:*</mirrorOf>
      <name>Pseudo repository to mirror external repositories initially using HTTP.</name>
      <url>http://0.0.0.0/</url>
      <blocked>true</blocked>
    </mirror>
  </mirrors>

  <!-- profiles
   | This is a list of profiles which can be activated in a variety of ways, and which can modify
   | the build process. Profiles provided in the settings.xml are intended to provide local machine-
   | specific paths and repository locations which allow the build to work in the local environment.
   |
   | For example, if you have an integration testing plugin - like cactus - that needs to know where
   | your Tomcat instance is installed, you can provide a variable here such that the variable is
   | dereferenced during the build process to configure the cactus plugin.
   |
   | As noted above, profiles can be activated in a variety of ways. One way - the activeProfiles
   | section of this document (settings.xml) - will be discussed later. Another way essentially
   | relies on the detection of a system property, either matching a particular value for the property,
   | or merely testing its existence. Profiles can also be activated by JDK version prefix, where a
   | value of '1.4' might activate a profile when the build is executed on a JDK version of '1.4.2_07'.
   | Finally, the list of active profiles can be specified directly from the command line.
   |
   | NOTE: For profiles defined in the settings.xml, you are restricted to specifying only artifact
   |       repositories, plugin repositories, and free-form properties to be used as configuration
   |       variables for plugins in the POM.
   |
   |-->
  <profiles>
    <!-- profile
     | Specifies a set of introductions to the build process, to be activated using one or more of the
     | mechanisms described above. For inheritance purposes, and to activate profiles via <activatedProfiles/>
     | or the command line, profiles have to have an ID that is unique.
     |
     | An encouraged best practice for profile identification is to use a consistent naming convention
     | for profiles, such as 'env-dev', 'env-test', 'env-production', 'user-jdcasey', 'user-brett', etc.
     | This will make it more intuitive to understand what the set of introduced profiles is attempting
     | to accomplish, particularly when you only have a list of profile id's for debug.
     |
     | This profile example uses the JDK version to trigger activation, and provides a JDK-specific repo.
    <profile>
      <id>jdk-1.4</id>

      <activation>
        <jdk>1.4</jdk>
      </activation>

      <repositories>
        <repository>
          <id>jdk14</id>
          <name>Repository for JDK 1.4 builds</name>
          <url>http://www.myhost.com/maven/jdk14</url>
          <layout>default</layout>
          <snapshotPolicy>always</snapshotPolicy>
        </repository>
      </repositories>
    </profile>
    -->

    <!--
     | Here is another profile, activated by the system property 'target-env' with a value of 'dev',
     | which provides a specific path to the Tomcat instance. To use this, your plugin configuration
     | might hypothetically look like:
     |
     | ...
     | <plugin>
     |   <groupId>org.myco.myplugins</groupId>
     |   <artifactId>myplugin</artifactId>
     |
     |   <configuration>
     |     <tomcatLocation>${tomcatPath}</tomcatLocation>
     |   </configuration>
     | </plugin>
     | ...
     |
     | NOTE: If you just wanted to inject this configuration whenever someone set 'target-env' to
     |       anything, you could just leave off the <value/> inside the activation-property.
     |
    <profile>
      <id>env-dev</id>

      <activation>
        <property>
          <name>target-env</name>
          <value>dev</value>
        </property>
      </activation>

      <properties>
        <tomcatPath>/path/to/tomcat/instance</tomcatPath>
      </properties>
    </profile>
    -->
  </profiles>

  <!-- activeProfiles
   | List of profiles that are active for all builds.
   |
  <activeProfiles>
    <activeProfile>alwaysActiveProfile</activeProfile>
    <activeProfile>anotherAlwaysActiveProfile</activeProfile>
  </activeProfiles>
  -->
</settings>

```



修改后的文件：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 https://maven.apache.org/xsd/settings-1.2.0.xsd">

		  
  <!--配置本地仓库位置-->
  <localRepository>D:/apache-maven-3.8.2/repo</localRepository>
  <pluginGroups/>
  <proxies/>
  <servers/>

  <!--配置maven国内镜像仓库 -->
  <mirrors>
	<mirror>
	  <id>aliyunmaven</id>
	  <mirrorOf>*</mirrorOf>
	  <name>阿里云公共仓库</name>
	  <url>https://maven.aliyun.com/repository/public</url>
	</mirror>
  </mirrors>
  <profiles>
	<profile>   
    <id>jdk16</id>    
    <activation>   
        <activeByDefault>true</activeByDefault>    
        <jdk>16</jdk>   
    </activation>    
    <properties>   
        <maven.compiler.source>16</maven.compiler.source>    
        <maven.compiler.target>16</maven.compiler.target>    
        <maven.compiler.compilerVersion>16</maven.compiler.compilerVersion>   
    </properties> 
	</profile>
  </profiles>
</settings>
```

### 2.6 idea

#### 2.6.1 内置maven使用

![image-20211216101729296](https://gitee.com/webrx/wx_note/raw/master/images/image-20211216101729296.png)

需要建立一个配置文件`C:\Users\Administrator\.m2\settings.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 http://maven.apache.org/xsd/settings-1.2.0.xsd">
  <pluginGroups></pluginGroups>
  <proxies></proxies>
  <servers></servers>
  <mirrors>
		<!-- 阿里镜像仓库 -->
		<mirror>
		<id>alimaven</id>
		<name>aliyun maven</name>
		<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
		<mirrorOf>central</mirrorOf>
		</mirror>
  </mirrors>
  <profiles>
	    <profile>
		<id>jdk-17</id>
		<activation>
		    <activeByDefault>true</activeByDefault>
		    <jdk>17</jdk>
		</activation>
		<properties>
		    <maven.compiler.source>17</maven.compiler.source>
		    <maven.compiler.target>17</maven.compiler.target>
		    <maven.compiler.compilerVersion>17</maven.compiler.compilerVersion>
		</properties>
	    </profile>
	</profiles>
</settings>
```





#### 2.6.2 自定义maven使用

![image-20210816152628631](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816152628631.png)

![image-20211216100957609](https://gitee.com/webrx/wx_note/raw/master/images/image-20211216100957609.png)

### 2.7 idea maven 项目结构

![image-20210816152555850](https://gitee.com/webrx/wx_note/raw/master/images/image-20210816152555850.png)



### 2.8 pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cn.webrx</groupId>
    <artifactId>mavendemo</artifactId>
    <version>1.0</version>
    <!-- 项目类型 pom jar war -->
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.14.1</version>
            <scope>compile</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.20</version>
            <scope>provided</scope>
        </dependency>


    </dependencies>

    <properties>
        <maven.compiler.source>16</maven.compiler.source>
        <maven.compiler.target>16</maven.compiler.target>
    </properties>

</project>

```



![image-20210817081847238](https://gitee.com/webrx/wx_note/raw/master/images/image-20210817081847238.png)



![image-20210817081913503](https://gitee.com/webrx/wx_note/raw/master/images/image-20210817081913503.png)



![image-20210812214130911](https://gitee.com/webrx/wx_note/raw/master/images/image-20210812214130911.png)

```xml
<mirror>
  <id>aliyunmaven</id>
  <mirrorOf>*</mirrorOf>
  <name>阿里云公共仓库</name>
  <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```



### 2.9 java基础常用的maven依赖

conf/settings.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.2.0 http://maven.apache.org/xsd/settings-1.2.0.xsd">
<localRepository>d:/maven/repo</localRepository>
  <pluginGroups></pluginGroups>
  <proxies></proxies>
  <servers></servers>
  <mirrors>
		<!-- 阿里镜像仓库 -->
		<mirror>
		<id>alimaven</id>
		<name>aliyun maven</name>
		<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
		<mirrorOf>central</mirrorOf>
		</mirror>
  </mirrors>
  <profiles>
	    <profile>
		<id>jdk-16</id>
		<activation>
		    <activeByDefault>true</activeByDefault>
		    <jdk>16</jdk>
		</activation>
		<properties>
		    <maven.compiler.source>16</maven.compiler.source>
		    <maven.compiler.target>16</maven.compiler.target>
		    <maven.compiler.compilerVersion>16</maven.compiler.compilerVersion>
		</properties>
	    </profile>
	</profiles>
</settings>
```

maven dependencies

```xml
<!-- https://mvnrepository.com/artifact/com.github.whvcse/easy-captcha -->
<dependency>
    <groupId>com.github.whvcse</groupId>
    <artifactId>easy-captcha</artifactId>
    <version>1.6.2</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.jsoup/jsoup -->
<dependency>
    <groupId>org.jsoup</groupId>
    <artifactId>jsoup</artifactId>
    <version>1.14.1</version>
</dependency>

<!-- https://mvnrepository.com/artifact/net.coobird/thumbnailator -->
<dependency>
    <groupId>net.coobird</groupId>
    <artifactId>thumbnailator</artifactId>
    <version>0.4.14</version>
</dependency>

<!-- https://mvnrepository.com/artifact/commons-codec/commons-codec -->
<dependency>
    <groupId>commons-codec</groupId>
    <artifactId>commons-codec</artifactId>
    <version>1.15</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient -->
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
    <version>4.5.13</version>
</dependency>

<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.26</version>
</dependency>

<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>2.11.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/junit/junit -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.13.2</version>
    <scope>test</scope>
</dependency>

<!-- https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine -->
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>5.7.2</version>
    <scope>test</scope>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.poi/poi -->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi</artifactId>
    <version>5.0.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.jfree/jfreechart -->
<dependency>
    <groupId>org.jfree</groupId>
    <artifactId>jfreechart</artifactId>
    <version>1.5.3</version>
</dependency>
```

## 附录三 enum枚举

枚举(enum)
>Java 枚举是一个特殊的类，一般表示一组常量，比如一年的 4 个季节，一个年的 12 个月份，一个星期的 7 天，方向有东南西北等。

>Java 枚举类使用 enum 关键字来定义，各个常量使用逗号 , 来分割。

```java
enum Color{
    RED, GREEN, BLUE;
}
 
public class Test{
    // 执行输出结果
    public static void main(String[] args){
        Color c1 = Color.RED;
        System.out.println(c1);
    }
}
```
每个枚举都是通过 Class 在内部实现的，且所有的枚举值都是 public static final 的。

以上的枚举类 Color 转化在内部类实现：
```java
class Color{
     public static final Color RED = new Color();
     public static final Color BLUE = new Color();
     public static final Color GREEN = new Color();
}

```
### 内部类中使用枚举

```java
public class Test{
    enum Color{
        RED, GREEN, BLUE;
    }
 
    // 执行输出结果
    public static void main(String[] args){
        Color c1 = Color.RED;
        System.out.println(c1);
    }
}

```
### switch语句中使用枚举

```java
enum Color
{
    RED, GREEN, BLUE;
}
public class MyClass {
  public static void main(String[] args) {
    Color myVar = Color.BLUE;

    switch(myVar) {
      case RED:
        System.out.println("红色");
        break;
      case GREEN:
         System.out.println("绿色");
        break;
      case BLUE:
        System.out.println("蓝色");
        break;
    }
  }
}
```

### 迭代枚举元素

```java
enum Color{
    RED, GREEN, BLUE;
}
public class MyClass {
  public static void main(String[] args) {
    for (Color myVar : Color.values()) {
      System.out.println(myVar);
    }
  }
}

for (var i : ElementType.values()) {
    System.out.println(i);
}

for (var p : RetentionPolicy.values()) {
    System.out.println(p);
}
```
### values(), ordinal() 和 valueOf() 方法

enum 定义的枚举类默认继承了 java.lang.Enum 类，并实现了 java.lang.Seriablizable 和 java.lang.Comparable 两个接口。

values(), ordinal() 和 valueOf() 方法位于 java.lang.Enum 类中：

values() 返回枚举类中所有的值。
ordinal()方法可以找到每个枚举常量的索引，就像数组索引一样。
valueOf()方法返回指定字符串值的枚举常量。

```java

enum Color
{
    RED, GREEN, BLUE;
}
 
public class Test
{
    public static void main(String[] args)
    {
        // 调用 values()
        Color[] arr = Color.values();
 
        // 迭代枚举
        for (Color col : arr)
        {
            // 查看索引
            System.out.println(col + " at index " + col.ordinal());
        }
 
        // 使用 valueOf() 返回枚举常量，不存在的会报错 IllegalArgumentException
        System.out.println(Color.valueOf("RED"));
        // System.out.println(Color.valueOf("WHITE"));
    }
}
```

## 附录四 record 值类型

> 经过 Java 14，15，16 的不断开发优化反馈，终于 Java 16 我们迎来了 Java 值类型的最终版设计，可以正式在生产使用 **Java 值类型**相关 API 也就是 
>
> 但是，使用这个值类型 Record 替代原有的所有 Pojo javabean entity model 类，会遇到很多问题。这些问题包括：
>
> 1. 由于值类型没有原来普通 Object 的对象头等信息，所以对于一些 Object 的特性是不兼容的。
> 2. 我们目前使用 Java 开发不可能不使用很多三方 jar 包，各种库。**这些库中使用的 Pojo 类型并没有使用值类型**。不过，不用太担心，只要这些开源库还比较活跃，那么一定早晚会兼容值类型的。
> 3. lombok 可能快速帮助我们生成pojo类



### Record 的产生背景

Record 要解决的问题最主要的一点就是，让Java适应现代硬件：在 Java 语言发布之初**，**一次内存访问和一次数字计算的消耗时间是差不多的，但是现在，一次内存访问耗时大概是一次数值计算的 200 ~ 1000 倍。从语言设计上来说，也就是间接访问带来的通过指针获取的需要操作的内存，对于整体性能影响很大。

Java 是基于对象的语言，也就是说，Java 是一种基于指针的间接引用的语言。这个基于指针的特性，给每个对象带来了唯一标识性。例如判断两个 Object 的 ==，其实判断的是两个对象的内存相对映射地址是否相同，尽管两个对象的 field 完全一样，他们的内存地址也不同。同时这个特性也给对象带来了多态性，易变性还有锁的特性。但是，并不是所有对象都需要这种特性。

由于指针与间接访问带来了性能瓶颈，Java 准备对于不需要以上提到的特性的对象移除这些特性。于是乎Record 出现了。

### 快速案例

![image-20210827163851819](https://gitee.com/webrx/wx_note/raw/master/images/image-20210827163851819.png)

![image-20210827163924219](https://gitee.com/webrx/wx_note/raw/master/images/image-20210827163924219.png)

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

/**
 * <p>Project: javaseapp - User
 * <p>Powered by webrx On 2021-08-27 16:14:22
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public record User(int id, String name, int score) {
}

```

> 这样编写代码之后，Record 类默认包含的元素和方法实现包括：
>
> 1. record 头指定的组成元素（`int id, String name, int age`），并且，这些元素都是 final 的。
>
> 2. record 默认只有一个构造器，是包含所有元素的构造器。
>
> 3. record 的每个元素都有一个对应的 getter（但这种 getter 并不是 getxxx()，而是直接用变量名命名，所以使用序列化框架，DAO 框架都要注意这一点）
>
> 4. 实现好的 hashCode()，equals()，toString() 方法（通过自动在编译阶段生成关于 hashCode()，equals()，toString() 方法实现的字节码实现）。
>
> 5. **不能用 abstract 修饰 Record 类**，会有编译错误。
>    可以用 final 修饰 Record 类，但是这其实是没有必要的，因为 **Record 类本身就是 final 的**。
>
>    **成员 Record 类，还有本地 Record 类，本身就是 static 的**，也可以用 static 修饰，但是没有必要。
>
>    和普通类一样，Record 类可以被 public, protected, private 修饰，也可以不带这些修饰，这样就是 package-private 的。
>
>    和一般类不同的是，Record 类的直接父类不是 `java.lang.Object` 而是 **`java.lang.Record`**。但是，**Record 类不能使用 extends**，因为 Record 类不能继承任何类。

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.thread;

/**
 * <p>Project: javaseapp - Demo
 * <p>Powered by webrx On 2021-08-27 16:15:41
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class Demo {
    public static void main(String[] args) {
        var u = new User(1, "jack", 80);
        System.out.println(u.id());
        System.out.println(u);

        User li = new User(2, "李四", 88);
        var wu = new User(3, "王五", 99);
        System.out.println(li.id());
        System.out.println(li.name());
        System.out.println(li.score());
        System.out.println(li.hashCode());
        System.out.println(li.toString());
        System.out.println(li);
        System.out.println(li.equals(wu));
        System.out.println(li == wu);
        System.out.println(li instanceof User);
    }
}

```

运行程序输出结果：

```text
1
User[id=1, name=jack, score=80]
2
李四
88
26105901
User[id=2, name=李四, score=88]
User[id=2, name=李四, score=88]
false
false
true
```



### Record 类体

Record 类属性必须在头部声明，**在 Record 类体只能声明静态属性**：

```
public record User(long id, String name, int age) {
    static long anotherId;
}
```

Record 类体可以声明成员方法和静态方法，和一般类一样。但是**不能声明 abstract 或者 native 方法**：

```
public record User(long id, String name, int age) {
    public void test(){}
    public static void test2(){}
}
```

Record 类体也不能包含实例初始化块，例如：

```
public record User(@A @B long id, String name, int age) {
    {
        System.out.println(); //编译异常
    }
}
```

### Record 成员

Record 的所有成员属性，都是 **public final 非 static 的**，对于每一个属性，**都有一个对应的无参数返回类型为属性类型方法名称为属性名称的方法，即这个属性的 accessor**。前面说这个方法是 getter 方法其实不太准确，因为方法名称中并没有 get 或者 is 而是只是纯属性名称作为方法名。

这个方法如果我们自己指定了，就不会自动生成：

```
public record User(long id) {
    @Override
    public long id() {
        return id;
    }
}
```

如果没有自己指定，则会自动生成这样一个方法:

1. **方法名就是属性名称**
2. **返回类型就是对应的属性类型**
3. **是一个 public 方法，并且没有声明抛出任何异常**
4. **方法体就是返回对应属性**
5. **如果属性上面有任何注解，那么这个注解如果能加到方法上那么也会自动加到这个方法上**。

### Record 构造器

如果没有指定构造器，Record 类会自动生成一个以所有属性为参数并且给每个属性赋值的构造器。我们可以通过两种方式自己声明构造器：

**第一种是明确声明以所有属性为参数并且给每个属性赋值的构造器**，这个构造器需要满足：

1. 构造器参数需要包括所有属性（同名同类型），并按照 Record 类头的声明顺序。
2. 不能声明抛出任何异常（不能使用 throws）
3. 不能调用其他构造器（即不能使用 this(xxx)）
4. 构造器需要对于每个属性进行赋值。

**对于其他构造器，需要明确调用这个包含所有属性的构造器**：

```
public record User(long id, String name, int age) {
    public User(int age, long id) {
        this(id, "name", age);
    }
    public User(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
```

**第二种是简略方式声明**，例如：

```
public record User(long id, String name, int age) {
    public User {
        System.out.println("initialized");
        id = 1000 + id;
        name = "prefix_" + name;
        age = 1 + age;
        //在这之后，对每个属性赋值
    }
}
```

![image-20210831112939451](https://gitee.com/webrx/wx_note/raw/master/images/image-20210831112939451.png)

这种方式相当于省略了参数以及对于每个属性赋值，相当于对这种构造器的开头插入代码

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

/**
 * <p>Project: javaseapp - Teacher
 * <p>Powered by webrx On 2021-08-31 08:55:04
 * <p>java.lang.Record 是record父类
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public record Teacher(int id, String name, int age, double money, String address) {
    int pf(int i) {
        return i * i;
    }

    public Teacher(int id, String name) {
        this(id, name, 19, 50, "郑州");
    }

    public Teacher {
        //相当于类的static程序段，用来初始化功能，每个构造方法执行前都要执行此代码段
        System.out.println("hello world record");
    }

    public Teacher(int id) {
        this(id, "佚名", 18, 500, "河南省郑州市");
        /*
        this.id = id;
        this.name = "jack";
        this.age = 18;
        this.money = 500;
        this.address = "河南省郑州";*/
    }

    public Teacher() {
        this(101, "佚名", 18, 500, "河南省郑州市");
    }

    public double money() {
        return money + 5;
    }
}


```

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import org.junit.Test;

/**
 * <p>Project: javaseapp - T1
 * <p>Powered by webrx On 2021-08-31 08:42:18
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
public class T1 {
    //值类型，成员
    record Order(int id, String name, double money) {
    }

    @Test
    public void m1() {
        System.out.println("Hello World");
        System.out.println(System.getProperty("java.home"));
        System.out.println(System.getProperty("java.version"));
        System.out.println(System.getProperty("os.name"));
    }

    @Test
    public void m2() {
        //record 值类型
        Teacher t = new Teacher(1, "李四", 18, 2000, "郑州市");
        System.out.println(t.money());
        var t2 = new Teacher(2, "赵强", 16, 6000, "北京市");
        System.out.println(t);
        System.out.println(t.name());
        System.out.println(t.pf(19));

        Order od = new Order(1, "李四20210831", 670);
        System.out.println(new Order(1, "李四20210831", 670));
        System.out.println(od);
        System.out.println(od.id());
        System.out.println(od.money);
        System.out.println(od.money());

        record user(int id, String name) {
        }

        var u = new user(10, "jack");
        user u2 = new user(22, "李四");
        System.out.println(u);
        System.out.println(u2);
    }

    @Test
    public void m3() {
        //record
        System.out.println(new Teacher());
        System.out.println(new Teacher(1, "王强"));

        System.out.println(new Teacher(101));

        System.out.println(new Teacher());
    }
}

```

lombok 编写pojo类

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */
package cn.webrx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> lombok
 * <p>(1) pom.xml 添加lombok依赖</p>
 * <p>(2) idea 安装lombok插件，新版idea已经内置，不需要自己安装</p>
 * <p>(3) idea 设置项目启用注解功能</p>
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private int id;
    private String name;
    private int age;
    private double money;
    private String address;
}

```

## 附录五 单元测试、断点调试

### 5.1 单元测试

junit4

```xml
<!-- https://mvnrepository.com/artifact/junit/junit -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.13.2</version>
    <scope>test</scope>
</dependency>

```

junit5

```xml
<!-- org.junit.jupiter/junit-jupiter-engine -->
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>5.8.2</version>
    <scope>test</scope>
</dependency>
```

JUnit作为目前Java领域内最为流行的单元测试框架已经走过了数十年。而JUnit5在JUnit4停止更新版本的3年后终于也于2017年发布了。

作为最新版本的JUnit框架，JUnit5与之前版本的Junit框架有很大的不同。首先Junit5由来自三个不同子项目的几个不同模块组成。

> **JUnit 5 = JUnit Platform + JUnit Jupiter + JUnit Vintage**

**JUnit Platform**: Junit Platform是在JVM上启动测试框架的基础，不仅支持Junit自制的测试引擎，其他测试引擎也都可以接入。

**JUnit Jupiter**: JUnit Jupiter提供了JUnit5的新的编程模型，是JUnit5新特性的核心。内部 包含了一个测试引擎，用于在Junit Platform上运行。

**JUnit Vintage**: 由于JUint已经发展多年，为了照顾老的项目，JUnit Vintage提供了兼容JUnit4.x,Junit3.x的测试引擎。

通过上述的介绍，不知道有没有发现JUint5似乎已经不再满足于安安静静做一个单元测试框架了，它的野心很大，想通过接入不同测试引擎，来支持各类测试框架的使用，成为一个单元测试的平台。因此它也采用了分层的架构，分成了平台层，引擎层，框架层。下图可以很清晰的体现出来:

![img](assets/v2-83db086d96181cc37847497a7ce766dc_720w.jpg)



只要实现了JUnit的测试引擎接口，任何测试框架都可以在JUnit Platform上运行，这代表着JUnit5将会有着很强的拓展性。

```java
import org.junit.jupiter.api.Test; //注意这里使用的是jupiter的Test注解！！
public class TestDemo {

  @Test
  @DisplayName("第一次测试")
  public void firstTest() {
      System.out.println("hello world");
  }
```

JUnit5的注解与JUnit4的注解有所变化，以下列出的注解为部分我觉得常用的注解

**@Test :**表示方法是测试方法。但是与JUnit4的@Test不同，他的职责非常单一不能声明任何属性，拓展的测试将会由Jupiter提供额外测试

**@ParameterizedTest :**表示方法是参数化测试，下方会有详细介绍

**@RepeatedTest :**表示方法可重复执行，下方会有详细介绍

**@DisplayName :**为测试类或者测试方法设置展示名称

**@BeforeEach :**表示在每个单元测试之前执行

**@AfterEach :**表示在每个单元测试之后执行

**@BeforeAll :**表示在所有单元测试之前执行

**@AfterAll :**表示在所有单元测试之后执行

**@Tag :**表示单元测试类别，类似于JUnit4中的@Categories

**@Disabled :**表示测试类或测试方法不执行，类似于JUnit4中的@Ignore

**@Timeout :**表示测试方法运行如果超过了指定时间将会返回错误

**@ExtendWith :**为测试类或测试方法提供扩展类引用



测试参数

> 利用**@ValueSource**等注解，指定入参，我们将可以使用不同的参数进行多次单元测试，而不需要每新增一个参数就新增一个单元测试，省去了很多冗余代码。
>
> **@ValueSource**: 为参数化测试指定入参来源，支持八大基础类以及String类型,Class类型
>
> **@NullSource**: 表示为参数化测试提供一个null的入参
>
> **@EnumSource**: 表示为参数化测试提供一个枚举入参

```java
/*
 * Copyright (c) 2006, 2021, webrx.cn All rights reserved.
 *
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.platform.commons.util.StringUtils;

/**
 * <p>Project: javase - Demo
 * <p>Powered by webrx On 2021-12-27 17:23:09
 * <p>Created by IntelliJ IDEA
 *
 * @author webrx [webrx@126.com]
 * @version 1.0
 * @since 17
 */
public class Demo {
    @ParameterizedTest
    @ValueSource(strings = {"one", "two", "three"})
    @DisplayName("参数化测试1")
    public void parameterizedTest1(String string) {
        System.out.println(string);
        Assertions.assertTrue(StringUtils.isNotBlank(string));
    }

    @ParameterizedTest
    @ValueSource(ints = 6)
    @DisplayName("参数化测试2")
    public void parameterizedTest1(int i) {
        System.out.println(i);
        Assertions.assertEquals(i * i, 38);
    }

}

```

| 断言                           | 描述                                                         |
| :----------------------------- | :----------------------------------------------------------- |
| assertEquals（预期的，实际的） | 预期不等于实际时失败                                         |
| assertFalse（表达式）          | 表达式不为假时失败                                           |
| assertNull（实际）             | 实际不为空时失败                                             |
| assertNotNull（实际）          | 当real为null时失败                                           |
| assertAll（）                  | 对许多断言进行分组，并且每个断言都会执行，即使其中一个或多个失败 |
| assertTrue（表达式）           | 如果表达式不正确则失败                                       |
| assertThrows（）               | 预期要测试的类将引发异常                                     |

超时断言，Junit5还提供了**Assertions.assertTimeout()** 为测试方法设置了超时时间

```java
@Test
@DisplayName("超时测试")
public void timeoutTest() {
    //如果测试方法时间超过1s将会异常
    Assertions.assertTimeout(Duration.ofMillis(1000), () -> Thread.sleep(500));
}
```

异常断言，在JUnit4时期，想要测试方法的异常情况时，需要用**@Rule**注解的ExpectedException变量还是比较麻烦的。而JUnit5提供了一种新的断言方式**Assertions.assertThrows()** ,配合函数式编程就可以进行使用。

```java
@Test
@DisplayName("异常测试")
public void exceptionTest() {
    ArithmeticException exception = Assertions.assertThrows(
           //扔出断言异常
            ArithmeticException.class, () -> System.out.println(1 % 0));

}
```

测试程序

```java
public class JUnit5Sample1Test {
 
  @BeforeAll
  static void beforeAll() {
    System.out.println("**--- Executed once before all test methods in this class ---**");
  }
 
  @BeforeEach
  void beforeEach() {
    System.out.println("**--- Executed before each test method in this class ---**");
  }
 
  @Test
  void testMethod1() {
    System.out.println("**--- Test method1 executed ---**");
  }
 
  @DisplayName("Test method2 with condition")
  @Test
  void testMethod2() {
    System.out.println("**--- Test method2 executed ---**");
  }
 
  @Test
  @Disabled("implementation pending")
  void testMethod3() {
	  System.out.println("**--- Test method3 executed ---**");
  }
 
  @AfterEach
  void afterEach() {
    System.out.println("**--- Executed after each test method in this class ---**");
  }
 
  @AfterAll
  static void afterAll() {
    System.out.println("**--- Executed once after all test methods in this class ---**");
  }
}
```





### 5.2 IDEA断点调试

